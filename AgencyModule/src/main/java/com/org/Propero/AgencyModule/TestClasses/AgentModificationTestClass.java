package com.org.propero.agencymodule.testClasses;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.org.propero.agencymodule.assertion.FooterError;
import com.org.propero.agencymodule.errormessages.PageNotFoundError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.login.LoginSingle;
import com.org.propero.agencymodule.modifyAgent.ModifyAgentDetails;
import com.org.propero.agencymodule.utility.BaseClass;
import com.org.propero.agencymodule.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class AgentModificationTestClass extends BaseClass implements ProjectInterface
{
	private static String testResult;
	private static Logger logger1 = LoggerFactory.getLogger(AgentModificationTestClass.class);

	@Test
	public static void modificationofAgent() throws Exception {
		logger1.info("*************  Agent Modification Test Case Started **************");
		// Reading Excel Sheet Data For Number of Test Cases
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		WriteExcel write = new WriteExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "AgentModification";
		int rowCount = read.getRowCount("AgentModification");
		logger1.info("Row Count is : " + rowCount);

		/*
		 * int j=0; int lastRowNum=0; String ENV =
		 * (read.getCellData("Credentials", "Environment", 1));
		 * if(ENV.equalsIgnoreCase("QC")){ j=2; lastRowNum=49; } else
		 * if(ENV.equalsIgnoreCase("UAT")){ j=51; lastRowNum=98; }
		 */
		int firstrowNum=1;
		int lastRowNum=44;
		for (int n = firstrowNum; n <=lastRowNum ; n++) {

			try {

				String executionstatus=read.getCellData(fis, "Execution_Status", n);
				if(executionstatus.equalsIgnoreCase("Execute"))	
				{
				
				// Reading Test Case Name from Excel
				String testCaseName = (read.getCellData("AgentModification", "Test_Case", n));
				logger = extent.startTest("Agent Modification - TC_"+ n +" - " + testCaseName);
				logger1.info("Agent Modification - TC_"+ n +" - " + testCaseName);

				// Here we are calling Login Method to Login in the Propero Application
				LoginSingle.login();
				Thread.sleep(5000);
				driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
				waitForElementsToBeClickable(By.xpath("//i[@title='FullScreen']"));
				driver.findElement(By.xpath("//i[@title='FullScreen']")).click();
				Thread.sleep(2000);

			     ModifyAgentDetails.modficationDetailsAgent(n);
				
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
				
				driver.quit();

				write.setCellData(fis, "Test_Result", n, "Pass");
				}
				else{
					logger1.info("No Need to Execute for Test case Number: " + n);
					//logger.log(LogStatus.PASS, "No Need to Execute for Test case Number: " + n);
					write.setCellData(fis, "Test_Result", n, "Skip");
				}
				
			}
            catch (AssertionError  e) {
            	FooterError.footererror();
            	write.setCellData(fis, "Test_Result", n, "Fail");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				driver.quit();
			}
			catch (Exception  e) {
				
				FooterError.footererror();
				PageNotFoundError.pagenotfound();
				write.setCellData(fis, "Test_Result", n, "Fail");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				
				logger1.info(e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				driver.quit();
					
			}
			    
			
			continue;
		}
		logger1.info("*************  Agent Modification Test Cases Completed **************");
	}
	
	
}
