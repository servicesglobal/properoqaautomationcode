package com.org.propero.agencymodule.modificationFn;

public interface ModificationInterface 
{

	//Intermediary Tab
	public static String ModIntermediaryCategory_Xpath="//div[@class='form-group focused boxvalue']//dynamic-dropdown[@lookupkey='INTERMEDIARYCATEGORY']//select[@id='undefined']";
	public static String ModIntermediaryCategoryOption_Xpath="//div[@class='form-group focused boxvalue']//dynamic-dropdown[@lookupkey='INTERMEDIARYCATEGORY']//select[@id='undefined']//option";
	public static String ModIntermediary_SubCategory_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDIARYSUBCATEGORY']//select[@id='undefined']";
	public static String ModIntermediary_SubCatOption_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDIARYSUBCATEGORY']//select[@id='undefined']//option";
	public static String ModSourcingVertical_Xpath="//dynamic-dropdown[@lookupkey='CHANNEL']//select[@id='undefined']";
	public static String ModSourcingVerticalOption_Xpath="//dynamic-dropdown[@lookupkey='CHANNEL']//select[@id='undefined']//option";
	public static String ModSub_Vertical_Xpath="//dynamic-dropdown[@lookupkey='SUBCHANNEL']//select[@id='undefined']";
	public static String ModSubVerticalOption_Xpath="//dynamic-dropdown[@lookupkey='SUBCHANNEL']//select[@id='undefined']//option";
	public static String ModSourcing_Location_Xpath="//dynamic-dropdown[@lookupkey='MONITORINGLOCATION']//select[@id='undefined']";
	public static String ModSourcingLocationOption_Xpath="//dynamic-dropdown[@lookupkey='MONITORINGLOCATION']//select[@id='undefined']//option";
	public static String ModSecondary_Vertical_Xpath="//dynamic-dropdown[@lookupkey='SECCHANNEL']//select[@id='undefined']";
	public static String ModSecondaryVerticalOption_Xpath="//dynamic-dropdown[@lookupkey='SECCHANNEL']//select[@id='undefined']//option";	
	public static String ModClassifiaction_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDCLASS']//select[@id='undefined']";
	public static String ModClassificationOption_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDCLASS']//select[@id='undefined']//option";
	

	//Personal Details Tab
	public String ModPreferred_Address_Xpath="//div[@class='col-xs-12 col-sm-6 col-md-3 col-lg-3']//div[@class='form-group']//select[@class='form-control ng-untouched ng-pristine ng-valid']";
	public String ModPreferredAddressoption_Xpath="//div[@class='col-xs-12 col-sm-6 col-md-3 col-lg-3']//div[@class='form-group']//select[@class='form-control ng-untouched ng-pristine ng-valid']//option";
	public String ModFather_Name_Xpath="//*[@id='proposer-details-0']/div[2]/div[5]/div/input";
	public String ModOfficeCity_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[2]//div[1]//div[1]//div[2]//div[4]//div[1]//select[1]";
	public String ModOfficeCitySelection_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[2]//div[1]//div[1]//div[2]//div[4]//div[1]//select[1]//option";
	public String ModResidentialAddCheckbox_Xpath="//span[contains(text(),'Same as Office Address')]";
	public String ModResidentialCity_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[3]//div[1]//div[1]//div[2]//div[4]//div[1]//select[1]";
	public String ModResidentialCitySelection_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[3]//div[1]//div[1]//div[2]//div[4]//div[1]//select[1]//option";
	public String ModStd="//div[contains(@class,'col-xs-12 col-sm-3 col-md-3 col-lg-3')]//div[contains(@class,'form-group')]//span//span//input[contains(@type,'text')]";
	public String ModContactNum_Xpath="//div[contains(@class,'col-xs-12 col-sm-9 col-md-9 col-lg-9')]//div[contains(@class,'form-group')]//span[2]//span[1]//input[1]";
	public String ModSecondaryEmailidBlank="//div[@class='col-xs-12 col-sm-6 col-md-3 col-lg-2']//div[@class='form-group']//span//span//input[@type='text']";
	public String ModSecondaryMobileNum_Xpath="//div[contains(@class,'col-xs-12 col-sm-10 col-md-10 col-lg-10')]//div[contains(@class,'form-group')]//span//span//input[contains(@type,'text')]";
	public String ModConstitutionofBusiness_Xpath="//div[contains(@class,'col-xs-12 col-sm-6 col-md-4 col-lg-3')]//select[contains(@class,'form-control ng-untouched ng-pristine ng-valid')]";
	public String ModConstitutionofBusinessoption_Xpath="//div[contains(@class,'col-xs-12 col-sm-6 col-md-4 col-lg-3')]//select[contains(@class,'form-control ng-untouched ng-pristine ng-valid')]//option";

	//Other Personal Details
	public String ModOtherPersonalDetails_Tab_Xpath="//a[@name='otherPersonal']";
	public String ModState_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']";
	public String ModStateOption_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']//option";
	public String ModCity_Xpath="//dynamic-dropdown[@lookupkey='CITY']//select[@id='undefined']";
	public String ModCityOption_Xpath="//dynamic-dropdown[@lookupkey='CITY']//select[@id='undefined']//option";
	
	//Payment and Financial
	public String ModPaymentFrequency_Id="paymentFrequencyCd-";
	public String ModPaymentFrequencyOption="//select[@id='paymentFrequencyCd-']//option";
	public String ModTDS_flag_Id="tdsFl-";
	public String ModTdsOption_Xpath="//select[@id='tdsFl-']//option";
	public String ModLowTds_Flag_Id="lowTdsFl-";
	public String ModStateSelc_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']";
	public String ModStateSelcOption_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']//option";
	public String ModCitySelc_Xpath="//dynamic-dropdown[@filtercriteria='stateCd']//select[@id='undefined']";
	public String ModCitySelcOption_Xpath="//dynamic-dropdown[@filtercriteria='stateCd']//select[@id='undefined']//option";
	
	//Exam and License
	public String ModExamCenter_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select[@id='undefined']";
	public String ModExamCenterOption_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select[@id='undefined']//option";
	public String ModLanguage_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select[@id='undefined']";
	public String ModLangaugeOption_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select[@id='undefined']//option";
	public String ModRecruitmentSource_Xpath="//dynamic-dropdown[@lookupkey='RECRUITMENTSOURCE']//select[@id='undefined']";
	public String ModRecruitmentSourceOption_Xpath="//dynamic-dropdown[@lookupkey='RECRUITMENTSOURCE']//select[@id='undefined']//option";
	public String ModCertificateCourse_Xpath="//dynamic-dropdown[@lookupkey='CERTIFICATECOURSECD']//select[@id='undefined']";
	public String ModCertificateCourseOption_Xpath="//dynamic-dropdown[@lookupkey='CERTIFICATECOURSECD']//select[@id='undefined']//option";
	public String ModKnowreligareEmployee_Xpath="//div[@class='col-xs-12 col-sm-6 col-md-4 col-lg-3']//div[@class='form-group']//select[@class='form-control ng-untouched ng-pristine ng-valid']";
	public String ModKnowreligareEmployeeOption_Xpath="//div[@id='license-exam-tab-0']//div[@class='row rhi-block-agent']//div[3]//div[1]//select//option";
	
	
	
	
	
}
