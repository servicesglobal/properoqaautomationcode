package com.org.propero.agencymodule.addagent;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.assertion.AssertionFn;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.errormessages.PageNotFoundError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ProductPlan extends BaseClass implements ProjectInterface
{

	private static String[] products = null;
	private static Logger logger1 = LoggerFactory.getLogger(ProductPlan.class);
	
	public static void productPlanTab(String intermediaryCode,int rownum) throws Exception
	{
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "Existing_Agents";

		String subCategory = read.getCellData(fis,"Sub_Category",rownum);
		if(subCategory.equalsIgnoreCase("Agent"))
		{
			ProductPlan.submitation(intermediaryCode);
		}
		else 
		{
		ProductPlan.policyIssuanceRules(intermediaryCode, rownum);
		ProductPlan.productDetails(rownum,intermediaryCode);
		ProductPlan.submitation(intermediaryCode);
	}
	}
	public static void policyIssuanceRules(String intermediaryCode,int rownum) throws Exception
	{
		int mcount;
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "ProductPlanOption";
		String fis1 = "Existing_Agents";
		
		Thread.sleep(5000);
		fluentwait(By.xpath(ProductPlanTab_Xpath), 60, "Couldn't find Product Plan Tab.");
		clickElement(By.xpath(ProductPlanTab_Xpath));
		
		String pplanoption= read.getCellData(fis1, "ProductPlan_Option", rownum);
		
		
		if(pplanoption.contains(","))
		{
		products = pplanoption.split(",");
		for(int j=1; j<=(products.length-1);j++)
		{
			clickElement(By.xpath(AddBtn_Xpath));
		}
		}
		else if(pplanoption.contains(""))
		{
			products = pplanoption.split("");
		}
		else if(pplanoption.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Product Plan");	
		}
		
		
		if(pplanoption.equals(""))
		{
			logger1.info("No Need To Enter Data in Product Plan.");	
		}else{
		for (int i = 0; i <= products.length - 1; i++)
		{
			mcount = Integer.parseInt(products[i].toString());
			
		//Selecting Product Line from Policy Issuance Rules
		String productLine= read.getCellData(fis, "Product_Line", mcount);
		clickElement(By.xpath("//td//dynamic-dropdown//select[@id='productFamilyCdIssuance-"+intermediaryCode+"-"+i+"']"));
		clickElement(By.xpath("//select[@id='productFamilyCdIssuance-"+intermediaryCode+"-"+i+"']//option[contains(text(),"+"'"+productLine+"'"+")]"));
		logger1.info("Selected Product Line is : "+productLine);
		
		/*//Selecting Effective From Date
		Calendar.calender(fis, By.xpath("//tr//td[2]//datepick//div//input"), "EffectiveFrom_Date", rownum);
	
		//Selecting Effective To Data
		Calendar.calender(fis, By.xpath("//tr//td[2]//datepick//div//input"), "EffectiveTo_Date", rownum);*/
		
		//Selecting Policy Issuance Rules
		
		//String[] Rule=null;
		String policyIssRule = read.getCellData(fis, "PolicyIssuance_Rules", mcount);
		selecttext(By.xpath("//tr//td[4]//select[@id='issuanceRuleCd-"+intermediaryCode+"-"+i+"']"), policyIssRule);
		logger1.info("Selected Policy Issuance Rule is : "+policyIssRule);
				
		//Selecting Money Before Rule
		if(policyIssRule.contains("Money After Transaction"))
		{
			logger1.info("No Need to Select Money Before Rule Option.");
			
			//Selecting Banking Days
			String bankingDays = read.getCellData(fis, "Banking_Days", mcount);
			enterText(By.xpath("//tr//td[6]//input[@id='bankingDaysIssuance-"+intermediaryCode+"-"+i+"']"), bankingDays);
			logger1.info("Entered Banking days is : "+bankingDays);
			
			//Selecting Banking Limit
			String bankingLimit = read.getCellData(fis, "Banking_Limit", mcount);
			enterText(By.xpath("//tr//td[7]//input[@id='bankingAmtIssuance-"+intermediaryCode+"-"+i+"']"), bankingLimit);
			logger1.info("Entered Banking Limit is : "+bankingLimit);
			
			
		}else if(policyIssRule.contains("Money Before Transaction"))
		{
		//String[] MoneyRule=null;
		String moneyBeforeRule = read.getCellData(fis, "MoneyBefore_Rules", mcount);
		selecttext(By.xpath("//tr//td[5]//select[@id='moneyBeforeCdIssuance-"+intermediaryCode+"-"+i+"']"), moneyBeforeRule);
		
		logger1.info("Selected Money Before Rule is : "+moneyBeforeRule);
		
		logger1.info("No Need to Select Banking Days.");
		
		logger1.info("No Need to Select Banking Limit.");
		
		}

		}
	
		}
	}
				
	
	public static void productDetails(int rownum,String intermediaryCode) throws Exception
	{
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "ProductPlanOption";
		String fis1 = "Existing_Agents";
		
		//Select manual Button on UI
		clickElement(By.xpath(Manual_Btn_Xpath));
		
		
		//Find the Total Number of Row and Column
		String[] chckData = null;
		//int Col;
		int row=0;
		int mcount;
		row = driver.findElements(By.xpath("//table[@class='agent-education-table agent-productdetails-table']//tbody//tr")).size();
		//Col = driver.findElements(By.xpath("//table[@class='agent-education-table agent-productdetails-table']//tbody//tr//td")).size();
		
		String pplanoption= read.getCellData(fis1, "ProductPlan_Option", rownum);
		
		if(pplanoption.contains(","))
		{
		products = pplanoption.split(",");
		}
		else if(pplanoption.contains(""))
		{
			products = pplanoption.split("");
			logger1.info("Product length: ", products.length - 1);
		}		
		
		for (int j = 0; j <= products.length - 1; j++)
		{
			
			mcount = Integer.parseInt(products[j].toString());
		
		String productId=read.getCellData(fis, "Product_Name", mcount);
		if(productId.contains(","))
			{
			chckData = productId.split(",");
			for(int i=1;i<=row;i++)
			{
				
				chckData = productId.split(",");
				
				for (String chdata : chckData) 
				{
					
						String getproductId = driver.findElement(By.xpath("//tbody//tr["+i+"]//td[2]//productlookup-text[1]//span[1]")).getText();
						
						if(chdata.equals(getproductId))
						{
							clickElement(By.xpath("//table[@class='agent-education-table agent-productdetails-table']//tbody//tr["+i+"]//td[3]//input"));
							logger1.info("Selected Product is : "+chdata);
							
						}else
						{
							continue;
						}
						
					}
			
			}	
			}
		else if (productId.contains(""))
		{
			for(int i=1;i<=row;i++)
			{
				String getproductId = driver.findElement(By.xpath("//tbody//tr["+i+"]//td[2]//productlookup-text[1]//span[1]")).getText();
				
				if(productId.equals(getproductId))
				{
					
					clickElement(By.xpath("//table[@class='agent-education-table agent-productdetails-table']//tbody//tr["+i+"]//td[3]//input"));
					logger1.info("Selected Product is : "+productId);
					
				}else
				{
					continue;
				}
				
			}
		}
		else if(productId.equals(""))
		{
			logger1.info("Please map product plan");
			logger.log(LogStatus.FAIL, "Please map product plan");
		}
		
		}
		
		verticalscrollup();
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Product Plan Tab")));	
		
		Thread.sleep(2000);
		
			
		
	}
	
		public static void submitation(String intermediaryCode) throws Exception
		{
			try{
				
			ImplicitWait(60000);	
			
			clickElement(By.xpath(Submit_Button_Xpath));
			
			ErrorMessages.fieldverification("ok");
			
			ParentAgentIdCheck.verifyAgentsDetails(intermediaryCode);
			
			//PageNotFoundError.pagenotfound();
			
			//Thread.sleep(35000);
			
			/*try{
			clickElement(By.xpath(Submit_Button_Xpath));
			}
			catch(Exception e)
			{
				logger1.info(e);
			}
			Fluentwait(By.xpath(OK_Btn_Xpath), 60, "Unable to Click on Ok Button");
			
			try{
			clickElement(By.xpath(OK_Btn_Xpath));
			}
			catch(Exception e){
				logger1.info("Second Exception");
			}
			*/
			
			//AssertionFn.copsagentstatusverification(intermediaryCode);
			
			//Thread.sleep(2000);
			
			/*DataVerificationinDb.DBVerification(intermediaryCode);
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Final Stage")));	*/	
			
			}catch(Exception e)
			{
				logger1.info(e.getMessage());
			}
			
			}
		
	}
