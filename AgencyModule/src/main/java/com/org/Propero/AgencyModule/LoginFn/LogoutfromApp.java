package com.org.propero.agencymodule.loginFn;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class LogoutfromApp extends BaseClass implements ProjectInterface
{
    static String testResult;
    private static Logger logger1 = LoggerFactory.getLogger(LogoutfromApp.class);
	public static void logoutfromApplication()
	{
		try{
			mousehover(By.xpath(Logout_xpath));
			clickElement(By.xpath(Logout_xpath));
			logger1.info("User has Logged out Successfully.");
			logger.log(LogStatus.PASS, "User has Logged out Successfully.");
			testResult="Pass";
		}catch(Exception e)
		{
			
			logger1.info("Test Case is Marked as Fail because Unable to Click on Logout Button.");
			testResult="Pass";
		}
	}
	
}
