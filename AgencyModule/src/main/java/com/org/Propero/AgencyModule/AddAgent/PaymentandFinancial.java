package com.org.propero.agencymodule.addagent;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.functions.PaymentandFinancialTab;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class PaymentandFinancial extends BaseClass implements ProjectInterface
{

	private static String testResult;
	
	public static String paymentAndFinancial(String sheetName,String workbookname,int rowNum) throws Exception
	{
		
		PaymentandFinancialTab obj = new PaymentandFinancialTab();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
		
		String newPayTo = read.getCellData(workbookname,"Pay_To",rowNum);
		String newClawbackFrom = read.getCellData(workbookname,"Clawback_From",rowNum);
		String newPaymentFrequency = read.getCellData(workbookname,"Payment_Frequency",rowNum);
		String newFinPaymentDisbursmentMode = read.getCellData(workbookname,"PaymentDisbursement_Mode",rowNum);
		String newServiceTaxnumber = read.getCellData(workbookname,"Service_Tax",rowNum);
		String newFinTds = read.getCellData(workbookname,"TDS_Sel",rowNum);
		String newLowTds = read.getCellData(workbookname,"Low_TDS",rowNum);
		String newFinPanNumber = read.getCellData(workbookname,"PAN_Number",rowNum);
		String newAadhaarCardNumber = read.getCellData(workbookname,"AadharCard_Number",rowNum);
		String newFinIfscNumber = read.getCellData(workbookname,"PayIFSC_Code",rowNum);
		String newFinBankAccountNumber = read.getCellData(workbookname,"PayBank_Account_Number",rowNum);
		String newFinMicrNumber = read.getCellData(workbookname,"PayMICR_Number",rowNum);
		String newFinBank = read.getCellData(workbookname,"PayBank",rowNum);
		String newFinBranchName = read.getCellData(workbookname,"PayBranch_Name",rowNum);
		String newEffectiveFrom = read.getCellData(workbookname,"PayEffectivefrom",rowNum);
		String newEffectiveTo = read.getCellData(workbookname,"PayEffectiveTo",rowNum);
		String newFinState = read.getCellData(workbookname,"PayState_Name",rowNum);
		String newFinCity = read.getCellData(workbookname,"PayCity_Name",rowNum);
		
		obj.setPayTo(newPayTo);
		obj.setClawbackFrom(newClawbackFrom);
		obj.setPaymentFrequency(newPaymentFrequency);
		obj.setFinPaymentDisbursmentMode(newFinPaymentDisbursmentMode);
		obj.setServiceTaxnumber(newServiceTaxnumber);
		obj.setFinTds(newFinTds);
		obj.setLowTds(newLowTds);
		obj.setFinPanNumber(newFinPanNumber);
		obj.setAadhaarCardNumber(newAadhaarCardNumber);
		obj.setFinIfscNumber(newFinIfscNumber);
		obj.setFinBankAccountNumber(newFinBankAccountNumber);
		obj.setFinMicrNumber(newFinMicrNumber);
		obj.setFinBank(newFinBank);
		obj.setFinBranchName(newFinBranchName);
		obj.setEffectiveFrom(newEffectiveFrom);
		obj.setEffectiveTo(newEffectiveTo);
		obj.setFinState(newFinState);
		obj.setFinCity(newFinCity);
	
		obj.paymentAndFinancialTab(obj.getPayTo(), obj.getClawbackFrom(), obj.getPaymentFrequency(), obj.getFinPaymentDisbursmentMode(), 
				obj.getServiceTaxnumber(), obj.getFinTds(), obj.getLowTds(), obj.getFinPanNumber(), obj.getAadhaarCardNumber(), 
				workbookname, rowNum, obj.getFinIfscNumber(), obj.getFinBankAccountNumber(), 
				obj.getFinMicrNumber(), obj.getFinBank(), obj.getFinBranchName(), obj.getEffectiveFrom(), 
				obj.getEffectiveTo(), obj.getFinState(), obj.getFinCity(),rowNum);
		testResult="Pass";
		obj=null;
		
		}catch(Exception e)
		{
			
			testResult="Fail";
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Fail")));
			throw e;
			//Assert.fail("Error in Payemnt and Financial Details Page");
		}
	
		return testResult;
	
	}
	
	//****************** Negative Test Case ****************************************************
	
	public static String paymentAndFinancialNegativeTestCase(String sheetName,String workbookname,int rowNum) throws Exception
	{
		
		PaymentandFinancialTab obj = new PaymentandFinancialTab();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
		
		String newPayTo = read.getCellData(workbookname,"Pay_To",rowNum);
		String newClawbackFrom = read.getCellData(workbookname,"Clawback_From",rowNum);
		String newPaymentFrequency = read.getCellData(workbookname,"Payment_Frequency",rowNum);
		String newFinPaymentDisbursmentMode = read.getCellData(workbookname,"PaymentDisbursement_Mode",rowNum);
		String newServiceTaxnumber = read.getCellData(workbookname,"Service_Tax",rowNum);
		String newFinTds = read.getCellData(workbookname,"TDS_Sel",rowNum);
		String newLowTds = read.getCellData(workbookname,"Low_TDS",rowNum);
		String newFinPanNumber = read.getCellData(workbookname,"PAN_Number",rowNum);
		String newAadhaarCardNumber = read.getCellData(workbookname,"AadharCard_Number",rowNum);
		String newFinIfscNumber = read.getCellData(workbookname,"PayIFSC_Code",rowNum);
		String newFinBankAccountNumber = read.getCellData(workbookname,"PayBank_Account_Number",rowNum);
		String newFinMicrNumber = read.getCellData(workbookname,"PayMICR_Number",rowNum);
		String newFinBank = read.getCellData(workbookname,"PayBank",rowNum);
		String newFinBranchName = read.getCellData(workbookname,"PayBranch_Name",rowNum);
		String newEffectiveFrom = read.getCellData(workbookname,"PayEffectivefrom",rowNum);
		String newEffectiveTo = read.getCellData(workbookname,"PayEffectiveTo",rowNum);
		String newFinState = read.getCellData(workbookname,"PayState_Name",rowNum);
		String newFinCity = read.getCellData(workbookname,"PayCity_Name",rowNum);
		
		obj.setPayTo(newPayTo);
		obj.setClawbackFrom(newClawbackFrom);
		obj.setPaymentFrequency(newPaymentFrequency);
		obj.setFinPaymentDisbursmentMode(newFinPaymentDisbursmentMode);
		obj.setServiceTaxnumber(newServiceTaxnumber);
		obj.setFinTds(newFinTds);
		obj.setLowTds(newLowTds);
		obj.setFinPanNumber(newFinPanNumber);
		obj.setAadhaarCardNumber(newAadhaarCardNumber);
		obj.setFinIfscNumber(newFinIfscNumber);
		obj.setFinBankAccountNumber(newFinBankAccountNumber);
		obj.setFinMicrNumber(newFinMicrNumber);
		obj.setFinBank(newFinBank);
		obj.setFinBranchName(newFinBranchName);
		obj.setEffectiveFrom(newEffectiveFrom);
		obj.setEffectiveTo(newEffectiveTo);
		obj.setFinState(newFinState);
		obj.setFinCity(newFinCity);
	
		obj.paymentAndFinancialTabNegativeTestcase(obj.getPayTo(), obj.getClawbackFrom(), obj.getPaymentFrequency(), obj.getFinPaymentDisbursmentMode(), 
				obj.getServiceTaxnumber(), obj.getFinTds(), obj.getLowTds(), obj.getFinPanNumber(), obj.getAadhaarCardNumber(), 
				sheetName, rowNum, obj.getFinIfscNumber(), obj.getFinBankAccountNumber(), 
				obj.getFinMicrNumber(), obj.getFinBank(), obj.getFinBranchName(), obj.getEffectiveFrom(), 
				obj.getEffectiveTo(), obj.getFinState(), obj.getFinCity());
		testResult="Pass";
		obj=null;
		
		}catch(Exception e)
		{
			
			testResult="Fail";
			throw e;
		}
	
		return testResult;
	
	}
	
}
