package com.org.propero.agencymodule.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;

public class DbManager extends BaseClass
{
	private static Connection con = null; //sql
	//private static Connection conn = null; //mysql
	private static Logger logger1 = LoggerFactory.getLogger(DbManager.class);
	
	//SQL Server
	public static void setDbConnection() throws Exception
	{
		
		ReadExcel read = new ReadExcel("D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx");
		String fis = "Credentials";
		try
		{
			String environment = (read.getCellData(fis, "environment", 1));
			
			
		if(environment.contains("UAT")){
		Class.forName(TestConfig.driverUAT);
		con =	DriverManager.getConnection(TestConfig.dbConnectionUrlUAT, TestConfig.dbUserNameUAT, TestConfig.dbPasswordUAT);
		
		if(!con.isClosed())
			System.out.println("Successfully connected to SQL server");
		}else if(environment.contains("QC")){
			Class.forName(TestConfig.driverQC);
			con =	DriverManager.getConnection(TestConfig.dbConnectionUrlQC, TestConfig.dbUserNameQC, TestConfig.dbPasswordQC);
			
			if(!con.isClosed())
				System.out.println("Successfully connected to SQL server");
			}	
		else if(environment.contains("Staging")){
			Class.forName(TestConfig.driverStage);
			con =	DriverManager.getConnection(TestConfig.dbConnectionUrlStage, TestConfig.dbUserNameStage, TestConfig.dbPasswordStage);
			
			if(!con.isClosed())
				System.out.println("Successfully connected to SQL server");
			}
		
		
	}catch(Exception e)
		{
		logger1.info("Exception: " + e.getMessage());

		}
		
		
	}
		
	public static List<String> getSqlQuery(String query) throws SQLException{
		Statement St = con.createStatement();
		ResultSet rs = St.executeQuery(query);
		List<String> values = new ArrayList<String>();
		
		while(rs.next())
		{
		
			values.add(rs.getString("AGENTSTATUSCD"));
			
			
		}
		return values;
	}
	
	public static Connection getConnection()
	{
		return con;
			}
}
