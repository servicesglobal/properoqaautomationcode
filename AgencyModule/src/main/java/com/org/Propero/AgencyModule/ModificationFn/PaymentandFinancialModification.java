package com.org.propero.agencymodule.modificationFn;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;

public class PaymentandFinancialModification extends BaseClass implements ModificationInterface,MessageInterface
{

	private static Logger logger1 = LoggerFactory.getLogger(PaymentandFinancialModification.class);
	
	public void setPayTo(String newPayTo) {
		Variable.PayTo = newPayTo;
	}

	public String getPayTo() {
		return Variable.PayTo;
	}
	
	public void setClawbackFrom(String newClawbackFrom)  {
		Variable.ClawbackFrom = newClawbackFrom;
	}

	public String getClawbackFrom() {
		return Variable.ClawbackFrom;
	}
	
	public void setPaymentFrequency(String newPaymentFrequency) {
		Variable.PaymentFrequency = newPaymentFrequency;
	}

	public String getPaymentFrequency() {
		return Variable.PaymentFrequency;
	}
	
	public void setFinPaymentDisbursmentMode(String newFinPaymentDisbursmentMode) {
		Variable.FinPaymentDisbursmentMode = newFinPaymentDisbursmentMode;
	}

	public String getFinPaymentDisbursmentMode() {
		return Variable.FinPaymentDisbursmentMode;
	}
	
	public void setServiceTaxnumber(String newServiceTaxnumber) {
		Variable.ServiceTaxnumber = newServiceTaxnumber;
	}

	public String getServiceTaxnumber() {
		return Variable.ServiceTaxnumber;
	}
	
	public void setFinTds(String newFinTds) {
		Variable.FinTds = newFinTds;
	}

	public String getFinTds() {
		return Variable.FinTds;
	}
	
	public void setLowTds(String newLowTds) {
		Variable.LowTds = newLowTds;
	}

	public String getLowTds() {
		return Variable.LowTds;
	}
	
	public void setFinPanNumber(String newFinPanNumber) {
		Variable.FinPanNumber = newFinPanNumber;
	}

	public String getFinPanNumber() {
		return Variable.FinPanNumber;
	}
	
	public void setAadhaarCardNumber(String newAadhaarCardNumber) {
		Variable.AadhaarCardNumber = newAadhaarCardNumber;
	}

	public String getAadhaarCardNumber() {
		return Variable.AadhaarCardNumber;
	}
	
	public void setFinIfscNumber(String newFinIfscNumber) {
		Variable.FinIfscNumber = newFinIfscNumber;
	}

	public String getFinIfscNumber() {
		return Variable.FinIfscNumber;
	}
	
	public void setFinBankAccountNumber(String newFinBankAccountNumber) {
		Variable.FinBankAccountNumber = newFinBankAccountNumber;
	}

	public String getFinBankAccountNumber() {
		return Variable.FinBankAccountNumber;
	}
	
	public void setFinMicrNumber(String newFinMicrNumber) {
		Variable.FinMicrNumber = newFinMicrNumber;
	}

	public String getFinMicrNumber() {
		return Variable.FinMicrNumber;
	}
	
	public void setFinBank(String newFinBank) {
		Variable.FinBank = newFinBank;
	}

	public String getFinBank() {
		return Variable.FinBank;
	}
	
	public void setFinBranchName(String newFinBranchName) {
		Variable.FinBranchName = newFinBranchName;
	}

	public String getFinBranchName() {
		return Variable.FinBranchName;
	}
	
	public void setEffectiveFrom(String newEffectiveFrom) {
		Variable.EffectiveFrom = newEffectiveFrom;
	}

	public String getEffectiveFrom() {
		return Variable.EffectiveFrom;
	}
	
	public void setEffectiveTo(String newEffectiveTo) {
		Variable.EffectiveTo = newEffectiveTo;
	}

	public String getEffectiveTo() {
		return Variable.EffectiveTo;
	}
	
	public void setFinState(String newFinState) {
		Variable.FinState = newFinState;
	}

	public String getFinState() {
		return Variable.FinState;
	}
	
	public void setFinCity(String newFinCity) {
		Variable.FinCity = newFinCity;
	}

	public String getFinCity() {
		return Variable.FinCity;
	}
	
	
	public void paymentAndFinancialTab(String agentid,String payTo,String clawbackFrom,String paymentFrequency,String finPaymentDisbursmentMode,
			String serviceTaxnumber,String finTds,String lowTds,String finPanNumber,String aadhaarCardNumber,
			String sheetName,int rowNum,String finIfscNumber,String finBankAccountNumber,String finMicrNumber,
			String finBank,String finBranchName,String effectiveFrom,String effectiveTo,String finState,String finCity) throws Exception
	{
		verticalscrollup();
		
		//Clicking on Payment and Financial Tab
	    clickElement(By.xpath(PaymentandFinancial_Xpath));
		
		
		//Filling Details in Agent Payments
		PaymentandFinancialModification.agentPayment(agentid, payTo, clawbackFrom, paymentFrequency, finPaymentDisbursmentMode, serviceTaxnumber, finTds, lowTds);
		
		//Filling Details in identity 
		PaymentandFinancialModification.identityDetails(agentid, finTds, finPanNumber, aadhaarCardNumber,rowNum);
		
		//Filling Details in Bank Details
		PaymentandFinancialModification.bankDetails(sheetName, rowNum, agentid, finPaymentDisbursmentMode, finIfscNumber, finBankAccountNumber, finMicrNumber, finBank, finBranchName, effectiveFrom, effectiveTo, finState, finCity);
		
	}
	
	
	public static void agentPayment(String agentid1,String payTo,String clawbackFrom,String paymentFrequency,String finPaymentDisbursmentMode,
			String serviceTaxnumber,String finTds,String lowTds) throws Exception
	{
		//Select pay To on Ui in Payment and Financial Tab
		String agentid=agentid1.trim();
		if(payTo.equals(""))
		{
			logger1.info(PAYTOMESSAGE);
		}else{
			clickElement(By.xpath("//select[@id='payToCd-"+agentid+"']"));
			fluentwait(By.xpath("//select[@id='payToCd-"+agentid+"']//option"), 60,
					"Pay To option is loading slow so couldn't find Pay To Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='payToCd-"+agentid+"']//option"), payTo);
			logger1.info("Selected Pay To Option is : " + payTo);
			
		}
		
		
		//Select ClawbackFrom on Ui in Payment and Financial Tab
		if(clawbackFrom.equals(""))
		{
				logger1.info(CLAWBACKMESSAGE);	
		}else{
			clickElement(By.xpath("//select[@id='clawbackFromCd-"+agentid+"']"));
			fluentwait(By.xpath("//select[@id='clawbackFromCd-"+agentid+"']//option"), 60,
					"Clawback Form option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='clawbackFromCd-"+agentid+"']//option"), clawbackFrom);
			logger1.info("Selected Clawback Option is : " + clawbackFrom);	
		}
		
		
		//Select Payment Frequency Option
		if(paymentFrequency.equals(""))
		{
			logger1.info(PAYMENTFREQUENCYMESSAGE);
		}else
		{
			clickElement(By.id(ModPaymentFrequency_Id));
			fluentwait(By.xpath(ModPaymentFrequencyOption), 60,
					"Payment Frequency option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModPaymentFrequencyOption), paymentFrequency);
			logger1.info("Selected Payment Frequency Option is : " + paymentFrequency);	
		}
		
		//Select Payment Disbursement Mode Option
		if(finPaymentDisbursmentMode.equals(""))
			{
				logger1.info(PAYMENTDISMODEMESSAGE);
			}else
			{
					clickElement(By.xpath("//select[@id='paymentModeCd--"+agentid+"']"));
					fluentwait(By.xpath("//select[@id='paymentModeCd--"+agentid+"']//option"), 60,
							"Payment Frequency option is loading slow so couldn't find Options.");
					DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='paymentModeCd--"+agentid+"']//option"), finPaymentDisbursmentMode);
					logger1.info("Selected Payment Disb Option is : " + finPaymentDisbursmentMode);	
			}
		
		//Entering Service Tax Number
		if(serviceTaxnumber.equals(""))
		{
			logger1.info(SERVICETAXMESSAGE);
		}else
		{
			clearTextfield(By.xpath("//input[@id='serviceTaxNum-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='serviceTaxNum-"+agentid+"']"), serviceTaxnumber);
			logger1.info("Entered Service Tax Number is : "+serviceTaxnumber);
		}
		
		/*//Selecting Agent TDS Status as Yes No
		if(finTds.equals(""))
		{
			logger1.info(FINTDSMESSAGE);
		}else{
			clickElement(By.id(ModTDS_flag_Id));
			fluentwait(By.xpath(ModTdsOption_Xpath), 60,
					"Tds option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModTdsOption_Xpath), finTds);
			logger1.info("Selected Tds Option is : " + finTds);	
		}*/
		
		if(payTo.equalsIgnoreCase("Parent")){
			System.out.println("No Need to Select TDS");
			}
			else{
				if(finTds.equals(""))
				{
					logger1.info(FINTDSMESSAGE);
				}else{
					clickElement(By.id(ModTDS_flag_Id));
					fluentwait(By.xpath(ModTdsOption_Xpath), 60,
							"Tds option is loading slow so couldn't find Options.");
					DropDownSelect.selectValuesfromDropdown(By.xpath(ModTdsOption_Xpath), finTds);
					logger1.info("Selected Tds Option is : " + finTds);	
				}
			}
		
		/*//Selecting Low Tds Option
		if(lowTds.equals(""))
		{
			logger1.info(LOWTDSMESSAGE);
		}else{
			if(lowTds.equalsIgnoreCase("Yes"))
			{
			clickElement(By.id(ModLowTds_Flag_Id));
			logger1.info("Selected LOW TDS is : "+lowTds);
			}
			else if(lowTds.equalsIgnoreCase("No"))
			{
				logger1.info("No need to select Low TDS.");
			}
		}*/
		
		//Selecting Low Tds Option
				if(payTo.equalsIgnoreCase("Parent")){
					System.out.println("No Need to Select Low TDS");
					}
				
				else{
					if(lowTds.equals(""))
					{
						logger1.info(LOWTDSMESSAGE);
					}else{
						if(lowTds.equalsIgnoreCase("Yes"))
						{
						clickElement(By.id(ModLowTds_Flag_Id));
						logger1.info("Selected LOW TDS is : "+lowTds);
						}
						else if(lowTds.equalsIgnoreCase("No"))
						{
							logger1.info("No need to select Low TDS.");
						}
					}

				}
		
		
		
	}
	
	
	public static void identityDetails(String agentid1,String finTds,String finPanNumber,String aadhaarCardNumber,int rowNum) throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String agentid=agentid1.trim();
		//Entering Pan Number on UI in Payment and Financial Page
		if(finTds.equalsIgnoreCase("Yes"))
			{
				if(finPanNumber.equals(""))
				{
						logger1.info(NOMINEEPANMESSAGE);
				}else{
				Thread.sleep(2000);
				clearTextfield(By.xpath("//input[@id='pan-"+agentid+"']"));
				enterText(By.xpath("//input[@id='pan-"+agentid+"']"), finPanNumber);
				logger1.info("Entered PanNumber is : "+finPanNumber);
				ErrorMessages.fieldverification("Pan Card");
				}
			}else if(finTds.equalsIgnoreCase("No"))
				{
					logger1.info("No Need to Enter Pan Number for Agent");
				}
				
		      String newCustomerType = read.getCellData("AgentModification","Customer_Type",rowNum);
		      
		      if(newCustomerType.equalsIgnoreCase("Corporate")){
		    	  logger1.info("For Corportae No Need to enter Adhar Details");
				//Entering Aadhaar Card Number
				
		      }
		      else{
		    	  if(aadhaarCardNumber.equals(""))
					{
						logger1.info(AADHAARCARDMESSAGE);
					}else{
						clearTextfield(By.xpath("//input[@id='aadhaarnumber-"+agentid+"']"));
						Thread.sleep(2000);
						enterText(By.xpath("//input[@id='aadhaarnumber-"+agentid+"']"), String.valueOf(aadhaarCardNumber));
						logger1.info("Entered Aadhar Card Number is : "+aadhaarCardNumber);
					}
		      }
				
	}
	
	public static void bankDetails(String sheetName,int rowNum,String agentid1,String finPaymentDisbursmentMode,String finIfscNumber,String finBankAccountNumber,String finMicrNumber,
			String finBank,String finBranchName,String effectiveFrom,String effectiveTo,String finState,String finCity) throws Exception
	{
		String agentid=agentid1.trim();
		
		if(finPaymentDisbursmentMode.equals(""))
		{
			logger1.info(PAYMENTDISMODEMESSAGE);
		}else if(finPaymentDisbursmentMode.equalsIgnoreCase("Cheque"))
		{
			logger1.info("No Need to fill bank details in Payment and Financial Tab.");
		}else if(finPaymentDisbursmentMode.equalsIgnoreCase("Direct Credit"))
		{
		
		//Entering FinIfscNumber 
		if(finIfscNumber.equals(""))
		{
			logger1.info(IFSCCODEMESSAGE);
		}else{
			clearTextfield(By.xpath("//input[@id='ifscCd-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='ifscCd-"+agentid+"']"), finIfscNumber);
			logger1.info("Entered Ifsc Code for Payment and Financial is : "+finIfscNumber);		
		}
		
		//Entering FinBankAccountNumber on UI
		if(finBankAccountNumber.equals(""))
		{
		logger1.info(BANKACCOUNTNUMBERMESSAGE);	
		}else{
			clearTextfield(By.xpath("//input[@id='accountNum-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='accountNum-"+agentid+"']"), finBankAccountNumber);
			logger1.info("Entered Bank Account Number for Payment and Financial is : "+finBankAccountNumber);
		}
		
		
		//Entering FinMicrNumber on UI
		if(finMicrNumber.equals(""))
		{
		logger1.info(MICRNUMBERMESSAGE);	
		}else{
			clearTextfield(By.xpath("//input[@id='micrNum-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='micrNum-"+agentid+"']"), finMicrNumber);
			logger1.info("Entered MICR Number for Payment and Financial is : "+finMicrNumber);
		}
		
		//Entering FinBank Number
		if(finBank.equals(""))
		{
		logger1.info(BANKNAMEMESSAGE);	
		}else{
			clearTextfield(By.xpath("//input[@id='bankName-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='bankName-"+agentid+"']"), finBank);
			logger1.info("Entered Bank Name for Payment and Financial is : "+finBank);
		}
		
		//Entering FinBranchName
		if(finBranchName.equals(""))
		{
			logger1.info(BRANCHNAMEMESSAGE);
		}else{
			clearTextfield(By.xpath("//input[@id='branchName-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='branchName-"+agentid+"']"), finBranchName);
			logger1.info("Entered Branch Name for Payment and Financial is : "+finBranchName);
		}
		
		
		//Entering EffectiveFrom
		if(effectiveFrom.equals(""))
		{
			logger1.info(EFFECTIVEFROMMESSAGE);
		}else{
			/*WebElement e1=driver.findElement(By.xpath("//input[@id='effectiveFromDt-"+Agentid+"']"));
			logger1.info(e1.getAttribute("id"));*/
			clearTextfield(By.xpath("//input[@id='effectiveFromDt-"+agentid+"']"));
			//e1.clear();
			Thread.sleep(2000);
			//Selecting Effective from Date using Excel Sheet Test data
			Calendar.calender(sheetName, By.xpath("//input[@id='effectiveFromDt-"+agentid+"']"), "PayEffectivefrom", rowNum);
			
			/*By by=By.xpath("//input[@id='effectiveFromDt-"+Agentid.trim()+"']");
			
			clearTextfield(by);
			Thread.sleep(2000);
			Calendar.calender(SheetName, by ,"PayEffectivefrom", rowNum);*/
		}
		
			/*// Entering EffectiveFrom
			if (EffectiveFrom.equals("")) {
				logger1.info(EffectivefromMessage);
			} else {
				// Selecting Effective from Date using Excel Sheet Test data
				clearTextfield(By.id(Effectivefrom_Id));
				Thread.sleep(5000);
				Calendar.calender(SheetName, By.id(Effectivefrom_Id), "PayEffectivefrom", rowNum);
			}*/
		
		
		//Entering EffectiveTo
		if(effectiveTo.equals(""))
		{
			logger1.info(EFFECTIVETOMESSAGE);
		}else{
			clearTextfield(By.xpath("//input[@id='effectiveToDt-"+agentid+"']"));
			Thread.sleep(2000);
			Calendar.calender(sheetName, By.xpath("//input[@id='effectiveToDt-"+agentid+"']"), "PayEffectiveTo", rowNum);
		}
	
		
		
		//Entering FinState
		if(finState.equals(""))
		{
			logger1.info(BRANCHSTATEMESSAGE);
		}else{
			Thread.sleep(2000);
			clickElement(By.xpath(ModStateSelc_Xpath));
			fluentwait(By.xpath(ModStateSelcOption_Xpath), 60,
					"Bank State option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModStateSelcOption_Xpath), finState);
			logger1.info("Selected Bank State Option is : " + finState);	
		}
	
		//Entering FinCity
		if(finCity.equals(""))
		{
			logger1.info(BRANCHCITYMESSAGE);
		}else{
			Thread.sleep(3000);
			clickElement(By.xpath(ModCitySelc_Xpath));
			fluentwait(By.xpath(ModCitySelcOption_Xpath), 60,
					"Bank State option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModCitySelcOption_Xpath), finCity);
			logger1.info("Selected Bank City Option is : " + finCity);	
		}
	
	}
	
}
	
	
}
