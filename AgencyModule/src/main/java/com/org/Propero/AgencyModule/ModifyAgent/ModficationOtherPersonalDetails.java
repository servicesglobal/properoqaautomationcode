package com.org.propero.agencymodule.modifyAgent;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.modificationFn.OtherPersonalDetailsModification;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ModficationOtherPersonalDetails extends BaseClass
{
	public static String testResult=null;
	private static Logger logger1 = LoggerFactory.getLogger(ModficationOtherPersonalDetails.class);
	
	public static String otherpersonaldetailstab(String sheetName,String workbookname,int rowNum,String agentid) throws Exception
	{
		OtherPersonalDetailsModification obj = new OtherPersonalDetailsModification();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
		String newCustomerType = read.getCellData(workbookname,"Customer_Type",rowNum);	
		String newNomineeTitle = read.getCellData(workbookname,"Nominee_Title",rowNum);
		String newNomineeFirstname = read.getCellData(workbookname,"Nominee_Name",rowNum);
		String newNomineeLastname = read.getCellData(workbookname,"NomineeLast_Name",rowNum);
		String newNomineeRelation = read.getCellData(workbookname,"Relation",rowNum);
		String newNomineePan = read.getCellData(workbookname,"Pan_Number",rowNum);
		String newNomineeAge = read.getCellData(workbookname,"Nominee_Age",rowNum);
		String newPaymentDisbursmentMode = read.getCellData(workbookname,"Payment_Disbursement_Mode",rowNum);
		String newTDS = read.getCellData(workbookname,"TDS_Certificate",rowNum);
		String newIFSCCode = read.getCellData(workbookname,"IFSC_Code",rowNum);
		String newBankAccountNumber = read.getCellData(workbookname,"Bank_Account_Number",rowNum);
		String newMicrNumber = read.getCellData(workbookname,"MICR_Number",rowNum);
		String newBankname = read.getCellData(workbookname,"Bank",rowNum);
		String newBranchName = read.getCellData(workbookname,"Branch_Name",rowNum);
		String newBranchState = read.getCellData(workbookname,"State_Name",rowNum);
		String newBranchCity = read.getCellData(workbookname,"City_Name",rowNum);
		
		obj.setCustomerType(newCustomerType);
		obj.setNomineeTitle(newNomineeTitle);
		obj.setNomineeFirstname(newNomineeFirstname);
		obj.setNomineeLastname(newNomineeLastname);
		obj.setNomineeRelation(newNomineeRelation);
		obj.setNomineePan(newNomineePan);
		obj.setNomineeAge(newNomineeAge);
		obj.setPaymentDisbursmentMode(newPaymentDisbursmentMode);
		obj.setTDS(newTDS);
		obj.setIFSCCode(newIFSCCode);
		obj.setBankAccountNumber(newBankAccountNumber);
		obj.setMicrNumber(newMicrNumber);
		obj.setBankname(newBankname);
		obj.setBranchName(newBranchName);
		obj.setBranchState(newBranchState);
		obj.setBranchCity(newBranchCity);
		
		
		obj.otherpersonalDetails(agentid,obj.getCustomerType(),obj.getNomineeTitle(), obj.getNomineeFirstname(), obj.getNomineeLastname(), 
				obj.getNomineeRelation(), obj.getNomineePan(), obj.getNomineeAge(), obj.getPaymentDisbursmentMode(), 
				obj.getTDS(), obj.getIFSCCode(), obj.getBankAccountNumber(), obj.getMicrNumber(), obj.getBankname(), 
				obj.getBranchName(), obj.getBranchState(), obj.getBranchCity());
		
		testResult="Pass";
		
		obj=null;
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		}catch(Exception  e)

		{
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			testResult="Fail";
			//Assert.fail("****Error in Other Personal Details Section****");
			throw e;
		}
		
		return testResult;
	}		

}
