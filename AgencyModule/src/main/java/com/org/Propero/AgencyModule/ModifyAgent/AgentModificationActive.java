package com.org.propero.agencymodule.modifyAgent;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.modificationFn.ModificationFnActive;
import com.org.propero.agencymodule.utility.BaseClass;

public class AgentModificationActive extends BaseClass implements ProjectInterface
{
	private static String testResult=null;
	private static Logger logger1 = LoggerFactory.getLogger(AgentModificationActive.class);
	
	public static String activeagentmodification(String sheetName,String workbookname,int rowNum,String AgentId) throws Exception{
		
		try
		{
		ModificationFnActive obj = new ModificationFnActive();
		ReadExcel read = new ReadExcel(sheetName);
		
		String newModificationReason = read.getCellData(workbookname,"Modification_Reason",rowNum);
		
		String newModificationRemark = read.getCellData(workbookname,"Modification_Remarks",rowNum);
		
		String newUploadDocument = read.getCellData(workbookname,"UploadDocument",rowNum);
		
		obj.setModificationReason(newModificationReason);
		obj.setModificationRemark(newModificationRemark);
		obj.setUploadDocument(newUploadDocument);
		
		obj.ModifyagentActive(AgentId, obj.getModificationReason(), obj.getModificationRemark(), obj.getUploadDocument());
		
		testResult="Pass";
		
		}
		catch(Exception  e)
		{
			testResult="Fail";
			//Assert.fail("****Error while searching Agent and Open Agent Details Section****");
			throw e;
		}
		
		return testResult;
	}
	

}
