package com.org.propero.agencymodule.errormessages;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class PageNotFoundError extends BaseClass implements ProjectInterface
{
	private static Logger logger1 = LoggerFactory.getLogger(PageNotFoundError.class);
	
	public static void pagenotfound() throws Exception
	{
		Boolean gotLost = driver.findElements(By.xpath("/html/body/app/main/error/div/div/div[1]/h4")).size()!= 0;
		if(gotLost==true)
		{
			String gotLostMessage = driver.findElement(By.xpath("/html/body/app/main/error/div/div/div[1]/h4")).getText();
		logger1.info("Getting Error : "+gotLostMessage);
		logger.log(LogStatus.FAIL, "Test Case is Failed because Getting Error : "+gotLostMessage);
		logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Got Lost Error")));
		}else{
			logger1.info("No Page Error Found and Moving Towards Agent Status.");	
		}
	}

}
