package com.org.propero.agencymodule.functions;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class PaymentandFinancialTab extends BaseClass implements ProjectInterface,MessageInterface
{

	private static Logger logger1 = LoggerFactory.getLogger(PaymentandFinancialTab.class);
	public void setPayTo(String newPayTo) {
		Variable.PayTo = newPayTo;
	}

	public String getPayTo() {
		return Variable.PayTo;
	}
	
	public void setClawbackFrom(String newClawbackFrom) {
		Variable.ClawbackFrom = newClawbackFrom;
	}

	public String getClawbackFrom() {
		return Variable.ClawbackFrom;
	}
	
	public void setPaymentFrequency(String newPaymentFrequency) {
		Variable.PaymentFrequency = newPaymentFrequency;
	}

	public String getPaymentFrequency() {
		return Variable.PaymentFrequency;
	}
	
	public void setFinPaymentDisbursmentMode(String newFinPaymentDisbursmentMode)  {
		Variable.FinPaymentDisbursmentMode = newFinPaymentDisbursmentMode;
	}

	public String getFinPaymentDisbursmentMode() {
		return Variable.FinPaymentDisbursmentMode;
	}
	
	public void setServiceTaxnumber(String newServiceTaxnumber)  {
		Variable.ServiceTaxnumber = newServiceTaxnumber;
	}

	public String getServiceTaxnumber() {
		return Variable.ServiceTaxnumber;
	}
	
	public void setFinTds(String newFinTds) throws Exception {
		Variable.FinTds = newFinTds;
	}

	public String getFinTds() {
		return Variable.FinTds;
	}
	
	public void setLowTds(String newLowTds) {
		Variable.LowTds = newLowTds;
	}

	public String getLowTds() {
		return Variable.LowTds;
	}
	
	public void setFinPanNumber(String newFinPanNumber) {
		Variable.FinPanNumber = newFinPanNumber;
	}

	public String getFinPanNumber() {
		return Variable.FinPanNumber;
	}
	
	public void setAadhaarCardNumber(String newAadhaarCardNumber) 	 {
		Variable.AadhaarCardNumber = newAadhaarCardNumber;
	}

	public String getAadhaarCardNumber() {
		return Variable.AadhaarCardNumber;
	}
	
	public void setFinIfscNumber(String newFinIfscNumber) {
		Variable.FinIfscNumber = newFinIfscNumber;
	}

	public String getFinIfscNumber() {
		return Variable.FinIfscNumber;
	}
	
	public void setFinBankAccountNumber(String newFinBankAccountNumber)  {
		Variable.FinBankAccountNumber = newFinBankAccountNumber;
	}

	public String getFinBankAccountNumber() {
		return Variable.FinBankAccountNumber;
	}
	
	public void setFinMicrNumber(String newFinMicrNumber)  {
		Variable.FinMicrNumber = newFinMicrNumber;
	}

	public String getFinMicrNumber() {
		return Variable.FinMicrNumber;
	}
	
	public void setFinBank(String newFinBank)  {
		Variable.FinBank = newFinBank;
	}

	public String getFinBank() {
		return Variable.FinBank;
	}
	
	public void setFinBranchName(String newFinBranchName)  {
		Variable.FinBranchName = newFinBranchName;
	}

	public String getFinBranchName() {
		return Variable.FinBranchName;
	}
	
	public void setEffectiveFrom(String newEffectiveFrom)  {
		Variable.EffectiveFrom = newEffectiveFrom;
	}

	public String getEffectiveFrom() {
		return Variable.EffectiveFrom;
	}
	
	public void setEffectiveTo(String newEffectiveTo)  {
		Variable.EffectiveTo = newEffectiveTo;
	}

	public String getEffectiveTo() {
		return Variable.EffectiveTo;
	}
	
	public void setFinState(String newFinState) {
		Variable.FinState = newFinState;
	}

	public String getFinState() {
		return Variable.FinState;
	}
	
	public void setFinCity(String newFinCity)  {
		Variable.FinCity = newFinCity;
	}

	public String getFinCity() {
		return Variable.FinCity;
	}
	
	
	public void paymentAndFinancialTab(String payTo,String clawbackFrom,String paymentFrequency,String finPaymentDisbursmentMode,
			String serviceTaxnumber,String finTds,String lowTds,String finPanNumber,String aadhaarCardNumber,
			String sheetName,int rowNum,String finIfscNumber,String finBankAccountNumber,String finMicrNumber,
			String finBank,String finBranchName,String effectiveFrom,String effectiveTo,String finState,String finCity,int rownum) throws Exception
	{
		verticalscrollup();
		
		//Clicking on Payment and Financial Tab
		clickElement(By.xpath(PaymentandFinancial_Xpath));
		
		//Filling Details in Agent Payments
		PaymentandFinancialTab.agentPayment(payTo, clawbackFrom, paymentFrequency, finPaymentDisbursmentMode, serviceTaxnumber, finTds, lowTds);
		
		//Filling Details in identity 
		PaymentandFinancialTab.identityDetails(payTo, finTds, finPanNumber, aadhaarCardNumber,rownum);
		
		//Filling Details in Bank Details
		PaymentandFinancialTab.bankDetails(sheetName, rowNum,payTo,finPaymentDisbursmentMode, finIfscNumber, finBankAccountNumber, finMicrNumber, finBank, finBranchName, effectiveFrom, effectiveTo, finState, finCity);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Payment and Financial Tab")));
		
	}
	
	
	public static void agentPayment(String payTo,String clawbackFrom,String paymentFrequency,String finPaymentDisbursmentMode,
			String serviceTaxnumber,String finTds,String lowTds) throws Exception
	{
		
		
		//Select pay To on Ui in Payment and Financial Tab
		if(payTo.equals(""))
		{
			logger1.info(PAYTOMESSAGE);
		}else{
			clickElement(By.id(PayTo_Id));
			fluentwait(By.xpath(PayToOption_Xpath), 60,
					"Pay To option is loading slow so couldn't find Pay To Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PayToOption_Xpath), payTo);
			logger1.info("Selected Pay To Option is : " + payTo);
			
		}
		
		
		//Select ClawbackFrom on Ui in Payment and Financial Tab
		if(clawbackFrom.equals(""))
		{
				logger1.info(CLAWBACKMESSAGE);	
		}else{
			clickElement(By.id(ClawbackForm_Id));
			fluentwait(By.xpath(ClawbackOption_Xpath), 60,
					"Clawback Form option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ClawbackOption_Xpath), clawbackFrom);
			logger1.info("Selected Clawback Option is : " + clawbackFrom);	
		}
		
		
		//Select Payment Frequency Option
		if(paymentFrequency.equals(""))
		{
			logger1.info(PAYMENTFREQUENCYMESSAGE);
		}else
		{
			clickElement(By.id(PaymentFrequency_Id));
			fluentwait(By.xpath(PaymentFrequencyOption), 60,
					"Payment Frequency option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentFrequencyOption), paymentFrequency);
			logger1.info("Selected Payment Frequency Option is : " + paymentFrequency);	
		}
		
		//Select Payment Disbursement Mode Option
		if(finPaymentDisbursmentMode.equals(""))
			{
				logger1.info(PAYMENTDISMODEMESSAGE);
			}else
			{
					clickElement(By.id(Paymntdisbmode_Id));
					fluentwait(By.xpath(PaymentDisbModeOption_Xpath), 60,
							"Payment Frequency option is loading slow so couldn't find Options.");
					DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentDisbModeOption_Xpath), finPaymentDisbursmentMode);
					logger1.info("Selected Payment Disb Option is : " + finPaymentDisbursmentMode);	
			}
		
		//Entering Service Tax Number
		if(serviceTaxnumber.equals(""))
		{
			logger1.info(SERVICETAXMESSAGE);
		}else
		{
			enterText(By.id(ServiceTaxNum_Id), serviceTaxnumber);
			logger1.info("Entered Service Tax Number is : "+serviceTaxnumber);
		}
		
	
		if(payTo.equalsIgnoreCase("Parent")){
		logger1.info("No Need to Enter TDS and Low TDS value");
		
		}
		else{
			//Selecting Agent TDS Status as Yes No
			if(finTds.equals(""))
			{
				logger1.info(FINTDSMESSAGE);
			}else{
				clickElement(By.id(TDS_flag_Id));
				fluentwait(By.xpath(TdsOption_Xpath), 60,
						"Tds option is loading slow so couldn't find Options.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(TdsOption_Xpath), finTds);
				logger1.info("Selected Tds Option is : " + finTds);	
			}
			
			//Selecting Low Tds Option
			if(lowTds.equals(""))
			{
				logger1.info(LOWTDSMESSAGE);
			}else{
				if(lowTds.equalsIgnoreCase("Yes"))
				{
				clickElement(By.id(LowTds_Flag_Id));
				logger1.info("Selected LOW TDS is : "+lowTds);
				}
				else if(lowTds.equalsIgnoreCase("No"))
				{
					logger1.info("No need to select Low TDS.");
				}
			}
		}
		
	}
	
	
	public static void identityDetails(String payTo, String finTds,String finPanNumber,String aadhaarCardNumber,int rownum) throws Exception
	{
		//Entering Pan Number on UI in Payment and Financial Page
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		String customerType = read.getCellData(workbookName,"Customer_Type",rownum);
		
		
		if(customerType.equalsIgnoreCase("Corporate"))
		{
			if(payTo.equalsIgnoreCase("Parent")){
				logger1.info("No Need to Enter Pan Number");
				}
				else{
					if(finTds.equalsIgnoreCase("Yes"))
					{
						if(finPanNumber.equals(""))
						{
								logger1.info(NOMINEEPANMESSAGE);
						}else{
						enterText(By.id(PanNumber_Id), finPanNumber);
						logger1.info("Entered PanNumber is : "+finPanNumber);
						ErrorMessages.fieldverification("Pan Card");
						}
					}
					else 
						{
							logger1.info("No Need to Enter Pan Number for Agent");
						}
				}
		}
		else{
		
		if(payTo.equalsIgnoreCase("Parent")){
		logger1.info("No Need to Enter Pan Number");
		}
		else{
			if(finTds.equalsIgnoreCase("Yes"))
			{
				if(finPanNumber.equals(""))
				{
						logger1.info(NOMINEEPANMESSAGE);
				}else{
				enterText(By.id(PanNumber_Id), finPanNumber);
				logger1.info("Entered PanNumber is : "+finPanNumber);
				ErrorMessages.fieldverification("Pan Card");
				}
			}
			else 
				{
					logger1.info("No Need to Enter Pan Number for Agent");
				}
		}
				//Entering Aadhaar Card Number
				if(aadhaarCardNumber.equals(""))
				{
					logger1.info(AADHAARCARDMESSAGE);
				}else{
					enterText(By.id(Aadharcardnumber_id), String.valueOf(aadhaarCardNumber));
					logger1.info("Entered Aadhar Card Number is : "+aadhaarCardNumber);
				}
		}	
	}
	
	public static void bankDetails(String sheetName,int rowNum,String payTo,String finPaymentDisbursmentMode,String finIfscNumber,String finBankAccountNumber,String finMicrNumber,
			String finBank,String finBranchName,String effectiveFrom,String effectiveTo,String finState,String finCity) throws Exception
	{
		
		if(finPaymentDisbursmentMode.equals(""))
		{
			logger1.info(PAYMENTDISMODEMESSAGE);
		}else if(finPaymentDisbursmentMode.equalsIgnoreCase("Cheque"))
		{
			logger1.info("No Need to fill bank details in Payment and Financial Tab.");
		}else if(finPaymentDisbursmentMode.equalsIgnoreCase("Direct Credit"))
		{
		
		if(payTo.equalsIgnoreCase("Parent")){
			logger1.info("No Need to Fill the Bank Details if Pay To selected as 'Parent' ");
		}
		else{
			//Entering FinIfscNumber 
			if(finIfscNumber.equals(""))
			{
				logger1.info(IFSCCODEMESSAGE);
			}else{
				
				enterText(By.id(Ifsc_Code_Id), finIfscNumber);
				logger1.info("Entered Ifsc Code for Payment and Financial is : "+finIfscNumber);		
			}
			
			//Entering FinBankAccountNumber on UI
			if(finBankAccountNumber.equals(""))
			{
			logger1.info(BANKACCOUNTNUMBERMESSAGE);	
			}else{
				enterText(By.id(BankAccountNum_Id), finBankAccountNumber);
				logger1.info("Entered Bank Account Number for Payment and Financial is : "+finBankAccountNumber);
			}
			
			
			//Entering FinMicrNumber on UI
			if(finMicrNumber.equals(""))
			{
			logger1.info(MICRNUMBERMESSAGE);	
			}else{
				enterText(By.id(MicrNum_Id), finMicrNumber);
				logger1.info("Entered MICR Number for Payment and Financial is : "+finMicrNumber);
			}
			
			//Entering FinBank Number
			if(finBank.equals(""))
			{
			logger1.info(BANKNAMEMESSAGE);	
			}else{
				enterText(By.id(BankDetails_Id), finBank);
				logger1.info("Entered Bank Name for Payment and Financial is : "+finBank);
			}
			
			//Entering FinBranchName
			if(finBranchName.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				enterText(By.id(Branch_Details_Id), finBranchName);
				logger1.info("Entered Branch Name for Payment and Financial is : "+finBranchName);
			}
			
			
			//Entering EffectiveFrom
			if(effectiveFrom.equals(""))
			{
				logger1.info(EFFECTIVEFROMMESSAGE);
			}else{
				//Selecting Effective from Date using Excel Sheet Test data
				Thread.sleep(5000);
				Calendar.calender(sheetName, By.id(Effectivefrom_Id), "PayEffectivefrom", rowNum);
			}
			
			
			//Entering EffectiveTo
			if(effectiveTo.equals(""))
			{
				logger1.info(EFFECTIVETOMESSAGE);
			}else{
				Thread.sleep(5000);
				Calendar.calender(sheetName, By.id(EffectiveTo_Id), "PayEffectiveTo", rowNum);
			}
		
			
			
			//Entering FinState
			if(finState.equals(""))
			{
				logger1.info(BRANCHSTATEMESSAGE);
			}else{
				clickElement(By.xpath(StateSelc_Xpath));
				fluentwait(By.xpath(StateSelcOption_Xpath), 60,
						"Bank State option is loading slow so couldn't find Options.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(StateSelcOption_Xpath), finState);
				logger1.info("Selected Bank State Option is : " + finState);	
			}
		
			//Entering FinCity
			if(finCity.equals(""))
			{
				logger1.info(BRANCHCITYMESSAGE);
			}else{
				Thread.sleep(5000);
				clickElement(By.xpath(CitySelc_Xpath));
				fluentwait(By.xpath(CitySelcOption_Xpath), 60,
						"Bank State option is loading slow so couldn't find Options.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(CitySelcOption_Xpath), finCity);
				logger1.info("Selected Bank City Option is : " + finCity);	
			}
		}
	
	}
	
		verticalscrollup();
}
	
//********************* Negative Functions *************************************************
	public void paymentAndFinancialTabNegativeTestcase(String payTo,String clawbackFrom,String paymentFrequency,String finPaymentDisbursmentMode,
			String serviceTaxnumber,String finTds,String lowTds,String finPanNumber,String aadhaarCardNumber,
			String sheetName,int rowNum,String finIfscNumber,String finBankAccountNumber,String finMicrNumber,
			String finBank,String finBranchName,String effectiveFrom,String effectiveTo,String finState,String finCity) throws Exception
	{
		verticalscrollup();
		
		//Clicking on Payment and Financial Tab
		clickElement(By.xpath(PaymentandFinancial_Xpath));
		
		//Filling Details in Agent Payments
		PaymentandFinancialTab.agentPaymentNegative(payTo, clawbackFrom, paymentFrequency, finPaymentDisbursmentMode, serviceTaxnumber, finTds, lowTds);
		
		//Filling Details in identity 
		PaymentandFinancialTab.identityDetailsNegative(finTds, finPanNumber, aadhaarCardNumber);
		
		//Filling Details in Bank Details
		PaymentandFinancialTab.bankDetailsNegative(sheetName, rowNum, finPaymentDisbursmentMode, finIfscNumber, finBankAccountNumber, finMicrNumber, finBank, finBranchName, effectiveFrom, effectiveTo, finState, finCity);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Payment and Financial Tab")));
		
	}
	
	
	public static void agentPaymentNegative(String payTo,String clawbackFrom,String paymentFrequency,String finPaymentDisbursmentMode,
			String serviceTaxnumber,String finTds,String lowTds) throws Exception
	{
		//Select pay To on Ui in Payment and Financial Tab
		if(payTo.equals(""))
		{
			logger1.info(PAYTOMESSAGE);
		}else{
			clickElement(By.id(PayTo_Id));
			fluentwait(By.xpath(PayToOption_Xpath), 60,
					"Pay To option is loading slow so couldn't find Pay To Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PayToOption_Xpath), payTo);
			logger1.info("Selected Pay To Option is : " + payTo);
			
		}
		
		
		//Select ClawbackFrom on Ui in Payment and Financial Tab
		if(clawbackFrom.equals(""))
		{
				logger1.info(CLAWBACKMESSAGE);	
		}else{
			clickElement(By.id(ClawbackForm_Id));
			fluentwait(By.xpath(ClawbackOption_Xpath), 60,
					"Clawback Form option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ClawbackOption_Xpath), clawbackFrom);
			logger1.info("Selected Clawback Option is : " + clawbackFrom);	
		}
		
		
		//Select Payment Frequency Option
		if(paymentFrequency.equals(""))
		{
			logger1.info(PAYMENTFREQUENCYMESSAGE);
		}else
		{
			clickElement(By.id(PaymentFrequency_Id));
			fluentwait(By.xpath(PaymentFrequencyOption), 60,
					"Payment Frequency option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentFrequencyOption), paymentFrequency);
			logger1.info("Selected Payment Frequency Option is : " + paymentFrequency);	
		}
		
		//Select Payment Disbursement Mode Option
		if(finPaymentDisbursmentMode.equals(""))
			{
				logger1.info(PAYMENTDISMODEMESSAGE);
			}else
			{
					clickElement(By.id(Paymntdisbmode_Id));
					fluentwait(By.xpath(PaymentDisbModeOption_Xpath), 60,
							"Payment Frequency option is loading slow so couldn't find Options.");
					DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentDisbModeOption_Xpath), finPaymentDisbursmentMode);
					logger1.info("Selected Payment Disb Option is : " + finPaymentDisbursmentMode);	
			}
		
		//Entering Service Tax Number
		if(serviceTaxnumber.equals(""))
		{
			logger1.info(SERVICETAXMESSAGE);
		}else
		{
			enterText(By.id(ServiceTaxNum_Id), serviceTaxnumber);
			logger1.info("Entered Service Tax Number is : "+serviceTaxnumber);
		}
		
		//Selecting Agent TDS Status as Yes No
		if(finTds.equals(""))
		{
			logger1.info(FINTDSMESSAGE);
		}else{
			clickElement(By.id(TDS_flag_Id));
			fluentwait(By.xpath(TdsOption_Xpath), 60,
					"Tds option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(TdsOption_Xpath), finTds);
			logger1.info("Selected Tds Option is : " + finTds);	
		}
		
		//Selecting Low Tds Option
		if(lowTds.equals(""))
		{
			logger1.info(LOWTDSMESSAGE);
		}else{
			if(lowTds.equalsIgnoreCase("Yes"))
			{
			clickElement(By.id(LowTds_Flag_Id));
			logger1.info("Selected LOW TDS is : "+lowTds);
			}
			else if(lowTds.equalsIgnoreCase("No"))
			{
				logger1.info("No need to select Low TDS.");
			}
		}
		
		
		
	}
	
	
	public static void identityDetailsNegative(String finTds,String finPanNumber,String aadhaarCardNumber) throws Exception
	{
		//Entering Pan Number on UI in Payment and Financial Page
		if(finTds.equalsIgnoreCase("Yes"))
			{
				if(finPanNumber.equals(""))
				{
						logger1.info(NOMINEEPANMESSAGE);
				}else{
				enterText(By.id(PanNumber_Id), finPanNumber);
				logger1.info("Entered PanNumber is : "+finPanNumber);
				ErrorMessages.fieldverification("Pan Card");
				}
			}else if(finTds.equalsIgnoreCase("No"))
				{
					logger1.info("No Need to Enter Pan Number for Agent");
				}
				
				//Entering Aadhaar Card Number
				if(aadhaarCardNumber.equals(""))
				{
					logger1.info(AADHAARCARDMESSAGE);
				}else{
					enterText(By.id(Aadharcardnumber_id), String.valueOf(aadhaarCardNumber));
					logger1.info("Entered Aadhar Card Number is : "+aadhaarCardNumber);
				}
				
	}
	
	public static void bankDetailsNegative(String sheetName,int rowNum,String finPaymentDisbursmentMode,String finIfscNumber,String finBankAccountNumber,String finMicrNumber,
			String finBank,String finBranchName,String effectiveFrom,String effectiveTo,String finState,String finCity) throws Exception
	{
		
		if(finPaymentDisbursmentMode.equals(""))
		{
			logger1.info(PAYMENTDISMODEMESSAGE);
		}else if(finPaymentDisbursmentMode.equalsIgnoreCase("Cheque"))
		{
			logger1.info("No Need to fill bank details in Payment and Financial Tab.");
		}else if(finPaymentDisbursmentMode.equalsIgnoreCase("Direct Credit"))
		{
		
		//Entering FinIfscNumber 
		if(finIfscNumber.equals(""))
		{
			logger1.info(IFSCCODEMESSAGE);
		}else{
			
			enterText(By.id(Ifsc_Code_Id), finIfscNumber);
			logger1.info("Entered Ifsc Code for Payment and Financial is : "+finIfscNumber);		
		}
		
		//Entering FinBankAccountNumber on UI
		if(finBankAccountNumber.equals(""))
		{
		logger1.info(BANKACCOUNTNUMBERMESSAGE);	
		}else{
			enterText(By.id(BankAccountNum_Id), finBankAccountNumber);
			logger1.info("Entered Bank Account Number for Payment and Financial is : "+finBankAccountNumber);
		}
		
		
		//Entering FinMicrNumber on UI
		if(finMicrNumber.equals(""))
		{
		logger1.info(MICRNUMBERMESSAGE);	
		}else{
			enterText(By.id(MicrNum_Id), finMicrNumber);
			logger1.info("Entered MICR Number for Payment and Financial is : "+finMicrNumber);
		}
		
		//Entering FinBank Number
		if(finBank.equals(""))
		{
		logger1.info(BANKNAMEMESSAGE);	
		}else{
			enterText(By.id(BankDetails_Id), finBank);
			logger1.info("Entered Bank Name for Payment and Financial is : "+finBank);
		}
		
		//Entering FinBranchName
		if(finBranchName.equals(""))
		{
			logger1.info(BRANCHNAMEMESSAGE);
		}else{
			enterText(By.id(Branch_Details_Id), finBranchName);
			logger1.info("Entered Branch Name for Payment and Financial is : "+finBranchName);
		}
		
		
		//Entering EffectiveFrom
		if(effectiveFrom.equals(""))
		{
			logger1.info(EFFECTIVEFROMMESSAGE);
		}else{
			//Selecting Effective from Date using Excel Sheet Test data
			Thread.sleep(5000);
			Calendar.calender(sheetName, By.id(Effectivefrom_Id), "PayEffectivefrom", rowNum);
		}
		
		
		//Entering EffectiveTo
		if(effectiveTo.equals(""))
		{
			logger1.info(EFFECTIVETOMESSAGE);
		}else{
			Thread.sleep(5000);
			Calendar.calender(sheetName, By.id(EffectiveTo_Id), "PayEffectiveTo", rowNum);
		}
	
		
		
		//Entering FinState
		if(finState.equals(""))
		{
			logger1.info(BRANCHSTATEMESSAGE);
		}else{
			clickElement(By.xpath(StateSelc_Xpath));
			fluentwait(By.xpath(StateSelcOption_Xpath), 60,
					"Bank State option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(StateSelcOption_Xpath), finState);
			logger1.info("Selected Bank State Option is : " + finState);	
		}
	
		//Entering FinCity
		if(finCity.equals(""))
		{
			logger1.info(BRANCHCITYMESSAGE);
		}else{
			clickElement(By.xpath(CitySelc_Xpath));
			fluentwait(By.xpath(CitySelcOption_Xpath), 60,
					"Bank State option is loading slow so couldn't find Options.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CitySelcOption_Xpath), finCity);
			logger1.info("Selected Bank City Option is : " + finCity);	
		}
	
	}
	
}
	
	
}
