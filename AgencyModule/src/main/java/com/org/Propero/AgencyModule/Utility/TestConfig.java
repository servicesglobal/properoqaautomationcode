package com.org.propero.agencymodule.utility;
public class TestConfig
{

	public static String server = "mail.religare.com";
	public static String from = "rhiclqctech@religare.com";
	public static String password = "Religare@123";
	
	//public static String from = "bijaya.sahoo@ext.religare.in";
	//public static String from = "religare\bijaya.sahoo";
	//public static String password = "Pantry@1234";
	public static String subject = "Agency Automation Test Report";
	//public static String[] to ={"harshit.jain@ext.religare.in","deepak.mahajan@ext.religare.in","jain.shashank@religare.com","rhiclqctech@religare.com","gaurav.kumr@religare.com"};
	public static String[] to = {"bijaya.sahoo@ext.religare.in","harshit.jain@ext.religare.in","gaurav.kumr@religare.com","k.santhosh@religare.com"};
	public static String messageBody = "Hi Team, <br> <br> Please Find the attached Agency Automation Report. <br> <br> <br> Kind Regards, <br> Ashish Singh <br> Software Test Engineer <br> Monocept";
	public static String attachmentPath = ".\\reports\\TestReport.html";
	public static String attachmentName = "Agency Automation TestReport.html";
	public static String attachmentPath1 = ".\\TestData\\TestData_AgencyModule.xlsx";
	public static String attachmentName1 = "TestData_AgencyModule.xlsx";
	
	//"ashish.sgh@ext.religare.in","harshit.jain@ext.religare.in","deepak.mahajan@ext.religare.in","jain.shashank@religare.com","roshan.km@ext.religare.in"
	
	//For Gmail
	/*public static String server="mail.religare.com";
	public static String from = "rhiclqctech@religare.com";
	public static String password = "mail@123";
	public static String[] to ={"ashish@monocept.com"};//"deepak@monocept.com","harshit@monocept.com","ashish@monocept.com","amit.ty@religare.com","raja.vaishnav@religare.com"
	public static String subject = "Faveo Automation Test Report";
	
	public static String messageBody ="Hi Team, <br> <br> Please Find the attached Faveo Automation Report. <br> <br> <br> Kind Regards, <br> Ashish Singh <br> Software Test Engineer <br> Monocept";
	public static String attachmentPath=System.getProperty("user.dir") + "/Reports/TestReport.html";
	public static String attachmentName="Automation TestReport.html";
	public static String attachmentPath1=System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx";
	public static String attachmentName1="Test Data Sheet.xlsx";
	*/
	
	//QC SQL DATABASE DETAILS	
	public static String driverQC="oracle.jdbc.driver.OracleDriver"; 
	public static String dbConnectionUrlQC="jdbc:oracle:thin:@10.216.30.112:1521:PROPDEV"; 
	public static String dbUserNameQC="RHQCDEV"; 
	public static String dbPasswordQC="RHQCDEV"; 
	
	
/*	//QC SQL DATABASE DETAILS	
		public static String driverQC="oracle.jdbc.driver.OracleDriver"; 
		public static String dbConnectionUrlQC="jdbc:oracle:thin:@10.216.30.112:1521:POC12CDB"; 
		public static String dbUserNameQC="RHQCDEV"; 
		public static String dbPasswordQC="RHQCDEV#059"; 
		*/
	
	//UAT SQL DATABASE DETAILS	
	public static String driverUAT="oracle.jdbc.driver.OracleDriver"; 
	public static String dbConnectionUrlUAT="jdbc:oracle:thin:@10.216.30.112:1521:POC12CDB"; 
	public static String dbUserNameUAT="RHUATDEV"; 
	public static String dbPasswordUAT="RHUATDEV"; 
	
	
	//Staging DATABASE DETAILS
	public static String driverStage="oracle.jdbc.driver.OracleDriver"; 
	public static String dbConnectionUrlStage="jdbc:oracle:thin:@10.216.30.112:1521:POC12CDB"; 
	public static String dbUserNameStage="rhstage"; 
	public static String dbPasswordStage="rhstage"; 
	
	
	
	
}
