package com.org.propero.agencymodule.communcationDetails;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class CommuncationDetailsGetterAndSetterMethod extends BaseClass implements ProjectInterface{

	String communicationType;
	String agentId;
	String pageCount;
	private static Logger logger1 = LoggerFactory.getLogger(CommuncationDetailsGetterAndSetterMethod.class);
 
	public void setCommunicationType(String commTypeValue){
		this.communicationType=commTypeValue;
	}
	public String getCommunicationType(){
		return communicationType;
	}
	
	public void setAgentId(String agentId){
		this.agentId=agentId;
	}
	public String getAgentId(){
		return agentId;
	}
	
	public void setPageCount(String pageCount){
		this.pageCount=pageCount;
	}
	public String getPageCount(){
		return pageCount;
	}
	
	public void setPagenaitionCount(String pageCount){
		this.pageCount=pageCount;
	}
	public String getPagenaitionCount(){
		return pageCount;
	}
	
	private static String testResult;

	public String setDetailscommuncationType(String workbookName, int rowNum, String communcationType, String agentId, String pageCount, String paginationCountValue) throws Exception{
		
		ReadExcel read = new ReadExcel("D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx");
		String testcaseName=read.getCellData(workbookName,"TestCaseCategory",rowNum);
		
		// Click on Channel Management
		fluentwait(By.xpath(ChannelManagement_Xpath), 60,
						"Page is loading Slow so couldn't click on Channel Management.");
		clickElement(By.xpath(ChannelManagement_Xpath));
		logger.log(LogStatus.PASS, "Channel Managment Selected");

		// Click on Search Intermediary
		clickElement(By.xpath(CommunicationDetails_Xpath));
		logger.log(LogStatus.PASS, "Communication Details Selected");
			
		//Select Communication Type
		WebElement e1=driver.findElement(By.id(CommunicationType_ID));	
		selectValue(e1, communcationType);
		logger.log(LogStatus.PASS, "Selected Communictaion Type is "+communcationType);
		
		//Enter agent Id
		enterText(By.id(AgentId_ID),agentId);
		logger.log(LogStatus.PASS, "Agent Id Entered: "+ agentId);
		
		if(testcaseName.equalsIgnoreCase("WithDate")){
			
		String fromDate=Calendar.calender(workbookName, By.id(FromDate_ID), "From_Date", rowNum);
		String toDate=Calendar.calender(workbookName, By.id(ToDate_ID), "To_Date", rowNum);
		
		logger.log(LogStatus.PASS, "From Date Selected "+fromDate);
		logger.log(LogStatus.PASS, "To Date Selected "+toDate);
		
		//Clicked on Search Button
		clickElement(By.xpath(SearchButton_XPATH));
		logger.log(LogStatus.PASS, "Clicked on Search Button");
		Thread.sleep(10000);
		
		verifySendEmailStatus();
		
		}
		else if(testcaseName.equalsIgnoreCase("WithoutDate")){
			//Clicked on Search Button
			clickElement(By.xpath(SearchButton_XPATH));
			logger.log(LogStatus.INFO, "Clicked on Search Button");
			Thread.sleep(10000);
			verifySendEmailStatus();
			
		}
		else if(testcaseName.equalsIgnoreCase("PageCountDropDown")){
			setPageCountDropDownValue(pageCount);
			logger.log(LogStatus.INFO, "PageCountSelcted: "+ pageCount);
			Thread.sleep(10000);
			verifySendEmailStatus();
			
		}
		else if(testcaseName.equalsIgnoreCase("PaginationCount")){
			clickOnPagination(paginationCountValue);
			logger.log(LogStatus.INFO, "PaginationCount: "+ paginationCountValue);
			Thread.sleep(10000);
			verifySendEmailStatus();
			
		}
		return testResult;
	}
	
	public void setPageCountDropDownValue(String pageCount) {
		// Select Communication Type
		WebElement e1 = driver.findElement(By.id(PageCount_ID));
		selectValue(e1, pageCount);
		logger.log(LogStatus.INFO, "Selected Communictaion Type is " + pageCount);
	}
	
	//Verify Sent Email Status of Agent
	public void verifySendEmailStatus(){
		List<WebElement> list=driver.findElements(By.xpath("//div[@colid='emailSent' and text()='YES']"));
		for(WebElement options:list){
			String txt=options.getText();
			if(txt.equalsIgnoreCase("Yes")){
				logger1.info("Agent Communcation Data is available and verified with Test Data");
				break;
			}
			else{
			
				Assert.fail("Agent Communcation Data is not available");
			}
		}
	}
	
	//Pagination Function
	public void clickOnPagination(String paginationValue){
		List<WebElement> list=driver.findElements(By.xpath(Pagination_XPATH));
		for(WebElement options:list){
			if(options.getText().equalsIgnoreCase(paginationValue)){
				options.click();
				break;
			}
			else{
				logger.log(LogStatus.INFO, "Wrong pagination value entered in Test data: " + paginationValue);
				Assert.fail("Wrong pagination value entered in Test data: " + paginationValue);
			}
		}
		
	}
	
}
