package com.org.propero.agencymodule.basefunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;


public class Calendar extends BaseClass implements ProjectInterface
{
	private static Logger logger1 = LoggerFactory.getLogger(Calendar.class);
	//private static Pattern dateFrmtPtrn = Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");
	@SuppressWarnings("unused")
	@Test
	public static String calender(String workbookName,By by,String columnName,int rowNum) throws Exception{
		
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		
		String fis = workbookName;
		
		String aRFReceiveddate = read.getCellData(fis, columnName, rowNum);
		if(aRFReceiveddate.equals(""))
		{
			logger1.info("There is No data in Excel for Date.");
		}
		else {
		String spilitter[]=aRFReceiveddate.split(",");			
		String eday = spilitter[0];
		String emonth = spilitter[1];
		String eYear = spilitter[2];
		//String emonthYear = (emonth +" "+eYear);
		
		fluentwait(by, 60, "Unable to Open Find Calender Element.");
		try{
			clickElement(by);	
		}catch(Exception e)
		{
			logger1.info("Unable to Click on CalenderXpath");
		}
		
		//String cMYear=driver.findElement(By.xpath("//th[@class='datepicker-switch']")).getText();
		String cMYear=null;
		//WebElement Next = driver.findElement(By.xpath("//div[contains(@class,'datepicker-days')]//th[contains(@class,'prev')][contains(text(),'«')]"));
		WebElement backButton = driver.findElement(By.xpath("//div[@class='datepicker-years']//th[@class='prev'][contains(text(),'«')]"));
		Thread.sleep(2000);
			
		try{
			/*clickElement(By.xpath("//th[@class='datepicker-switch']"));*/
			clickElement(By.xpath("//div[@class='datepicker-days']//table[@class='table-condensed']//th[@class='datepicker-switch']"));
			clickElement(By.xpath("//div[@class='datepicker-months']//th[@class='datepicker-switch']"));
		}
		catch(Exception e)
		{
			logger1.info("Unable to ");
		}
			
			for(int i=1; i<=12;i++)
			{
				String currentYear = driver.findElement(By.xpath("/html/body/div[3]/div[3]/table/tbody/tr/td/span[1]")).getText();
				String currentYear1 = driver.findElement(By.xpath("/html/body/div[3]/div[3]/table/tbody/tr/td/span[12]")).getText();
				int cyear=Integer.parseInt(currentYear);
				int cyear1=Integer.parseInt(currentYear1);
				int year1 = Integer.parseInt(eYear);
				
				if(year1 >= cyear && year1 <= cyear1)
				{
					
					clickElement(By.xpath("/html/body/div[3]/div[3]/table/tbody/tr/td/span[contains(text(),"+"'"+year1+"'"+")]"));
					break;
				}else{
					logger1.info("Expectation Not Meet");
					backButton.click();
				}
				
			}
			clickElement(By.xpath("//tbody//tr//td//span[contains(text(),"+"'"+emonth+"'"+")]"));
			cMYear=driver.findElement(By.xpath("//th[@class='datepicker-switch']")).getText();
			driver.findElement(By.xpath("//td[@class='day' or @class='today day' or @class='today active day' or @class='active day'][contains(text(),"+"'"+eday+"'"+")]")).click();
			
			//driver.findElement(By.xpath("//td[contains(@class,'day')][contains(text(),"+"'"+eday+"'"+")]")).click();
			//logger1.info("Entered data is : "+ARFReceiveddate);
	}
		
		return aRFReceiveddate;
	}
	
	
}
