package com.org.propero.agencymodule.addagent;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.functions.IntermediaryTab;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class IntermediaryDetails extends BaseClass
{
	private static String testResult;
	
	public static String agentsDetails(String sheetName,String workbookname,int rowNum) throws Exception
	{
		
		try
		{
		IntermediaryTab obj = new IntermediaryTab();
		ReadExcel read = new ReadExcel(sheetName);
		
		
		String intermediaryCategory = read.getCellData(workbookname,"Intermediary_Category",rowNum);
		
		String intermediarySubCategory = read.getCellData(workbookname,"Sub_Category",rowNum);
		
		String parentIntermediary = read.getCellData(workbookname,"Parent_Intermediary_Code",rowNum);
		
		String rmCode = read.getCellData(workbookname,"RM_Code",rowNum);
		
		String sourcingVertical = read.getCellData(workbookname,"Sourcing_Veritical",rowNum);
		
		String sourcingSubVertical = read.getCellData(workbookname,"Sub_Vertical",rowNum);
		
		String sourcingLocation = read.getCellData(workbookname,"Sourcing_Location",rowNum);
		
		String secondaryVertical = read.getCellData(workbookname,"Secondary_Vertical",rowNum);
		
		String arfReceivedDate = read.getCellData(workbookname,"ARFRcvd_Date",rowNum);
		
		String symbioLogin = read.getCellData(workbookname,"Symbosis_Login",rowNum);
		
		String chequeDD = read.getCellData(workbookname,"Cheque_DD",rowNum);
		
		String classificationType = read.getCellData(workbookname, "Classification", rowNum);
		
		obj.setIntermediaryCategory(intermediaryCategory);
		obj.setInterSubCategory(intermediarySubCategory);
		obj.setParentIntermediary(parentIntermediary);
		obj.setRmCode(rmCode);
		obj.setSourcingVertical(sourcingVertical);
		obj.setSourcingSubVertical(sourcingSubVertical);
		obj.setSourcingLocation(sourcingLocation);
		obj.setSecondaryVertical(secondaryVertical);
		obj.setArfReceivedDate(arfReceivedDate);
		obj.setSymbLogin(symbioLogin);
		obj.setChequeDD(chequeDD);
		obj.setClassification(classificationType);
		
		
		obj.internediaryDetailstab(workbookname, rowNum, obj.getIntermediaryCategory(), obj.getIntermediarySubCategory(), obj.getParentIntermediary(), 
				obj.getRmCode(), obj.getSourcingVertical(), obj.getSourcingSubVertical(), obj.getSourcingLocation(), obj.getSecondaryVertical(), 
									obj.getArfReceivedDate(), obj.getSymbLogin(), obj.getChequeDD(), obj.getClassification());
		
		testResult="Pass";
		
		obj=null;
		
		}catch(Exception e)
		{
			testResult="Fail";
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Fail")));
			throw e;
			//Assert.fail("Error in Intermediary Details Page");
			
		}
		
		return testResult;
		
	}

}
