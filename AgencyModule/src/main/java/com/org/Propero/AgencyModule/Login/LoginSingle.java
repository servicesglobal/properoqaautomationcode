package com.org.propero.agencymodule.login;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class LoginSingle extends BaseClass implements ProjectInterface{
	public static String testResult;
	public static Logger log = Logger.getLogger("devpinoyLogger");

	public static void login() throws Exception
	{
	
		loginCasesVerification();
		//VerifyLogin();
		//loginData();
		
	}
	
	public static void loginData() throws Exception{
	  launchbrowser();
      ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
	  String fis = "Credentials";
	  String userName=read.getCellData(fis, "UserName", 1);
	  String password=read.getCellData(fis, "Password", 1);
	  waitForElements(By.xpath(UserName_Xpath));
	  enterText(By.xpath(UserName_Xpath),userName);
	  waitForElements(By.xpath(Password_Id));
	  driver.findElement(By.id(Password_Id)).sendKeys(password);
	  waitForElements(By.xpath(SignIn_Button_Xpath));
	  clickElement(By.xpath(SignIn_Button_Xpath));
	  
	  
	}
	
	public static void loginCasesVerification() throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		
         String fis = "Credentials";
		
		//Launching Browser Using Base Class Method
		launchbrowser();
		
		try{
		//Entering Username on Login Page by Fecthing Test Data from Excel
		String userName=read.getCellData(fis, "UserName", 1);
		if(userName.contains("@"))
		{
		fluentwait(By.xpath(UserName_Xpath), 60, "Unable to Enter Value in Username text field because your execution preferred environment is down.");
		Thread.sleep(1000);
		enterText(By.xpath(UserName_Xpath),userName);
		System.out.println("User Name Entered: " + userName);
		logger.log(LogStatus.PASS, "User Name Entered: " + userName);
		}
		else if(!userName.contains("@"))
		{
			fluentwait(By.xpath(UserName_Xpath), 60, "Unable to Enter Value in Username text field because your execution preferred environment is down.");
			/*enterText(By.xpath(UserName_Xpath),UserName);*/
			driver.findElement(By.xpath(UserName_Xpath)).sendKeys(userName);
			System.out.println("Please include an '@' in the email address. "+userName+" is missing an '@'.");
			logger.log(LogStatus.PASS,"Please include an '@' in the email address. "+userName+" is missing an '@'." );
		}
		
		
		//Entering Password on Loigin Page by Fetching Test Data from Excel
		String password=read.getCellData(fis, "Password", 1);
		fluentwait(By.id(Password_Id), 60, "Unable to Enter Value in Password text field because your execution preffred environment is down.");
		Thread.sleep(2000);
		driver.findElement(By.id(Password_Id)).sendKeys(password);
		//enterText(By.id(Password_Id), Password);
		System.out.println("Password Entered: " + password);
		logger.log(LogStatus.PASS, "Password Entered: " + password);
		if(driver.findElement(By.xpath(ValidCredentials_Error_Xpath)).isDisplayed())
		{
			
			String ValidCredentialsError = driver.findElement(By.xpath(ValidCredentials_Error_Xpath)).getText();
			System.out.println(ValidCredentialsError);
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Login - Valid Credentials")));
			logger.log(LogStatus.FAIL, "Getting Error : "+ValidCredentialsError);
			Assert.fail(ValidCredentialsError);
			
		}
		else{
			System.out.println("Credentials Filled Properly.");
		}
		waitForElementsToBeClickable(By.xpath(SignIn_Button_Xpath));
		clickElement(By.xpath(SignIn_Button_Xpath));
		/*//Clicking on Login Button from Login Page
		try{
		if(userName.contains("@"))
		{
		clickElement(By.xpath(SignIn_Button_Xpath));
		}else if(!userName.contains("@"))
		{
			clickElement(By.xpath(SignIn_Button_Xpath));
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Login - Submit Button.")));
		}
		}catch(Exception e)
		{
			System.out.println(e);
		}*/
		}catch(Exception  e)
		{
			System.out.println(e);
			logger.log(LogStatus.FAIL, e);
			testResult="Fail";
			Assert.fail("There is Error in Login Page");
		}
			
	}
	
	public static void loginCasesVerification2() throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		
         String fis = "Credentials";
		
		//Launching Browser Using Base Class Method
		launchbrowser();
		
		String userName=read.getCellData(fis, "UserName", 1);
		if(userName.contains(" "))
		{
		 System.out.println("User Name is Not aviablein Test data");
		}
		else{
			
			fluentwait(By.xpath(UserName_Xpath), 60, "Unable to Enter Value in Username text field because your execution preferred environment is down.");
			driver.findElement(By.xpath(UserName_Xpath)).sendKeys(userName);
		}
		
		
		//Entering Password on Loigin Page by Fetching Test Data from Excel
		String password=read.getCellData(fis, "Password", 1);
		if(password.contains("")){
			 System.out.println("User Name is Not aviablein Test data");
		}
		else{
			Thread.sleep(2000);
			driver.findElement(By.id(Password_Id)).sendKeys(password);
		}
		
		if(driver.findElement(By.xpath(ValidCredentials_Error_Xpath)).isDisplayed())
		{
			String ValidCredentialsError = driver.findElement(By.xpath(ValidCredentials_Error_Xpath)).getText();
			System.out.println(ValidCredentialsError);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Login - Valid Credentials")));
			logger.log(LogStatus.PASS, "Getting Error : "+ValidCredentialsError);
		}
		else{
			System.out.println("Credentials Filled Properly.");
		}
		
		clickElement(By.xpath(SignIn_Button_Xpath));	
	}
	
	public static void verifyLogin() throws Exception
	{
		try{
			fluentwait(By.xpath(Dashboard_Text_Xpath), 30, "Page is Loading Slow so unable to identify Dashboard.");
			driver.findElement(By.xpath(Dashboard_Text_Xpath));
			System.out.println("Login Successfully in Application.");
			logger.log(LogStatus.PASS, "Logged in Sucessfully in Agency Application.");
			testResult="Pass";
		}
	catch(Exception e)
		{	
			ExplicitWait(By.xpath(InvalidCredentials_Xpath), 5);
			clickElement(By.xpath(InvalidCredentials_Xpath));
			String error_Message = driver.findElement(By.xpath(InvalidCredentials_Xpath)).getText();
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Login - Authentication")));
			clickElement(By.xpath(Popup_OK_Xpath));
			System.out.println("Test Case is Marked as Pass because getting Error : "+error_Message);
			logger.log(LogStatus.PASS, "Test Case is Marked as Pass because Getting Error : "+error_Message);
			testResult="Pass";
	}
	
	
}
	
}
