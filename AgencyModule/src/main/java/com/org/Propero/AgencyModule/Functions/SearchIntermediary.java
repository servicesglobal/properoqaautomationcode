package com.org.propero.agencymodule.functions;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.asserts.Assertion;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;


public class SearchIntermediary extends BaseClass implements ProjectInterface
{
	private static String testResult;
	private static Logger logger1 = LoggerFactory.getLogger(SearchIntermediary.class);
	
	public static String searchAgent(String intermediaryCode) throws Exception
	{
		try{
			
			Thread.sleep(5000);
			
			//Click on Channel Management
			fluentwait(By.xpath(ChannelManagement_Xpath), 60, "Page is loading Slow so couldn't click on Channel Management.");
			clickElement(By.xpath(ChannelManagement_Xpath));
			
			//Click on Search Intermediary
			clickElement(By.xpath(Search_Intermediary_Xpath));
			//clickElement(By.xpath("//nav[@id='main_nav']//ul//a[text()='Channel Management ']//following-sibling::ul//li//a[text()='Search Intermediar']"));
			
			//Search Agent Using Search Criteria
			waitForElements(By.xpath(Intermediary_Code_Xpath));
			enterText(By.xpath(Intermediary_Code_Xpath),intermediaryCode);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
			//Clicking on Search Button
			Thread.sleep(2000);
			clickElement(By.xpath(SearchBtn_Xpath));
			Thread.sleep(2000);
			//Opening Agent from Search
			fluentwait(By.xpath("//a[contains(text(),"+"'"+intermediaryCode.trim()+"'"+")]"), 60, "Couldn't find Agent in Search");
			clickElement(By.xpath("//a[contains(text(),"+"'"+intermediaryCode.trim()+"'"+")]"));
			
			testResult="Pass";
			
		}catch(Exception e)
		{
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			testResult="Fail";
			logger1.info("Error in Search Details Section");
			throw e;
			
		}
		return testResult;
		
		
	}
	
	public static void f(String intermediaryCode){
		try{
			searchAgent( intermediaryCode);
		}
		catch(Exception e){
			Assert.fail();
		}
	}
	
	//*************************************************************************
	
	public static String searchAgent1(String intermediaryCode)
	{
		
		try{
			
			
			//Click on Channel Management
			fluentwait(By.xpath(ChannelManagement_Xpath), 60, "Page is loading Slow so couldn't click on Channel Management.");
			clickElement(By.xpath(ChannelManagement_Xpath));
			
			//Click on Search Intermediary
			clickElement(By.xpath(Search_Intermediary_Xpath));
			
			//Search Agent Using Search Criteria
			waitForElements(By.xpath(Intermediary_Code_Xpath));
			enterText(By.xpath(Intermediary_Code_Xpath),intermediaryCode);
			
			//Clicking on Search Button
			Thread.sleep(2000);
			clickElement(By.xpath(SearchBtn_Xpath));
			Thread.sleep(2000);
			//Opening Agent from Search
			fluentwait(By.xpath("//a[contains(text(),"+"'"+intermediaryCode.trim()+"'"+")]"), 60, "Couldn't find Agent in Search");
			clickElement(By.xpath("//a[contains(text(),"+"'"+intermediaryCode.trim()+"'"+")]"));
			
			testResult="Pass";
			
		}catch(Exception e)
		{
			testResult="Fail";
			System.out.println("****Error in Search Details Section****");
		}
		return testResult;
		
		
	}
	
}
