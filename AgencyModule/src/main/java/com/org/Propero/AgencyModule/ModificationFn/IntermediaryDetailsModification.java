package com.org.propero.agencymodule.modificationFn;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.utility.BaseClass;

import junit.framework.Assert;

public class IntermediaryDetailsModification extends BaseClass implements ModificationInterface,MessageInterface
{
	
	 static String agentCategory=null;
	 static String subAgentCategory=null;
	 static String parentAgentCode=null;
	 static String parentrmcode=null;
	 static String vertical=null;
	 static String sVertical=null;
	 static String location=null;
	 static String receivedDate=null;
	 static String urnno=null;
	 static String urngen=null;
	 static String modRemarks=null;
	 static String modReason=null;
	 static String classfi=null;
	 static String rejection=null;
	 static String secVertical=null;
	 static String symblogin=null;
	 static String cheqDD=null;
	private static Logger logger1 = LoggerFactory.getLogger(IntermediaryDetailsModification.class);
	
	
	public void setIntermediaryCategory(String intermediaryCategory) 
	{
		agentCategory=intermediaryCategory;
	}
	
	public String getIntermediaryCategory() 
	{
	return agentCategory;
	}
	
	public void setInterSubCategory(String IntermediarySubCategory) 
	{
		subAgentCategory=IntermediarySubCategory;
	}
	
	public String getIntermediarySubCategory() 
	{
	return subAgentCategory;
	}
	
	public void setParentIntermediary(String parentIntermediary) 
	{
		parentAgentCode=parentIntermediary;
	}
	
	public String getParentIntermediary() 
	{
	return parentAgentCode;
	}
	
	public void setRmCode(String RmCode) 
	{
		parentrmcode=RmCode;
	}
	
	public String getRmCode() 
	{
	return parentrmcode;
	}
	
	public void setSourcingVertical(String sourcingVertical) 
	{
		vertical=sourcingVertical;
	}
	
	public String getSourcingVertical() 
	{
	return vertical;
	}
	
	public void setSourcingSubVertical(String SourcingSubVertical) 
	{
		sVertical=SourcingSubVertical;
	}
	
	public String getSourcingSubVertical() 
	{
	return sVertical;
	}
	
	public void setSourcingLocation(String SourcingLocation) 
	{
		location=SourcingLocation;
	}
	
	public String getSourcingLocation() 
	{
	return location;
	}
	
	public void setArfReceivedDate(String ArfReceivedDate) 
	{
		receivedDate=ArfReceivedDate;
	}
	
	public String getArfReceivedDate() 
	{
	return receivedDate;
	}
	
	public void setUrnNumber(String urnNumber) 
	{
		urnno=urnNumber;
	}
	
	public String getUrnNumber() 
	{
	return urnno;
	}
	
	public void setUrnGenerated(String UrnGenerated) 
	{
		urngen=UrnGenerated;
	}
	
	public String getUrnGenerated() 
	{
	return urngen;
	}
	
	public void setModificationRemarks(String ModificationRemarks) 
	{
		modRemarks=ModificationRemarks;
	}
	
	public String getModificationRemarks() 
	{
	return modRemarks;
	}
	
	public void setModificationReason(String modificationReason) 
	{
		modReason=modificationReason;
	}
	
	public String getModificationReason() 
	{
	return modReason;
	}
	
	public void setClassification(String ClassificationType) 
	{
		classfi=ClassificationType;
	}
	
	public String getClassification() 
	{
	return classfi;
	}
	
	public void setRejectionReason(String RejectionReason) 
	{
		rejection=RejectionReason;
	}
	
	public String getRejectionReason() 
	{
	return rejection;
	}
	
	
	public void setSymbLogin(String symbioLogin) 
	{
		symblogin=symbioLogin;
	}
	
	public String getSymbLogin() 
	{
	return symblogin;
	}
	

	public void setChequeDD(String chequeDD)
	{
		cheqDD=chequeDD;
	}
	
	public String getChequeDD() 
	{
	return cheqDD;
	}
	
	public void setSecondaryVertical(String SecondaryVertical) 
	{
		secVertical=SecondaryVertical;
	}
	
	public String getSecondaryVertical() 
	{
	return secVertical;
	}
	
	
	public void internediaryDetailstab(String workbookname,int rowNum,String agentid,String agentCategory,String subAgentCategory,String parentAgentCode,String parentrmcode,String vertical,String sVertical,String location,String secVertical,
	String receivedDate,String symblogin,String cheqDD,String classfi) throws Exception
	{
		
		//Selecting Agent Category
		if(agentCategory.equals(""))
		{
			logger1.info(INTERMEDIARYCATEGORYBLANK);
		}else
		{	
			Thread.sleep(2000);
			fluentwait(By.xpath(ModIntermediaryCategory_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			clickElement(By.xpath(ModIntermediaryCategory_Xpath));
			Thread.sleep(2000);
			fluentwait(By.xpath(ModIntermediaryCategoryOption_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");	
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModIntermediaryCategoryOption_Xpath), agentCategory);
			logger1.info("Selected Intermediary Category : "+agentCategory);
		}
		
		//Selecting Agent Sub Category
		if(subAgentCategory.equals(""))
		{
			logger1.info(INTERMEDIARYSUBCATEGORYBLANK);
		}else
		{
			
			if(subAgentCategory instanceof String)
			{
			Thread.sleep(2000);
			fluentwait(By.xpath(ModIntermediary_SubCategory_Xpath), 60, "Page is Loading Slow so Couldn't find Intermediary Sub Category.");
			clickElement(By.xpath(ModIntermediary_SubCategory_Xpath));
			Thread.sleep(2000);
			fluentwait(By.xpath(ModIntermediary_SubCatOption_Xpath), 60, "Page is Loading Slow so Couldn't find Intermediary Sub Category.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModIntermediary_SubCatOption_Xpath), subAgentCategory);
			logger1.info("Selected Sub Intermediary Category : "+subAgentCategory);
			}else
			{
				logger1.info(VALIDSTRINGINPUT);
			}
		}
		
		//Entering ParentIntermediary Code on UI 
		if(parentAgentCode.equals(""))
		{
			logger1.info(PARENTAGENTCODEBLANK);
		}
		else{
			Thread.sleep(2000);
			boolean isInteger = isInteger(parentAgentCode);
			if(parentAgentCode.length()==8 && isInteger)
			{
			clearTextfield(By.xpath("//*[@id='intermediary-details-"+agentid.trim()+"']/div[1]/div[3]/div/div/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='intermediary-details-"+agentid.trim()+"']/div[1]/div[3]/div/div/input"), parentAgentCode);
			logger1.info("Entered Parent Intermediary Code : "+parentAgentCode);
			driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
			}
			else
			{
			clearTextfield(By.xpath("//*[@id='intermediary-details-"+agentid.trim()+"']/div[1]/div[3]/div/div/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='intermediary-details-"+agentid.trim()+"']/div[1]/div[3]/div/div/input"), parentAgentCode);
			logger1.info("Entered Parent Intermediary Code : "+parentAgentCode);
			//Verifying Error Message
			ErrorMessages.fieldverification("Parent Intermediary Code");
			driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
			}
		
		}
		
		//Entering RmCode on UI Using below 
		if(parentrmcode.equals(""))
		{
			logger1.info(RMCODEBLANK);
		}
		else
		{
			Thread.sleep(2000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='intermediary-details-"+agentid.trim()+"']/div[1]/div[5]/div/div/input")));
			clearTextfield(By.xpath("//*[@id='intermediary-details-"+agentid.trim()+"']/div[1]/div[5]/div/div/input"));
			Thread.sleep(2000);
			WebElement rmCodeelement = driver.findElement(By.xpath("//*[@id='intermediary-details-"+agentid.trim()+"']/div[1]/div[5]/div/div/input"));
			rmCodeelement.sendKeys(parentrmcode);
			logger1.info("Entered Rm Code is : "+parentrmcode);
			//Field Verification for RM Code
			ErrorMessages.fieldverification("RmCode");
	}
		
		
		//Selecting Sourcing Vertical from Here
		if(vertical.equals(""))
		{
			logger1.info(VERTICALBLANK);
		}else
		{
			
			clickElement(By.xpath(ModSourcingVertical_Xpath));
			Thread.sleep(2000);
			fluentwait(By.xpath(ModSourcingVerticalOption_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModSourcingVerticalOption_Xpath), vertical);
			logger1.info("Selected Sourcing Vertical is : "+vertical);
		}
		
		
		//Selecting Sub Vertical from Here
		if(sVertical.equals(""))
		{
			logger1.info(SUBVERTICALBLANK);
		}else{
			ExplicitWait(By.xpath(ModSub_Vertical_Xpath), 20);
			clickElement(By.xpath(ModSub_Vertical_Xpath));
			fluentwait(By.xpath(ModSubVerticalOption_Xpath), 60, "Unable to Select Sourcing Sub Vertical.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModSubVerticalOption_Xpath), sVertical);
			logger1.info("Selected Sourcing Sub Vertical : "+sVertical);
		}
		
		//Selecting Sourcing Location Using Below
		if(location.equals(""))
		{
			logger1.info(SOURCINGLOCATIONBLANK);
		}else{
			ExplicitWait(By.xpath(ModSourcing_Location_Xpath), 20);
			clickElement(By.xpath(ModSourcing_Location_Xpath));
			fluentwait(By.xpath(ModSourcingLocationOption_Xpath), 60, "Unable to Select Sourcing Sub Vertical.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModSourcingLocationOption_Xpath), location);
			logger1.info("Selected Sourcing Location is : "+location);
		}
		
		//Selecting Secondary Vertical Using Below 
		if(secVertical.equals(""))
		{
			logger1.info(SECONDARYVERTICALBLANK);
		}else{
			clickElement(By.xpath(ModSecondary_Vertical_Xpath));
			fluentwait(By.xpath(ModSecondaryVerticalOption_Xpath), 60, "Unable to Select Sourcing Sub Vertical.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModSecondaryVerticalOption_Xpath), secVertical);
			logger1.info("Entered Secondary Vertical is : "+secVertical);
		}
		
		//Selecting Arf Received Date
		if(receivedDate.equals(""))
		{
			logger1.info(RECEIVEDDATEBLANK);
		}else{
			//clearTextfield(By.xpath("//input[@id='arfDate-"+Agentid.trim()+"']"));
			Thread.sleep(2000);
			Calendar.calender(workbookname, By.xpath("//input[@id='arfDate-"+agentid.trim()+"']"),"ARFRcvd_Date", rowNum);
		}
		
		
		//Checking Symbiosys Login
		if(symblogin.equals(""))
		{
		logger1.info(SYMBIOSYSLOGINBLANK);	
		}else{
		if(symblogin.equalsIgnoreCase("Yes"))
			{
				logger1.info("Symbiosys Login Activated.");
			}
		else if(symblogin.equalsIgnoreCase("No"))
			{
				clickElement(By.xpath("//input[@name='createSymLogin-"+agentid.trim()+"']"));
				logger1.info("Sysbiosys Login is Deactivated.");
			}
		}
		

		
		//Checking Cheque and DD Option from below String CheqDD
		if(cheqDD.equals(""))
		{
			logger1.info(CHEQUEDDBLANK);
		}
		else{
			if(cheqDD.equalsIgnoreCase("Yes"))
			{
				clickElement(By.xpath("//input[@name='chequeAndDD-"+agentid.trim()+"']"));
				logger1.info("Cheque and DD option is Selected as : "+cheqDD);
			}else if(cheqDD.equalsIgnoreCase("No"))
			{
				logger1.info("Sysbiosys Login is Selected as : "+cheqDD);
			}
		}
		
		//Selecting Classification using below 
		if(classfi.equals(""))
		{
			logger1.info(CLASSIFIACTIONBLANK);
		}else
		{
			clickElement(By.xpath(Classifiaction_Xpath));
			fluentwait(By.xpath(ClassificationOption_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ClassificationOption_Xpath), classfi);
			logger1.info("Selected Classification is : "+classfi);
		}
		
		
		
	}
	

	
	
	public static boolean isInteger(String s) 
	{
	      boolean isValidInteger = false;
	      try
	      {
	         Integer.parseInt(s);
	         // s is a valid integer	 
	         isValidInteger = true;
	      }
	      catch (NumberFormatException ex)
	      {
	        logger1.info(ex.getMessage());
	      }
	 
	      return isValidInteger;
	   }
	
}
