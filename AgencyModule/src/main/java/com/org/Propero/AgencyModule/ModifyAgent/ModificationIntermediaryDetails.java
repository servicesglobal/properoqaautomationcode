package com.org.propero.agencymodule.modifyAgent;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.modificationFn.IntermediaryDetailsModification;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ModificationIntermediaryDetails extends BaseClass
{
	private static String testResult=null;
	private static Logger logger1 = LoggerFactory.getLogger(ModificationIntermediaryDetails.class);
	
	public static String agentsDetails(String sheetName,String workbookname,int rowNum,String agentId) throws Exception
	{
		
		try
		{
		IntermediaryDetailsModification obj = new IntermediaryDetailsModification();
		ReadExcel read = new ReadExcel(sheetName);
		
		
		String intermediaryCategory = read.getCellData(workbookname,"Intermediary_Category",rowNum);
		
		String intermediarySubCategory = read.getCellData(workbookname,"Sub_Category",rowNum);
		
		String parentIntermediary = read.getCellData(workbookname,"Parent_Intermediary_Code",rowNum);
		
		String rmCode = read.getCellData(workbookname,"RM_Code",rowNum);
		
		String sourcingVertical = read.getCellData(workbookname,"Sourcing_Veritical",rowNum);
		
		String sourcingSubVertical = read.getCellData(workbookname,"Sub_Vertical",rowNum);
		
		String sourcingLocation = read.getCellData(workbookname,"Sourcing_Location",rowNum);
		
		String secondaryVertical = read.getCellData(workbookname,"Secondary_Vertical",rowNum);
		
		String arfReceivedDate = read.getCellData(workbookname,"ARFRcvd_Date",rowNum);
		
		String symbioLogin = read.getCellData(workbookname,"Symbosis_Login",rowNum);
		
		String chequeDD = read.getCellData(workbookname,"Cheque_DD",rowNum);
		
		String ClassificationType = read.getCellData(workbookname, "Classification", rowNum);
		
		obj.setIntermediaryCategory(intermediaryCategory);
		obj.setInterSubCategory(intermediarySubCategory);
		obj.setParentIntermediary(parentIntermediary);
		obj.setRmCode(rmCode);
		obj.setSourcingVertical(sourcingVertical);
		obj.setSourcingSubVertical(sourcingSubVertical);
		obj.setSourcingLocation(sourcingLocation);
		obj.setSecondaryVertical(secondaryVertical);
		obj.setArfReceivedDate(arfReceivedDate);
		obj.setSymbLogin(symbioLogin);
		obj.setChequeDD(chequeDD);
		obj.setClassification(ClassificationType);
		
		
		obj.internediaryDetailstab(workbookname, rowNum, agentId,obj.getIntermediaryCategory(), obj.getIntermediarySubCategory(), obj.getParentIntermediary(), 
				obj.getRmCode(), obj.getSourcingVertical(), obj.getSourcingSubVertical(), obj.getSourcingLocation(), obj.getSecondaryVertical(), 
									obj.getArfReceivedDate(), obj.getSymbLogin(), obj.getChequeDD(), obj.getClassification());
		
		testResult="Pass";
		
		obj=null;
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		}catch(Exception  e)
		{
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			testResult="Fail";
			//Assert.fail("****Error in Agent Details Section****");
			
		}
		
		return testResult;
		
	}

}
