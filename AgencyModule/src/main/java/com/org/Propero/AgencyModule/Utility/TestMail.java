package com.org.propero.agencymodule.utility;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestMail extends BaseClass {

	public static Logger logger1 = LoggerFactory.getLogger(TestMail.class);

	public static void mailsent() throws AddressException, MessagingException, InterruptedException {
		try {
			
		SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
		mail.postMail(TestConfig.server, TestConfig.from, TestConfig.to, TestConfig.subject, TestConfig.messageBody, TestConfig.attachmentPath, TestConfig.attachmentName, TestConfig.attachmentPath1, TestConfig.attachmentName1);
	
		
		} catch (Exception e) 
		{
			//e.printStackTrace();
			logger1.error(e.getMessage());
		}
	
	}

}
