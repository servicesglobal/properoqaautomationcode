package com.org.propero.agencymodule.functions;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.basefunctions.ValidExpression;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class PersonalTab extends BaseClass implements ProjectInterface, MessageInterface {

	private static Pattern dateFrmtPtrn = Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");

	private static Logger logger1 = LoggerFactory.getLogger(PersonalTab.class);
	
	public void setCustomerType(String newCustomerType) {
		Variable.CustomerType = newCustomerType;
	}

	public String getCustomerType() {
		return Variable.CustomerType;
	}

	public void setTitle(String newTitle) {
		Variable.Title = newTitle;
	}

	public String getnewTitle() {
		return Variable.Title;
	}

	public void setFirstName(String newFirstName) {
		Variable.FirstName = newFirstName;
	}

	public String getFirstName() {
		return Variable.FirstName;
	}

	public void setLastName(String newLastName) {
		Variable.LastName = newLastName;
	}

	public String getLastName() {
		return Variable.LastName;
	}

	public void setDateofBirth(String newDateofBirth) {
		Variable.DateofBirth = newDateofBirth;
	}

	public String getDateofBirth() {
		return Variable.DateofBirth;
	}

	public void setPincode(String newPincode) {
		Variable.Pincode = newPincode;
	}

	public String getPincode() {
		return Variable.Pincode;
	}

	public void setPreferredadress(String newPreferredadress) {
		Variable.Preferredadress = newPreferredadress;
	}

	public String getPreferredadress() {
		return Variable.Preferredadress;
	}

	public void setFathersName(String newFathersName) {
		Variable.FathersName = newFathersName;
	}

	public String getFathersName() {
		return Variable.FathersName;
	}

	public void setOfficeAddressline1(String newOfficeAddressline1) {
		Variable.OfficeAddressline1 = newOfficeAddressline1;
	}

	public String getOfficeAddressline1() {
		return Variable.OfficeAddressline1;
	}

	public void setOfficeAddressline2(String newOfficeAddressline2){
		Variable.OfficeAddressline2 = newOfficeAddressline2;
	}

	public String getOfficeAddressline2() {
		return Variable.OfficeAddressline2;
	}

	public void setOfficePincode(String newOfficePincode)  {
		Variable.OfficePincode = newOfficePincode;
	}

	public String getOfficePincode() {
		return Variable.OfficePincode;
	}

	public void setOfficeCity(String newOfficeCity)  {
		Variable.OfficeCity = newOfficeCity;
	}

	public String getOfficeCity() {
		return Variable.OfficeCity;
	}

	public void setofficeState(String newofficeState)  {
		Variable.officeState = newofficeState;
	}

	public String getofficeState() {
		return Variable.officeState;
	}

	public void setOfficeLocality(String newOfficeLocality)  {
		Variable.OfficeLocality = newOfficeLocality;
	}

	public String getOfficeLocality() {
		return Variable.OfficeLocality;
	}

	public void setResidentialAddress(String newResidentialAddress)  {
		Variable.ResidentialAddress = newResidentialAddress;
	}

	public String getResidentialAddress() {
		return Variable.ResidentialAddress;
	}

	public void setResidentialAddressline1(String newResidentialAddressline1) {
		Variable.ResidentialAddressline1 = newResidentialAddressline1;
	}

	public String getResidentialAddressline1() {
		return Variable.ResidentialAddressline1;
	}

	public void setResidentialAddressLine2(String newResidentialAddressLine2)  {
		Variable.ResidentialAddressLine2 = newResidentialAddressLine2;
	}

	public String getResidentialAddressLine2() {
		return Variable.ResidentialAddressLine2;
	}

	public void setResidentialPincode(String newResidentialPincode) {
		Variable.ResidentialPincode = newResidentialPincode;
	}

	public String getResidentialPincode() {
		return Variable.ResidentialPincode;
	}

	public void setResidentialCity(String newResidentialCity) {
		Variable.ResidentialCity = newResidentialCity;
	}

	public String getResidentialCity() {
		return Variable.ResidentialCity;
	}

	public void setResidentialState(String newResidentialState)  {
		Variable.ResidentialState = newResidentialState;
	}

	public String getResidentialState() {
		return Variable.ResidentialState;
	}

	public void setResidentialLocality(String newResidentialLocality)  {
		Variable.ResidentialLocality = newResidentialLocality;
	}

	public String getResidentialLocality() {
		return Variable.ResidentialLocality;
	}

	public void setEmailid(String newEmailid)  {
		Variable.Emailid = newEmailid;
	}

	public String getEmailid() {
		return Variable.Emailid;
	}

	public void setMobileNumber(String newMobileNumber) {
		Variable.MobileNumber = newMobileNumber;
	}

	public String getMobileNumber() {
		return Variable.MobileNumber;
	}

	public void setStd(String newStd){
		Variable.Std = newStd;
	}

	public String getStd() {
		return Variable.Std;
	}

	public void setContactNumber(String newContactNumber)  {
		Variable.ContactNumber = newContactNumber;
	}

	public String getContactNumber() {
		return Variable.ContactNumber;
	}

	public void setSecondaryEmailid(String newSecondaryEmailid)  {
		Variable.SecondaryEmailid = newSecondaryEmailid;
	}

	public String getSecondaryEmailid() {
		return Variable.SecondaryEmailid;
	}

	public void setSecondaryMobileNumber(String newSecondaryMobileNumber)  {
		Variable.SecondaryMobileNumber = newSecondaryMobileNumber;
	}

	public String getSecondaryMobileNumber() {
		return Variable.SecondaryMobileNumber;
	}

	public void setGstRegistrationStatus(String newGstRegistrationStatus) {
		Variable.GstRegistrationStatus = newGstRegistrationStatus;
	}

	public String getGstRegistrationStatus() {
		return Variable.GstRegistrationStatus;
	}

	public void setGstRegistrationdate(String newGstRegistrationdate)  {
		Variable.GstRegistrationdate = newGstRegistrationdate;
	}

	public String getGstRegistrationdate() {
		return Variable.GstRegistrationdate;
	}

	public void setGstNumber(String newGstNumber) {
		Variable.GstNumber = newGstNumber;
	}

	public String getGstNumber() {
		return Variable.GstNumber;
	}

	public void setConstitutionBusiness(String newConstitutionBusiness)  {
		Variable.ConstitutionBusiness = newConstitutionBusiness;
	}

	public String getConstitutionBusiness() {
		return Variable.ConstitutionBusiness;
	}

	public void setCustomerGstType(String newCustomerGstType)  {
		Variable.CustomerGstType = newCustomerGstType;
	}

	public String getCustomerGstType() {
		return Variable.CustomerGstType;
	}

	public void setCustomerClassifiaction(String newCustomerClassifiaction)  {
		Variable.CustomerClassifiaction = newCustomerClassifiaction;
	}

	public String getCustomerClassifiaction() {
		return Variable.CustomerClassifiaction;
	}

	public void setCorporatename(String newCorporatename) {
		Variable.Corporatename = newCorporatename;
	}

	public String getCorporatename() {
		return Variable.Corporatename;
	}

	public void setContactPerson(String newContactPerson)  {
		Variable.ContactPerson = newContactPerson;
	}

	public String getContactPerson() {
		return Variable.ContactPerson;
	}

	public void setDOI(String newDOI) {
		Variable.DOI = newDOI;
	}

	public String getDOI() {
		return Variable.DOI;
	}

	public void intermediarydetails(String workbookName, int rowNum, String customerType, String title,
			String firstName, String lastName, String dateofBirth, String pincode, String preferredadress,
			String fathersName, String corporatename, String contactPerson, String officeAddressline1,
			String officeAddressline2, String officePincode, String officeCity, String officeLocality,
			String residentialAddress, String residentialAddressline1, String residentialAddressLine2,
			String residentialPincode, String residentialCity, String residentialLocality,String emailid, String mobileNumber, String std, String contactNumber,
			String secondaryEmailid, String secondaryMobileNumber, String gstRegistrationStatus,
			String gstRegistrationdate, String gstNumber, String constitutionBusiness, String customerGstType,
			String customerClassifiaction) throws Exception{

		//Moving to Personal Details after Filling data in Intermediary Details
		clickElement(By.xpath(Personal_Detailstab_Xpath));
	
		
		
		// Selecting Customer Type using below
		if (customerType.equals("")) 
		{
			logger1.info(CUSTOMERTYPEBLANK);
		} 
		else {
			Thread.sleep(2000);
			clickElement(By.id(Customer_Type_Id));
			Thread.sleep(5000);
			fluentwait(By.xpath(CustomerTypeoption_Xpath), 10,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CustomerTypeoption_Xpath), customerType);
			logger1.info("Selected Intermediary Category : " + customerType);
		}

		if (customerType.equalsIgnoreCase("Individual")) {
			PersonalTab.customerTypeIndividual(workbookName, rowNum, title, firstName, lastName, dateofBirth, pincode,
					preferredadress, fathersName);
			PersonalTab.officeAddressDetails(officeAddressline1, officeAddressline2, officePincode, officeCity,
					officeLocality);
			PersonalTab.residentialAddressDetails(residentialAddress, residentialAddressline1, residentialAddressLine2,
					residentialPincode, residentialCity, residentialLocality);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab")));
			PersonalTab.fillContactDetails(emailid, mobileNumber, std, contactNumber, secondaryEmailid, secondaryMobileNumber);
			PersonalTab.gstRegistration(workbookName, rowNum, gstRegistrationStatus, gstRegistrationdate, gstNumber,
					constitutionBusiness, customerGstType, customerClassifiaction);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab 1")));

		} else if (customerType.equalsIgnoreCase("Corporate")) {
			PersonalTab.customerTypeCorporate(workbookName, rowNum, corporatename, contactPerson, dateofBirth);
			PersonalTab.officeAddressDetails(officeAddressline1, officeAddressline2, officePincode, officeCity,
					officeLocality);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab")));
			PersonalTab.fillContactDetails(emailid, mobileNumber, std, contactNumber, secondaryEmailid, secondaryMobileNumber);
			PersonalTab.gstRegistration(workbookName, rowNum, gstRegistrationStatus, gstRegistrationdate, gstNumber,
					constitutionBusiness, customerGstType, customerClassifiaction);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab 1")));
		}
		
		verticalscrollup();
		verticalscrollup();
		
		

	}

	public static void customerTypeIndividual(String workbookName, int rowNum, String title, String firstName,
			String lastName, String dateofBirth, String pincode, String preferredadress, String fathersName)
			throws Exception {

		// Selecting Title on UI using below
		if (title.equals("")) {
			logger1.info(TITLEBLANK);
		} else {
			clickElement(By.id(Title_Id));
			fluentwait(By.xpath(TitleOption_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(TitleOption_Xpath), title);
			logger1.info("Selected Intermediary Title is : " + title);
		}

		// Selcting Intermediary Firstname is
		if (firstName.equals("")) {
			logger1.info(FIRSTNAMEBLANK);
		} else {
			if (firstName instanceof String && firstName.length() <= 20) {
				enterText(By.id(FirstName_Id), firstName);
				logger1.info("Entered Intermediary First Name is : " + firstName);
				ErrorMessages.fieldverification("First Name");
			} else {
				enterText(By.id(FirstName_Id), firstName);
				logger1.info("Entered Intermediary First Name is : " + firstName);
				// Field Verification for First name
				ErrorMessages.fieldverification("First Name");
			}

		}

		// Entering Last name on UI
		if (lastName.equals("")) {
			logger1.info(LASTNAMEBLANK);
		} else {
			if (lastName instanceof String && lastName.length() <= 30) {
				Thread.sleep(2000);
				enterText(By.id(LastName_Id), lastName);
				logger1.info("Entered Intermediary First Name is : " + lastName);
				ErrorMessages.fieldverification("Last Name");
			} else {
				enterText(By.id(LastName_Id), lastName);
				logger1.info("Entered Intermediary First Name is : " + lastName);
				ErrorMessages.fieldverification("Last Name");
			}

		}

		// Entering date of birth on UI
		if (dateofBirth.equals("")) {
			logger1.info(DATEOFBIRTHBLANK);
		} else {

			Matcher mtch = dateFrmtPtrn.matcher(dateofBirth);
			if (mtch.matches()) {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
			} else {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
				ErrorMessages.fieldverification("Date of Birth");
			}

		}

		// Entering PinCode On UI
		if (pincode.equals("")) {
			logger1.info(PINCODEBLANK);
		} else {
			enterText(By.id(PinCode_Id), pincode);
			if (pincode instanceof String && pincode.length() <= 6) 
			{
				ErrorMessages.fieldverification("Pin Code");
				logger1.info("Entered Pin Code is : " + pincode);
				ExplicitWait(By.xpath("//button[@class='btn btn-search modify-info agent-button']"), 20);
				clickElement(By.xpath("//button[@class='btn btn-search modify-info agent-button']"));
				Thread.sleep(2000);
				ErrorMessages.duplicatepartycheck("Pin Code");
				
			} else {
				ErrorMessages.fieldverification("Pin Code");
			}

		}

		// Selecting Preferred Address
		if (preferredadress.equals("")) {
			logger1.info(PREFERREDADRESSBLANK);
		} else {
			Thread.sleep(2000);
			clickElement(By.xpath(Preferred_Address_Xpath));
			fluentwait(By.xpath(PreferredAddressoption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PreferredAddressoption_Xpath), preferredadress);
			logger1.info("Selected Intermediary Title is : " + preferredadress);
		}

		// Entering Father's Name on UI
		
		if (fathersName.equals("")) {
			logger1.info(FATHERSNAMEBLANK);
		} else {
			waitForElements(By.xpath(Father_Name_Xpath));
			enterText(By.xpath(Father_Name_Xpath), fathersName);
			if (fathersName instanceof String && fathersName.length() <= 20) {
				logger1.info("Entered Intermediary First Name is : " + fathersName);
			} else {
				logger1.info("Entered Intermediary First Name is : " + fathersName);
				ErrorMessages.fieldverification("Fathers Name");
			}
		}

	}

	public static void customerTypeCorporate(String workbookName, int rowNum, String corporatename,
			String contactPerson, String dateofBirth) throws Exception {

		// Enter CorporateName
		if (corporatename.equals("")) {
			logger1.info(CORPORATENAMEBLANK);
		} else {
			enterText(By.id(CorporateName_Id), corporatename);
			logger1.info("Corporate Name is : " + corporatename);
		}

		// Entering Contact Person details
		if (contactPerson.equals("")) {
			logger1.info(CONTACTPERSONBLANK);
		} else {
			enterText(By.id(ContactPerson_Id), contactPerson);
			logger1.info("Contact Person Name is : " + contactPerson);
		}

		// Entering Date of Birth
		if (dateofBirth.equals("")) {
			logger1.info(DATEOFBIRTHBLANK);
		} else {

			Matcher mtch = dateFrmtPtrn.matcher(dateofBirth);
			if (mtch.matches()) {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
			} else {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
				ErrorMessages.fieldverification("Date of Birth");
			}

		}

	}

	public static void officeAddressDetails(String officeAddressline1, String officeAddressline2, String officePincode,
			String officeCity, String officeLocality) throws Exception {

		// Entering Office AddressLine 1
		if (officeAddressline1.equals("")) {
			logger1.info(OFFICEADDRESSLINE1BLANK);
		} else {
			enterText(By.id(AddressLine1_Id), officeAddressline1);
			logger1.info("Entered Office AddressLine 1 is : " + officeAddressline1);
		}

		// Entering Office AddressLine 2
		if (officeAddressline2.equals("")) {
			logger1.info(OFFICEADDRESSLINE2BLANK);
		} else {
			enterText(By.id(AddressLine2_Id), officeAddressline2);
			logger1.info("Entered Office AddressLine2 is : " + officeAddressline2);
		}

		// Entering PinCode from UI

		if (officePincode.equals("")) {
			logger1.info(OFFICEPINCODEBLANK);
		} else {
			if (officePincode instanceof String && officePincode.length() <= 6) {
				logger1.info("Entered Pin Code is : " + officePincode);
				enterText(By.id(OffPinCode_Id), officePincode);
				ErrorMessages.fieldverification("Pin Code");
			} else {
				logger1.info("Entered Pin Code is : " + officePincode);
				ErrorMessages.fieldverification("Pin Code");
			}

			logger1.info("Entered office Address Pin Code is : " + officePincode);
		}

		// Selecting City from listed dropdown values
		if (officeCity.equals("")) {
			logger1.info(OFFICECITYBLANK);
		} else {
			clickElement(By.xpath(City_Selection_Xpath));
			fluentwait(By.xpath(CitySelectionOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CitySelectionOption_Xpath), officeCity);
			logger1.info("Selected Office City is  : " + officeCity);
		}

		// Entering Office Locality on UI
		if (officeLocality.equals("")) {
			logger1.info(OFFICELOCALITYBLANK);
		} else {
			enterText(By.id(Locality_Id), officeLocality);
			logger1.info("Entered Office Address Locality is : " + officeLocality);
		}

	}

	public static void residentialAddressDetails(String residentialAddress, String residentialAddressline1,
			String residentialAddressLine2, String residentialPincode, String residentialCity,
			String residentialLocality) throws Exception {

		// Selection of Residential Address Details
		if (residentialAddress.equals("")) {
			logger1.info(RESIDENTIALADDRESS);
		} else if (residentialAddress.equalsIgnoreCase("Yes")) {
			
			// Clicking on Check Box for Residential Address is Same as Office Address.
			clickElement(By.xpath(ResidentialAddCheckbox_Xpath));

			logger1.info("Residential Address is Same as Office Address.");

			verticalscrolldown();
		} else if (residentialAddress.equalsIgnoreCase("No")) {

			// Entering Residential AddressLine 1
			if (residentialAddressline1.equals("")) {
				logger1.info(RESIDENTIALADDRESSLINE1BLANK);
			} else {
				enterText(By.id(ResAddressLine1_Id), residentialAddressline1);
				logger1.info("Entered Residential AddressLine 1 is : " + residentialAddressline1);
			}

			// Entering Office AddressLine 2
			if (residentialAddressLine2.equals("")) {
				logger1.info(RESIDENTIALADDRESSLINE2BLANK);
			} else {
				enterText(By.id(ResAddressLine2_Id), residentialAddressLine2);
				logger1.info("Entered Residential AddressLine2 is : " + residentialAddressLine2);
			}

			// Entering PinCode from UI
			if (residentialPincode.equals("")) {
				logger1.info(RESIDENTIALPINCODEBLANK);
			} else {
				
				if (residentialPincode instanceof String && residentialPincode.length() <= 6) {
					enterText(By.id(ResPincode_Id), residentialPincode);
					logger1.info("Entered Residential Pin Code is : " + residentialPincode);
					ErrorMessages.fieldverification("Pin Code");
				} else {
					logger1.info("Entered Pin Code is : " + residentialPincode);
					ErrorMessages.fieldverification("Pin Code");
				}

				logger1.info("Entered Residential Address Pin Code is : " + residentialPincode);
			}

			// Selecting City from listed dropdown values
			if (residentialCity.equals("")) {
				logger1.info(RESIDENTIALCITYBLANK);
			} else {
				clickElement(By.xpath(ResCity_Xpath));
				fluentwait(By.xpath(ResCityOption_Xpath), 60,
						"Page is Loading Slow so Couldn't find IntermediaryCategory.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(ResCityOption_Xpath), residentialCity);
				logger1.info("Selected Residential City is  : " + residentialCity);
			}

			// Entering Office Locality on UI
			if (residentialLocality.equals("")) {
				logger1.info(RESIDENTIALLOCALITYBLANK);
			} else {
				enterText(By.id(ResLocality_Xpath), residentialLocality);
				logger1.info("Entered Residential Address Locality is : " + residentialLocality);
			}

		}


		verticalscrolldown();
		
	}

	public static void fillContactDetails(String emailid, String mobileNumber, String std, String contactNumber,
			String secondaryEmailid, String secondaryMobileNumber) throws Exception {

		// Entering Email address
		Thread.sleep(2000);
		if (emailid.equals("")) {
			logger1.info(EMAILIDBLANK);
		} else {
			if (ValidExpression.isValid(emailid)) {
				enterText(By.xpath(Emailid_Xpath), emailid);
				logger1.info("Entered Email Id in Contact Detail is : " + emailid);
				ErrorMessages.agentExistverification();
			} else {
				logger1.info("Please Enter a Valid Email address");
				ErrorMessages.fieldverification("Email Id");
			}
		}

		// Entering Mobile Number
		if (mobileNumber.equals("")) {
			logger1.info(MOBILENUMBERBLANK);
		} else {
			if (mobileNumber.length() > 9 && mobileNumber.length() <= 15) {
				enterText(By.xpath(MobileNum_Xpath), mobileNumber);
				logger1.info("Entered Primary Mobile Number is : " + mobileNumber);
				ErrorMessages.agentExistverification();
			} else {
				enterText(By.xpath(MobileNum_Xpath), mobileNumber);
				logger1.info("Entered Primary Mobile Number is : " + mobileNumber);
				ErrorMessages.fieldverification("Mobile Number");
			}
		}

		// Entering Std Number from here
		if (std.equals("")) {
			logger1.info(STDBLANK);
		} else {
			enterText(By.xpath(STD_Xpath), std);
			if (std instanceof String && std.length() >= 3 && std.length() <= 5) {
				logger1.info("Entered Std Number is : " + std);

			} else {
				logger1.info("Entered Std Number is : " + std);
				// Field Verification for STD Code
				ErrorMessages.fieldverification("STD");
			}

		}

		// Entering Secondary Contact Number
		if (contactNumber.contains("")) {
			logger1.info(CONTACTNUMBERBLANK);
		} else {
			enterText(By.xpath(ContactNo_Xpath), contactNumber);
			if (contactNumber.length() >= 6 && contactNumber.length() <= 8) {
				logger1.info("Entered Landline Number is : " + std + "-" + contactNumber);
			} else {
				// Field Verification for Landline Number
				ErrorMessages.fieldverification("LandLine Number");
			}

		}

		// Entering Secondary Email Id
		if (secondaryEmailid.equals("")) {
			logger1.info(SECONDARYEMAILIDBLANK);
		} else {
			if (ValidExpression.isValid(secondaryEmailid)) {
				enterText(By.xpath(SecondaryEamil_Xpath), secondaryEmailid);
				logger1.info("Entered Email Id in Contact Detail is : " + secondaryEmailid);
				ErrorMessages.fieldverification("Email Id");
			} else {
				logger1.info("Please Enter a Valid Secondary Email address");
				ErrorMessages.fieldverification("Email Id");
				ErrorMessages.agentExistverification();
			}

		}

		// Secondary Mobile Number
		if (secondaryMobileNumber.equals("")) {
			logger1.info(SECONDARYMOBILENUMBERBLANK);
		} else {

			if (secondaryMobileNumber.length() > 9 && secondaryMobileNumber.length() <= 15) {
				enterText(By.xpath(SecondaryMobileNum_Xpath), secondaryMobileNumber);
				logger1.info("Entered Secondary Mobile Number is : " + secondaryMobileNumber);
			} else {
				enterText(By.xpath(SecondaryMobileNum_Xpath), secondaryMobileNumber);
				logger1.info("Secondary Mobile Number is : " + secondaryMobileNumber);
				ErrorMessages.fieldverification("Mobile Number");
			}
		}

	}

	public static void gstRegistration(String workbookName, int rowNum, String gstRegistrationStatus,
			String gstRegistrationdate, String gstNumber, String constitutionBusiness, String customerGstType,
			String customerClassifiaction) throws Exception 
	{

		// Selecting GST Registration
		if (gstRegistrationStatus.equals("")) {
			logger1.info(GSTREGISTRATIONSTATUSBLANK);
		} 
		else {
			waitForElementsToBeClickable(By.xpath(GST_Registration_Xpath));
			clickElement(By.xpath(GST_Registration_Xpath));
			fluentwait(By.xpath(GSTRegistrationoption), 60,"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(GSTRegistrationoption), gstRegistrationStatus);
			
			logger1.info("Selected GstRegistration Status is : " + gstRegistrationStatus);
		} 

		
		//Selecting Other parameters
		if(gstRegistrationStatus.equalsIgnoreCase("No"))
		{
			logger1.info("No Need to Fill Other Details in GST Registration.");
		}
		else if (gstRegistrationStatus.equalsIgnoreCase("Yes")) 
		{
		
			// Gst Registration Date
			if (gstRegistrationdate.equals("")) {
				logger1.info(GSTREGISTRATIONDATEBLANK);
			} else {

				Matcher mtch = dateFrmtPtrn.matcher(gstRegistrationdate);
				if (mtch.matches()) {
					Calendar.calender(workbookName, By.xpath(GST_regdate_Xpath), "DO_Birth", rowNum);
				} else {
					Calendar.calender(workbookName, By.xpath(GST_regdate_Xpath), "DO_Birth", rowNum);
					ErrorMessages.fieldverification("GstRegistrationdate");
				}

			}

		// Gst Registration Number
		if (gstNumber.equals("")) {
			logger1.info(GSTNUMBERBLANK);
		} else {
			enterText(By.xpath(GSTIN_Number_Xpath), gstNumber);
			logger1.info("Entered GST IN/UIN Number is : " + gstNumber);
			ErrorMessages.fieldverification("GST IN/UI number");
		}

		// Selecting ConstitutionBusiness
		if (constitutionBusiness.equals("")) {
			logger1.info(CONSTITUTIONBUSINESSBLANK);
		} else {
			clickElement(By.xpath(ConstitutionofBusiness_Xpath));
			fluentwait(By.xpath(ConstitutionofBusinessoption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ConstitutionofBusinessoption_Xpath), constitutionBusiness);
			logger1.info("Selected GstRegistration Status is : " + constitutionBusiness);
		}

		// Selecting Customer Type
		if (customerGstType.equals("")) {
			logger1.info(CUSTOMERGSTTYPEBLANK);
		} else {
			clickElement(By.xpath(CustomerType_Xpath));
			fluentwait(By.xpath(CustomerTypeOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CustomerTypeOption_Xpath), customerGstType);
			logger1.info("Selected GstRegistration Status is : " + customerGstType);
		}

		// Customer CustomerClassifiaction
		if (customerClassifiaction.equals("")) {
			logger1.info(CUSTOMERCLASSIFIACTIONBLANK);
		} else {
			clickElement(By.xpath(CustomerClassification_Xpath));
			fluentwait(By.xpath(CustomerClassficationoption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CustomerClassficationoption_Xpath),
					customerClassifiaction);
			logger1.info("Selected GstRegistration Status is : " + customerClassifiaction);
		}
	}
		}

	//***************** Negative Functions *****************************************
	
	public void intermediarydetailsNegative(String workbookName, int rowNum, String customerType, String title,
			String firstName, String lastName, String dateofBirth, String pincode, String preferredadress,
			String fathersName, String corporatename, String contactPerson, String officeAddressline1,
			String officeAddressline2, String officePincode, String officeCity, String officeLocality,
			String residentialAddress, String residentialAddressline1, String residentialAddressLine2,
			String residentialPincode, String residentialCity, String residentialLocality,String emailid, String mobileNumber, String std, String contactNumber,
			String secondaryEmailid, String secondaryMobileNumber, String gstRegistrationStatus,
			String gstRegistrationdate, String gstNumber, String constitutionBusiness, String customerGstType,
			String customerClassifiaction) throws Exception 
	{

		//Moving to Personal Details after Filling data in Intermediary Details
		clickElement(By.xpath(Personal_Detailstab_Xpath));
		
		// Selecting Customer Type using below
		if (customerType.equals("")) 
		{
			logger1.info(CUSTOMERTYPEBLANK);
		} 
		else {
			
			Thread.sleep(2000);
			clickElement(By.id(Customer_Type_Id));
			Thread.sleep(5000);
			fluentwait(By.xpath(CustomerTypeoption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CustomerTypeoption_Xpath), customerType);
			logger1.info("Selected Intermediary Category : " + customerType);
		}

		if (customerClassifiaction.equalsIgnoreCase("Individual")) {
			PersonalTab.customerTypeIndividualNegative(workbookName, rowNum, title, firstName, lastName, dateofBirth, pincode,
					preferredadress, fathersName);
			PersonalTab.officeAddressDetailsNegative(officeAddressline1,officeAddressline2, officePincode,officeCity,
					officeLocality);
			PersonalTab.residentialAddressDetailsNegative(residentialAddress, residentialAddressline1, residentialAddressLine2,
					residentialPincode, residentialCity, residentialLocality);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab")));
			PersonalTab.fillContactDetailsNegative(emailid, mobileNumber, std, contactNumber, secondaryEmailid, secondaryMobileNumber);
			PersonalTab.gstRegistrationNegative(workbookName, rowNum, gstRegistrationStatus, gstRegistrationdate, gstNumber,
					constitutionBusiness, customerGstType, customerClassifiaction);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab 1")));

		} else if (customerType.equalsIgnoreCase("Corporate")) {
			PersonalTab.customerTypeCorporateNegative(workbookName, rowNum, corporatename, contactPerson, dateofBirth);
			PersonalTab.officeAddressDetailsNegative(officeAddressline1, officeAddressline2, officePincode, officeCity,
					officeLocality);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab")));
			PersonalTab.fillContactDetailsNegative(emailid, mobileNumber, std, contactNumber, secondaryEmailid, secondaryMobileNumber);
			PersonalTab.gstRegistration(workbookName, rowNum, gstRegistrationStatus, gstRegistrationdate, gstNumber,
					constitutionBusiness, customerGstType, customerClassifiaction);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Personal Details Tab 1")));
		}
		
		verticalscrollup();
		verticalscrollup();

	}

	public static void customerTypeIndividualNegative(String workbookName, int rowNum, String title, String firstName,
			String lastName, String dateofBirth, String pincode, String preferredadress, String fathersName)
			throws Exception {

		// Selecting Title on UI using below
		if (title.equals("")) {
			logger1.info(TITLEBLANK);
		} else {
			clickElement(By.id(Title_Id));
			fluentwait(By.xpath(TitleOption_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(TitleOption_Xpath), title);
			logger1.info("Selected Intermediary Title is : " + title);
		}

		// Selcting Intermediary Firstname is
		if (firstName.equals("")) {
			logger1.info(FIRSTNAMEBLANK);
		} else {
			if (firstName instanceof String && firstName.length() <= 20) {
				enterText(By.id(FirstName_Id), firstName);
				logger1.info("Entered Intermediary First Name is : " + firstName);
				ErrorMessages.fieldverification("First Name");
			} else {
				enterText(By.id(FirstName_Id), firstName);
				logger1.info("Entered Intermediary First Name is : " + firstName);
				// Field Verification for First name
				ErrorMessages.fieldverification("First Name");
			}

		}

		// Entering Last name on UI
		if (lastName.equals("")) {
			logger1.info(LASTNAMEBLANK);
		} else {
			if (lastName instanceof String && lastName.length() <= 30) {
				Thread.sleep(2000);
				enterText(By.id(LastName_Id), lastName);
				logger1.info("Entered Intermediary First Name is : " + lastName);
				ErrorMessages.fieldverification("Last Name");
			} else {
				enterText(By.id(LastName_Id), lastName);
				logger1.info("Entered Intermediary First Name is : " + lastName);
				ErrorMessages.fieldverification("Last Name");
			}

		}

		// Entering date of birth on UI
		if (dateofBirth.equals("")) {
			logger1.info(DATEOFBIRTHBLANK);
		} else {

			Matcher mtch = dateFrmtPtrn.matcher(dateofBirth);
			if (mtch.matches()) {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
			} else {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
				ErrorMessages.fieldverification("Date of Birth");
			}

		}

		// Entering PinCode On UI
		if (pincode.equals("")) {
			logger1.info(PINCODEBLANK);
		} else {
			enterText(By.id(PinCode_Id), pincode);
			if (pincode instanceof String && pincode.length() <= 6) 
			{
				ErrorMessages.fieldverification("Pin Code");
				logger1.info("Entered Pin Code is : " + pincode);
				ExplicitWait(By.xpath("//button[@class='btn btn-search modify-info agent-button']"), 20);
				clickElement(By.xpath("//button[@class='btn btn-search modify-info agent-button']"));
				Thread.sleep(2000);
				ErrorMessages.duplicatepartycheckNegative("Pin Code");
				
			} else {
				ErrorMessages.fieldverification("Pin Code");
			}

		}

		// Selecting Preferred Address
		if (preferredadress.equals("")) {
			logger1.info(PREFERREDADRESSBLANK);
		} else {
			Thread.sleep(2000);
			clickElement(By.xpath(Preferred_Address_Xpath));
			fluentwait(By.xpath(PreferredAddressoption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PreferredAddressoption_Xpath), preferredadress);
			logger1.info("Selected Intermediary Title is : " + preferredadress);
		}

		// Entering Father's Name on UI
		
		if (fathersName.equals("")) {
			logger1.info(FATHERSNAMEBLANK);
		} else {
			waitForElements(By.xpath(Father_Name_Xpath));
			enterText(By.xpath(Father_Name_Xpath), fathersName);
			if (fathersName instanceof String && fathersName.length() <= 20) {
				logger1.info("Entered Intermediary First Name is : " + fathersName);
			} else {
				logger1.info("Entered Intermediary First Name is : " + fathersName);
				ErrorMessages.fieldverification("Fathers Name");
			}
		}

	}

	public static void customerTypeCorporateNegative(String workbookName, int rowNum, String corporatename,
			String contactPerson, String dateofBirth) throws Exception {

		// Enter CorporateName
		if (corporatename.equals("")) {
			logger1.info(CORPORATENAMEBLANK);
		} else {
			enterText(By.id(CorporateName_Id), corporatename);
			logger1.info("Corporate Name is : " + corporatename);
		}

		// Entering Contact Person details
		if (contactPerson.equals("")) {
			logger1.info(CONTACTPERSONBLANK);
		} else {
			enterText(By.id(ContactPerson_Id), contactPerson);
			logger1.info("Contact Person Name is : " + contactPerson);
		}

		// Entering Date of Birth
		if (dateofBirth.equals("")) {
			logger1.info(DATEOFBIRTHBLANK);
		} else {

			Matcher mtch = dateFrmtPtrn.matcher(dateofBirth);
			if (mtch.matches()) {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
			} else {
				Calendar.calender(workbookName, By.id(DateofBirth_Id), "DO_Birth", rowNum);
				ErrorMessages.fieldverification("Date of Birth");
			}

		}

	}

	public static void officeAddressDetailsNegative(String officeAddressline1, String officeAddressline2, String officePincode,
			String officeCity, String officeLocality) throws Exception {

		// Entering Office AddressLine 1
		if (officeAddressline1.equals("")) {
			logger1.info(OFFICEADDRESSLINE1BLANK);
		} else {
			enterText(By.id(AddressLine1_Id), officeAddressline1);
			logger1.info("Entered Office AddressLine 1 is : " + officeAddressline1);
		}

		// Entering Office AddressLine 2
		if (officeAddressline2.equals("")) {
			logger1.info(OFFICEADDRESSLINE2BLANK);
		} else {
			enterText(By.id(AddressLine2_Id), officeAddressline2);
			logger1.info("Entered Office AddressLine2 is : " + officeAddressline2);
		}

		// Entering PinCode from UI

		if (officePincode.equals("")) {
			logger1.info(OFFICEPINCODEBLANK);
		} else {
			enterText(By.id(OffPinCode_Id), officePincode);
			if (officePincode instanceof String && officePincode.length() <= 6) {
				logger1.info("Entered Pin Code is : " + officePincode);
			} else {
				logger1.info("Entered Pin Code is : " + officePincode);
				ErrorMessages.fieldverification("Pin Code");
			}

			logger1.info("Entered office Address Pin Code is : " + officePincode);
		}

		// Selecting City from listed dropdown values
		if (officeCity.equals("")) {
			logger1.info(OFFICECITYBLANK);
		} else {
			clickElement(By.xpath(City_Selection_Xpath));
			fluentwait(By.xpath(CitySelectionOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CitySelectionOption_Xpath), officeCity);
			logger1.info("Selected Office City is  : " + officeCity);
		}

		// Entering Office Locality on UI
		if (officeLocality.equals("")) {
			logger1.info(OFFICELOCALITYBLANK);
		} else {
			enterText(By.id(Locality_Id), officeLocality);
			logger1.info("Entered Office Address Locality is : " + officeLocality);
		}

	}

	public static void residentialAddressDetailsNegative(String residentialAddress, String residentialAddressline1,
			String residentialAddressLine2, String residentialPincode, String residentialCity,
			String residentialLocality) throws Exception {

		// Selection of Residential Address Details
		if (residentialAddress.equals("")) {
			logger1.info(RESIDENTIALADDRESS);
		} else if (residentialAddress.equalsIgnoreCase("Yes")) {
			
			// Clicking on Check Box for Residential Address is Same as Office Address.
			clickElement(By.xpath(ResidentialAddCheckbox_Xpath));

			logger1.info("Residential Address is Same as Office Address.");

			verticalscrolldown();
		} else if (residentialAddress.equalsIgnoreCase("No")) {

			// Entering Residential AddressLine 1
			if (residentialAddressline1.equals("")) {
				logger1.info(RESIDENTIALADDRESSLINE1BLANK);
			} else {
				enterText(By.id(ResAddressLine1_Id), residentialAddressline1);
				logger1.info("Entered Residential AddressLine 1 is : " + residentialAddressline1);
			}

			// Entering Office AddressLine 2
			if (residentialAddressLine2.equals("")) {
				logger1.info(RESIDENTIALADDRESSLINE2BLANK);
			} else {
				enterText(By.id(ResAddressLine2_Id), residentialAddressLine2);
				logger1.info("Entered Residential AddressLine2 is : " + residentialAddressLine2);
			}

			// Entering PinCode from UI
			if (residentialPincode.equals("")) {
				logger1.info(RESIDENTIALPINCODEBLANK);
			} else {
				enterText(By.id(ResPincode_Id), residentialPincode);
				if (residentialPincode instanceof String && residentialPincode.length() <= 6) {
					logger1.info("Entered Residential Pin Code is : " + residentialPincode);
				} else {
					logger1.info("Entered Pin Code is : " + residentialPincode);
					ErrorMessages.fieldverification("Pin Code");
				}

				logger1.info("Entered Residential Address Pin Code is : " + residentialPincode);
			}

			// Selecting City from listed dropdown values
			if (residentialCity.equals("")) {
				logger1.info(RESIDENTIALCITYBLANK);
			} else {
				clickElement(By.xpath(ResCity_Xpath));
				fluentwait(By.xpath(ResCityOption_Xpath), 60,
						"Page is Loading Slow so Couldn't find IntermediaryCategory.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(ResCityOption_Xpath), residentialCity);
				logger1.info("Selected Residential City is  : " + residentialCity);
			}

			// Entering Office Locality on UI
			if (residentialLocality.equals("")) {
				logger1.info(RESIDENTIALLOCALITYBLANK);
			} else {
				enterText(By.id(ResLocality_Xpath), residentialLocality);
				logger1.info("Entered Residential Address Locality is : " + residentialLocality);
			}

		}


		verticalscrolldown();
		
	}

	public static void fillContactDetailsNegative(String emailid, String mobileNumber, String std, String contactNumber,
			String secondaryEmailid, String secondaryMobileNumber) throws Exception {

		// Entering Email address
		Thread.sleep(2000);
		if (emailid.equals("")) {
			logger1.info(EMAILIDBLANK);
		} else {
			if (ValidExpression.isValid(emailid)) {
				enterText(By.xpath(Emailid_Xpath), emailid);
				logger1.info("Entered Email Id in Contact Detail is : " + emailid);
				ErrorMessages.agentExistverification();
			} else {
				logger1.info("Please Enter a Valid Email address");
				ErrorMessages.fieldverification("Email Id");
			}
		}

		// Entering Mobile Number
		if (mobileNumber.equals("")) {
			logger1.info(MOBILENUMBERBLANK);
		} else {
			if (mobileNumber.length() > 9 && mobileNumber.length() <= 15) {
				enterText(By.xpath(MobileNum_Xpath), mobileNumber);
				logger1.info("Entered Primary Mobile Number is : " + mobileNumber);
				ErrorMessages.agentExistverification();
			} else {
				enterText(By.xpath(MobileNum_Xpath), mobileNumber);
				logger1.info("Entered Primary Mobile Number is : " + mobileNumber);
				ErrorMessages.fieldverification("Mobile Number");
			}
		}

		// Entering Std Number from here
		if (std.equals("")) {
			logger1.info(STDBLANK);
		} else {
			enterText(By.xpath(STD_Xpath), std);
			if (std instanceof String && std.length() >= 3 && std.length() <= 5) {
				logger1.info("Entered Std Number is : " + std);

			} else {
				logger1.info("Entered Std Number is : " + std);
				// Field Verification for STD Code
				ErrorMessages.fieldverification("STD");
			}

		}

		// Entering Secondary Contact Number
		if (contactNumber.contains("")) {
			logger1.info(CONTACTNUMBERBLANK);
		} else {
			enterText(By.xpath(ContactNo_Xpath), contactNumber);
			if (contactNumber.length() >= 6 && contactNumber.length() <= 8) {
				logger1.info("Entered Landline Number is : " + std + "-" + contactNumber);
			} else {
				// Field Verification for Landline Number
				ErrorMessages.fieldverification("LandLine Number");
			}

		}

		// Entering Secondary Email Id
		if (secondaryEmailid.equals("")) {
			logger1.info(SECONDARYEMAILIDBLANK);
		} else {
			if (ValidExpression.isValid(contactNumber)) {
				enterText(By.xpath(SecondaryEamil_Xpath), contactNumber);
				logger1.info("Entered Email Id in Contact Detail is : " + contactNumber);
				ErrorMessages.fieldverification("Email Id");
			} else {
				logger1.info("Please Enter a Valid Secondary Email address");
				ErrorMessages.fieldverification("Email Id");
				ErrorMessages.agentExistverification();
			}

		}

		// Secondary Mobile Number
		if (secondaryMobileNumber.equals("")) {
			logger1.info(SECONDARYMOBILENUMBERBLANK);
		} else {

			if (secondaryMobileNumber.length() > 9 && secondaryMobileNumber.length() <= 15) {
				enterText(By.xpath(SecondaryMobileNum_Xpath), secondaryMobileNumber);
				logger1.info("Entered Secondary Mobile Number is : " + secondaryMobileNumber);
			} else {
				enterText(By.xpath(SecondaryMobileNum_Xpath), secondaryMobileNumber);
				logger1.info("Secondary Mobile Number is : " + secondaryMobileNumber);
				ErrorMessages.fieldverification("Mobile Number");
			}
		}

	}

	public static void gstRegistrationNegative(String workbookName, int rowNum, String gstRegistrationStatus,
			String gstRegistrationdate, String gstNumber, String constitutionBusiness, String customerGstType,
			String customerClassifiaction) throws Exception 
	{

		// Selecting GST Registration
		if (gstRegistrationStatus.equals("")) {
			logger1.info(GSTREGISTRATIONSTATUSBLANK);
		} 
		else {
			clickElement(By.xpath(GST_Registration_Xpath));
			fluentwait(By.xpath(GSTRegistrationoption), 60,"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(GSTRegistrationoption), gstRegistrationStatus);
			logger1.info("Selected GstRegistration Status is : " + gstRegistrationStatus);
		} 

		
		//Selecting Other parameters
		if(gstRegistrationStatus.equalsIgnoreCase("No"))
		{
			logger1.info("No Need to Fill Other Details in GST Registration.");
		}
		else if (gstRegistrationStatus.equalsIgnoreCase("Yes")) 
		{
		
			// Gst Registration Date
			if (gstRegistrationdate.equals("")) {
				logger1.info(GSTREGISTRATIONDATEBLANK);
			} else {

				Matcher mtch = dateFrmtPtrn.matcher(gstRegistrationdate);
				if (mtch.matches()) {
					Calendar.calender(workbookName, By.xpath(GST_regdate_Xpath), "DO_Birth", rowNum);
				} else {
					Calendar.calender(workbookName, By.xpath(GST_regdate_Xpath), "DO_Birth", rowNum);
					ErrorMessages.fieldverification("GstRegistrationdate");
				}

			}

		// Gst Registration Number
		if (gstNumber.equals("")) {
			logger1.info(GSTNUMBERBLANK);
		} else {
			enterText(By.xpath(GSTIN_Number_Xpath), gstNumber);
			logger1.info("Entered GST IN/UIN Number is : " + gstNumber);
			ErrorMessages.fieldverification("GST IN/UI number");
		}

		// Selecting ConstitutionBusiness
		if (constitutionBusiness.equals("")) {
			logger1.info(CONSTITUTIONBUSINESSBLANK);
		} else {
			clickElement(By.xpath(ConstitutionofBusiness_Xpath));
			fluentwait(By.xpath(ConstitutionofBusinessoption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ConstitutionofBusinessoption_Xpath), constitutionBusiness);
			logger1.info("Selected GstRegistration Status is : " + constitutionBusiness);
		}

		// Selecting Customer Type
		if (customerGstType.equals("")) {
			logger1.info(CUSTOMERGSTTYPEBLANK);
		} else {
			clickElement(By.xpath(CustomerType_Xpath));
			fluentwait(By.xpath(CustomerTypeOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CustomerTypeOption_Xpath), customerGstType);
			logger1.info("Selected GstRegistration Status is : " + customerGstType);
		}

		// Customer CustomerClassifiaction
		if (customerClassifiaction.equals("")) {
			logger1.info(CUSTOMERCLASSIFIACTIONBLANK);
		} else {
			clickElement(By.xpath(CustomerClassification_Xpath));
			fluentwait(By.xpath(CustomerClassficationoption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CustomerClassficationoption_Xpath),
					customerClassifiaction);
			logger1.info("Selected GstRegistration Status is : " + customerClassifiaction);
		}
	}
		}
	
}