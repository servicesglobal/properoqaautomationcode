package com.org.propero.agencymodule.loginFn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;


public class Login extends BaseClass implements ProjectInterface
{
   @SuppressWarnings("unused")
   private static String testResult;
   private static Logger logger1 = LoggerFactory.getLogger(Login.class);
	
    @Test(priority=1)
	public static void loginPage(String sheetname,String sheetName,int n, String colnumusername, String colnumpassword) throws Exception
	{
		try
		{
			LogintoApp obj = new LogintoApp();
			ReadExcel read = new ReadExcel(sheetname);        
			
			String newUsername = read.getCellData(sheetName,colnumusername,n);
			logger1.info("Entered UserName in User Id Text Field : "+newUsername);
			logger.log(LogStatus.INFO, "Entered UserName in User Id Text Field : "+newUsername);
			
			String newPassword = read.getCellData(sheetName,colnumpassword,n);
			logger1.info("Entered Password in Password Text Field : "+newPassword);
			logger.log(LogStatus.INFO, "Entered Password in Password Text Field : "+newPassword);
						
			obj.setUsername(newUsername);
			
			obj.setPassword(newPassword);

			//Launching Browser Using Base Class Method
			launchbrowser();
			
			//Entering Credentials and Verifying Login
			obj.loginwithCredendial(obj.getUsername(), obj.getpassword());
			
			/*obj.VerifyLogin();*/
			
			testResult="Pass";
		}
		catch(Exception e)
		{
			logger1.info(e.getMessage());
			testResult="Fail";
		}
	}
}
