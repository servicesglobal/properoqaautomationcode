package com.org.propero.agencymodule.modificationFn;
import java.awt.AWTException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ModificationFnActive extends BaseClass implements ProjectInterface, MessageInterface {
	private static Logger logger1 = LoggerFactory.getLogger(ModificationFnActive.class);

	public void setModificationReason(String newModificationReason) throws Exception {
		Variable.ModificationReason = newModificationReason;
	}

	public String getModificationReason() {
		return Variable.ModificationReason;
	}

	public void setModificationRemark(String newModificationRemark) throws Exception {
		Variable.ModificationRemark = newModificationRemark;
	}

	public String getModificationRemark() {
		return Variable.ModificationRemark;
	}

	public void setUploadDocument(String newUploadDocument) throws Exception {
		Variable.UploadDocument = newUploadDocument;
	}

	public String getUploadDocument() {
		return Variable.UploadDocument;
	}

	public void ModifyagentActive(String AgentId, String ModificationReason, String ModificationRemark,
			String UploadDocument) throws InterruptedException, AWTException, Exception {

		// Clicking on Modify Button from Agent dashboard
		
		//Thread.sleep(10000);
		ImplicitWait(60);
	   
		
		Boolean b=driver.findElements(By.xpath(ModificationButton_Xpath)).size()!=0;
		logger1.info(b.toString());
		
	    if(b==true){
		fluentwait(By.xpath(ModificationButton_Xpath), 60, "Modify button not found.");
		clickElement(By.xpath(ModificationButton_Xpath));

		// Selecting Modification Reason on Ui using Excel Sheet Test Data
		if (ModificationReason.equals("")) {
			logger1.info(ModificationReasonMessage);
		} else {
			Thread.sleep(2000);
			WebElement e1=driver.findElement(By.xpath("//select[@id='popmodreason-" + AgentId.trim() + "']//ancestor::div[1]"));
			logger1.info(e1.getAttribute("class"));
			clickByJS(e1);
			fluentwait(By.xpath("//select[@id='popmodreason-" + AgentId.trim() + "']//option"), 60,
					"Modification Reason Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='popmodreason-" + AgentId.trim() + "']//option"),
					ModificationReason);
			logger1.info("Selected Modification Reason is : " + ModificationReason);
		}

		// Selecting Modification Remarks on UI using Excel Sheet Test Data
		if (ModificationRemark.equals("")) {
			logger1.info(ModificationRemarksMessage);
		} else {
			enterText(By.xpath("//textarea[@id='modificationRemarkPopup-" + AgentId.trim() + "']"), ModificationRemark);
			logger1.info("Entered Modification Remark is : " + ModificationRemark);
		}

		/*// Upload Document fn
		if(UploadDocument.equals(""))
		{
			logger1.info(UploadDocumentMessage);
		}else{
			
			if(UploadDocument.equalsIgnoreCase("Yes"))
			{
			//Here we are using Upload File Class to Upload Document
			UploadFile.Uploadfile(By.xpath(BrowseFile_Xpath));
			clickElement(By.xpath(UploadButton_Xpath));
			}else if(UploadDocument.equalsIgnoreCase("No"))
			{
				logger1.info("No Need to Upload File in Modification.");
			}
		}*/
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		//Clicking on Submit Buttin
		Thread.sleep(2000);
		
		clickElement(By.xpath(SubmitModificationButton_Xpath));
		//Thread.sleep(15000);
		
		String modificationStatus="PENDINGFORMODIFICATION";
		verifyAgentStatusAfterMovedToActiveToPendingForModification_ForAgentModification(modificationStatus);
		ErrorMessages.fieldverification("Ok");
		}
		else{
			logger1.info("No Need to click on Modify Button");
			logger.log(LogStatus.PASS, "No Need to click on Modify Button");
		}
		
	}
	
	
	
}
