package com.org.propero.agencymodule.addagent;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class AgentType extends BaseClass implements ProjectInterface 
{
	
	private static String testresult;
	private static final Logger logger1 = LoggerFactory.getLogger(AgentType.class);
	
    /* Click on Channel mangement to Add Agent*/	
	public static void clickOnChannelManagement() throws Exception{
		
		//fluentwait(By.xpath(ChannelManagement_Xpath), 60, "Page is loading Slow so couldn't click on Channel Management.");
		//clickElement(By.xpath(ChannelManagement_Xpath));
		fluentwait(By.xpath("//nav[@id='main_nav']//ul//a[text()='Channel Management ']"), 60, "Page is loading Slow so couldn't click on Channel Management.");
		clickElement(By.xpath("//nav[@id='main_nav']//ul//a[text()='Channel Management ']"));
		logger1.info("Clicked on Channel Management");
		logger.log(LogStatus.PASS, "Clicked on Channel Management");
	}
	
	//Click on Add Intermediary
	public static void clickOnAddInterMediatory() throws Exception{
		//clickElement(By.xpath(AddIntermediary_Btn_Xpath));
		clickElement(By.xpath("//nav[@id='main_nav']//ul//a[text()='Channel Management ']//following-sibling::ul//li//a[text()='Add Intermediary']"));
		logger1.info("Clicked on Add Intermediary");
		logger.log(LogStatus.PASS, "Clicked on Add Intermediary");
	}
	
	//Select Agent Type
	public static void selectAgentType(String workbookName,String columnName,int rowNum) throws Exception{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String agentType = read.getCellData(workbookName,columnName,rowNum);
		if(agentType.equals(""))
		{
			clickElement(By.xpath(Select_Agent_type));
			clickElement(By.xpath(Submit_Btn_Xpath));
			ErrorMessages.selectagentError();
			testresult="Pass";
		}
		else
		{
		clickElement(By.xpath(Select_Agent_type));
		selecttext(By.xpath(Select_Agent_type), agentType);
		logger1.info("Selected Agent Type is : "+agentType);
		logger.log(LogStatus.INFO, "Selected Agent Type is : "+agentType);
		testresult="Pass";
		//click on Submit Button 
		clickElement(By.xpath(Submit_Btn_Xpath));
		}
	}
	
	public static String agentTypeCreation(String workbookName,String columnName,int rowNum) throws Exception
	{
			clickOnChannelManagement();
			clickOnAddInterMediatory();
			selectAgentType(workbookName,columnName,rowNum);
			return testresult;
	}
	
}
