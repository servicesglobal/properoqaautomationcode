package com.org.propero.agencymodule.innterface;

public interface ProjectInterface 
{

	//Login Page Elements
	public String UserName_Xpath="//input[@type='email' or @name='username']";
	public String Password_Id="inputPassword3";
	public String SignIn_Button_Xpath="//button[@class='btn btn-default btn-signin btn-login'][contains(text(),'Sign in')]";
	public String Dashboard_Text_Xpath="//span[contains(text(),'Dashboard')]";
	public String ValidCredentials_Error_Xpath="//div[@class='btn-login-msg']";
	public String InvalidCredentials_Xpath="//div[@id='swal2-content']";
	public String Popup_OK_Xpath="//button[@type='button'][contains(text(),'OK')]";
	
	//Logout Elements
	public String Logout_xpath="//div[@class='al-user-profile']//i[@class='fa fa-power-off']";
	
	//Dashboard Elements
	//public String ChannelManagement_Xpath="//a[@class='dropdown-toggle'][contains(text(),'Channel Management')]";
	public String ChannelManagement_Xpath="//nav[@id='main_nav']//ul//a[text()='Channel Management ']";
	public String AddIntermediary_Btn_Xpath="//a[contains(text(),'Add Intermediary')]";
	public String Select_Agent_type="//select[@class='swal2-select']";
	public String Select_Agent="//select[@class='swal2-select']//option[contains(text(),'')]";
	public String Submit_Btn_Xpath="//button[contains(text(),'Submit')]";
	public String Search_Intermediary="//a[contains(text(),'Search Intermediary')]";
	public String SelectAgentError_Xpath="//div[@id='swal2-validationerror']";
	public String Upload_Intermediary_Xpath="//a[contains(text(),'Upload Intermediary')]";
	
	
	//IntermediaryDetails
	public String IntermediaryTab_Xapth="//a[@name='intermediary']";
	public String Intermediary_Category_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDIARYCATEGORY']//select[@id='undefined']";
	public String IntermediaryCategoryOption_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDIARYCATEGORY']//select[@id='undefined']//option";
	public String Intermediary_SubCategory_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDIARYSUBCATEGORY']//select[@id='undefined']";
	public String Intermediary_SubCatOption_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDIARYSUBCATEGORY']//select[@id='undefined']//option";
	public String Parent_Intermediary_Xpath="//div[@class='form-group']//div[@class='col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding']//input[@type='text']";
	public String Parent_IntermediaryName_Xpath="//div[@class='col-xs-12 col-sm-6 col-md-6 col-lg-3']//div[@class='col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding agent-position']//input[@type='text']";
	public String RmCode_Xpath="//body/app[@ng-version='2.4.9']/main[@class='ng2']/pages/div[@class='al-main']/div[@class='al-content']/dynamic-tabs/tabset/div[@class='tab-content']/tab[@class='active tab-pane']/dcl-wrapper[@class='tab-pane']/agent/div[@id='0']/div[@id='floating-label-agent']/form[@class='ng-valid ng-touched ng-dirty']/div[@id='agent-main-section']/div[@class='row rhi-common-block rhi-common-block-agent']/div[@class='row proposal-tab-section']/div[@class='tab-content agent-pdf-position']/div[@id='intermediary-details-tab-0']/fieldset/div[@class='proposal-tab-section-view']/div[@id='intermediary-details-0']/div[@class='row rhi-block-agent']/div[5]/div[1]/div[1]/input[1]";
	public String RmCodeEnter_Xpath="//div[@class='row rhi-block-agent']/div[5]/div[1]/div[1]/input[1]";
	public String SourcingVertical_Xpath="//dynamic-dropdown[@lookupkey='CHANNEL']//select[@id='undefined']";
	public String SourcingVerticalOption_Xpath="//dynamic-dropdown[@lookupkey='CHANNEL']//select[@id='undefined']//option";
	public String Sub_Vertical_Xpath="//dynamic-dropdown[@lookupkey='SUBCHANNEL']//select[@id='undefined']";
	public String SubVerticalOption_Xpath="//dynamic-dropdown[@lookupkey='SUBCHANNEL']//select[@id='undefined']";
	public String Secondary_Vertical_Xpath="//dynamic-dropdown[@lookupkey='SECCHANNEL']//select[@id='undefined']";
	public String SecondaryVerticalOption_Xpath="//dynamic-dropdown[@lookupkey='SECCHANNEL']//select[@id='undefined']//option";
	public String Sourcing_Location_Xpath="//dynamic-dropdown[@lookupkey='MONITORINGLOCATION']//select[@id='undefined']";
	public String SourcingLocationOption_Xpath="//dynamic-dropdown[@lookupkey='MONITORINGLOCATION']//select[@id='undefined']//option";
	public String Symbiosys_Login_Xpath="//input[@name='createSymLogin-0']";
	public String ChequeandDD_Xpath="//input[@name='chequeAndDD-0']";
	public String Arf_RcvDate_Xpath="//input[@id='arfDate-0']";
	public String DuplicatepartyCheck_Xpath="//div[@class='modal fade popup-modal in']//div[@class='sub-header-main']";
	public String SelectDuplicatePartyCheck_Xpath="//label[@class='radio-inline custom-radio custom-radio-dd nowrap']//span";
	public String DuplictaePartyOk_btn_Xpath="//div[@class='modal fade popup-modal in']//button[@class='btn btn-bucket-filter btn-bucket-ok'][contains(text(),'Ok')]";
	public String URNNO_Xpath="//*[@id='intermediary-details-0']/div[3]/div[4]/div/input";
	public String URNDate_Id="urnDate-0";
	
	
	//Personal Details
	public String Personal_Detailstab_Xpath="//a[@name='personal'][contains(text(),'Personal')]";
	public String Customer_Type_Id="customertype-0";
	public String CustomerTypeoption_Xpath="//select[@id='customertype-0']//option";
	public String Title_Id="title-0";
	public String TitleOption_Xpath="//select[@id='title-0']//option";
	public String FirstName_Id="firstName-0";
	public String LastName_Id="lastName-0";
	public String PinCode_Id="pincode-0";
	public String DateofBirth_Id="birthDt-0";
	public String CorporateName_Id="corporateName1-0";
	public String ContactPerson_Id="contactPerson-0";
	public String Preferred_Address_Xpath="//*[@id='proposer-details-0']/div[2]/div[4]/div/select";
	public String PreferredAddressoption_Xpath="//*[@id='proposer-details-0']/div[2]/div[4]/div/select//option";
	public String Father_Name_Xpath="//*[@id='proposer-details-0']/div[2]/div[5]/div/input";

	//Office Address
	public String AddressLine1_Id="addressLine1Lang1CP-0";
	public String AddressLine2_Id="addressLine2Lang1CP-0";
	public String OffPinCode_Id="pincodeCP-0";
	public String City_Selection_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[2]//div[1]//div[1]//div[2]//select";
	public String CitySelectionOption_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[2]//div[1]//div[1]//div[2]//select//option";
	public String Locality_Id="areaCP-0";
	
	//Residential Address
	public String ResidentialAddCheckbox_Xpath="//span[contains(text(),'Same as Office Address')]";
	public String ResAddressLine1_Id="addressDO.addressLine1Lang1C-0";
	public String ResAddressLine2_Id="addressLine2Lang1C-0";
	public String ResPincode_Id="pincodeC-0";
	public String ResCity_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[3]//select";
	public String ResCityOption_Xpath="//div[contains(@class,'proposal-tab-section-view proposal-tab-noborder')]//div[3]//select//option";
	public String ResLocality_Xpath="areaC-0";

	//Contact Details
	public String Emailid_Xpath="//div[@id='contact-details-0']//span[1]//span//input[@type='text']";
	public String MobileNum_Xpath="//div[@id='contact-details-0']//span[2]//span[1]//input[@maxlength='15']";
	public String STD_Xpath="//div[contains(@class,'col-xs-12 col-sm-3 col-md-3 col-lg-3')]//div[contains(@class,'form-group')]//span//span//input[contains(@type,'text')]";
	public String ContactNo_Xpath="//div[contains(@class,'col-xs-12 col-sm-9 col-md-9 col-lg-9')]//div[contains(@class,'form-group')]//span[3]//span[1]//input[1]";
	public String SecondaryEamil_Xpath="//div[@id='contact-details-0']//div[4]//input[@type='text']";
	public String SecondaryMobileNum_Xpath="//div[@id='contact-details-0']//div[5]//input[@type='text']";
	
	//GST Registration 
	public String GST_Registration_Xpath="//div[@id='gst-details-0']//div[1]//div[1]//select";
	public String GSTRegistrationoption="//div[@id='gst-details-0']//div[1]//div[1]//select//option";
	public String GST_regdate_Xpath="//input[@id='gstregdate-0']";
	public String Classifiaction_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDCLASS']//select[@id='undefined']";
	public String ClassificationOption_Xpath="//dynamic-dropdown[@lookupkey='INTERMEDCLASS']//select[@id='undefined']//option";
	public String GSTIN_Number_Xpath="//div[@id='gst-details-0']//div[3]//div[1]//input[@type='text']";
	public String ConstitutionofBusiness_Xpath="//div[@id='gst-details-0']//div[4]//div[1]//Select";
	public String CustomerType_Xpath="//div[@id='gst-details-0']//div[5]//div[1]//Select";
	public String ConstitutionofBusinessoption_Xpath="//div[@id='gst-details-0']//div[4]//div[1]//Select//option";
	public String CustomerTypeOption_Xpath="//div[@id='gst-details-0']//div[5]//div[1]//Select//option";
	public String CustomerClassification_Xpath="//div[@id='gst-details-0']//div[6]//div[1]//Select";
	public String CustomerClassficationoption_Xpath="//div[@id='gst-details-0']//div[6]//div[1]//Select//option";
	
	//Other Personal Details
	public String OtherPersonalDetails_Tab_Xpath="//a[@name='otherPersonal']";//[contains(text(),'Other Personal Details')]"
	public String NomineeTitle_Xpath="//div[@id='nominee-details-0']//div[@class='col-xs-12 col-sm-3 col-md-2 col-lg-2']//div[1]//select[1]";
	public String NomineeTitleOption_Xpath="//div[@id='nominee-details-0']//div[@class='col-xs-12 col-sm-3 col-md-2 col-lg-2']//div[1]//select[1]//option";
	public String NomineeName_Xpath="//div[@class='col-xs-12 col-sm-3 col-md-3 col-lg-2']//div[@class='form-group']//input[@type='text']";
	public String NomineeLastname_Xapth="//div[@class='proposal-tab-section-view proposal-tab-noborder']//div[@class='row']//div[3]//div[1]//input[1]";
	public String Relation_Xpath="//div[@class='row proposal-tab-section']//div[@id='nominee-details-0']//div//div[4]//select";
	public String RelationOption_Xpath="//div[@class='row proposal-tab-section']//div[@id='nominee-details-0']//div//div[4]//select//option";
	public String PanNumber_Xpath="//div[@id='nominee-details-0']//div[@class='col-xs-12 col-sm-3 col-md-2 col-lg-3']//input[@type='text']";
	public String NomineeAge_Xpath="//div[@class='proposal-tab-section-view proposal-tab-noborder']//div[@class='row']//div[6]//div[1]//input[1]";
	public String PaymentDisbursementMode_Xpath="//div[@id='nomineeaccount-details-0']//div[@class='col-xs-12 col-sm-6 col-md-4 col-lg-3 pdm-dms']//Select[1]";
	public String PaymentDisbursementModeOption_Xpath="//div[@id='nomineeaccount-details-0']//div[@class='col-xs-12 col-sm-6 col-md-4 col-lg-3 pdm-dms']//Select[1]//option";
	public String TDS_Xpath="//*[@id='nomineeaccount-details-0']/div/div[2]/div/select";
	public String TDSOption_Xpath="//*[@id='nomineeaccount-details-0']/div/div[2]/div/select//option";
	public String IFSC_Code_Xpath="//div[@class='col-xs-12 col-sm-2 col-md-2 col-lg-2']//input[@type='text']";
	public String BankAccountNumber_Xpath="//div[@id='nomineeaccount-details-0']//div[@class='col-xs-12 col-sm-6 col-md-4 col-lg-2']//input[@type='text']";
	public String MICRNumber_Xpath="//div[@class='col-xs-12 col-sm-6 col-md-3 col-lg-3']//input[@type='text']";
	public String Bank_Xpath="//div[@class='row rhi-block-agent']//div[@class='col-xs-12 col-sm-3 col-md-3 col-lg-3']//div//input[@type='text']";
	public String BranchName_Xpath="//div[@class='row rhi-block-agent']//div[@class='col-xs-12 col-sm-3 col-md-2 col-lg-2']//input[@type='text']";
	public String State_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']";
	public String StateOption_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']//option";
	public String City_Xpath="//dynamic-dropdown[@lookupkey='CITY']//select[@id='undefined']";
	public String CityOption_Xpath="//dynamic-dropdown[@lookupkey='CITY']//select[@id='undefined']//option";
	
	public String RCDEditable_Xpath="//select[@id='rcdEditable-0']";
	public String Employee_Code_Xpath="//input[@id='intManagerCd-0']";
	public String Total_Experience_Xpath="//div[@class='proposal-tab-section-view']//div[@class='row']//div[3]//div[1]//input[1]";
	public String PassportNumber_Xpath="//input[@name='passportNo']";
	public String NumberofChildren_Xpath="//div[@class='proposal-tab-section-view']//div[@class='row']//div[6]//div[1]//input[1]";
	public String YearofStay_Xpath="//div[@class='row']//div[7]//div[1]//input[1]";
	public String Education_Xpath="//div[@class='col-xs-12 col-sm-3 col-md-3 col-lg-3']//select[@class='form-control ng-pristine ng-valid ng-touched']";
	public String Occupation_Xpath="//select[@id='employmentStatusCd-']";
	public String WorkLocation_Xpath="//div[@id='spouse-details-0']//div[@class='col-xs-12 col-sm-3 col-md-2 col-lg-3']//input[@type='text']";
	public String Languageknown_Xpath="//select[@title='EN']";
	public String SpouseTitle_Xpath="//div[@class='row']//div[12]//div[1]//select[1]";
	public String SpouseFirstname_Xpath="//div[@class='row']//div[13]//div[1]//input[1]";
	public String SpouseLastname_Xpath="//div[@class='row']//div[14]//div[1]//input[1]";
	public String SpouseQualification_Xpath="//div[@class='col-xs-12 col-sm-3 col-md-2 col-lg-3']//select[@class='form-control ng-untouched ng-pristine ng-valid']";
	public String SpouseOccupation_Xpath="//div[@class='col-xs-12 col-sm-3 col-md-3 col-lg-2']//div[@class='form-group']//select[@class='form-control ng-untouched ng-pristine ng-valid']";
	public String AssociatednameEntityName_Xpath="//div[@class='row']//div[17]//div[1]//input[1]";
	public String AssociatedEntityDetails_Xpath="//div[@class='row']//div[18]//div[1]//input[1]";
	public String RefrenceName_Xpath="//tr//td[1]//input[1]";
	public String RefrenceAddress_Xpath="//tr//td[2]//input[1]";
	public String RefrenceContactNumber_Xpath="//tr//td[3]//input[1]";
	public String Refrencerelationship_Xpath="//tr//td[4]//input[1]";
	
	
	//Payment and Financial
	public String PaymentandFinancial_Xpath="//a[@name='payment']";
	public String PayTo_Id="payToCd-0";
	public String PayToOption_Xpath="//select[@id='payToCd-0']//option";
	public String ClawbackForm_Id="clawbackFromCd-0";
	public String ClawbackOption_Xpath="//select[@id='clawbackFromCd-0']//option";
	public String AgentPaymentTab_Xpath="//h2[contains(text(),'Agent Payment')]";
	public String PaymentFrequency_Id="paymentFrequencyCd-";
	public String PaymentFrequencyOption="//select[@id='paymentFrequencyCd-']//option";
	public String Paymntdisbmode_Id="paymentModeCd--0";
	public String PaymentDisbModeOption_Xpath="//select[@id='paymentModeCd--0']//option";
	public String PaymentDismode_Xpath="//select[@id='paymentModeCd--0']";
	public String ServiceTaxNum_Id="serviceTaxNum-0";
	public String TDS_flag_Id="tdsFl-";
	public String TdsOption_Xpath="//select[@id='tdsFl-']//option";
	public String LowTds_Flag_Id="lowTdsFl-";
	public String PanNumber_Id="pan-0";
	public String Aadharcardnumber_id="aadhaarnumber-0";
	
	
	//License Details
	public String ExamandLicense_Xpath="//a[@name='license']";
	public String IRDALicenseNum_Id="licenseNo-0-0";
	public String LicenseTypeCd_Id="licenseTypeCd-0-0";
	public String LicenseTypeCdOption="//select[@id='licenseTypeCd-0-0']//option";
	public String CompanyAssociated_Id="companyAssociateWith-0-0";
	public String CompanyAssociatedOption_Xpath="//select[@id='companyAssociateWith-0-0']//option";
	public String State_Id="state-0-0";
	public String LicenseStateOption_Xpath="//select[@id='state-0-0']//option";
	public String City_Id="city-0-0";
	public String LicenseCityOption_Xpath="//select[@id='city-0-0']//option";
	public String Branch_Id="branch-0-0";
	public String Issuedate_Id="issueDt-0-0";
	public String ExpiryDate_Id="expiryDt-0-0";
	
	public String Other_Details_Xpath="//h2[@class='uparrow'][contains(text(),'Other Details')]";
	public String IntermediaryJoin_Id="joiningDt-0";
	public String IntermediaryEnd_Id="terminationDt-0";
	public String Noof_Policies_Xpath="//div[@id='license-exam-tab-0']//div[contains(@class,'row')]//div[3]//div[1]//input[1]";
	public String Premium_Amount_Xpath="//div[@id='license-exam-tab-0']//div[contains(@class,'row')]//div[4]//div[1]//input[1]";
	public String RecruitmentSource_Xpath="//dynamic-dropdown[@lookupkey='RECRUITMENTSOURCE']//select[@id='undefined']";
	public String RecruitmentSourceOption_Xpath="//dynamic-dropdown[@lookupkey='RECRUITMENTSOURCE']//select[@id='undefined']//option";
	public String CertificateCourse_Xpath="//dynamic-dropdown[@lookupkey='CERTIFICATECOURSECD']//select[@id='undefined']";
	public String CertificateCourseOption_Xpath="//dynamic-dropdown[@lookupkey='CERTIFICATECOURSECD']//select[@id='undefined']//option";
	public String KnowreligareEmployee_Xpath="//div[@id='license-exam-tab-0']//div[@class='row rhi-block-agent']//div[3]//div[1]//select";
	public String KnowreligareEmployeeOption_Xpath="//div[@id='license-exam-tab-0']//div[@class='row rhi-block-agent']//div[3]//div[1]//select//option";
	public String TrainingStart_Xpath="//input[@id='trainingStartDt-0']";
	public String TrainingEnd_Xpath="//input[@id='trainingEndDt-0']";
	
	public String Submit_Button_Xpath="//input[@value='Submit']";
	public String Agent_Status_Id="status-0";
	public String Agent_Status_Xpath="//span[contains(@id,'status')]";
	public String Agent_Status_ActiveToPendingforModification_Xpath="//div[@id='swal2-content']//following::b[3]";

	public String HPName_Id="HP_Name-0";
	public String VerifyPan_Id="Verify_PAN-0";
	public String VerifyAadhar_Id="Verify_Aadhaar-0";
	public String VerifyBankDetails_Id="Verify_Bank_Details-0";
	public String OK_Btn_Xpath="//div[@class='modal fade hp-name-modal in']//div[@class='modal-footer']//button[contains(text(),'Ok')]";
	public String Intermediary_Code_Id="agentId-0";
	public String IntermediaryCode_Xpath="//div[@class='row']//div[1]//span[@class='proname']";

	//Search Criteria
	//public String Search_Intermediary_Xpath="//a[contains(text(),'Search Intermediary')]";
	public String Search_Intermediary_Xpath="//nav[@id='main_nav']//ul//a[text()='Channel Management ']//following-sibling::ul//li//a[text()='Search Intermediary']";
	public String Intermediary_Code_Xpath="//input[@name='agentId']";
	public String Interm_Category_Xpath="//select[@id='undefined' or @title='select']";
	public String Interm_Name_Xpath="//input[@name='agentName']";
	public String Rm_Id_Xpath="//input[@name='staffId']";
	public String BranchId_Xpath="//input[@name='monitoringLocationId']";
	public String SearchBtn_Xpath="//i[@class='fa fa-search']";
	
	//Bank Details
	public String Ifsc_Code_Id="ifscCd-0";
	public String BankAccountNum_Id="accountNum-0";
	public String MicrNum_Id="micrNum-0";
	public String BankDetails_Id="bankName-0";
	public String Branch_Details_Id="branchName-0";
	public String Effectivefrom_Id="effectiveFromDt-0";
	public String EffectiveTo_Id="effectiveToDt-0";
	public String StateSelc_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']";
	public String StateSelcOption_Xpath="//dynamic-dropdown[@lookupkey='STATE']//select[@id='undefined']//option";
	public String CitySelc_Xpath="//dynamic-dropdown[@filtercriteria='stateCd']//select[@id='undefined']";
	public String CitySelcOption_Xpath="//dynamic-dropdown[@filtercriteria='stateCd']//select[@id='undefined']//option";
	public String Horizontal_Slide_Xpath="//div[@class='agent-table-responsive']//div[@class='container no-padding']//table";
	
	//Documnet Sync page
	public String Options_Xpath="//div[@class='btn-group dropdown open']//button[@type='button'][contains(text(),'options')]";
	public String DocumentSync_Btn_Xpath="//ul[@class='dropdown-menu']//li[2]//a[1]//i[1]";
	
	
	//Product Plan
	public String ProductPlanTab_Xpath="//a[@name='productPlan']";
	public String ProductPlan_Id="productFamilyCdIssuance-0-0";
	public String Manual_Btn_Xpath="//span[contains(text(),'Manual')]";
	public String CrossBtn_Xpath="//a[@class='nav-link active']//span[@class='glyphicon glyphicon-remove-circle']";
	public String ConfirmBtn_Xpath="//button[contains(text(),'Confirm')]";//"//div[@class='swal2-buttonswrapper']//button[@type='button'][contains(text(),'Confirm')]";
	public String AddBtn_Xpath="//button[@class='btn-add btn-agent-add']";
	
	
	//Education Details
	public String Eduacation_Xpath="//table[@class='agent-education-table']//td//select";
	public String EducationOption_Xpath="//table[@class='agent-education-table']//td//select//option";
	public String BoardName_Xpath="//table[@class='agent-education-table']//td[2]//input";
	public String RollNum_Xpath="//table[@class='agent-education-table']//td[3]//input";
	public String PassingYear_Xpath="//table[@class='agent-education-table']//td[4]//input";
	public String FinalExam_Date="";
	public String ExamRoll_Xpath="//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[7]/input";
	public String Marks_Xpath="//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[8]/input";
	
	
	//Exam Details
	public String ExamCenter_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select[@id='undefined']";
	public String ExamCenterOption_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select[@id='undefined']//option";
	public String Language_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select[@id='undefined']";
	public String LangaugeOption_Xpath="//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select[@id='undefined']//option";
	
	
	
	//Modification Remarks Details
	public String ModificationButton_Xpath="//input[@value='Modify']";
	public String ModificationRemarks_Id="modificationremarks-0";
	public String ModificationReason_Id="modificationReason-0";
	public String BrowseFile_Xpath="//input[@name='input-file-preview']";
	public String UploadButton_Xpath="//span[@class='input-group-btn']//button[@type='button']";
	public String SubmitModificationButton_Xpath="//div[@class='modal-footer']//button[@type='button']";
	
	
	//Communication Details
	public String CommunicationDetails_Xpath="//a[contains(text(),'Communication Details')]";
	public String CommunicationType_ID="communicationTo";
	public String AgentId_ID="agentId";
	public String SearchButton_XPATH="//button[contains(@class,'agent-button')]/i";
    public String FromDate_ID="fromDt";
	public String ToDate_ID="toDt";
	public String PageCount_ID="pageCount";
	public String Pagination_XPATH="//ul[@class='pagination']//li//a]";
	
	
	//Bulk Upload
	public String BrowseButton_Xpath="//input[@name='input-file-preview']";
	public String ValidateButton_Xpath="//button[text()='Validate Excel']";
	public String RecordStatus_ID="swal2-content";
	
	
}
