package com.org.propero.agencymodule.functions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;

public class ExamlicenseTraining extends BaseClass implements ProjectInterface,MessageInterface
{
	private static Logger logger1 = LoggerFactory.getLogger(ExamlicenseTraining.class);
	
	private static Pattern dateFrmtPtrn = Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");

	public void setIrdaLicenseNumber(String newIrdaLicenseNumber) throws Exception {
		Variable.IrdaLicenseNumber = newIrdaLicenseNumber;
	}

	public String getIrdaLicenseNumber() {
		return Variable.IrdaLicenseNumber;
	}
	
	public void setLicenseTypeCode(String newLicenseTypeCode) throws Exception {
		Variable.LicenseTypeCode = newLicenseTypeCode;
	}

	public String getLicenseTypeCode() {
		return Variable.LicenseTypeCode;
	}
	
	public void setCompanyassociatewith(String newCompanyassociatewith) throws Exception {
		Variable.Companyassociatewith = newCompanyassociatewith;
	}

	public String getCompanyassociatewith() {
		return Variable.Companyassociatewith;
	}
	
	public void setLicenseState(String newLicenseState) throws Exception {
		Variable.LicenseState = newLicenseState;
	}

	public String getLicenseState() {
		return Variable.LicenseState;
	}
	
	public void setLicenseCity(String newLicenseCity) throws Exception {
		Variable.LicenseCity = newLicenseCity;
	}

	public String getLicenseCity() {
		return Variable.LicenseCity;
	}
	
	public void setLicenseBranch(String newLicenseBranch) throws Exception {
		Variable.LicenseBranch = newLicenseBranch;
	}

	public String getLicenseBranch() {
		return Variable.LicenseBranch;
	}
	
	public void setLicenseIssueDate(String newLicenseIssueDate) throws Exception {
		Variable.LicenseIssueDate = newLicenseIssueDate;
	}

	public String getLicenseIssueDate() {
		return Variable.LicenseIssueDate;
	}
	
	public void setLicenseEndDate(String newLicenseEndDate) throws Exception {
		Variable.LicenseEndDate = newLicenseEndDate;
	}

	public String getLicenseEndDate() {
		return Variable.LicenseEndDate;
	}
	
	public void setIntermediaryStartDate(String newIntermediaryStartDate) throws Exception {
		Variable.IntermediaryStartDate = newIntermediaryStartDate;
	}

	public String getIntermediaryStartDate() {
		return Variable.IntermediaryStartDate;
	}
	
	public void setNoofPolicies(String newNoofPolicies) throws Exception {
		Variable.NoofPolicies = newNoofPolicies;
	}

	public String getNoofPolicies() {
		return Variable.NoofPolicies;
	}
	
	public void setPremiumAmount(String newPremiumAmount) throws Exception {
		Variable.PremiumAmount = newPremiumAmount;
	}

	public String getPremiumAmount() {
		return Variable.PremiumAmount;
	}
	
	public void setRecruitmentSource(String newRecruitmentSource) throws Exception {
		Variable.RecruitmentSource = newRecruitmentSource;
	}

	public String getRecruitmentSource() {
		return Variable.RecruitmentSource;
	}
	
	public void setCertificateCourse(String newCertificateCourse) throws Exception {
		Variable.CertificateCourse = newCertificateCourse;
	}

	public String getCertificateCourse() {
		return Variable.CertificateCourse;
	}
	
	public void setKnowReligareEmployee(String newKnowReligareEmployee) throws Exception {
		Variable.KnowReligareEmployee = newKnowReligareEmployee;
	}

	public String getKnowReligareEmployee() {
		return Variable.KnowReligareEmployee;
	}
	
	public void setTrainingStartDate(String newTrainingStartDate) throws Exception {
		Variable.TrainingStartDate = newTrainingStartDate;
	}

	public String getTrainingStartDate() {
		return Variable.TrainingStartDate;
	}
	
	public void setTrainingEndDate(String newTrainingEndDate) throws Exception {
		Variable.TrainingEndDate = newTrainingEndDate;
	}

	public String getTrainingEndDate() {
		return Variable.TrainingEndDate;
	}
	
	
	public void setAgentEducation(String newAgentEducation) throws Exception {
		Variable.AgentEducation = newAgentEducation;
	}

	public String getAgentEducation() {
		return Variable.AgentEducation;
	}
	
	public void setBoardName(String newBoardName) throws Exception {
		Variable.BoardName = newBoardName;
	}

	public String getBoardName() {
		return Variable.BoardName;
	}
	
	public void setRollNumber(String newRollNumber) throws Exception {
		Variable.RollNumber = newRollNumber;
	}

	public String getRollNumber() {
		return Variable.RollNumber;
	}
	
	
	public void setPassingYear(String newPassingYear) throws Exception {
		Variable.PassingYear = newPassingYear;
	}

	public String getPassingYear() {
		return Variable.PassingYear;
	}
	
	
	public void setExamCenter(String newExamCenter) throws Exception {
		Variable.ExamCenter = newExamCenter;
	}

	public String getExamCenter() {
		return Variable.ExamCenter;
	}
	
	public void setLanguage(String newLanguage) throws Exception {
		Variable.Language = newLanguage;
	}

	public String getLanguage() {
		return Variable.Language;
	}
	
	public void setTentativeExamdate(String newTentativeExamdate) throws Exception {
		Variable.TentativeExamdate = newTentativeExamdate;
	}

	public String getTentativeExamdate() {
		return Variable.TentativeExamdate;
	}
	
	public void setFinalExamDate(String newFinalExamDate) throws Exception {
		Variable.FinalExamDate = newFinalExamDate;
	}

	public String getFinalExamDate() {
		return Variable.FinalExamDate;
	}
	
	public void setTimeSlot(String newTimeSlot) throws Exception {
		Variable.TimeSlot = newTimeSlot;
	}

	public String getTimeSlot() {
		return Variable.TimeSlot;
	}
	
	public void setResult(String newResult) throws Exception {
		Variable.Result = newResult;
	}

	public String getResult() {
		return Variable.Result;
	}
	
	public void setExamRoll(String newExamRoll) throws Exception {
		Variable.ExamRoll = newExamRoll;
	}

	public String getExamRoll() {
		return Variable.ExamRoll;
	}
	
	
	public void setMarks(String newMarks) throws Exception {
		Variable.Marks = newMarks;
	}

	public String getMarks() {
		return Variable.Marks;
	}
	
	
	
	public static void licenseDetails(String workbookName,int rowNum,String irdaLicenseNumber,String licenseTypeCode,String companyassociatewith,
			String licenseState,String licenseCity,String licenseBranch,String licenseIssueDate) throws Exception
	{
		
		//Filling IRDA Number on UI in License Details in Exam and License Details
		if(irdaLicenseNumber.equals(""))
		{
			logger1.info(IRDALICENSENUMBERMESSAGE);
		}else{
			enterText(By.id(IRDALicenseNum_Id), irdaLicenseNumber);
			logger1.info("Entered Irda License Number is : "+irdaLicenseNumber);
		}
		
		//Filling License Type Code in License Details in Exam and License Details
		if(licenseTypeCode.equals(""))
		{
			logger1.info(LICENSETYPEMESSAGE);
		}else{
			clickElement(By.id(LicenseTypeCd_Id));
			fluentwait(By.xpath(LicenseTypeCdOption), 60,
					"License Type Code Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(LicenseTypeCdOption), licenseTypeCode);
			logger1.info("Selected LicenseType Code is : " + licenseTypeCode);
			
		}
		
		//Filling Company Associated with in License Details in Exam and License Details
		if(companyassociatewith.equals(""))
		{
			logger1.info(COMPANYASSOCIATEDWITHMESSAGE);
		}else{
			clickElement(By.id(CompanyAssociated_Id));
			fluentwait(By.xpath(CompanyAssociatedOption_Xpath), 60,
					"Company Associate Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CompanyAssociatedOption_Xpath), companyassociatewith);
			logger1.info("Selected Company associate with is : " + companyassociatewith);
		}
		
		//Filling State with in License Details in Exam and License Details
		if(licenseState.equals(""))
		{
			logger1.info(LICENSESTATEMESSAGE);
		}else
		{
			clickElement(By.id(State_Id));
			fluentwait(By.xpath(LicenseStateOption_Xpath), 60,
					"License State Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(LicenseStateOption_Xpath), licenseState);
			logger1.info("Selected License State is : " + licenseState);
		}
		
		
		//Filling City with in License Details in Exam and License Details
		if(licenseCity.equals(""))
		{
			logger1.info(LICENSECITYMESSAGE);
		}else{
			clickElement(By.id(City_Id));
			fluentwait(By.xpath(LicenseCityOption_Xpath), 60,
					"License City Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(LicenseCityOption_Xpath), licenseCity);
			logger1.info("Selected License City is : " + licenseCity);
		}
		
		
		//Filling Branch With in License Details in Exam and License Details
		if(licenseBranch.equals(""))
		{
			logger1.info(LICENSEBRANCHMESSAGE);
		}else{
			enterText(By.id(Branch_Id), licenseBranch);
			logger1.info("Entered License Branch Name : "+licenseBranch);
		}
		
		//Issue Date with in License Details in Exam and License Details
		if(licenseIssueDate.equals(""))
		{
			logger1.info(LICENSEISSUEDATEMESSAGE);
		}else{
			
			Matcher mtch = dateFrmtPtrn.matcher(licenseIssueDate);
			if (mtch.matches()) {
				Calendar.calender(workbookName, By.id(Issuedate_Id), "Issue_Date", rowNum);
			} else {
				Calendar.calender(workbookName, By.id(Issuedate_Id), "Issue_Date", rowNum);
				ErrorMessages.fieldverification("License Issue Date");
			}
		}
		
		
	}
	
	public static void educationDetails(String agentEducation,String boardName,String rollNumber,String passingYear) throws Exception
	{
		
		//Filling Exam Center on UI
		if(agentEducation.equals(""))
		{
			logger1.info(AGENTEDUCATIONMESSAGE);
		}else{
			clickElement(By.xpath(Eduacation_Xpath));
			fluentwait(By.xpath(EducationOption_Xpath), 60,
					"Education Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(EducationOption_Xpath), agentEducation);
			logger1.info("Selected Education is : " + agentEducation);
		}
		
		//Filling Board Name on UI
		if(boardName.equals(""))
		{
			logger1.info(BOARDNAMEMESSAGE);
		}else{
			enterText(By.xpath(BoardName_Xpath), boardName);
			logger1.info("Entered Board Name is : "+boardName);
		}
		
		
		//Filling Roll Number 
		if(rollNumber.equals(""))
		{
			logger1.info(EXAMROLLMESSAGE);
		}else{
			enterText(By.xpath(RollNum_Xpath), rollNumber);
			logger1.info("Entered Roll Number is : "+rollNumber);
			ErrorMessages.fieldverification("RollNumber");
		}
	
	
		
		//Filling Passing Year
		if(passingYear.equals(""))
		{
			logger1.info(PASSINGYEARMESSAGE);
		}else{
			enterText(By.xpath(PassingYear_Xpath), passingYear);
			logger1.info("Entered Passing Year is : "+passingYear);
			ErrorMessages.fieldverification("PassingYear");
		}
		
		
	}
	
	public static void examDetails(String examCenter,String language,String tentativeExamdate,String finalExamDate,
			String timeSlot,String examRoll,String marks) throws Exception
	{
	
		//Filling Exam Center Details
		if(examCenter.equals(""))
		{
			logger1.info(EXAMCENTERMESSAGE);
		}else{
			
			clickElement(By.xpath(ExamCenter_Xpath));
			fluentwait(By.xpath(ExamCenterOption_Xpath), 60,
					"Exam Center Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ExamCenterOption_Xpath), examCenter);
			logger1.info("Selected Exam Center is : " + examCenter);
		}
		
		
		//Filling Language in Center Exam Details
		if(language.equals(""))
		{
			logger1.info(LANGUAGEMESSAGE);
		}else{
			clickElement(By.xpath(Language_Xpath));
			fluentwait(By.xpath(LangaugeOption_Xpath), 60,
					"Language Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(LangaugeOption_Xpath), language);
			logger1.info("Selected Language is : "+language);
		}
		
		//Filling Tentative Exam Date in Center Exam Details
		if(tentativeExamdate.equals(""))
		{
			logger1.info(TENTATIVEEXAMMESSAGE);
		}else{
			enterText(By.xpath("//input[@id='tentativeExamDate-0-0']"), tentativeExamdate);
		}
		
		
	}
	
	public static void othersDetails(String sheetName,int rowNum,String intermediaryStartDate,String noofPolicies,String premiumAmount, String recruitmentSource
			,String certificateCourse,String knowReligareEmployee) throws Exception
	{
		
		//Internediary Start Date in OtherDetails
		if(intermediaryStartDate.equals(""))
		{
			logger1.info(INTERMEDIARYSTARTMESSAGE);
		}else{
			
			Matcher mtch = dateFrmtPtrn.matcher(intermediaryStartDate);
			if (mtch.matches()) {
				Calendar.calender(sheetName, By.id(IntermediaryJoin_Id),"Intermediary_Start", rowNum);
			} else {
				Calendar.calender(sheetName, By.id(IntermediaryJoin_Id),"Intermediary_Start", rowNum);
				ErrorMessages.fieldverification("IntermediaryStartDate");
			}
			
		}
		
	
		//Filling No of Policies
		if(noofPolicies.equals(""))
		{
			logger1.info(NOOFPOLICIESMESSAGE);
		}else{
			enterText(By.xpath(Noof_Policies_Xpath), noofPolicies);
			logger1.info("Entered No of policies are : "+noofPolicies);
			ErrorMessages.fieldverification("NoofPolicies");
		}
	
		
		//Filling Premium Amount in Last Five Year
		if(premiumAmount.equals(""))
		{
			logger1.info(PREMIUMAMOUNTMESSAGE);
		}else{
			enterText(By.xpath(Premium_Amount_Xpath), premiumAmount);
			logger1.info("Entered Premium Amount is : "+premiumAmount);
			ErrorMessages.fieldverification("PremiumAmount");
		}
		
		
		//Select Recruitment Source
		if(recruitmentSource.equals(""))
		{
			logger1.info(RecruitmentSourceMessage);
		}
		else{
			clickElement(By.xpath(RecruitmentSource_Xpath));
			fluentwait(By.xpath(RecruitmentSourceOption_Xpath), 60,
					"Recruitment Source Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(RecruitmentSourceOption_Xpath), recruitmentSource);
			logger1.info("Selected Language is : "+recruitmentSource);
		}
		
		//Select Certificae Course
		if(certificateCourse.equals(""))
		{
			logger1.info(CertificateCourseMessage);
		}else{
			clickElement(By.xpath(CertificateCourse_Xpath));
			fluentwait(By.xpath(CertificateCourseOption_Xpath), 60,
					"Certificate Course Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(CertificateCourseOption_Xpath), certificateCourse);
			logger1.info("Selected Certificate Course is : "+certificateCourse);
		}
		
		//Select Know Your Religare Employee
		if(knowReligareEmployee.equals(""))
		{
		logger1.info(KnowreligareEmployeeMessage);	
		}else{
			clickElement(By.xpath(KnowreligareEmployee_Xpath));
			fluentwait(By.xpath(KnowreligareEmployeeOption_Xpath), 60,
					"Knowreligare Employee Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(KnowreligareEmployeeOption_Xpath), knowReligareEmployee);
			logger1.info("Selected Know Religare Employee is : "+knowReligareEmployee);
		}
	}
	
	public static void training(String trainingStartDate,String trainingEndDate) throws Exception
	{
		
		//Select Training Start Date
		if(trainingStartDate.equals(""))
		{
			logger1.info(TrainingStartDateMessage);
		}else{
			enterText(By.xpath(TrainingStart_Xpath), trainingStartDate);
			ErrorMessages.fieldverification("TrainingStartDate");
		}
		
		//Select Training End Date
		if(trainingEndDate.equals(""))
		{
			logger1.info(TrainingEndDateMessage);
		}else{
			enterText(By.xpath(TrainingEnd_Xpath), trainingEndDate);
			ErrorMessages.fieldverification("TrainingEndDate");
		}
		
	}
	
}
