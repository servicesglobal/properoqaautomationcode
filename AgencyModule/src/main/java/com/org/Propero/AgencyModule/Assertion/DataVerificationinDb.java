package com.org.propero.agencymodule.assertion;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.utility.BaseClass;
import com.org.propero.agencymodule.utility.DbManager;
import com.relevantcodes.extentreports.LogStatus;

public class DataVerificationinDb extends BaseClass
{
	private static Logger logger1 = LoggerFactory.getLogger(DataVerificationinDb.class);
	
	@SuppressWarnings("null")
	public static void dBVerification(String intermediaryCode) throws Exception
	{
		DbManager.setDbConnection();
		String agentStatus="ACTIVE";
		String environment = null;
		String dBStatus = null;
		try {
			ReadExcel read = new ReadExcel("D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx");
			String fis = "Credentials";
			environment = (read.getCellData(fis, "Environment", 1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger1.error(e.getMessage());
		}
		
		if(environment.contains("QC")){
		List<String> dBValues = DbManager.getSqlQuery("Select AGENTSTATUSCD from rhqc.agent where agentid='"+ intermediaryCode + "'");
		logger1.info(dBValues.toString());
		
		StringBuilder sb = new StringBuilder();
		for (String s : dBValues)
		{
			sb.append(s);
			dBStatus = s;
			logger1.info("DBStatus is : "+dBStatus);
		}
		
		
		try{
			if(dBStatus.equals(agentStatus))
			{
			logger1.info("Agent Status is Verified in DB which is : " + dBValues);
			logger.log(LogStatus.INFO,"Agent Status is Verified in DB which is : " + dBValues);
			}
			else{
				logger1.info("Agent Status is not matched with DB.");
				logger.log(LogStatus.INFO,"Agent Status is not matched with DB.");
			}
		}catch(Exception e)
		{
			logger1.info(e.getMessage());
		}
		
		
		}
		else if(environment.contains("UAT"))
		{
			List<String> dBValues = DbManager.getSqlQuery("Select AGENTSTATUSCD from rhuat.agent where agentid='"+ intermediaryCode + "'");
			
			try{
				if(dBStatus.equals(agentStatus))
				{
				logger1.info("Agent Status is Verified in DB which is : " + dBValues);
				logger.log(LogStatus.INFO,"Agent Status is Verified in DB which is : " + dBValues);
				}
				else{
					logger1.info("Agent Status is not matched with DB.");
					logger.log(LogStatus.INFO,"Agent Status is not matched with DB.");
				}

				
			}catch(Exception e)
			{
				logger1.info(e.getMessage());
			}
			
		}
	}
}
