package com.org.propero.agencymodule.loginFn;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class LogintoApp extends BaseClass implements ProjectInterface
{
	@SuppressWarnings("unused")
	private static String testResult;
	private static String userName=null;
	private static String password=null;
	private static Logger logger1 = LoggerFactory.getLogger(LogintoApp.class);
	
	public String getUsername() 
	{
	return userName;
	}

	public String getpassword() 
	{
	return password;
	}
	
	
	public void setUsername(String newUsername) {
		userName = newUsername;
	}

	public void setPassword(String newPassword){
		
		password = newPassword;
	}
	
	public void loginwithCredendial(String userName,String password) {
		try{
		
			//Entering Username on Login Page by Fecthing Test Data from Excel
			if(userName.contains("@"))
			{
			fluentwait(By.xpath(UserName_Xpath), 60, "Unable to Enter Value in Username text field because your execution preffred environment is down.");
			enterText(By.xpath(UserName_Xpath),userName);
			}
			else if(!userName.contains("@"))
			{
				fluentwait(By.xpath(UserName_Xpath), 60, "Unable to Enter Value in Username text field because your execution preffred environment is down.");
				/*enterText(By.xpath(UserName_Xpath),UserName);*/
				driver.findElement(By.xpath(UserName_Xpath)).sendKeys(userName);
				logger1.info("Please include an '@' in the email address. "+userName+" is missing an '@'.");
				logger.log(LogStatus.PASS,"Please include an '@' in the email address. "+userName+" is missing an '@'." );
			}
				
			//Entering Password on Loigin Page by Fetching Test Data from Excel
			fluentwait(By.id(Password_Id), 60, "Unable to Enter Value in Password text field because your execution preffred environment is down.");
			driver.findElement(By.id(Password_Id)).sendKeys(password);
		
			if(driver.findElement(By.xpath(ValidCredentials_Error_Xpath)).isDisplayed())
			{
				String validCredentialsError = driver.findElement(By.xpath(ValidCredentials_Error_Xpath)).getText();
				logger1.info(validCredentialsError);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Login - Valid Credentials")));
				logger.log(LogStatus.PASS, "Getting Error : "+validCredentialsError);
			}
			else{
				logger1.info("Credentials Filled Properly.");
			}
			

			//Clicking on Login Button from Login Page
			try{
			if(userName.contains("@"))
			{
			clickElement(By.xpath(SignIn_Button_Xpath));
			}else if(!userName.contains("@"))
			{
				clickElement(By.xpath(SignIn_Button_Xpath));
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Login - Submit Button.")));
			}
			}catch(Exception e)
			{
				logger1.info(e.getMessage());
			}
			
		}catch(Exception e)
		{
			logger1.info(e.getMessage());
			logger.log(LogStatus.FAIL, e);
			testResult="Fail";
		}
		
		
	}
	
	public static void verifyLogin() throws Exception
	{
		try{
			fluentwait(By.xpath(Dashboard_Text_Xpath), 30, "Page is Loading Slow so unable to identify Dashboard.");
			driver.findElement(By.xpath(Dashboard_Text_Xpath));
			logger1.info("Login Successfully in Application.");
			logger.log(LogStatus.PASS, "Logged in Sucessfully in Agency Application.");
			testResult="Pass";
		}
	catch(Exception e)
		{	
			ExplicitWait(By.xpath(InvalidCredentials_Xpath), 5);
			clickElement(By.xpath(InvalidCredentials_Xpath));
			String error_Message = driver.findElement(By.xpath(InvalidCredentials_Xpath)).getText();
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Login - Authentication")));
			clickElement(By.xpath(Popup_OK_Xpath));
			logger1.info("Test Case is Marked as Pass because getting Error : "+error_Message);
			logger.log(LogStatus.PASS, "Test Case is Marked as Pass because Getting Error : "+error_Message);
			testResult="Pass";
	}
	
	
}
	
	}
