package com.org.propero.agencymodule.addagent;
import static org.testng.Assert.assertThrows;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.org.propero.agencymodule.assertion.AssertionFn;
import com.org.propero.agencymodule.assertion.FooterError;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.functions.SearchIntermediary;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class FreshAgent extends BaseClass implements ProjectInterface
{

	private static String intermediaryCode;
	public static String testResult;
	public static Logger logger1 = LoggerFactory.getLogger(FreshAgent.class);
	
	public static void freshAgents(int rownum) throws Throwable
	{
		
		
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		String excel=".\\TestData\\TestData_AgencyModule.xlsx";
		
		
		String intermediaryCategory = read.getCellData(workbookName,"Intermediary_Category",rownum);
		String subCategory = read.getCellData(workbookName,"Sub_Category",rownum);
		
		if(intermediaryCategory.equalsIgnoreCase("Agent") && subCategory.equalsIgnoreCase("Agent"))
		{
		
			
			//Here we are calling Intermediary Module Tab Method to Fill data
			testResult = IntermediaryDetails.agentsDetails(excel, workbookName, rownum);
			
			
			//Here We are calling Personal Tab Method to Fill data
			testResult = PersonalDetails.personalDetailsTab(excel, workbookName, rownum);
			
			
			//Here We are Calling Other Details tab method to fill data
			testResult = OtherPersonalDetails.otherpersonaldetailstab(excel, workbookName, rownum);
			
			//Here we are calling Payment and Financial Tab method to fill data
			testResult = PaymentandFinancial.paymentAndFinancial(excel, workbookName, rownum);
			
			//Here we are Calling Exam and License Tab Method to fill data
			ExamandLicense.licenceDetails(workbookName,rownum);	
		
		//Here We are Using Assertion to Verify Agent Bucket Status
		  AssertionFn.agentBopsBucketAssertion(rownum);
		
		}
		else 
		{
			logger1.info("Else Part");
			
			//Here we are calling Intermediary Module Tab Method to Fill data
			testResult = IntermediaryDetails.agentsDetails(excel, workbookName, rownum);
			
			
			//Here We are calling Personal Tab Method to Fill data
			testResult = PersonalDetails.personalDetailsTab(excel, workbookName, rownum);
			
			
			//Here We are Calling Other Details tab method to fill data
			testResult = OtherPersonalDetails.otherpersonaldetailstab(excel, workbookName, rownum);
			
			//Here we are calling Payment and Financial Tab method to fill data
			testResult = PaymentandFinancial.paymentAndFinancial(excel, workbookName, rownum);
			
			//Here we are Calling Exam and License Tab Method to fill data
			ExamandLicense.licenceDetails(workbookName,rownum);	
			
			 AssertionFn.agentBopsBucketAssertion(rownum);
			
			//Capturing Agent Code 
			intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
			
			//Cross the Open Window
			
			closeTab();
			
			//Here we are calling Search Agent Method tpo search any agent
			SearchIntermediary.searchAgent(intermediaryCode);
			//SearchIntermediary.searchAgent("22678009");
			
			//Here we are calling Product Plan 
			ProductPlan.productPlanTab(intermediaryCode, rownum);
			
			//Verify Agent Active Status
			try{
				String agentStatus8="ACTIVE";
				verifyAgentStatusAfterMovedToBucketsFinal(agentStatus8);
				ErrorMessages.fieldverification("ok");
				}
				catch(AssertionError  e){
					String agentStatus9="PENDINGFORACTIVATION";
					verifyAgentStatusAfterMovedToBucketsFinal(agentStatus9);
					ErrorMessages.fieldverification("ok");
					Assert.fail();
				}
		}
		
		
	}
	
	//************ Function for Negative Test cases *****************************************
	public static void freshAgentsNegativeTestCase(int rownum) throws Exception
	{
		
		
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		String excel=".\\TestData\\TestData_AgencyModule.xlsx";
		
		
		String intermediaryCategory = read.getCellData(workbookName,"Intermediary_Category",rownum);
		String subCategory = read.getCellData(workbookName,"Sub_Category",rownum);
		
		if(intermediaryCategory.equalsIgnoreCase("Agent") && subCategory.equalsIgnoreCase("Agent"))
		{
		
			
			//Here we are calling Intermediary Module Tab Method to Fill data
			testResult = IntermediaryDetails.agentsDetails(excel, workbookName, rownum);
			
			
			//Here We are calling Personal Tab Method to Fill data
			testResult = PersonalDetails.personalDetailsTabNegative(excel, workbookName, rownum);
			
			
			//Here We are Calling Other Details tab method to fill data
			testResult = OtherPersonalDetails.otherpersonaldetailstabNegative(excel, workbookName, rownum);
			
			//Here we are calling Payment and Financial Tab method to fill data
			testResult = PaymentandFinancial.paymentAndFinancialNegativeTestCase(excel, workbookName, rownum);
			
			//Here we are Calling Exam and License Tab Method to fill data
			ExamandLicense.licenceDetailsForNegativeTestcases(workbookName,rownum);	
		
		//Here We are Using Assertion to Verify Agent Bucket Status
		 /* AssertionFn.AgentBopsBucketAssertion(rownum);*/
		
		}
		else 
		{
			logger1.info("Else Part");
			
			//Here we are calling Intermediary Module Tab Method to Fill data
			testResult = IntermediaryDetails.agentsDetails(excel, workbookName, rownum);
			
			
			//Here We are calling Personal Tab Method to Fill data
			testResult = PersonalDetails.personalDetailsTabNegative(excel, workbookName, rownum);
			
			
			//Here We are Calling Other Details tab method to fill data
			testResult = OtherPersonalDetails.otherpersonaldetailstabNegative(excel, workbookName, rownum);
			
			//Here we are calling Payment and Financial Tab method to fill data
			testResult = PaymentandFinancial.paymentAndFinancial(excel, workbookName, rownum);
			
			//Here we are Calling Exam and License Tab Method to fill data
			ExamandLicense.licenceDetailsForNegativeTestcases(workbookName,rownum);	
			
			 /* AssertionFn.AgentBopsBucketAssertion(rownum);*/
			
			/*//Here We are Checking For Modification to Verify Agent Bucket Status
			String ModificationTask=read.getCellData(workbookName, "Modification_Task",rownum);
			if(ModificationTask.contains("") || ModificationTask.equalsIgnoreCase("No"))
			{
				logger1.info("No Need to Do Modification.");
			}else if(ModificationTask.equalsIgnoreCase("Yes"))
			{
				CopsAgentModification.AgentModificationCops(rownum);
			}
			
			//Here We are Using Assertion to Verify Agent Bucket Status
			AssertionFn.AgentBopsBucketAssertion(rownum);
			
			//Capturing Agent Code 
			intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
			
			//Cross the Open Window
			CloseTab();
			
			//Here we are calling Search Agent Method tpo search any agent
			SearchIntermediary.SearchAgent(intermediaryCode);
			
			//Here we are calling Product Plan 
			ProductPlan.ProductPlanTab(intermediaryCode, rownum);*/
		}
		
		
	}
	
	public static void freshAgentsNegativeTestCase2(int rownum) throws Exception
	{
		
		
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		String excel=".\\TestData\\TestData_AgencyModule.xlsx";
		
		
		String intermediaryCategory = read.getCellData(workbookName,"Intermediary_Category",rownum);
		String subCategory = read.getCellData(workbookName,"Sub_Category",rownum);
		
		if(intermediaryCategory.equalsIgnoreCase("Agent") && subCategory.equalsIgnoreCase("Agent"))
		{
		
			
			//Here we are calling Intermediary Module Tab Method to Fill data
			testResult = IntermediaryDetails.agentsDetails(excel, workbookName, rownum);
			
			
			//Here We are calling Personal Tab Method to Fill data
			testResult = PersonalDetails.personalDetailsTabNegative(excel, workbookName, rownum);
			
			
			//Here We are Calling Other Details tab method to fill data
			testResult = OtherPersonalDetails.otherpersonaldetailstabNegative(excel, workbookName, rownum);
			
			//Here we are calling Payment and Financial Tab method to fill data
			testResult = PaymentandFinancial.paymentAndFinancialNegativeTestCase(excel, workbookName, rownum);
			
			//Here we are Calling Exam and License Tab Method to fill data
			ExamandLicense.licenceDetailsForNegativeTestcases(workbookName,rownum);	
		
		//Here We are Using Assertion to Verify Agent Bucket Status
		// AssertionFn.agentBopsBucketAssertionNegativeTestcase(rownum);
		
		}
		else 
		{
			logger1.info("Else Part");
			
			//Here we are calling Intermediary Module Tab Method to Fill data
			testResult = IntermediaryDetails.agentsDetails(excel, workbookName, rownum);
			
			
			//Here We are calling Personal Tab Method to Fill data
			testResult = PersonalDetails.personalDetailsTabNegative(excel, workbookName, rownum);
			
			
			//Here We are Calling Other Details tab method to fill data
			testResult = OtherPersonalDetails.otherpersonaldetailstabNegative(excel, workbookName, rownum);
			
			//Here we are calling Payment and Financial Tab method to fill data
			testResult = PaymentandFinancial.paymentAndFinancial(excel, workbookName, rownum);
			
			//Here we are Calling Exam and License Tab Method to fill data
			ExamandLicense.licenceDetailsForNegativeTestcases(workbookName,rownum);	
			
			//AssertionFn.agentBopsBucketAssertionNegativeTestcase(rownum);
			
			/*//Here We are Checking For Modification to Verify Agent Bucket Status
			String ModificationTask=read.getCellData(workbookName, "Modification_Task",rownum);
			if(ModificationTask.contains("") || ModificationTask.equalsIgnoreCase("No"))
			{
				logger1.info("No Need to Do Modification.");
			}else if(ModificationTask.equalsIgnoreCase("Yes"))
			{
				CopsAgentModification.AgentModificationCops(rownum);
			}
			
			//Here We are Using Assertion to Verify Agent Bucket Status
			AssertionFn.AgentBopsBucketAssertion(rownum);
			
			//Capturing Agent Code 
			intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
			
			//Cross the Open Window
			CloseTab();
			
			//Here we are calling Search Agent Method tpo search any agent
			SearchIntermediary.SearchAgent(intermediaryCode);
			
			//Here we are calling Product Plan 
			ProductPlan.ProductPlanTab(intermediaryCode, rownum);*/
		}
		
		
	}

}
