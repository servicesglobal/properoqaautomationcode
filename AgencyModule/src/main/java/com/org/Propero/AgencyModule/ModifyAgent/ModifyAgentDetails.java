package com.org.propero.agencymodule.modifyAgent;

import org.junit.Assert;
import org.openqa.selenium.By;
import com.org.propero.agencymodule.assertion.AssertionFn;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.errormessages.PageNotFoundError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.functions.SearchIntermediary;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;

public class ModifyAgentDetails extends BaseClass implements ProjectInterface {
	private static String testResult;

	public static String modficationDetailsAgent(int rowNum) throws Exception {
		

			ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
			String excel = ".\\TestData\\TestData_AgencyModule.xlsx";
			String fis = "AgentModification";

			String agentId = read.getCellData(fis, "Agent_Id", rowNum);
			System.out.println("Agent Id for Modification : " + agentId);
			// calling Search Method to search Agent
			testResult = SearchIntermediary.searchAgent(agentId);
			

			// Here we are calling Modification Method to fill all mandatory
			// details during Modification
			testResult = AgentModificationActive.activeagentmodification(excel, fis, rowNum, agentId);

			// Here we are calling Intermediary Module Tab Method to Fill data
			testResult = ModificationIntermediaryDetails.agentsDetails(excel, fis, rowNum, agentId);

			Thread.sleep(2000);
			// Here We are calling Personal Tab Method to Fill data
			testResult = ModificationPersonalDetails.personalDetailsTab(excel, fis, rowNum, agentId);

			// Here We are Calling Other Details tab method to fill data
			Thread.sleep(2000);
			testResult = ModficationOtherPersonalDetails.otherpersonaldetailstab(excel, fis, rowNum, agentId);

			// Here we are calling Payment and Financial Tab method to fill data
			Thread.sleep(2000);
			testResult = ModificationPaymentandFinancial.paymentAndFinancial(excel, fis, rowNum, agentId);

			// Here we are Calling Exam and License Tab Method to fill data
			Thread.sleep(2000);
			testResult = ModificationExamandLicenseTraining.examLicenseTrainingTab(excel, fis, rowNum, agentId);

			verticalscrollup();

			//Thread.sleep(3000);
			//waitForElements(By.xpath("//input[@class='btn btn-rhi-yellow']"));
			waitForElementsToBeClickable(By.xpath("//input[@class='btn btn-rhi-yellow']"));
			clickElement(By.xpath("//input[@class='btn btn-rhi-yellow']"));

			ErrorMessages.fieldverification("Error Field Verification");

			testResult = AssertionFn.modificationAgentStatusActive(agentId);

		
		   return testResult;
	}

}
