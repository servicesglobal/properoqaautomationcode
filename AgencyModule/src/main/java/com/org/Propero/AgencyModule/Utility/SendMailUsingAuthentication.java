package com.org.propero.agencymodule.utility;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.io.*;

@SuppressWarnings({ "restriction", "unused" })
public class SendMailUsingAuthentication extends BaseClass
{
	private static Logger logger1 = LoggerFactory.getLogger(SendMailUsingAuthentication.class);
  /*public static String server="mail.religare.com";
	public static String from = "rhiclqctech@religare.com";
	public static String Password = "mail@123";

	public static String[] to ={"ashish.sgh@ext.religare.in","harshit.jain@ext.religare.in","deepak.mahajan@ext.religare.in","raja.vaishnav@religare.com","amit.ty@religare.com","k.santhosh@religare.com","prateek.jaiswal@religare.com"};
	public static String subject = "Faveo Automation Test Report";
	
	public static String messageBody ="Hi Team, <br> <br> Please Find the attached Faveo Automation Report. <br> <br> <br> Kind Regards, <br> Ashish Singh <br> Software Test Engineer <br> Monocept";
	public static String attachmentPath=System.getProperty("user.dir") + "/Reports/TestReport.html";
	public static String attachmentName="Automation TestReport.html";
	public static String attachmentPath1=System.getProperty("user.dir") + "\\TestData\\Favio_Framework.xlsx";
	public static String attachmentName1="Test Data Sheet.xlsx";
	*/
	


private BodyPart messageBodyPart;

  public void postMail( String mailServer, String from, String[] to, String subject, String messageBody, String attachmentPath, String attachmentName, String attachmentPath1, String attachmentName1) throws MessagingException
  {
    boolean debug = false;

     //Set the host smtp address
     Properties props = new Properties();
     props.put("mail.smtp.host", TestConfig.server);
     props.put("mail.smtp.auth", "true");

    Authenticator auth = new SMTPAuthenticator();
    Session session = Session.getDefaultInstance(props, auth);

    session.setDebug(debug);

    // create a message
    Message msg = new MimeMessage(session);

    // set the from and to address
    InternetAddress addressFrom = new InternetAddress(from);
    msg.setFrom(addressFrom);
  
    // new code added
  Multipart multipart = new MimeMultipart();
 // multipart.addBodyPart(messageBodyPart);

    // Part two is attachment
  messageBodyPart = new MimeBodyPart();
  //String filename = "D:\\TestData_AgencyModule\\Reports\\TestReport.html";
  String filename = System.getProperty("user.dir") + "/reports/TestReport.html";
  DataSource source = new FileDataSource(filename);
  messageBodyPart.setDataHandler(new DataHandler(source));
  
 // messageBodyPart.setFileName("Propero Automation Test Report");
  messageBodyPart.setFileName("AgencyModuleTestAutomationReport.html");
  messageBodyPart.setDescription(attachmentName);
  multipart.addBodyPart(messageBodyPart);
  
  messageBodyPart = new MimeBodyPart();
  String filename1 = System.getProperty("user.dir") + "/TestData/TestData_AgencyModule.xlsx";
  DataSource source1 = new FileDataSource(filename1);
  messageBodyPart.setDataHandler(new DataHandler(source1));
    
    messageBodyPart.setFileName("TestData_AgencyModule.xlsx");
    messageBodyPart.setDescription(attachmentName1);
    multipart.addBodyPart(messageBodyPart);

    // Put parts in message
   msg.setContent(multipart);

    InternetAddress[] addressTo = new InternetAddress[to.length];
    for (int i = 0; i < to.length; i++)
    {
        addressTo[i] = new InternetAddress(to[i]);
    }
    msg.setRecipients(Message.RecipientType.TO, addressTo); 

  // Setting the Subject and Content Type
msg.setSubject(subject);
    msg.setContent(multipart);
      Transport.send(msg);
      
      logger1.info("Sucessfully Sent mail to All Users");
 }

/**
* SimpleAuthenticator is used to do simple authentication
* when the SMTP server requires it.
*/
private class SMTPAuthenticator extends javax.mail.Authenticator
{

    public PasswordAuthentication getPasswordAuthentication()
    {
        String username = TestConfig.from;
        String password = TestConfig.password;
        return new PasswordAuthentication(username, password);
    }
}

}
