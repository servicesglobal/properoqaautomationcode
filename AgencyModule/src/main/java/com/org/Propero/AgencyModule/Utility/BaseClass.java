package com.org.propero.agencymodule.utility;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.google.common.base.Function;
import com.org.propero.agencymodule.assertion.FooterError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseClass implements ProjectInterface
{
	public static Properties credential = null;
	public static WebDriver driver;
	public static XSSFRow row = null;
	public static XSSFCell cell = null;
	public static ExtentTest test;
	public static ExtentReports report;
	public static ExtentTest logger;
	public static ExtentReports extent;
	public static Logger log = Logger.getLogger("devpinoyLogger");

	//Code to Launch browser as per the requirement
	public static void launchbrowser() throws Exception 
	{

		//Here we are Fetching Data from Excel to Load the environment Details / browser Details
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");      
		String browser = read.getCellData("Credentials","Browser",1);
		
		credential = new Properties();
		FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + "\\Config Files\\credential.properties");
		credential.load(ip);
		
		// Verifying browser from credentials.properties file and Executing the
		if (browser.equalsIgnoreCase("Firefox")) {
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("Chrome")) {
			DesiredCapabilities ds = DesiredCapabilities.chrome();
			ds.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver = new ChromeDriver(ds);
		} else if (browser.equalsIgnoreCase("Ie")) {
			DesiredCapabilities ds = DesiredCapabilities.internetExplorer();
			ds.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}

		driver.manage().window().maximize();

		String environment = read.getCellData("Credentials","Environment",1);
		if (environment.contains("QC")) {
			
			//String BaseURLQC = "http://10.216.30.81:8090/agency/#/login";
			openURL((String) credential.get("BaseURLQC"));
		} else if (environment.contains("UAT")) {
			//String BaseURLUAT = "http://10.216.9.165:8090/agency/#/login";
			openURL((String) credential.get("BaseURLUAT"));
		} else if (environment.contains("Staging")) {
			//String BaseURLStage = "https://properostage.religarehealthinsurance.com/agency/#/login";
			openURL((String) credential.get("BaseURLStage"));
		}

	}

	public static void openURL(String url) {
		driver.get(url);
	}

	@DataProvider(name = "Login")
	public static String[][] excel_Files(String sheetname) throws Exception {
		String[][] excelData = null;
		try {

			String filePath = "D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx";
			FileInputStream finputStream = new FileInputStream(new File(filePath));

			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(finputStream);
			XSSFSheet sheet = workbook.getSheet(sheetname);
			
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//It Will give you number of Column Present in Excel Sheet
			int colCount = sheet.getRow(0).getPhysicalNumberOfCells();

			//it Will give your number of Row in Excel Sheet
			int rowCount = sheet.getPhysicalNumberOfRows();

			ArrayList<String> sheetNames = new ArrayList<String>();
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(workbook.getSheetName(i));

			}

			excelData = new String[rowCount][colCount];

			for (int nrow = 0; nrow < rowCount; nrow++) {

				row = sheet.getRow(nrow);

				for (int ncolumn = 0; ncolumn < colCount; ncolumn++) {

					cell = sheet.getRow(nrow).getCell(ncolumn);

					DataFormatter df = new DataFormatter();
					excelData[nrow][ncolumn] = df.formatCellValue(cell);

				}

			}

		} catch (Exception e) {
		}
		// return null;

		return excelData;

	}

	public static void waitForElements(By by) {
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		
	}
	
    public static void waitForElementsToBeClickable(By by) throws Exception {
	
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(by));
		
		
	}
    
    public static void fluentWait(final String str){
    	FluentWait<WebDriver> wait=new FluentWait<WebDriver>(driver);
    	wait.withTimeout(80, TimeUnit.SECONDS)
    	.pollingEvery(5, TimeUnit.SECONDS)
    	.ignoring(NoSuchElementException.class);
    	
    	WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
    		 
    	     public WebElement apply(WebDriver driver) {
    	 WebElement e1=driver.findElement(By.xpath(str));
    	       return e1;
    	 
    	     }
    	 
    	   });
    	foo.click();
    }
    
    /*public static void verifyAgentStatusAfterMovedToBuckets(String str) throws Exception {
		boolean flag = true;
      l1:
		while (flag) {
			Thread.sleep(5000);
			
			for(int i=0; i<=5; i++){
			try {
				if (driver.findElement(By.xpath("//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divLV']//div[@id='divSubject']")).isDisplayed()) {
				if(driver.findElement(By.id(Agent_Status_Id)).getText().equalsIgnoreCase(str)){	
				System.out.println("Agent Status Verified: "+ str);
					logger.log(LogStatus.PASS, "Agent Status Verified");
					break l1;
				}
			} catch (Exception e) {
				if(i==5){
					if(driver.findElement(By.id(Agent_Status_Id)).getText().equalsIgnoreCase(str)){
						FooterError.footererror();
					}
					
					else{
						logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
						System.out.println("Agent Status Not Verified Because of Loader has been taken more than 30 seconds of time to display the page");
						logger.log(LogStatus.FAIL, "Agent Status Not Verified Because of Loader has been taken more than 30 seconds of time to display the page");
						break l1;
					}
				}
				else{
					System.out.println("Wait For Agent Status Verified");
					
				}
			}
		}
		}
	}*/
    
   /* public static void verifyAgentStatusAfterMovedToBuckets(String str) throws Exception {
		boolean flag = true;
      l1:
		while (flag) {
			
			
			for(int i=0; i<=5; i++){
				Thread.sleep(1000);
			try {
				if (driver.findElement(By.xpath("//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divLV']//div[@id='divSubject']")).isDisplayed()) {
				if(driver.findElement(By.id(Agent_Status_Id)).getText().equalsIgnoreCase("ghdgdhg")){	
				System.out.println("Agent Status Verified: "+ str);
					logger.log(LogStatus.PASS, "Agent Status Verified");
					break l1;
				}
			} catch (Exception e) {
				if(i==5){
					   logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
						System.out.println("Agent Status Not Verified Because of Loader has been taken more than 30 seconds of time to display the page");
						logger.log(LogStatus.FAIL, "Agent Status Not Verified Because of Loader has been taken more than 30 seconds of time to display the page");
						break l1;
					
				}
				else{
					System.out.println("Wait For Agent Status Verified");
					
				}
			}
		}
		}
	}*/
    
  //This code wait till 5 times means then it will break the loop
  	// Function for Click on Refresh Button Until New Email Not Displayed
  	public static void verifyAgentStatusAfterMovedToBuckets(String str) throws Exception {
  		try{
  		boolean flag = true;
        l1:
  		while (flag) {
  			
  			for(int i=0; i<=12; i++){
  			
  				if(driver.findElement(By.id(Agent_Status_Id)).getText().equalsIgnoreCase(str)){	
  					logger.log(LogStatus.PASS, "Agent Status Verified");
  					System.out.println("Agent Status Verified");
  					break l1;
  				}
  				else{
  					
  					if(i==12){	
  						logger.log(LogStatus.FAIL, "Status was not matching because loader was continues loading more than 1 minute");
  						logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
  						System.err.println("Status was not matching because loader was continues loading");
  						break l1;
  						
  					}
  					else{
  						Thread.sleep(5000);
  						logger.log(LogStatus.PASS, "Number of Seconds Waiting to be Verify the Status "+ str +" "+ i*5000 );
  						System.out.println("Number of Seconds Waiting to be Verify the Status" +str +" "+ i*5000);
  					}
  				}
  			
  		}
  		}
  		
  		if(driver.findElement(By.id(Agent_Status_Id)).getText().equalsIgnoreCase(str)){
  			logger.log(LogStatus.PASS, "Agent Status Verified - Acutal Status: "+driver.findElement(By.id(Agent_Status_Id)).getText()+" "+"Expceted Status: "+ str );
  			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
  			System.out.println("Agent Status Verified - Acutal Status: "+driver.findElement(By.id(Agent_Status_Id)).getText()+" "+"Expceted Status: "+ str );
  		}
  		else{
  			logger.log(LogStatus.FAIL, "Agent Status Not Verified - Acutal Status: "+driver.findElement(By.id(Agent_Status_Id)).getText()+" "+"Expceted Status: "+ str );
  			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
  			Assert.assertEquals(driver.findElement(By.id(Agent_Status_Id)).getText(), str );
  			
  		}
  	
  	}
  	catch(Exception e){
  		throw e;
  		//Assert.fail();
  	}
}
  	
  	 //This code wait till 5 times means then it will break the loop
  	// Function for Click on Refresh Button Until New Email Not Displayed
  	public static void verifyAgentStatusAfterMovedToBucketsFinal(String str) throws Exception {
  		try{
  		boolean flag = true;
        l1:
  		while (flag) {
  			
  			for(int i=0; i<=12; i++){
  			
  				if(driver.findElement(By.xpath(Agent_Status_Xpath)).getText().equalsIgnoreCase(str)){	
  					logger.log(LogStatus.PASS, "Agent Status Verified");
  					System.out.println("Agent Status Verified");
  					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
  					break l1;
  				}
  				else{
  					
  					if(i==12){
  						logger.log(LogStatus.FAIL, "Status was not matching because loader was continues loading more than 1 minute");
  						logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
  						System.err.println("Status was not matching because loader was continues loading");
  						break l1;
  						
  					}
  					else{
  						Thread.sleep(5000);
  						logger.log(LogStatus.PASS, "Number of Seconds Waiting to be Verify the Status"+str +" "+ i*5000 );
  						System.out.println("Number of Seconds Waiting to be Verify the Status"+str +" "+ i*5000);
  					}
  				}
  			
  		}
  		}
  		
  		if(driver.findElement(By.xpath(Agent_Status_Xpath)).getText().equalsIgnoreCase(str)){
  			logger.log(LogStatus.PASS, "Agent Status Verified - Acutal Status: "+driver.findElement(By.xpath(Agent_Status_Xpath)).getText()+" "+"Expceted Status: "+ str );
  			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
  			System.out.println("Agent Status Verified - Acutal Status: "+driver.findElement(By.xpath(Agent_Status_Xpath)).getText()+" "+"Expceted Status: "+ str );
  		}
  		else{
  			logger.log(LogStatus.FAIL, "Agent Status Not Verified - Acutal Status: "+driver.findElement(By.xpath(Agent_Status_Xpath)).getText()+" "+"Expceted Status: "+ str );
  			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
  			Assert.fail();
  			
  		}
  	
  	}
  	catch(Exception e){
  		throw e;
  	}
}
  	

 	 //This code wait till 5 times means then it will break the loop
 	// Function for Click on Refresh Button Until New Email Not Displayed
	public static void verifyAgentStatusAfterMovedToActiveToPendingForModification_ForAgentModification(String str) throws Exception {
 		boolean flag = true;
       l1:
 		while (flag) {
 			
 			  l2:
 			   for(int i=0; i<=12; i++){
 				try{
 				if(driver.findElement(By.xpath("//div[@id='swal2-content']//following::b[3]")).isDisplayed()){	
 					logger.log(LogStatus.PASS, "Agent Status Verified");
 					System.out.println("Agent Status Verified");
 					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Agent Modification Pass")));
 					break l1;
 				}
 		
 				}	
 			catch(Exception e){
 			 		if(i==12){
 			 			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Agent Modification Failure")));
 			 			break l1;
 			 			
 			 		}
 			 		else{
 			 			Thread.sleep(5000);
 			 			logger.log(LogStatus.PASS, "Number of Seconds Waiting to be Verify the Status"+str +" "+ i*5000 );
  						System.out.println("Number of Seconds Waiting to be Verify the Status"+str +" "+ i*5000);
 			 		}
 			 	}	
 			   }
 	}
 		
 	
}
    /*public static void verifyAgentStatusAfterMovedToBucketsForAgentModification(String str, String agentId) throws Exception {
		boolean flag = true;
      l1:
		while (flag) {
			Thread.sleep(5000);
			
			for(int i=0; i<=5; i++){
			try {
				if (driver.findElement(By.xpath("//div[@id='divMainViewPane']//div[@id='divMainView']/div[2]//div[@id='divVw']//div[@id='divLV']//div[@id='divSubject']")).isDisplayed()) {
				if(driver.findElement(By.xpath("//*[@id='status-"+agentId.trim()+"']")).getText().equalsIgnoreCase(str)){	
				System.out.println("Agent Status Verified: "+ str);
					logger.log(LogStatus.PASS, "Agent Status Verified");
					break l1;
				}
			} catch (Exception e) {
				if(i==5){
					System.out.println("Agent Status Not Verified");
					logger.log(LogStatus.FAIL, "Agent Status Not Verified");
					break l1;
				}
				else{
					System.out.println("Wait For Agent Status Verified");
					
				}
			}
		}
		}
	}*/
    
    public static void verifyAgentStatusAfterMovedToBucketsForAgentModification(String str, String agentId) throws Exception {
  		try{
    	boolean flag = true;
  		String Actualstatus=null;
        l1:
  		while (flag) {
  			
  			for(int i=0; i<=20; i++){
  			
  				if(driver.findElement(By.xpath("//*[@id='status-"+agentId.trim()+"']")).getText().equalsIgnoreCase(str)){	
  					logger.log(LogStatus.PASS, "Agent Status Verified");
  					System.out.println("Agent Status Verified");
  					Actualstatus=driver.findElement(By.xpath("//*[@id='status-"+agentId.trim()+"']")).getText();
  					logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
  					break l1;
  				}
  				else{
  					
 					if(i==12){
 						logger.log(LogStatus.FAIL, "Status was not matching because loader was continues loading more than 1 minute");
  						logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
  						System.err.println("Status was not matching because loader was continues loading");
  						break l1;
  						
  					}
  					else{
  						Thread.sleep(5000);
  						logger.log(LogStatus.PASS, "Number of Seconds Waiting to be Verify the Status"+str +" "+ i*5000 );
  						System.out.println("Number of Seconds Waiting to be Verify the Status"+str +" "+ i*5000);
  					}
  				}
  			
  		}
  		}
  		if(Actualstatus.equalsIgnoreCase(str)){
  			logger.log(LogStatus.PASS, "Agent Status Verified - Acutal Status: "+Actualstatus+" "+"Expceted Status: "+ str );
  			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
  			System.out.println("Agent Status Verified - Acutal Status: "+Actualstatus+" "+"Expceted Status: "+ str );
  			
  		}
  		else{
  			logger.log(LogStatus.FAIL, "Agent Status Not Verified - Acutal Status: "+Actualstatus+" "+"Expceted Status: "+ str );
  			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
  			Assert.assertEquals(Actualstatus, str );
  			
  		}
  		}
  		catch(Exception e){
  			//Assert.fail();
  			throw e;
  		}
  	}
	
	public static void waitForTextVisiblity(By by){
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(by, "Text Not Visbile after 60 Minutes"));
	}
	
	public static void clearTextfield(By by) {
		waitForElements(by);
		driver.findElement(by).clear();
	}

	public static String clickElement(By by) throws Exception {

		//waitForElements(by);
		
		waitForElementsToBeClickable(by);
		driver.findElement(by).click();
		log.debug("SuccessFully Click on : " + by);
		return null;
	}

	
	
	public static void enterText(By by, String string) {
		waitForElements(by);
		//driver.findElement(by).sendKeys(string);
		
		WebElement element = driver.findElement(by);
		element.sendKeys(string);
		element.sendKeys(Keys.TAB);
		
		
		log.debug("SuccessFully Click on : " + by + ":" + string);
	}
	
	
	
	
	public static void clickbyHover(By by) {

		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(by)).click().build().perform();

	}

	public static String getText(By by) {
		driver.findElement(by).getText();
		return null;
	}

	@SuppressWarnings("unused")
	public static void ExplicitWait(By by, int time) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static void mousehover(By by) {
		waitForElements(by);
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(by)).perform();
	}

	/*@BeforeTest
	public static void Report() {
		String dateName = new SimpleDateFormat("dd-MM-yyyy hh.mm").format(new Date());
		extent = new ExtentReports("D:/TestData_AgencyModule/Reports/"+ dateName+" "+"TestReport.html", true);
		extent = new ExtentReports("D:/TestData_AgencyModule/Reports/TestReport "+dateName+".html", true);
		extent.addSystemInfo("Host Name", "Agency Module").addSystemInfo("environment", "QC Enviroment")
				.addSystemInfo("User Name", "ASHISH KUMAR SINGH");
		extent.loadConfig(new File(System.getProperty("user.dir") + "/Config Files/extent-config.xml"));

	}*/
	
	@BeforeTest
	public static void Report() {
		//String dateName = new SimpleDateFormat("dd-MM-yyyy hh.mm").format(new Date());
		extent = new ExtentReports(".//reports//TestReport.html", true);
		//report = new ExtentReports(".//Reports//TestReport2.html", false);
		//extent = new ExtentReports("D:/TestData_AgencyModule/Reports/TestReport "+dateName+".html", true);
		extent.addSystemInfo("Host Name", "Agency Module").addSystemInfo("environment", "QC Enviroment")
				.addSystemInfo("User Name", "Bijay Kumar Sahoo");
		extent.loadConfig(new File(System.getProperty("user.dir") + "/Config Files/extent-config.xml"));

	}

	// This method is to capture the screenshot and return the path of the screenshot.
	public static String getScreenhot(WebDriver driver, String screenshotName) throws Exception {
		try{
		String dateName = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + screenshotName + dateName+ ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
		}
		catch(Exception e){
			return e.getMessage();
		}
	}

	public static String getScreenhotpass(WebDriver driver, String screenshotName) throws Exception {
		try{
		String dateName = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		String destination = System.getProperty("user.dir") + "/PassTestsScreenshots/" + screenshotName +" "+ dateName+ ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
		}
		catch(Exception e){
			return e.getMessage();
		}
	}
	
	
	@AfterMethod
	public void getResult(ITestResult result) throws Exception
	{
		if(result.getStatus() == ITestResult.FAILURE)
		{
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
			//To capture screenshot path and store the path of the screenshot in the string "screenshotPath"
            //We do pass the path captured by this mehtod in to the extent reports using "logger.addScreenCapture" method. 			
            String screenshotPath = getScreenhot(driver, result.getName());
            //To add it in the extent report 
            logger.log(LogStatus.FAIL, logger.addScreenCapture(screenshotPath));

		}else if(result.getStatus() == ITestResult.SKIP){
			logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
		}else if (result.getStatus() == ITestResult.SUCCESS) {
			//logger.log(LogStatus.PASS, "Test Case Pass is " + result.getName());
		}
		// ending test
		//endTest(logger) : It ends the current test and prepares to create HTML report
		extent.endTest(logger);
	}
	

	public static void fluentwait(By by, int Time, String Message){
		
		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(Time, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).withMessage(Message).ignoring(NoSuchElementException.class);
		
		wait2.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	
	public static void verticalscrollup()
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0,-250)", "");
		
	}
	
	public static void verticalscrolldown(){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0,100)", "");
	}

	
	public static void scrollHorizotalbyLocator(By by)
	{
	
		WebElement scroll = driver.findElement(by);
	    scroll.click();
		        Actions move = new Actions(driver);
		        move.moveToElement(scroll);
		        move.moveByOffset(40,0);
		        move.release();
		        move.perform();
	}
	
	public static void scrollHorizotalbyLocatorLeft(By by) throws InterruptedException
	{
	
		WebElement scroll = driver.findElement(by);
	    scroll.click();
	            Actions move = new Actions(driver);
	            Thread.sleep(5000);
	            move.moveToElement(scroll).clickAndHold();
		        move.moveByOffset(-500,0);
		        move.release();
		        move.perform();
	}
	

	public static void nextTab(By by)
	{
		driver.findElement(by).sendKeys(Keys.TAB);
	}
	
	
	// Select Option Values using Xpath
	public static void selecttext(By by, String selectText) 
	{
			waitForElements(by);
			WebElement mySelectElement = driver.findElement(by);
			Select titleDropdown = new Select(mySelectElement);
			titleDropdown.selectByVisibleText(selectText.toString());

	}
	
	// Select Option Values using Xpath
	public static void selecttext2(By by, String selectText) {
		waitForElements(by);
		WebElement mySelectElement = driver.findElement(by);
		Select titleDropdown = new Select(mySelectElement);
		List<WebElement> list=titleDropdown.getOptions();
		for(WebElement option:list){
			if(option.getText().equalsIgnoreCase(selectText)){
				option.click();
				break;
			}
			else{
				System.out.println("Value is not avialable inside dropdown");
			}
		}
		//titleDropdown.selectByVisibleText(selectText.toString());

	}
	
	
	public static void ImplicitWait(int time)
	{
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}
	
	
	public static void closeTab()
	{
		try{
			Thread.sleep(5000);
			clickElement(By.xpath(CrossBtn_Xpath));
			ExplicitWait(By.xpath(ConfirmBtn_Xpath), 10);
			clickElement(By.xpath(ConfirmBtn_Xpath));
			}
			catch(Exception e)
			{
				log.info(e.getMessage());
			}	
	}
	
	public static boolean isValidMobile(String s) {

		Pattern p = Pattern.compile("(0/91)?[2-9][0-9]{9}");
		Matcher m = p.matcher(s);
		return (m.find() && m.group().equals(s));
	}
	
	public void selectValue(WebElement e1, String value){
		Select option=new Select(e1);
		option.selectByVisibleText(value);
	}
	
	// Point To Element
		public static void pointToElement(WebElement e1) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", e1);
		}

		// Click functionality by Java Script
		public static void clickByJS(WebElement e1) {
			try {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", e1);

			} catch (Exception e) {
				e.getMessage();
			}
		}
	
	@AfterSuite
	public void tearDown() 
	{
		extent.flush();
	}
	
}
