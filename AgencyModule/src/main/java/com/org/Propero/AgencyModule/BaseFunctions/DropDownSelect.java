package com.org.propero.agencymodule.basefunctions;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.org.propero.agencymodule.utility.BaseClass;

public class DropDownSelect extends BaseClass 
{
	
	public static void selectValuesfromDropdown(By xpath,String actualValue) throws Exception
	{
		Thread.sleep(2000);
		List<WebElement> listValues = driver.findElements(xpath);	
	
		for (WebElement size1 : listValues )
		   {
	         if(size1.getText().equalsIgnoreCase(actualValue))
	         {
	        	 Thread.sleep(2000);
	        	 size1.click();
	             break;
	         }
	         else
	         {
	             /*System.out.println("match not found"); */
	         }  
		   }
		
	}
	
	
}
