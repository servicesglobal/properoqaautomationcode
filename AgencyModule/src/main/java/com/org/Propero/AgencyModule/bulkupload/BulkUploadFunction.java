package com.org.propero.agencymodule.bulkupload;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.addagent.AgentType;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class BulkUploadFunction extends BaseClass implements ProjectInterface {
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentType.class);
	
	 /* Click on Channel mangement to Add Agent*/	
		public static void clickOnChannelManagement() throws Exception{
			fluentwait(By.xpath(ChannelManagement_Xpath), 60, "Page is loading Slow so couldn't click on Channel Management.");
			clickElement(By.xpath(ChannelManagement_Xpath));
			LOGGER.info("Clicked on Channel Management");
			logger.log(LogStatus.PASS, "Clicked on Channel Management");
		}
		
		//Click on Add Intermediary
		public static void clickOnAddInterMediatory() throws Exception{
			fluentwait(By.xpath(Upload_Intermediary_Xpath), 60, "Unable to click on Upload Intermidary");
			clickElement(By.xpath(Upload_Intermediary_Xpath));
			LOGGER.info("Clicked on Upload Intermediary");
			logger.log(LogStatus.PASS, "Clicked on Upload Intermediary");
		}
		
		public static void selectUploadIntermediary() throws Exception{
			clickOnChannelManagement();
			clickOnAddInterMediatory();
		}
		
		public static void setUploadIntermediaryDetails(String sheetName, int rowNum) throws Exception{
			BulkUploadGetterandSetter obj=new BulkUploadGetterandSetter();
			ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
			String intermediaryCategory = read.getCellData(sheetName,"Intermediary_Category",rowNum);
			String subintermediaryCategory = read.getCellData(sheetName,"Intermediary_Category",rowNum);
			
			//String corportaeUploadFile=".\\BulkUploadTestData\\CorporateAgentBulkUploadFormat1.xlsx";
			String corportaeUploadFile=System.getProperty("user.dir") + "\\BulkUploadTestData\\CorporateAgentBulkUploadFormat3.xlsx";
			String corportaePosUploadFile=".\\TestData\\TestData_AgencyModule.xlsx";
			String brokerUploadFile=".\\TestData\\TestData_AgencyModule.xlsx";
			String brokerPosUploadFile=".\\TestData\\TestData_AgencyModule.xlsx";
			
			obj.setIntermediaryCategory("CorporateAgent");
			obj.setsubIntermediaryCategory("CorporateAgent");
			obj.setcorporateAgentpath(corportaeUploadFile);
			obj.setcorporateAgentPOSpath(corportaePosUploadFile);
			obj.setbrokerpath(brokerUploadFile);
			obj.setbrokerPospath(brokerPosUploadFile);
			
			obj.setDetailsOfBulkUpload(sheetName, rowNum, obj.getIntermediaryCategory(), obj.getsubIntermediaryCategory(), obj.getcorporateAgentpath(),obj.getcorporateAgentPOSpath(),obj.getbrokerpath(),obj.getbrokerPospath());
			
		}
		

}
