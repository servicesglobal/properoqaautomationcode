package com.org.propero.agencymodule.assertion;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class FooterError extends BaseClass implements ProjectInterface
{
	public static Logger logger1 = LoggerFactory.getLogger(FooterError.class);
	public static void footererror() throws Exception
	{
		
		//String agentStatus6="PENDINGFORACTIVATION";
		//verifyAgentStatusAfterMovedToBuckets(agentStatus6);
		//ErrorMessages.fieldverification("ok");
		Thread.sleep(5000);
		try{
		//waitForElementsToBeClickable(By.xpath("//i[@name='footerError']"));
		WebElement e1=driver.findElement(By.xpath("//i[@name='footerError']"));
		clickByJS(e1);
		logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
		String error=driver.findElement(By.xpath("//div[@class='flexdisplayTable fixedThead']//table//tbody//tr//td[2]")).getText();
		if(error.equalsIgnoreCase("Since Intermediary is known to Religare Employee, hence it cannot be active."))
		{
			logger.log(LogStatus.INFO, error);
			logger1.info(error);
		}else 
		{
			int row=0;
			row = driver.findElements(By.xpath("//table[@class='table table-bordered']//tbody//tr")).size();
			System.out.println("Total Rows : "+row);
			row=row-1;
			for(int i=1;i<=row;i++)
			{
			
			
			//String issue = driver.findElement(By.xpath("//table[@class='table table-bordered']//tbody//tr["+i+"]//td[2]")).getText();	
			String issue=driver.findElement(By.xpath("//div[@class='flexdisplayTable fixedThead']//table[@class='table table-bordered']//tbody//tr["+i+"]//td[2]")).getText();
			System.out.println("Footer Error: "+issue);
			logger.log(LogStatus.FAIL, issue);
			//logger1.info(issue);
		}
			}
		}
		catch(Exception e){
			System.out.println("No Footer Error Found");
		}
	}
	
	
	public static void footererror2() throws Exception
	{
		//clickElement(By.xpath("//i[@name='footerError']"));
		waitForElementsToBeClickable(By.xpath("//i[@name='footerError']"));
		WebElement e1=driver.findElement(By.xpath("//i[@name='footerError']"));
		clickByJS(e1);
		String error=driver.findElement(By.xpath("//div[@class='flexdisplayTable fixedThead']//table//tbody//tr//td[2]")).getText();
		if(error.equalsIgnoreCase("Since Intermediary is known to Religare Employee, hence it cannot be active."))
		{
			logger.log(LogStatus.INFO, error);
			logger1.info(error);
		}else 
		{
			int row=0;
			row = driver.findElements(By.xpath("//table[@class='table table-bordered']//tbody//tr")).size();
			System.out.println("Total Rows : "+row);
			for(int i=1;i<=row;i++)
			{
			String issue = driver.findElement(By.xpath("//table[@class='table table-bordered']//tbody//tr["+i+"]//td[2]")).getText();	
			
			logger.log(LogStatus.FAIL, issue);
			logger1.info(issue);
		}
			}
	}
	
}
