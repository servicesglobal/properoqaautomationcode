package com.org.propero.agencymodule.addagent;
import org.openqa.selenium.By;
import com.org.propero.agencymodule.assertion.AssertionFn;
import com.org.propero.agencymodule.assertion.FooterError;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.functions.SearchIntermediary;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;


public class ExistingAgents extends BaseClass implements ProjectInterface
{
	private static String testResult;

	public static String fillIntermediary(int rownum) throws Throwable{
		
		String excel=".\\TestData\\TestData_AgencyModule.xlsx";
		String fis = "Existing_Agents";
		
		try{
		//Here we are calling Intermediary Module Tab Method to Fill data
		testResult = IntermediaryDetails.agentsDetails(excel, fis, rownum);
		
		
		//Here We are calling Personal Tab Method to Fill data
		testResult = PersonalDetails.personalDetailsTab(excel, fis, rownum);
		
		
		//Here We are Calling Other Details tab method to fill data
		testResult = OtherPersonalDetails.otherpersonaldetailstab(excel, fis, rownum);
		
		//Here we are calling Payment and Financial Tab method to fill data
		testResult = PaymentandFinancial.paymentAndFinancial(excel, fis, rownum);
		
		//Here we are Calling Exam and License Tab Method to fill data
		testResult=ExamandLicense.licenceDetails(fis,rownum);
		
		//Here We are Checking For Modification to Verify Agent Bucket Status
		/*String ModificationTask=read.getCellData(fis, "Modification_Task",rownum);
		if(ModificationTask.equalsIgnoreCase("No"))
		{
			System.out.println("No Need to Do Modification.");
		}else if(ModificationTask.equalsIgnoreCase("Yes"))
		{
			CopsAgentModification.AgentModificationCops(rownum);
		}*/
		
		//verifying Assertion Criteria
		AssertionFn.agentBopsBucketAssertion(rownum);
		
		//Capturing Agent Code 
		String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		
		//Cross the Open Window
		closeTab();
		
		//Here we are calling Search Agent Method tpo search any agent
		SearchIntermediary.searchAgent(intermediaryCode);
		//Here we are calling Product Plan 
		ProductPlan.productPlanTab(intermediaryCode, rownum);
		
		//Verify Agent Active Status
		try
		{
			//ErrorMessages.fieldverification("ok");
			String agentStatus8="ACTIVE";
			verifyAgentStatusAfterMovedToBuckets(agentStatus8);
			ErrorMessages.fieldverification("ok");
		}
		catch(Exception e){
		    FooterError.footererror();
			
		}
		
		testResult="Pass";
		}
		catch(Exception e){
			//testResult="Fail";
		}
		return testResult;
	}
	
	//*************** Negative Test Cases *************************************************
	public static String fillIntermediaryForNegativeTesting(int rownum) throws Exception
	{
		
		//Readexcel read = new Readexcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String excel=".\\TestData\\TestData_AgencyModule.xlsx";
		String fis = "Existing_Agents";
		
		try{
		//Here we are calling Intermediary Module Tab Method to Fill data
		testResult = IntermediaryDetails.agentsDetails(excel, fis, rownum);
		
		
		//Here We are calling Personal Tab Method to Fill data
		testResult = PersonalDetails.personalDetailsTabNegative(excel, fis, rownum);
		
		
		//Here We are Calling Other Details tab method to fill data
		testResult = OtherPersonalDetails.otherpersonaldetailstabNegative(excel, fis, rownum);
		
		//Here we are calling Payment and Financial Tab method to fill data
		testResult = PaymentandFinancial.paymentAndFinancialNegativeTestCase(excel, fis, rownum);
		
		//Here we are Calling Exam and License Tab Method to fill data
		testResult=ExamandLicense.licenceDetailsForNegativeTestcases(fis,rownum);
		
		//Here We are Checking For Modification to Verify Agent Bucket Status
		/*String ModificationTask=read.getCellData(fis, "Modification_Task",rownum);
		if(ModificationTask.equalsIgnoreCase("No"))
		{
			System.out.println("No Need to Do Modification.");
		}else if(ModificationTask.equalsIgnoreCase("Yes"))
		{
			CopsAgentModification.AgentModificationCops(rownum);
		}*/
		
		//verifying Assertion Criteria
		//AssertionFn.AgentBopsBucketAssertion(rownum);
		
		/*//Capturing Agent Code 
		String IntermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		
		//Cross the Open Window
		CloseTab();
		
		//Here we are calling Search Agent Method tpo search any agent
		SearchIntermediary.SearchAgent(IntermediaryCode);
		
		//Here we are calling Product Plan 
		ProductPlan.ProductPlanTab(IntermediaryCode, rownum);*/
		
		testResult="Pass";
		}
		catch(Exception e){
			testResult="Fail";
		}
		return testResult;
	}
	
	//**************************  Negative Test case ***********************************************
	public static String fillIntermediaryForNegativeTesting2(int rownum) throws Exception
	{
		
		//Readexcel read = new Readexcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String excel=".\\TestData\\TestData_AgencyModule.xlsx";
		String fis = "Existing_Agents";
		
		
		//Here we are calling Intermediary Module Tab Method to Fill data
		testResult = IntermediaryDetails.agentsDetails(excel, fis, rownum);
		
		
		//Here We are calling Personal Tab Method to Fill data
		testResult = PersonalDetails.personalDetailsTabNegative(excel, fis, rownum);
		
		
		//Here We are Calling Other Details tab method to fill data
		testResult = OtherPersonalDetails.otherpersonaldetailstabNegative(excel, fis, rownum);
		
		//Here we are calling Payment and Financial Tab method to fill data
		testResult = PaymentandFinancial.paymentAndFinancialNegativeTestCase(excel, fis, rownum);
		
		//Here we are Calling Exam and License Tab Method to fill data
		testResult=ExamandLicense.licenceDetailsForNegativeTestcases(fis,rownum);
		
		
		//Here We are Checking For Modification to Verify Agent Bucket Status
		/*String ModificationTask=read.getCellData(fis, "Modification_Task",rownum);
		if(ModificationTask.equalsIgnoreCase("No"))
		{
			System.out.println("No Need to Do Modification.");
		}else if(ModificationTask.equalsIgnoreCase("Yes"))
		{
			CopsAgentModification.AgentModificationCops(rownum);
		}*/
		
		//verifying Assertion Criteria
		//AssertionFn.agentBopsBucketAssertionNegativeTestcase(rownum);
		
		/*//Capturing Agent Code 
		String IntermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		
		//Cross the Open Window
		CloseTab();
		
		//Here we are calling Search Agent Method tpo search any agent
		SearchIntermediary.SearchAgent(IntermediaryCode);
		
		//Here we are calling Product Plan 
		ProductPlan.ProductPlanTab(IntermediaryCode, rownum);*/
		
		testResult="Pass";
		
		return testResult;
	}
	
}































