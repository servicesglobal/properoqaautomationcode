package com.org.propero.agencymodule.modifyAgent;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.modificationFn.PersonalDetailsModification;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ModificationPersonalDetails extends BaseClass implements ProjectInterface
{
	private static String testResult=null;
	private static Logger logger1 = LoggerFactory.getLogger(ModificationPersonalDetails.class);
	
	public static String personalDetailsTab(String sheetName,String workbookname,int rowNum,String agentid) throws Exception
	{
		
		PersonalDetailsModification obj = new PersonalDetailsModification();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
		
		String newCustomerType = read.getCellData(workbookname,"Customer_Type",rowNum);
		String newTitle = read.getCellData(workbookname, "Title", rowNum);
		String newFirstName = read.getCellData(workbookname, "First_Name", rowNum);
		String newLastName=read.getCellData(workbookname, "Last_Name", rowNum);
		String newCorporatename=read.getCellData(workbookname, "Corporate_Name", rowNum);
		String newContactPerson=read.getCellData(workbookname, "Contact_Person", rowNum);
		String newDateofBirth=read.getCellData(workbookname, "DO_Birth", rowNum);
		String newPincode=read.getCellData(workbookname, "Pin_Code", rowNum);
		String newPreferredadress=read.getCellData(workbookname, "Prefferred_Address", rowNum);
		String newFathersName=read.getCellData(workbookname, "Fathers_Name", rowNum);
		
		String newOfficeAddressline1=read.getCellData(workbookname, "OffAddressLine1", rowNum);
		String newOfficeAddressline2=read.getCellData(workbookname, "OffAddressLine2", rowNum);
		String newOfficePincode=read.getCellData(workbookname, "OffPinCode", rowNum);
		String newOfficeCity=read.getCellData(workbookname, "OffCity", rowNum);
		String newOfficeLocality=read.getCellData(workbookname, "OffLocality", rowNum);
		
		String newResidentialAddress=read.getCellData(workbookname, "Residential_Address", rowNum);
		String newResidentialAddressline1=read.getCellData(workbookname, "ResAddressLine1", rowNum);
		String newResidentialAddressLine2=read.getCellData(workbookname, "ResAddressLine2", rowNum);
		String newResidentialPincode=read.getCellData(workbookname, "ResPinCode", rowNum);
		String newResidentialCity=read.getCellData(workbookname, "ResCity", rowNum);
		String newResidentialLocality=read.getCellData(workbookname, "ResLocality", rowNum);
		
		String newEmailid=read.getCellData(workbookname, "Email_Id", rowNum);
		String newMobileNumber=read.getCellData(workbookname, "Mobile_Number", rowNum);
		String newStd=read.getCellData(workbookname, "STD", rowNum);
		String newContactNumber=read.getCellData(workbookname, "Contact_Number", rowNum);
		String newSecondaryEmailid=read.getCellData(workbookname, "Secondary_Email", rowNum);
		String newSecondaryMobileNumber=read.getCellData(workbookname, "Secondary_MobileNum", rowNum);
		
		String newGstRegistrationStatus=read.getCellData(workbookname, "GST_Registration_Status", rowNum);
		String newGstRegistrationdate=read.getCellData(workbookname, "GST_Reg_Date", rowNum);
		String newGstNumber=read.getCellData(workbookname, "GSTIN_UIN_Number", rowNum);
		String newConstitutionBusiness=read.getCellData(workbookname, "Constitution_Business", rowNum);
		String newCustomerGstType=read.getCellData(workbookname, "Customer_type", rowNum);
		String newCustomerClassifiaction=read.getCellData(workbookname, "Customer_Classification", rowNum);
		
		
		obj.setCustomerType(newCustomerType);
		obj.setTitle(newTitle);
		obj.setFirstName(newFirstName);
		obj.setLastName(newLastName);
		obj.setDateofBirth(newDateofBirth);
		obj.setPincode(newPincode);
		obj.setPreferredadress(newPreferredadress);
		obj.setFathersName(newFathersName);
		obj.setCorporatename(newCorporatename);
		obj.setContactPerson(newContactPerson);
		
		obj.setOfficeAddressline1(newOfficeAddressline1);
		obj.setOfficeAddressline2(newOfficeAddressline2);
		obj.setOfficePincode(newOfficePincode);
		obj.setOfficeCity(newOfficeCity);
		obj.setOfficeLocality(newOfficeLocality);
		
		obj.setResidentialAddress(newResidentialAddress);
		obj.setResidentialAddressline1(newResidentialAddressline1);
		obj.setResidentialAddressLine2(newResidentialAddressLine2);
		obj.setResidentialPincode(newResidentialPincode);
		obj.setResidentialCity(newResidentialCity);
		obj.setResidentialLocality(newResidentialLocality);
		
		obj.setEmailid(newEmailid);
		obj.setMobileNumber(newMobileNumber);
		obj.setStd(newStd);
		obj.setContactNumber(newContactNumber);
		obj.setSecondaryEmailid(newSecondaryEmailid);
		obj.setSecondaryMobileNumber(newSecondaryMobileNumber);
		
		obj.setGstRegistrationStatus(newGstRegistrationStatus);
		obj.setGstRegistrationdate(newGstRegistrationdate);
		obj.setGstNumber(newGstNumber);
		obj.setConstitutionBusiness(newConstitutionBusiness);
		obj.setCustomerGstType(newCustomerGstType);
		obj.setCustomerClassifiaction(newCustomerClassifiaction);
		
		
		obj.intermediarydetails(workbookname, rowNum, agentid, obj.getCustomerType(), 
				obj.getTitle(), obj.getFirstName(), obj.getLastName(), obj.getDateofBirth(), obj.getPincode(), obj.getPreferredadress(), 
				obj.getFathersName(), obj.getCorporatename(), obj.getContactPerson(), obj.getOfficeAddressline1(), 
				obj.getOfficeAddressline2(), obj.getOfficePincode(), obj.getOfficeCity(), obj.getOfficeLocality(), 
				obj.getResidentialAddress(), obj.getOfficeAddressline1(), obj.getResidentialAddressLine2(), 
				obj.getResidentialPincode(), obj.getResidentialCity(), obj.getResidentialLocality(), obj.getEmailid(), 
				obj.getMobileNumber(), obj.getStd(), obj.getContactNumber(), obj.getSecondaryEmailid(), obj.getSecondaryMobileNumber(), 
				obj.getGstRegistrationStatus(), obj.getGstRegistrationdate(), obj.getGstNumber(), obj.getConstitutionBusiness(), 
				obj.getCustomerGstType(), obj.getCustomerClassifiaction());
		
		testResult="Pass";
		
		obj=null;
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		
		}
		
		catch(Exception  e)
		{
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			testResult="Fail";
			//Assert.fail("****Error in Personal Details Section****");
			throw e;
		}
		
		return testResult;
		
	}
	
}
