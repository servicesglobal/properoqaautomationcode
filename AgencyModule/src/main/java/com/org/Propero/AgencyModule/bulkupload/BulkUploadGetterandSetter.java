package com.org.propero.agencymodule.bulkupload;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;

import ch.qos.logback.classic.Logger;

public class BulkUploadGetterandSetter extends BaseClass implements ProjectInterface{
	private String IntermediaryCategory;
	private String subIntermediaryCategory;
	private String corporateAgentFilepath;
	private String corporateAgentPOSFilepath;
	private String brokerFilepath;
	private String brokerFileFilepath;
	
	public String getIntermediaryCategory(){
		return IntermediaryCategory;
	}
	public void setIntermediaryCategory(String IntermediaryCategory){
		this.IntermediaryCategory=IntermediaryCategory;
	}
	public String getsubIntermediaryCategory(){
		return subIntermediaryCategory;
	}
	public void setsubIntermediaryCategory(String subIntermediaryCategory){
		this.subIntermediaryCategory=subIntermediaryCategory;
	}
	
	public String getcorporateAgentpath(){
		return corporateAgentFilepath;
	}
	public void setcorporateAgentpath(String corporateAgentFilepath){
		this.corporateAgentFilepath=corporateAgentFilepath;
	}
	public String getcorporateAgentPOSpath(){
		return corporateAgentPOSFilepath;
	}
	public void setcorporateAgentPOSpath(String corporateAgentPOSFilepath){
		this.corporateAgentPOSFilepath=corporateAgentPOSFilepath;
	}
	public String getbrokerpath(){
		return brokerFilepath;
	}
	public void setbrokerpath(String brokerFilepath){
		this.brokerFilepath=brokerFilepath;
	}
	public String getbrokerPospath(){
		return brokerFileFilepath;
	}
	public void setbrokerPospath(String brokerFileFilepath){
		this.brokerFileFilepath=brokerFileFilepath;
	}
	
	//Function for upload excel file
	public void uploadTheFile(String pathOfFile) throws Exception{
		fluentwait(By.xpath(BrowseButton_Xpath), 20, "Browse Button not visible");
		WebElement e1=driver.findElement(By.xpath(BrowseButton_Xpath));
		e1.sendKeys(pathOfFile);
		clickOnValidateButton();
		String excpectedrecordStatus="Records validated successfully";
		String actualRecordStatus=driver.findElement(By.id(RecordStatus_ID)).getText();
		if(actualRecordStatus.contains(excpectedrecordStatus))
		{
		ErrorMessages.fieldverification("ok");
		}
		else{
			System.out.println("Record Validated not Found");
		}
		
	}
	
	//Function for click on Validate Button
	public void clickOnValidateButton() throws Exception{
		fluentwait(By.xpath(ValidateButton_Xpath), 20, "Unable to clicked on Validate Button");
		clickElement(By.xpath(ValidateButton_Xpath));
		
	}
	
	//Function for Upload files
	public void setDetailsOfBulkUpload(String SheetName, int rowNum, String IntermediaryCategory, String subIntermediarycategory,  String corporateAgentFilePath, String corporateAgentPOSFilePath, String brokerFilePath, String brokerPOSFilePath) throws Exception{
		if(IntermediaryCategory.equalsIgnoreCase("CorporateAgent") && subIntermediarycategory.equalsIgnoreCase("CorporateAgent")){
			uploadTheFile(corporateAgentFilePath);
		}
       if(IntermediaryCategory.equalsIgnoreCase("CorporateAgent") && subIntermediarycategory.equalsIgnoreCase("CorporateAgentPOS")){
    	   uploadTheFile(corporateAgentPOSFilePath);
		}
         if(IntermediaryCategory.equalsIgnoreCase("Broker") && subIntermediarycategory.equalsIgnoreCase("Broker")){
        	 uploadTheFile(brokerFilePath);
        }
       if(IntermediaryCategory.equalsIgnoreCase("Broker") && subIntermediarycategory.equalsIgnoreCase("BrokerPos")){
    	   uploadTheFile(brokerPOSFilePath);
        }
       
	}

}
