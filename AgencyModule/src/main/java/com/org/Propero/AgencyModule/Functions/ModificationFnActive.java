package com.org.propero.agencymodule.functions;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;

public class ModificationFnActive extends BaseClass implements ProjectInterface, MessageInterface {

	private static Logger logger1 = LoggerFactory.getLogger(ModificationFnActive.class);
	
	public void setModificationReason(String newModificationReason){
		Variable.ModificationReason = newModificationReason;
	}

	public String getIrdaLicenseNumber() {
		return Variable.ModificationReason;
	}

	public void setModificationRemark(String newModificationRemark){
		Variable.ModificationRemark = newModificationRemark;
	}

	public String getModificationRemark() {
		return Variable.ModificationRemark;
	}

	public void setUploadDocument(String newUploadDocument){
		Variable.UploadDocument = newUploadDocument;
	}

	public String getUploadDocument() {
		return Variable.UploadDocument;
	}

	public static void modifyagentActive(String agentId, String modificationReason, String modificationRemark,
			String uploadDocument) throws Exception {

		// Clicking on Modify Button from Agent dashboard
		fluentwait(By.xpath(ModificationButton_Xpath), 60, "Modify button not found.");
		clickElement(By.xpath(ModificationButton_Xpath));

		// Selecting Modification Reason on Ui using Excel Sheet Test Data
		if (modificationReason.equals("")) {
			logger1.info(ModificationReasonMessage);
		} else {
			clickElement(By.xpath("//select[@id='popmodreason-" + agentId + "']"));
			fluentwait(By.xpath("//select[@id='popmodreason-" + agentId + "']//option"), 60,
					"Modification Reason Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='popmodreason-" + agentId + "']//option"),
					modificationReason);
			logger1.info("Selected Modification Reason is : " + modificationReason);
		}

		// Selecting Modification Remarks on UI using Excel Sheet Test Data
		if (modificationRemark.equals("")) {
			logger1.info(ModificationRemarksMessage);
		} else {
			enterText(By.xpath("//textarea[@id='modificationRemarkPopup-" + agentId + "']"), modificationRemark);
			logger1.info("Entered Modification Remark is : " + modificationRemark);
		}

		// Upload Document fn
		if(uploadDocument.equals(""))
		{
			logger1.info(UploadDocumentMessage);
		}else{
			
			
		}
		
	}

}
