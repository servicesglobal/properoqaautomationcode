package com.org.propero.agencymodule.addagent;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class SearchIntermediary extends BaseClass implements ProjectInterface
{

	public static Logger logger1 = LoggerFactory.getLogger(SearchIntermediary.class);
	
	public static void searchAgent(String intermediaryCode)
	{
		
		try{
			//Click on Channel Management
			fluentwait(By.xpath(ChannelManagement_Xpath), 60, "Page is loading Slow so couldn't click on Channel Management.");
			clickElement(By.xpath(ChannelManagement_Xpath));
			
			//Click on Search Intermediary
			clickElement(By.xpath(Search_Intermediary_Xpath));
			
			//Search Agent Using Search Criteria
			enterText(By.xpath(Intermediary_Code_Xpath), intermediaryCode);
			
			//Clicking on Search Button
			clickElement(By.xpath(SearchBtn_Xpath));
			
			//Opening Agent from Search
			fluentwait(By.xpath("//a[contains(text(),"+"'"+intermediaryCode+"'"+")]"), 60, "Couldn't find Agent in Search");
			clickElement(By.xpath("//a[contains(text(),"+"'"+intermediaryCode+"'"+")]"));
			
			
		}catch(Exception e)
		{
			logger1.info(e.getMessage());
			logger.log(LogStatus.FAIL, e);
		}
		
		
	}
	
	
}
