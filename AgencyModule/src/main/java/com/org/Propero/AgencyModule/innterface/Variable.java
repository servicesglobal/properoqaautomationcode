package com.org.propero.agencymodule.innterface;

public class Variable 
{

	//Pesonal Details Tab Variables
	public static String CustomerType=null;
	public static String Title=null;
	public static String FirstName=null;
	public static String LastName=null;
	public static String DateofBirth=null;
	public static String Pincode=null;
	public static String Preferredadress=null;
	public static String FathersName=null;
	public static String OfficeAddressline1=null;
	public static String OfficeAddressline2=null;
	public static String OfficePincode=null;
	public static String OfficeCity=null;
	public static String officeState=null;
	public static String OfficeLocality=null;
	public static String ResidentialAddress=null;
	public static String ResidentialAddressline1=null;
	public static String ResidentialAddressLine2=null;
	public static String ResidentialPincode=null;
	public static String ResidentialCity=null;
	public static String ResidentialState=null;
	public static String ResidentialLocality=null;
	public static String Emailid=null;
	public static String MobileNumber=null;
	public static String Std=null;
	public static String ContactNumber=null;
	public static String SecondaryEmailid=null;
	public static String SecondaryMobileNumber=null;
	public static String GstRegistrationStatus=null;
	public static String GstRegistrationdate=null;
	public static String GstNumber=null;
	public static String ConstitutionBusiness=null;
	public static String CustomerGstType=null;
	public static String CustomerClassifiaction=null;
	public static String Corporatename=null;
	public static String ContactPerson=null;
	public static String DOI=null;
	
	
	
	//Other Persoanl Details Variables
	public static String NomineeTitle=null;
	public static String NomineeFirstname=null;
	public static String NomineeLastname=null;
	public static String NomineeRelation=null;
	public static String NomineePan=null;
	public static String NomineeAge=null;
	public static String PaymentDisbursmentMode=null;
	public static String TDS=null;
	public static String IFSCCode=null;
	public static String BankAccountNumber=null;
	public static String MicrNumber=null;
	public static String Bankname=null;
	public static String BranchName=null;
	public static String BranchState=null;
	public static String BranchCity=null;
	public static String RcdEditable=null;
	public static String EmployeeCode=null;
	public static String TotalExperience=null;
	public static String PassportNumber=null;
	public static String MarriageDate=null;
	public static String NumberofChildren=null;
	public static String YearofStay=null;
	public static String Education=null;
	public static String Occupation=null;
	public static String WorkLocation=null;
	public static String Languageknown=null;
	public static String SpouseTitle=null;
	public static String SpouseFname=null;
	public static String SpouseLName=null;
	public static String SpouseQualification=null;
	public static String SpouseOccupation=null;
	public static String AssociateEntityName=null;
	public static String AssociateEntityDetails=null;
	public static String ReferenceName=null;
	public static String ReferenceAddress=null;
	public static String ReferenceContactNumber=null;
	public static String ReferenceRelationShip=null;
	
	
	//Payment and Financial Tab
	public static String PayTo=null;
	public static String ClawbackFrom=null;
	public static String PaymentFrequency=null;
	public static String FinPaymentDisbursmentMode=null;
	public static String ServiceTaxnumber=null;
	public static String FinTds=null;	
	public static String LowTds=null;
	public static String FinPanNumber=null;
	public static String AadhaarCardNumber=null;
	public static String FinIfscNumber=null;
	public static String FinBankAccountNumber=null;
	public static String FinMicrNumber=null;
	public static String FinBank=null;
	public static String FinBranchName=null;
	public static String EffectiveFrom=null;
	public static String EffectiveTo=null;
	public static String FinState=null;
	public static String FinCity=null;
	
	
	//Exam and License Training
	public static String IrdaLicenseNumber=null;
	public static String LicenseTypeCode=null;
	public static String Companyassociatewith=null;
	public static String LicenseState=null;
	public static String LicenseCity=null;
	public static String LicenseBranch=null;
	public static String LicenseIssueDate=null;
	public static String LicenseEndDate=null;
	public static String IntermediaryStartDate=null;
	public static String IntermediaryEndDate=null;
	public static String NoofPolicies=null;
	public static String PremiumAmount=null;
	public static String RecruitmentSource=null;
	public static String CertificateCourse=null;
	public static String KnowReligareEmployee=null;
	public static String TrainingStartDate=null;
	public static String TrainingEndDate=null;
	public static String AgentEducation=null;
	public static String BoardName=null;
	public static String RollNumber=null;
	public static String PassingYear=null;
	public static String ExamCenter=null;
	public static String Language=null;
	public static String TentativeExamdate=null;
	public static String FinalExamDate=null;
	public static String TimeSlot=null;
	public static String Result=null;
	public static String ExamRoll=null;
	public static String Marks=null;
	
	//Modification after Active
	public static String ModificationReason=null;
	public static String ModificationRemark=null;
	public static String UploadDocument=null;
	
	
	
}
