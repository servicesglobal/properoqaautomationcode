package com.org.propero.agencymodule.modifyAgent;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.modificationFn.PaymentandFinancialModification;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ModificationPaymentandFinancial extends BaseClass
{
	private static String testResult=null;
	private static Logger logger1 = LoggerFactory.getLogger(ModificationPaymentandFinancial.class);
	
	public static String paymentAndFinancial(String sheetName,String workbookname,int rowNum,String agentid) throws Exception
	{
		
		PaymentandFinancialModification obj = new PaymentandFinancialModification();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
		
		String newPayTo = read.getCellData(workbookname,"Pay_To",rowNum);
		String newClawbackFrom = read.getCellData(workbookname,"Clawback_From",rowNum);
		String newPaymentFrequency = read.getCellData(workbookname,"Payment_Frequency",rowNum);
		String newFinPaymentDisbursmentMode = read.getCellData(workbookname,"PaymentDisbursement_Mode",rowNum);
		String newServiceTaxnumber = read.getCellData(workbookname,"Service_Tax",rowNum);
		String newFinTds = read.getCellData(workbookname,"TDS_Sel",rowNum);
		String newLowTds = read.getCellData(workbookname,"Low_TDS",rowNum);
		String newFinPanNumber = read.getCellData(workbookname,"PAN_Number",rowNum);
		String newAadhaarCardNumber = read.getCellData(workbookname,"AadharCard_Number",rowNum);
		String newFinIfscNumber = read.getCellData(workbookname,"PayIFSC_Code",rowNum);
		String newFinBankAccountNumber = read.getCellData(workbookname,"PayBank_Account_Number",rowNum);
		String newFinMicrNumber = read.getCellData(workbookname,"PayMICR_Number",rowNum);
		String newFinBank = read.getCellData(workbookname,"PayBank",rowNum);
		String newFinBranchName = read.getCellData(workbookname,"PayBranch_Name",rowNum);
		String newEffectiveFrom = read.getCellData(workbookname,"PayEffectivefrom",rowNum);
		String newEffectiveTo = read.getCellData(workbookname,"PayEffectiveTo",rowNum);
		String newFinState = read.getCellData(workbookname,"PayState_Name",rowNum);
		String newFinCity = read.getCellData(workbookname,"PayCity_Name",rowNum);
		
		obj.setPayTo(newPayTo);
		obj.setClawbackFrom(newClawbackFrom);
		obj.setPaymentFrequency(newPaymentFrequency);
		obj.setFinPaymentDisbursmentMode(newFinPaymentDisbursmentMode);
		obj.setServiceTaxnumber(newServiceTaxnumber);
		obj.setFinTds(newFinTds);
		obj.setLowTds(newLowTds);
		obj.setFinPanNumber(newFinPanNumber);
		obj.setAadhaarCardNumber(newAadhaarCardNumber);
		obj.setFinIfscNumber(newFinIfscNumber);
		obj.setFinBankAccountNumber(newFinBankAccountNumber);
		obj.setFinMicrNumber(newFinMicrNumber);
		obj.setFinBank(newFinBank);
		obj.setFinBranchName(newFinBranchName);
		obj.setEffectiveFrom(newEffectiveFrom);
		obj.setEffectiveTo(newEffectiveTo);
		obj.setFinState(newFinState);
		obj.setFinCity(newFinCity);
	
		obj.paymentAndFinancialTab(agentid,obj.getPayTo(), obj.getClawbackFrom(), obj.getPaymentFrequency(), obj.getFinPaymentDisbursmentMode(), 
				obj.getServiceTaxnumber(), obj.getFinTds(), obj.getLowTds(), obj.getFinPanNumber(), obj.getAadhaarCardNumber(), 
				workbookname, rowNum, obj.getFinIfscNumber(), obj.getFinBankAccountNumber(), 
				obj.getFinMicrNumber(), obj.getFinBank(), obj.getFinBranchName(), obj.getEffectiveFrom(), 
				obj.getEffectiveTo(), obj.getFinState(), obj.getFinCity());
		testResult="Pass";
		obj=null;
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		}catch(Exception  e)
		{
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			testResult="Fail";
			//Assert.fail("****Error in Payment and Financial Section****");
			throw e;
		}
	
		return testResult;
	
	}
	
	

	
	
}
