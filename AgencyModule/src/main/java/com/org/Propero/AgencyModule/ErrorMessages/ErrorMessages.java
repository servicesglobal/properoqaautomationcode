package com.org.propero.agencymodule.errormessages;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ErrorMessages extends BaseClass implements ProjectInterface
{
	private static String testResult;
	private static Logger logger1 = LoggerFactory.getLogger(ErrorMessages.class);
	
	public static String fieldverification(String fieldName) throws Exception
	{
		    Thread.sleep(5000);
			ImplicitWait(2);
			
			Boolean iselementpresent = driver.findElements(By.id("swal2-title")).size()!= 0;

			if(iselementpresent==true)
			{
				String title = driver.findElement(By.id("swal2-title")).getText();
				String actualMessage= driver.findElement(By.id("swal2-content")).getText();
				logger.log(LogStatus.PASS, "Bucket Message : "+title+" - "+actualMessage);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Length Validation ")));
				clickElement(By.xpath(Popup_OK_Xpath));
				logger1.info("Bucket Message : "+actualMessage);	
				//Assert.fail(actualMessage);
			
			}
			else
			{
				logger1.info(fieldName+" Filled Properly.");		
				testResult="Pass";
			}
			
			return testResult;

	}

	public static void agentExistverification() throws Exception
	{
		
		ImplicitWait(2);
		Boolean iselementpresent = driver.findElements(By.id("swal2-title")).size()!= 0;
		
		if(iselementpresent==true)
		{
			
			fluentwait(By.id("swal2-title"), 2, "ABCD");
			String title = driver.findElement(By.id("swal2-title")).getText();
			if(title.contains("Email Format"))
			{
				String actualMessage= driver.findElement(By.id("swal2-content")).getText();
				logger1.info(title+ " " +actualMessage);
				logger.log(LogStatus.FAIL, title+ " " +actualMessage);
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Validation Error - ")));
				clickElement(By.xpath(Popup_OK_Xpath));
				
			}else
			{
			String actualMessage= driver.findElement(By.id("swal2-content")).getText();
			logger1.info(title+ " " +actualMessage);
			clickElement(By.xpath(Popup_OK_Xpath));
			}
			}
		else
		{
			logger1.info("Eamil / Cell Number Filled Properoly.");
		}
		
	}

	public static void recordverification(String fieldName) throws Exception
	{
			ImplicitWait(2);
			
			Boolean iselementpresent = driver.findElements(By.id("swal2-title")).size()!= 0;

			if(iselementpresent==true)
			{
				//String title = driver.findElement(By.id("swal2-title")).getText();
				String actualMessage= driver.findElement(By.id("swal2-content")).getText();
				clickElement(By.xpath(Popup_OK_Xpath));
				logger1.info(actualMessage);	
	
			}
			else
			{
				logger1.info(fieldName+" Filled Properly.");			
			}

	}

public static String selectagentError() throws Exception
{		
		Boolean selectVal = driver.findElements(By.xpath(SelectAgentError_Xpath)).size()!=0;
		if(selectVal==true)
		{
			String errorMes = driver.findElement(By.xpath(SelectAgentError_Xpath)).getText();
			logger1.info("Getting Error Message : "+errorMes);
			logger.log(LogStatus.PASS, "Please Enter some data in Excel against Agent Type beacuse we are gettig : "+errorMes);
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Agent Type Blank")));
			testResult="Pass";
		}
		else if(selectVal==false)
		{
			
		}
		return testResult;
}
	
public static String duplicatepartycheck(String fieldName) throws Exception
{
		ImplicitWait(2);
		
		Boolean iselementpresent = driver.findElements(By.id("swal2-title")).size()!= 0;

		if(iselementpresent==true)
		{
			//String title = driver.findElement(By.id("swal2-title")).getText();
			fluentwait(By.id("swal2-content"), 60, "Unable to Read message.");
			String actualMessage= driver.findElement(By.id("swal2-content")).getText();
			fluentwait(By.xpath(Popup_OK_Xpath), 60, "Unable to click on Ok button due to Time out.");
			clickElement(By.xpath(Popup_OK_Xpath));
			logger1.info("Getting Message : "+actualMessage);	
			testResult="Pass";
		}
		else
		{
			
			fluentwait(By.xpath(DuplicatepartyCheck_Xpath), 60, "DuplicatepartyCheck");
				//String DuplicateCheck = driver.findElement(By.xpath(DuplicatepartyCheck_Xpath)).getText();
				fluentwait(By.xpath(SelectDuplicatePartyCheck_Xpath), 60, "Unable to select Duplicate party Check button.");
				clickElement(By.xpath(SelectDuplicatePartyCheck_Xpath));
				fluentwait(By.xpath(DuplictaePartyOk_btn_Xpath), 60, "Unable to check Duplicate party beacuse cordys is down.");
				clickElement(By.xpath(DuplictaePartyOk_btn_Xpath));
				testResult="Pass";
			
			
		}
		
		return testResult;

}	

//******************** Negative Functions ******************************************************
  public static String duplicatepartycheckNegative(String fieldName) throws Exception
 {
	    Thread.sleep(5000);
		ImplicitWait(2);
		
		Boolean iselementpresent = driver.findElements(By.id("swal2-title")).size()!= 0;

		if(iselementpresent==true)
		{
			//String title = driver.findElement(By.id("swal2-title")).getText();
			fluentwait(By.id("swal2-content"), 60, "Unable to Read message.");
			String actualMessage= driver.findElement(By.id("swal2-content")).getText();
			fluentwait(By.xpath(Popup_OK_Xpath), 60, "Unable to click on Ok button due to Time out.");
			clickElement(By.xpath(Popup_OK_Xpath));
			logger1.info("Getting Message : "+actualMessage);	
			testResult="Pass";
		}
		else
		{
			Thread.sleep(5000);
			Boolean iselementpresent1 = driver.findElements(By.xpath(DuplicatepartyCheck_Xpath)).size()!= 0;
			if(iselementpresent1==true){
				fluentwait(By.xpath(DuplicatepartyCheck_Xpath), 60, "DuplicatepartyCheck");
				//String DuplicateCheck = driver.findElement(By.xpath(DuplicatepartyCheck_Xpath)).getText();
				fluentwait(By.xpath(SelectDuplicatePartyCheck_Xpath), 60, "Unable to select Duplicate party Check button.");
				clickElement(By.xpath(SelectDuplicatePartyCheck_Xpath));
				fluentwait(By.xpath(DuplictaePartyOk_btn_Xpath), 60, "Unable to check Duplicate party beacuse cordys is down.");
				clickElement(By.xpath(DuplictaePartyOk_btn_Xpath));
				testResult="Pass";
			}
			else{
				logger1.info(fieldName+" Filled Properly.");	
			}
			
		}
		
		return testResult;

}	

}
