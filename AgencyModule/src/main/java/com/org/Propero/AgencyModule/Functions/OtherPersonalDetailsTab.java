package com.org.propero.agencymodule.functions;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.basefunctions.ValidExpression;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class OtherPersonalDetailsTab extends BaseClass implements ProjectInterface, MessageInterface {

	private static Logger logger1 = LoggerFactory.getLogger(OtherPersonalDetailsTab.class);
	
	public void setNomineeTitle(String newNomineeTitle) {
		Variable.NomineeTitle = newNomineeTitle;
	}

	public String getNomineeTitle() {
		return Variable.NomineeTitle;
	}
	
	public void setNomineeFirstname(String newNomineeFirstname) {
		Variable.NomineeFirstname = newNomineeFirstname;
	}

	public String getNomineeFirstname() {
		return Variable.NomineeFirstname;
	}

	public void setNomineeLastname(String newNomineeLastname) {
		Variable.NomineeLastname = newNomineeLastname;
	}

	public String getNomineeLastname() {
		return Variable.NomineeLastname;
	}
	
	public void setNomineeRelation(String newNomineeRelation) {
		Variable.NomineeRelation = newNomineeRelation;
	}

	public String getNomineeRelation() {
		return Variable.NomineeRelation;
	}
	
	public void setNomineePan(String newNomineePan) {
		Variable.NomineePan = newNomineePan;
	}

	public String getNomineePan() {
		return Variable.NomineePan;
	}
	
	public void setNomineeAge(String newNomineeAge) {
		Variable.NomineeAge = newNomineeAge;
	}

	public String getNomineeAge() {
		return Variable.NomineeAge;
	}
	
	public void setPaymentDisbursmentMode(String newPaymentDisbursmentMode) {
		Variable.PaymentDisbursmentMode = newPaymentDisbursmentMode;
	}

	public String getPaymentDisbursmentMode() {
		return Variable.PaymentDisbursmentMode;
	}

	public void setTDS(String newTDS) {
		Variable.TDS = newTDS;
	}

	public String getTDS() {
		return Variable.TDS;
	}

	public void setIFSCCode(String newIFSCCode) {
		Variable.IFSCCode = newIFSCCode;
	}

	public String getIFSCCode() {
		return Variable.IFSCCode;
	}

	public void setBankAccountNumber(String newBankAccountNumber) {
		Variable.BankAccountNumber = newBankAccountNumber;
	}

	public String getBankAccountNumber() {
		return Variable.BankAccountNumber;
	}

	public void setMicrNumber(String newMicrNumber) {
		Variable.MicrNumber = newMicrNumber;
	}

	public String getMicrNumber() {
		return Variable.MicrNumber;
	}

	public void setBankname(String newBankname) {
		Variable.Bankname = newBankname;
	}

	public String getBankname() {
		return Variable.Bankname;
	}

	public void setBranchName(String newBranchName) {
		Variable.BranchName = newBranchName;
	}

	public String getBranchName() {
		return Variable.BranchName;
	}

	public void setBranchState(String newBranchState) {
		Variable.BranchState = newBranchState;
	}

	public String getBranchState() {
		return Variable.BranchState;
	}

	public void setBranchCity(String newBranchCity) {
		Variable.BranchCity = newBranchCity;
	}

	public String getBranchCity() {
		return Variable.BranchCity;
	}

	public void setRcdEditable(String newRcdEditable) {
		Variable.RcdEditable = newRcdEditable;
	}

	public String getRcdEditable() {
		return Variable.RcdEditable;
	}

	public void setEmployeeCode(String newEmployeeCode) {
		Variable.EmployeeCode = newEmployeeCode;
	}

	public String getEmployeeCode() {
		return Variable.EmployeeCode;
	}

	public void setTotalExperience(String newTotalExperience) {
		Variable.TotalExperience = newTotalExperience;
	}

	public String getTotalExperience() {
		return Variable.TotalExperience;
	}

	public void setPassportNumber(String newPassportNumber) {
		Variable.PassportNumber = newPassportNumber;
	}

	public String getPassportNumber() {
		return Variable.PassportNumber;
	}

	public void setMarriageDate(String newMarriageDate) {
		Variable.MarriageDate = newMarriageDate;
	}

	public String getMarriageDate() {
		return Variable.MarriageDate;
	}

	public void setNumberofChildren(String newNumberofChildren) {
		Variable.NumberofChildren = newNumberofChildren;
	}

	public String getNumberofChildren() {
		return Variable.NumberofChildren;
	}

	public void setYearofStay(String newYearofStay) {
		Variable.YearofStay = newYearofStay;
	}

	public String getYearofStay() {
		return Variable.YearofStay;
	}

	public void setEducation(String newEducation) {
		Variable.Education = newEducation;
	}

	public String getEducation() {
		return Variable.Education;
	}

	public void setOccupation(String newOccupation) {
		Variable.Occupation = newOccupation;
	}

	public String getOccupation() {
		return Variable.Occupation;
	}

	public void setWorkLocation(String newWorkLocation) {
		Variable.WorkLocation = newWorkLocation;
	}

	public String getWorkLocation() {
		return Variable.WorkLocation;
	}

	public void setLanguageknown(String newLanguageknown) {
		Variable.Languageknown = newLanguageknown;
	}

	public String getLanguageknown() {
		return Variable.Languageknown;
	}

	public void setSpouseTitle(String newSpouseTitle) {
		Variable.SpouseTitle = newSpouseTitle;
	}

	public String getSpouseTitle() {
		return Variable.SpouseTitle;
	}

	public void setSpouseFname(String newSpouseFname) {
		Variable.SpouseFname = newSpouseFname;
	}

	public String getSpouseFname() {
		return Variable.SpouseFname;
	}

	public void setSpouseLName(String newSpouseLName) {
		Variable.SpouseLName = newSpouseLName;
	}

	public String getSpouseLName() {
		return Variable.SpouseLName;
	}

	public void setSpouseQualification(String newSpouseQualification) {
		Variable.SpouseQualification = newSpouseQualification;
	}

	public String getSpouseQualification() {
		return Variable.SpouseQualification;
	}

	public void setSpouseOccupation(String newSpouseOccupation) {
		Variable.SpouseOccupation = newSpouseOccupation;
	}

	public String getSpouseOccupation() {
		return Variable.SpouseOccupation;
	}

	public void setAssociateEntityName(String newAssociateEntityName) {
		Variable.AssociateEntityName = newAssociateEntityName;
	}

	public String getAssociateEntityName() {
		return Variable.AssociateEntityName;
	}

	public void setAssociateEntityDetails(String newAssociateEntityDetails) {
		Variable.AssociateEntityDetails = newAssociateEntityDetails;
	}

	public String getAssociateEntityDetails() {
		return Variable.AssociateEntityDetails;
	}

	public void setReferenceName(String newReferenceName) {
		Variable.ReferenceName = newReferenceName;
	}

	public String getReferenceName() {
		return Variable.ReferenceName;
	}

	public void setReferenceAddress(String newReferenceAddress) {
		Variable.ReferenceAddress = newReferenceAddress;
	}

	public String getReferenceAddress() {
		return Variable.ReferenceAddress;
	}

	public void setReferenceContactNumber(String newReferenceContactNumber) {
		Variable.ReferenceContactNumber = newReferenceContactNumber;
	}

	public String getReferenceContactNumber() {
		return Variable.ReferenceContactNumber;
	}

	public void setReferenceRelationShip(String newReferenceRelationShip) {
		Variable.ReferenceRelationShip = newReferenceRelationShip;
	}

	public String getReferenceRelationShip() {
		return Variable.ReferenceRelationShip;
	}

	public void otherpersonalDetails(String nomineeTitle, String nomineeFirstname, String nomineeLastname,
			String nomineeRelation, String nomineePan, String nomineeAge,String paymentDisbursmentMode,String tDS,String iFSCCode,String bankAccountNumber,
			String micrNumber, String bankname,String branchName,String branchState, String branchCity,int rownum) throws Exception 
	{

		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		String customerType = read.getCellData(workbookName,"Customer_Type",rownum);
		
		//Clicking on Other Personal tab
		clickElement(By.xpath(OtherPersonalDetails_Tab_Xpath));
		
		
		
		if(customerType.equalsIgnoreCase("Corporate"))
		{
		 System.out.println("No Need to Fill Other Personal Details Section");
		}
		else{
			verticalscrollup();
			//Calling Nominee Details Method to Fill all details in Other Personal Details Tab
			OtherPersonalDetailsTab.nomineeDetails(nomineeTitle, nomineeFirstname, nomineeLastname, nomineeRelation, nomineePan, nomineeAge);
			
			//Calling Nominee Account Details Method to Fill all details in Other Personal Details Tab
			OtherPersonalDetailsTab.nomineeAccountDetails(paymentDisbursmentMode, tDS, iFSCCode, bankAccountNumber, micrNumber, bankname, branchName, branchState, branchCity);
		}
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Others Personal Details Tab")));
	
	}

	public static void nomineeDetails(String nomineeTitle, String nomineeFirstname, String nomineeLastname,
			String nomineeRelation, String nomineePan, String nomineeAge) throws Exception {

		// Selecting Nominee Title on UI in Other Personal Details Tab
		if (nomineeTitle.equals("")) {
			logger1.info(NOMINEETITLEMESSAGE);
		} else {
			clickElement(By.xpath(NomineeTitle_Xpath));
			fluentwait(By.xpath(NomineeTitleOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(NomineeTitleOption_Xpath), nomineeTitle);
			logger1.info("Selected Intermediary Title is : " + nomineeTitle);
		}

		// Entering Nominee First Name on UI in Other Personal Details Tab
		if (nomineeFirstname.equals("")) {
			logger1.info(NOMINEEFIRSTNAMEMESSAGE);
		} else {

			enterText(By.xpath(NomineeName_Xpath), nomineeFirstname);
			if (nomineeFirstname instanceof String && nomineeFirstname.length() <= 60) {
				logger1.info("Nominee First Name is : " + nomineeFirstname);
			} else {
				logger1.info("Nominee First Name is : " + nomineeFirstname);
				ErrorMessages.fieldverification("Nominee First Name");
			}
		}

		// Entering Nominee Last Name on UI in Other Personal Details Tab
		if (nomineeLastname.equals("")) {
			logger1.info(NOMINEELASTNAMEMESSAGE);
		} else {

			enterText(By.xpath(NomineeLastname_Xapth), nomineeLastname);
			if (nomineeLastname instanceof String && nomineeLastname.length() <= 60) {
				logger1.info("Nominee Last Name is : " + nomineeLastname);
			} else {
				logger1.info("Nominee Last Name is : " + nomineeLastname);
				ErrorMessages.fieldverification("Nominee Last Name");
			}
		}

		// Selecting Nominee Relation on UI in Other Personal Details Tab
		if (nomineeRelation.equals("")) {
			logger1.info(NOMINEERELATIONMESSAGE);
		} else {
			clickElement(By.xpath(Relation_Xpath));
			fluentwait(By.xpath(RelationOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(RelationOption_Xpath), nomineeRelation);
			logger1.info("Selected Nominee Relation is : " + nomineeRelation);
		}

		// Entering Pan Number on UI in other Personal Details Tab
		if (nomineePan.equals("")) {
			logger1.info(NOMINEEPANMESSAGE);
		} else {
			if (ValidExpression.isValidPanNumber(nomineePan)) {
				enterText(By.xpath(PanNumber_Xpath), nomineePan);
				logger1.info("Entered Nominee Pan Number is : " + nomineePan);
			} else {
				logger1.info(VALIDPANNUMBER);
				ErrorMessages.fieldverification("Nominee Pan Number");
			}
		}

		// Entering Nominee Age on UI in other Personal Details Tab
		if (nomineeAge.equals("")) {
			logger1.info(NOMINEEAGEMESSAGE);
		} else {
			enterText(By.xpath(NomineeAge_Xpath), nomineeAge);
			logger1.info("Entered Nominee Age is : " + nomineeAge);
		}		

	}

	public static void nomineeAccountDetails(String paymentDisbursmentMode,String tDS,String iFSCCode,String bankAccountNumber,
			String micrNumber, String bankname,String branchName,String branchState, String branchCity) throws Exception
	{
		
		//Selecting Payment Disbursement Mode
		if(paymentDisbursmentMode.equals(""))
		{
			logger1.info(PAYMENTDISBURSMENTMODEMESSAGE);
		}else if(paymentDisbursmentMode.equalsIgnoreCase("Cheque")){
		
			
			clickElement(By.xpath(PaymentDisbursementMode_Xpath));
			fluentwait(By.xpath(PaymentDisbursementModeOption_Xpath), 60,
					"Payment Disbursement Mode Option is Loading Slow so Couldn't find Payment Disbursment Mode.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentDisbursementModeOption_Xpath), paymentDisbursmentMode);
			logger1.info("Selected Payment Disbursment Mode is : " + paymentDisbursmentMode);	
			
			//Selecting TDS Details
			if(tDS.equals("")){
				logger1.info(TDSMESSAGE);
			}else{
				clickElement(By.xpath(TDS_Xpath));
				fluentwait(By.xpath(TDSOption_Xpath), 60,
						"Tds Option is Loading Slow so Couldn't find Tds Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(TDSOption_Xpath), tDS);
				logger1.info("Selected Tds Details is : " + tDS);	
			}

				
		}else if(paymentDisbursmentMode.equalsIgnoreCase("Direct Credit"))
		{
			
			clickElement(By.xpath(PaymentDisbursementMode_Xpath));
			fluentwait(By.xpath(PaymentDisbursementModeOption_Xpath), 60,
					"Payment Disbursement Mode Option is Loading Slow so Couldn't find Payment Disbursment Mode.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentDisbursementModeOption_Xpath), paymentDisbursmentMode);
			logger1.info("Selected Payment Disbursment Mode is : " + paymentDisbursmentMode);	
			
			//Selecting TDS Details
			if(tDS.equals("")){
				logger1.info(TDSMESSAGE);
			}else{
				Thread.sleep(2000);
				clickElement(By.xpath(TDS_Xpath));
				fluentwait(By.xpath(TDSOption_Xpath), 60,"Tds Option is Loading Slow so Couldn't find Tds Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(TDSOption_Xpath), tDS);
				logger1.info("Selected Tds Details is : " + tDS);	
			}
			
			//Entering IFSC Code on UI in Nominee Account Details Section in Other Personal Details Tab
			if(iFSCCode.equals(""))
			{
				logger1.info(IFSCCODEMESSAGE);
			}
			else{
				
				if(ValidExpression.isValidIfscCode(iFSCCode)){
				enterText(By.xpath(IFSC_Code_Xpath), iFSCCode);
				logger1.info("Entered IFSC Code is : "+iFSCCode);
				}else{
					logger1.info("Entered IFSC Code is : "+iFSCCode);
					ErrorMessages.fieldverification("IFSC Code");
				}
				
		}
			
			//Entering Bank Account Number on UI in Nominee Account Details Section in Other Personal Details Tab
			if(bankAccountNumber.equals(""))
			{
				logger1.info(BANKACCOUNTNUMBERMESSAGE);
			}else{
				enterText(By.xpath(BankAccountNumber_Xpath), bankAccountNumber);
				logger1.info("Entered Bank Account Number is : "+bankAccountNumber);
				ErrorMessages.fieldverification("Bank Account Number");
			}
			
			//Entering MICR Number on UI in Nominee Account Details Section in Other Personal Details Tab
			if(micrNumber.equals(""))
			{
				logger1.info(MICRNUMBERMESSAGE);
			}else{
				if(ValidExpression.isvalidMicrNumber(micrNumber))
				{
					enterText(By.xpath(MICRNumber_Xpath), micrNumber);
					logger1.info("Entered MICR Number is : "+micrNumber);
				}else{
					logger1.info("Entered MICR Number is : "+micrNumber);
					ErrorMessages.fieldverification("Micr Number");
				}
			}
			
			//Entering Bank Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(bankname.equals(""))
			{
				logger1.info(BANKNAMEMESSAGE);
			}else{
				enterText(By.xpath(Bank_Xpath), bankname);
				logger1.info("Entered Bank Name is : "+bankname);
			}
			
			//Entering Branch Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchName.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				enterText(By.xpath(BranchName_Xpath), branchName);
				logger1.info("Entered Bank Name is : "+branchName);
			}
			
			
			//Entering Branch Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchName.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				enterText(By.xpath(BranchName_Xpath), branchName);
				logger1.info("Entered Bank Name is : "+branchName);
			}
			
			//Selecting State Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchState.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				clickElement(By.xpath(State_Xpath));
				fluentwait(By.xpath(StateOption_Xpath), 60,
						"State Option is Loading Slow so Couldn't find State Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(StateOption_Xpath), branchState);
				logger1.info("Selected State is : " + branchState);	
			}
			
			//Selecting City Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchCity.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				Thread.sleep(5000);
				clickElement(By.xpath(City_Xpath));
				fluentwait(By.xpath(CityOption_Xpath), 60,
						"City Option is Loading Slow so Couldn't find City Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(CityOption_Xpath), branchCity);
				logger1.info("Selected City is : " + branchCity);	
			}
			
		}	
		
	}
	
	//******************* Negative Test Cases ***************************************
	public void otherpersonalDetailsNegative(String nomineeTitle, String nomineeFirstname, String nomineeLastname,
			String nomineeRelation, String nomineePan, String nomineeAge,String paymentDisbursmentMode,String tDS,String iFSCCode,String bankAccountNumber,
			String micrNumber, String bankname,String branchName,String branchState, String branchCity) throws Exception 
	{

		verticalscrollup();
		
		//Clicking on Other Personal tab
		clickElement(By.xpath(OtherPersonalDetails_Tab_Xpath));
		
		//Calling Nominee Details Method to Fill all details in Other Personal Details Tab
		OtherPersonalDetailsTab.nomineeDetailsNegative(nomineeTitle, nomineeFirstname, nomineeLastname, nomineeRelation, nomineePan, nomineeAge);
		
		//Calling Nominee Account Details Method to Fill all details in Other Personal Details Tab
		OtherPersonalDetailsTab.nomineeAccountDetailsNegative(paymentDisbursmentMode, tDS, iFSCCode, bankAccountNumber, micrNumber, bankname, branchName, branchState, branchCity);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Others Personal Details Tab")));
	}

	public static void nomineeDetailsNegative(String nomineeTitle, String nomineeFirstname, String nomineeLastname,
			String nomineeRelation, String nomineePan, String nomineeAge) throws Exception {

		// Selecting Nominee Title on UI in Other Personal Details Tab
		if (nomineeTitle.equals("")) {
			logger1.info(NOMINEETITLEMESSAGE);
		} else {
			clickElement(By.xpath(NomineeTitle_Xpath));
			fluentwait(By.xpath(NomineeTitleOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(NomineeTitleOption_Xpath), nomineeTitle);
			logger1.info("Selected Intermediary Title is : " + nomineeTitle);
		}

		// Entering Nominee First Name on UI in Other Personal Details Tab
		if (nomineeFirstname.equals("")) {
			logger1.info(NOMINEEFIRSTNAMEMESSAGE);
		} else {

			enterText(By.xpath(NomineeName_Xpath), nomineeFirstname);
			if (nomineeFirstname instanceof String && nomineeFirstname.length() <= 60) {
				logger1.info("Nominee First Name is : " + nomineeFirstname);
				ErrorMessages.fieldverification("Nominee First Name");
			} else {
				logger1.info("Nominee First Name is : " + nomineeFirstname);
				ErrorMessages.fieldverification("Nominee First Name");
			}
		}

		// Entering Nominee Last Name on UI in Other Personal Details Tab
		if (nomineeLastname.equals("")) {
			logger1.info(NOMINEELASTNAMEMESSAGE);
		} else {

			enterText(By.xpath(NomineeLastname_Xapth), nomineeLastname);
			if (nomineeLastname instanceof String && nomineeLastname.length() <= 60) {
				logger1.info("Nominee Last Name is : " + nomineeLastname);
				ErrorMessages.fieldverification("Nominee Last Name");
			} else {
				logger1.info("Nominee Last Name is : " + nomineeLastname);
				ErrorMessages.fieldverification("Nominee Last Name");
			}
		}

		// Selecting Nominee Relation on UI in Other Personal Details Tab
		if (nomineeRelation.equals("")) {
			logger1.info(NOMINEERELATIONMESSAGE);
		} else {
			clickElement(By.xpath(Relation_Xpath));
			fluentwait(By.xpath(RelationOption_Xpath), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(RelationOption_Xpath), nomineeRelation);
			logger1.info("Selected Nominee Relation is : " + nomineeRelation);
		}

		// Entering Pan Number on UI in other Personal Details Tab
		if (nomineePan.equals("")) {
			logger1.info(NOMINEEPANMESSAGE);
		} else {
			if (ValidExpression.isValidPanNumber(nomineePan)) {
				enterText(By.xpath(PanNumber_Xpath), nomineePan);
				logger1.info("Entered Nominee Pan Number is : " + nomineePan);
				ErrorMessages.fieldverification("Nominee PAN Number");
			} else {
				logger1.info(VALIDPANNUMBER);
				ErrorMessages.fieldverification("Nominee Pan Number");
			}
		}

		// Entering Nominee Age on UI in other Personal Details Tab
		if (nomineeAge.equals("")) {
			logger1.info(NOMINEEAGEMESSAGE);
		} else {
			enterText(By.xpath(NomineeAge_Xpath), nomineeAge);
			logger1.info("Entered Nominee Age is : " + nomineeAge);
			ErrorMessages.fieldverification("Nominee Age");
		}		

	}

	public static void nomineeAccountDetailsNegative(String paymentDisbursmentMode,String tDS,String iFSCCode,String bankAccountNumber,
			String micrNumber, String bankname,String branchName,String branchState, String branchCity) throws Exception
	{
		
		//Selecting Payment Disbursement Mode
		if(paymentDisbursmentMode.equals(""))
		{
			logger1.info(PAYMENTDISBURSMENTMODEMESSAGE);
		}else if(paymentDisbursmentMode.equalsIgnoreCase("Cheque")){
		
			
			clickElement(By.xpath(PaymentDisbursementMode_Xpath));
			fluentwait(By.xpath(PaymentDisbursementModeOption_Xpath), 60,
					"Payment Disbursement Mode Option is Loading Slow so Couldn't find Payment Disbursment Mode.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentDisbursementModeOption_Xpath), paymentDisbursmentMode);
			logger1.info("Selected Payment Disbursment Mode is : " + paymentDisbursmentMode);	
			
			//Selecting TDS Details
			if(tDS.equals("")){
				logger1.info(TDSMESSAGE);
			}else{
				clickElement(By.xpath(TDS_Xpath));
				fluentwait(By.xpath(TDSOption_Xpath), 60,
						"Tds Option is Loading Slow so Couldn't find Tds Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(TDSOption_Xpath), tDS);
				logger1.info("Selected Tds Details is : " + tDS);	
			}

				
		}else if(paymentDisbursmentMode.equalsIgnoreCase("Direct Credit"))
		{
			
			clickElement(By.xpath(PaymentDisbursementMode_Xpath));
			fluentwait(By.xpath(PaymentDisbursementModeOption_Xpath), 60,
					"Payment Disbursement Mode Option is Loading Slow so Couldn't find Payment Disbursment Mode.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(PaymentDisbursementModeOption_Xpath), paymentDisbursmentMode);
			logger1.info("Selected Payment Disbursment Mode is : " + paymentDisbursmentMode);	
			
			//Selecting TDS Details
			if(tDS.equals("")){
				logger1.info(TDSMESSAGE);
			}else{
				Thread.sleep(2000);
				clickElement(By.xpath(TDS_Xpath));
				fluentwait(By.xpath(TDSOption_Xpath), 60,"Tds Option is Loading Slow so Couldn't find Tds Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(TDSOption_Xpath), tDS);
				logger1.info("Selected Tds Details is : " + tDS);	
			}
			
			//Entering IFSC Code on UI in Nominee Account Details Section in Other Personal Details Tab
			if(iFSCCode.equals(""))
			{
				logger1.info(IFSCCODEMESSAGE);
			}
			else{
				
				if(ValidExpression.isValidIfscCode(iFSCCode)){
				enterText(By.xpath(IFSC_Code_Xpath), iFSCCode);
				logger1.info("Entered IFSC Code is : "+iFSCCode);
				}else{
					logger1.info("Entered IFSC Code is : "+iFSCCode);
					ErrorMessages.fieldverification("IFSC Code");
				}
				
		}
			
			//Entering Bank Account Number on UI in Nominee Account Details Section in Other Personal Details Tab
			if(bankAccountNumber.equals(""))
			{
				logger1.info(BANKACCOUNTNUMBERMESSAGE);
			}else{
				enterText(By.xpath(BankAccountNumber_Xpath), bankAccountNumber);
				logger1.info("Entered Bank Account Number is : "+bankAccountNumber);
				ErrorMessages.fieldverification("Bank Account Number");
			}
			
			//Entering MICR Number on UI in Nominee Account Details Section in Other Personal Details Tab
			if(micrNumber.equals(""))
			{
				logger1.info(MICRNUMBERMESSAGE);
			}else{
				if(ValidExpression.isvalidMicrNumber(micrNumber))
				{
					enterText(By.xpath(MICRNumber_Xpath), micrNumber);
					logger1.info("Entered MICR Number is : "+micrNumber);
				}else{
					logger1.info("Entered MICR Number is : "+micrNumber);
					ErrorMessages.fieldverification("Micr Number");
				}
			}
			
			//Entering Bank Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(bankname.equals(""))
			{
				logger1.info(BANKNAMEMESSAGE);
			}else{
				enterText(By.xpath(Bank_Xpath), bankname);
				logger1.info("Entered Bank Name is : "+bankname);
			}
			
			//Entering Branch Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchName.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				enterText(By.xpath(BranchName_Xpath), branchName);
				logger1.info("Entered Bank Name is : "+branchName);
			}
			
			
			/*//Entering Branch Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchName.equals(""))
			{
				logger1.info(BranchNameMessage);
			}else{
				enterText(By.xpath(BranchName_Xpath), branchName);
				logger1.info("Entered Bank Name is : "+branchName);
			}*/
			
			//Selecting State Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchState.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				clickElement(By.xpath(State_Xpath));
				fluentwait(By.xpath(StateOption_Xpath), 60,
						"State Option is Loading Slow so Couldn't find State Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(StateOption_Xpath), branchState);
				logger1.info("Selected State is : " + branchState);	
			}
			
			//Selecting City Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchCity.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				clickElement(By.xpath(City_Xpath));
				fluentwait(By.xpath(CityOption_Xpath), 60,
						"City Option is Loading Slow so Couldn't find City Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(CityOption_Xpath), branchCity);
				logger1.info("Selected City is : " + branchCity);	
			}
			
		}	
		
	}
	
	
}
