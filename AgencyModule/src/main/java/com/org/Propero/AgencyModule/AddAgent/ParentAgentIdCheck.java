package com.org.propero.agencymodule.addagent;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;

public class ParentAgentIdCheck extends BaseClass implements ProjectInterface
{
	private static Logger logger1 = LoggerFactory.getLogger(ParentAgentIdCheck.class);
	
	public static void parentagentcheck() throws Exception
	{
	   
		Boolean parentAgent = driver.findElements(By.xpath("//*[@id='agent-main-section']/div[9]/div/div/div[1]/h4/div")).size()!= 0;
			
			if(parentAgent==true)
			{
				
			Boolean hPName = driver.findElements(By.id(HPName_Id)).size()!= 0;
			if(hPName==true)
			{
			clickElement(By.id(HPName_Id));
			
			}else{
				logger1.info("No need to select HP Name checkbox.");
			}
			
			Boolean panNumber = driver.findElements(By.id(VerifyPan_Id)).size()!=0;
			if(panNumber==true)
			{
			clickElement(By.id(VerifyPan_Id));
			}
			else{
				logger1.info("No Need to Select Pan Number checkbox.");
			}
			
			Boolean verifyAadhar = driver.findElements(By.id(VerifyAadhar_Id)).size()!=0;
			if(verifyAadhar==true){
			clickElement(By.id(VerifyAadhar_Id));
			}else{
				logger1.info("No Need to Select Verify Aadhar checkbox.");
			}
			
			Boolean verifyBank = driver.findElements(By.id(VerifyBankDetails_Id)).size()!=0;
			if(verifyBank==true)
			{
			clickElement(By.id(VerifyBankDetails_Id));
			}
			else {
				logger1.info("No Need to Select Verify bank checkbox.");
			}
			
			clickElement(By.xpath(OK_Btn_Xpath));
			
			}else{
				logger1.info("Parent Agent Field is not available.");
			}
			
	   }
	   
		
	

	public static void verifyAgentsDetails(String intermediaryCode) throws Exception
	{
		
		Boolean parentAgent = driver.findElements(By.xpath("//div[@class='modal fade hp-name-modal in']//div[@class='sub-header-main']")).size()!= 0;
		
		if(parentAgent==true)
		{
		
		ImplicitWait(10);
		Boolean hPName = driver.findElements(By.xpath("//*[@id='HP_Name-"+intermediaryCode+"']")).size()!= 0;
		if(hPName==true)
		{
			//Thread.sleep(2000);
		clickElement(By.xpath("//input[@id='HP_Name-"+intermediaryCode+"']"));
		
		}else{
			logger1.info("No need to select HP Name checkbox.");
		}
		
		Boolean panNumber = driver.findElements(By.xpath("//div[contains(text(),'Verify PAN')]")).size()!=0;
		if(panNumber==true)
		{
		clickElement(By.xpath("//input[@id='Verify_PAN-"+intermediaryCode+"']"));
		}
		else{
			logger1.info("No Need to Select Pan Number checkbox.");
		}
		
		Boolean verifyAadhar = driver.findElements(By.xpath("//div[contains(text(),'Verify Aadhaar')]")).size()!=0;
		if(verifyAadhar==true){
		clickElement(By.xpath("//input[@id='Verify_Aadhaar-"+intermediaryCode+"']"));
		}else{
			logger1.info("No Need to Select Verify Aadhar checkbox.");
		}
		
		clickElement(By.xpath(OK_Btn_Xpath));
		
		}else{
			logger1.info("Parent Agent Field is not available.");
		}
				
		
	}
	
}
