package com.org.propero.agencymodule.assertion;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.errormessages.PageNotFoundError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class AssertionFn extends BaseClass implements ProjectInterface{
	private static String testResult=null;
	private static Logger logger1 = LoggerFactory.getLogger(AssertionFn.class);
	
	public static void setDetailsOfExistingAgent() throws Exception{
		
		String agentStatus="SCANCOMPLETED";
		verifyAgentStatusAfterMovedToBuckets(agentStatus);
		/*Thread.sleep(10000);
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		String getAgentStatus=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getAgentStatus.equalsIgnoreCase(agentStatus)){
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus);
		}else{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus);
		}*/
		ErrorMessages.fieldverification("Ok");
		verticalscrollup();
		
		String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		fluentwait(By.id(Intermediary_Code_Id), 60, "Unable to Read Intermediary Code because response is coming late on UI.");
		if(intermediaryCode.equals("")){
			logger1.info("Intermediary Code is not generated.");
			logger.log(LogStatus.FAIL, "Intermediary Code is not Generated.");
		}else{
			logger1.info("Intermediary Code is : "+intermediaryCode);
			logger.log(LogStatus.INFO, "Intermediary Code is : "+intermediaryCode);
		
		}
		
	}
	
	public static void setDetailsOfFreshAgent(int rowNum) throws Exception{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "Existing_Agents";
		
		String subCategory = read.getCellData(fis,"Sub_Category",rowNum);
		
		if(subCategory.equalsIgnoreCase("Agent")){
		logger1.info("Sub Category is Agent.");
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "URNPENDING")));
		String agentStatus="URNPENDING";
		verifyAgentStatusAfterMovedToBuckets(agentStatus);
		ErrorMessages.fieldverification("Ok");
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		//clickElement(By.xpath(Submit_Button_Xpath));
		WebElement e1=driver.findElement(By.xpath(Submit_Button_Xpath));
		clickByJS(e1);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "URNOK")));
		String agentStatus1="URNOK";
		verifyAgentStatusAfterMovedToBuckets(agentStatus1);
		ErrorMessages.fieldverification("Ok");
		//Thread.sleep(5000);
		waitForElementsToBeClickable(By.xpath(IntermediaryTab_Xapth));
		//clickElement(By.xpath(IntermediaryTab_Xapth));
		WebElement e2=driver.findElement(By.xpath(IntermediaryTab_Xapth));
		clickByJS(e2);
		
		//Enter URN Number in Intermediary Details
		String uRNNumber = read.getCellData(fis, "URN_Number", rowNum);
		enterText(By.xpath(URNNO_Xpath), uRNNumber);
		logger1.info("Entered URN Number is : "+uRNNumber);
		
		//Enter URN Date
		Calendar.calender(fis, By.id(URNDate_Id), "URN_Date", rowNum);
		//Thread.sleep(2000);
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "URNGENERATED")));
		String agentStatus2="URNGENERATED";
		verifyAgentStatusAfterMovedToBuckets(agentStatus2);
		ErrorMessages.fieldverification("Ok");
		waitForElementsToBeClickable(By.xpath(ExamandLicense_Xpath));
		//clickElement(By.xpath(ExamandLicense_Xpath));
		WebElement e3=driver.findElement(By.xpath(ExamandLicense_Xpath));
		clickByJS(e3);
		
		//Training Start Date
		String trainingStart = read.getCellData(fis, "Training_Start", rowNum);
		enterText(By.xpath(TrainingStart_Xpath), trainingStart);
		logger1.info("Entered Traing Start Date is : "+trainingStart);
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		
		//Training End date
		String trainingEnd = read.getCellData(fis, "Training_End", rowNum);
		enterText(By.xpath(TrainingEnd_Xpath), trainingEnd);
		logger1.info("Entered Traing End Date is : "+trainingEnd);
		
		verticalscrollup();
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "TRAININGDONE")));
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		String agentStatus3="TRAININGDONE";
		verifyAgentStatusAfterMovedToBuckets(agentStatus3);
		ErrorMessages.fieldverification("Ok");
		//Thread.sleep(2000);
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		//clickElement(By.xpath(Submit_Button_Xpath));
		WebElement e4=driver.findElement(By.xpath(Submit_Button_Xpath));
		clickByJS(e4);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "PENDINGFOREXAMSCHEDULING")));
		//ErrorMessages.fieldverification("Ok");
		String agentStatus4="PENDINGFOREXAMSCHEDULING";
		verifyAgentStatusAfterMovedToBuckets(agentStatus4);
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		logger1.info(intermediaryCode);
		
		//Final Exam date
		String finalExamDate = read.getCellData(fis, "FinalExam_Date", rowNum);
		waitForElements(By.xpath("//input[@id='finalExamDate-0-0']"));
		enterText(By.xpath("//input[@id='finalExamDate-0-0']"), finalExamDate);
		/*Calendar.calender(fis, By.xpath("//input[@id='finalExamDate-0-0']"), "FinalExam_Date", rowNum);	*/	
		
		//Time Slot
		String timeSlot = read.getCellData(fis, "Time_Slot", rowNum);
		waitForElements(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"));
		enterText(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"), timeSlot);
		
		verticalscrollup();
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "RESULTAWAITED")));
		String agentStatus5="RESULTAWAITED";
		verifyAgentStatusAfterMovedToBuckets(agentStatus5);
		//Fill Result Value in Exam and License Training
		ErrorMessages.fieldverification("Ok");
		String result = read.getCellData(fis, "Result", rowNum);
		if(result.equals(""))
		{
			logger1.info("There is no Data in Excel sheer wrt to Result.");
		}
		selecttext(By.xpath("//*[@id='examResultCd-0-0']"), result);
		logger1.info("Selected Result is : "+result);
		
		//Eaxm Roll
		String examRoll = read.getCellData(fis, "Exam_Roll", rowNum);
		if(examRoll.equals(""))
		{
			logger1.info("There is no Data in Excel sheer wrt to Exam Roll Number.");
		}else{
			waitForElements(By.xpath(ExamRoll_Xpath));
		enterText(By.xpath(ExamRoll_Xpath), examRoll);
		logger1.info("Exam Roll is : "+examRoll);
		}
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		
		//Fill Marks
		String marks = read.getCellData(fis, "Marks", rowNum);
		if(marks.equals("")){
			logger1.info("There is no Data in Excel sheer wrt to Marks.");
		}else{
			
		waitForElements(By.xpath(Marks_Xpath));
		enterText(By.xpath(Marks_Xpath), marks);
		logger1.info("Entered Mraks is : "+marks);
		}
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		
		verticalscrollup();
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "PENDINGFORACTIVATION")));
		//ErrorMessages.fieldverification("Ok");
		String agentStatus6="PENDINGFORACTIVATION";
		verifyAgentStatusAfterMovedToBuckets(agentStatus6);
		ErrorMessages.fieldverification("Ok");
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		//clickElement(By.xpath(Submit_Button_Xpath));
		WebElement e5=driver.findElement(By.xpath(Submit_Button_Xpath));
		clickByJS(e5);
		Thread.sleep(5000);
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "PENDINGFORACTIVATION")));
		if(driver.findElement(By.xpath(OK_Btn_Xpath)).isEnabled()==true){
			System.out.println("No need to Click");
		}
		else{
			clickElement(By.id(HPName_Id));
			clickElement(By.id(HPName_Id));
		}
		clickElement(By.xpath(OK_Btn_Xpath));
		
		//PageNotFoundError.pagenotfound();
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "ACTIVE")));
		try{
		String agentStatus8="ACTIVE";
		verifyAgentStatusAfterMovedToBuckets(agentStatus8);
		ErrorMessages.fieldverification("ok");
		}
		catch(AssertionError  e){
			String agentStatus9="PENDINGFORACTIVATION";
			verifyAgentStatusAfterMovedToBuckets(agentStatus9);
			ErrorMessages.fieldverification("ok");
			//FooterError.footererror();
		}
		
		/*try
		{
			String agentStatus8="ACTIVE";
			verifyAgentStatusAfterMovedToBuckets(agentStatus8);
			ErrorMessages.fieldverification("ok");
		}
		catch(Exception e){
		    FooterError.footererror();
			
		}*/
		
		}
		
		else 
		{
			logger1.info("Sub Category is not Agent.");
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "URNPENDING")));
			String agentStatus="URNPENDING";
			verifyAgentStatusAfterMovedToBuckets(agentStatus);
			ErrorMessages.fieldverification("Ok");
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			//clickElement(By.xpath(Submit_Button_Xpath));
			WebElement e1=driver.findElement(By.xpath(Submit_Button_Xpath));
			clickByJS(e1);
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "URNOK")));
			String agentStatus1="URNOK";
			verifyAgentStatusAfterMovedToBuckets(agentStatus1);
			ErrorMessages.fieldverification("Ok");
			waitForElementsToBeClickable(By.xpath(IntermediaryTab_Xapth));
			//clickElement(By.xpath(IntermediaryTab_Xapth));
			WebElement e2=driver.findElement(By.xpath(IntermediaryTab_Xapth));
			clickByJS(e2);
			
			//Enter URN Number in Intermediary Details
			String uRNNumber = read.getCellData(fis, "URN_Number", rowNum);
			enterText(By.xpath(URNNO_Xpath), uRNNumber);
			logger1.info("Entered URN Number is : "+uRNNumber);
			
			//Enter URN Date
			Calendar.calender(fis, By.id(URNDate_Id), "URN_Date", rowNum);
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "URNGENERATED")));
			ErrorMessages.fieldverification("Ok");
			String agentStatus2="URNGENERATED";
			verifyAgentStatusAfterMovedToBuckets(agentStatus2);
			
			//Click on Exam and License tab
			ErrorMessages.fieldverification("Ok");
			waitForElementsToBeClickable(By.xpath(ExamandLicense_Xpath));
			//clickElement(By.xpath(ExamandLicense_Xpath));
			WebElement e3=driver.findElement(By.xpath(ExamandLicense_Xpath));
			clickByJS(e3);
			
			Thread.sleep(2000);
			//Training Start Date
			String trainingStart = read.getCellData(fis, "Training_Start", rowNum);
			enterText(By.xpath(TrainingStart_Xpath), trainingStart);
			logger1.info("Entered Traing Start Date is : "+trainingStart);
			/*Calendar.calender(fis, By.xpath(TrainingStart_Xpath), "Training_Start", rowNum);*/
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("ok");
			Thread.sleep(2000);
			//Training End date
			String trainingEnd = read.getCellData(fis, "Training_End", rowNum);
			enterText(By.xpath(TrainingEnd_Xpath), trainingEnd);
			logger1.info("Entered Traing End Date is : "+trainingEnd);
			
			verticalscrollup();
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "TRAININGDONE")));
			ErrorMessages.fieldverification("Ok");
			//Field Verification for First Name
			String agentStatus3="TRAININGDONE";
			verifyAgentStatusAfterMovedToBuckets(agentStatus3);
			ErrorMessages.fieldverification("Ok");
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			//clickElement(By.xpath(Submit_Button_Xpath));
			WebElement e4=driver.findElement(By.xpath(Submit_Button_Xpath));
			clickByJS(e4);
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "PENDINGFOREXAMSCHEDULING")));
			//ErrorMessages.fieldverification("Ok");
			String agentStatus4="PENDINGFOREXAMSCHEDULING";
			verifyAgentStatusAfterMovedToBuckets(agentStatus4);
			ErrorMessages.fieldverification("Ok");
			Thread.sleep(2000);
			
			//Final Exam date
			String finalExamDate = read.getCellData(fis, "FinalExam_Date", rowNum);
			enterText(By.xpath("//input[@id='finalExamDate-0-0']"), finalExamDate);
			/*Calendar.calender(fis, By.xpath("//input[@id='finalExamDate-0-0']"), "FinalExam_Date", rowNum);	*/	
			
			//Time Slot
			String timeSlot = read.getCellData(fis, "Time_Slot", rowNum);
			enterText(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"), timeSlot);
			
			verticalscrollup();
			//Thread.sleep(2000);
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "RESULTAWAITED")));
			//ErrorMessages.fieldverification("Ok");
			String agentStatus5="RESULTAWAITED";
			verifyAgentStatusAfterMovedToBuckets(agentStatus5);
			ErrorMessages.fieldverification("Ok");
			Thread.sleep(2000);
			//Fill Result Value in Exam and License Training
			String result = read.getCellData(fis, "Result", rowNum);
			selecttext(By.xpath("//*[@id='examResultCd-0-0']"), result);
			logger1.info("Selected Result is : "+result);
			
			//Eaxm Roll
			String examRoll = read.getCellData(fis, "Exam_Roll", rowNum);
			enterText(By.xpath(ExamRoll_Xpath), examRoll);
			logger1.info("Exam Roll is : "+examRoll);
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("Exam Roll number");
			
			//Fill Marks
			String marks = read.getCellData(fis, "Marks", rowNum);
			enterText(By.xpath(Marks_Xpath), marks);
			logger1.info("Entered Mraks is : "+marks);
			
			verticalscrollup();
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "PENDINGFORACTIVATION")));
			String agentStatus6="PENDINGFORACTIVATION";
			//verifyAgentStatusAfterMovedToBuckets(agentStatus6);
			verifyAgentStatusAfterMovedToBuckets(agentStatus6);
			ErrorMessages.fieldverification("ok");
			
			}
				
     	/*String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		fluentwait(By.id(Intermediary_Code_Id), 60, "Unable to Read Intermediary Code because response is coming late on UI.");
		if(intermediaryCode.equals(""))
		{
			logger1.info("Intermediary Code is not generated.");
			logger.log(LogStatus.FAIL, "Intermediary Code is not Generated.");
		}else{
			logger1.info("Intermediary Code is : "+intermediaryCode);
			logger.log(LogStatus.INFO, "Intermediary Code is : "+intermediaryCode);
		
		}*/
	
		}
	
	public static void setDetailsOfFreshAgentOldFunction(int rowNum) throws Exception{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "Existing_Agents";
		
		String subCategory = read.getCellData(fis,"Sub_Category",rowNum);
		
		if(subCategory.equalsIgnoreCase("Agent")){
		try{	
		logger1.info("Sub Category is Agent.");
		String agentStatus="URNPENDING";
		verifyAgentStatusAfterMovedToBuckets(agentStatus);
		/*Thread.sleep(10000);
		//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		String getAgentStatus=driver.findElement(By.id(Agent_Status_Id)).getText();
		if(getAgentStatus.contains(agentStatus)){
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus);
		}else{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus);
		}*/
		ErrorMessages.fieldverification("Ok");
		//Thread.sleep(2000);
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		//clickElement(By.xpath(Submit_Button_Xpath));
		WebElement e1=driver.findElement(By.xpath(Submit_Button_Xpath));
		clickByJS(e1);
		
		String agentStatus1="URNOK";
		verifyAgentStatusAfterMovedToBuckets(agentStatus1);
		//Thread.sleep(25000);
		//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		//waitForElements(By.xpath(Agent_Status_Id));
		/*ErrorMessages.fieldverification("Ok");
		waitForElements(By.id(Agent_Status_Id));
		String getagentStatus1=driver.findElement(By.id(Agent_Status_Id)).getText();
		if(getagentStatus1.contains(agentStatus1)){
			logger1.info("Agent Status is Verified and it is : "+getagentStatus1);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus1);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification its should be : "+agentStatus1+" but found : "+getagentStatus1);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification its should be : "+agentStatus1+" but found : "+getagentStatus1);
		}*/
		
		//Moving Towards intermediary Tab
		//ErrorMessages.fieldverification("Ok");
		//Thread.sleep(5000);
		ErrorMessages.fieldverification("Ok");
		waitForElementsToBeClickable(By.xpath(IntermediaryTab_Xapth));
		clickElement(By.xpath(IntermediaryTab_Xapth));
		
		//Enter URN Number in Intermediary Details
		String uRNNumber = read.getCellData(fis, "URN_Number", rowNum);
		enterText(By.xpath(URNNO_Xpath), uRNNumber);
		logger1.info("Entered URN Number is : "+uRNNumber);
		
		//Enter URN Date
		Calendar.calender(fis, By.id(URNDate_Id), "URN_Date", rowNum);
		//Thread.sleep(2000);
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		//ErrorMessages.fieldverification("Ok");
		String agentStatus2="URNGENERATED";
		verifyAgentStatusAfterMovedToBuckets(agentStatus2);
		//Thread.sleep(20000);
		/*waitForElements(By.id(Agent_Status_Id));
		String getagentStatus2=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus2.equalsIgnoreCase(agentStatus2))
		{
			logger1.info("Agent Status is Verified and it is : "+getagentStatus2);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus2);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus2);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus2);
		}*/
		
		//Field Verification for Pin Code
		//ErrorMessages.fieldverification("Ok");
		//Thread.sleep(5000);
		//Click on Exam and License tab
		ErrorMessages.fieldverification("Ok");
		waitForElementsToBeClickable(By.xpath(ExamandLicense_Xpath));
		clickElement(By.xpath(ExamandLicense_Xpath));
		
		//Training Start Date
		String trainingStart = read.getCellData(fis, "Training_Start", rowNum);
		enterText(By.xpath(TrainingStart_Xpath), trainingStart);
		logger1.info("Entered Traing Start Date is : "+trainingStart);
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		
		//Training End date
		String trainingEnd = read.getCellData(fis, "Training_End", rowNum);
		enterText(By.xpath(TrainingEnd_Xpath), trainingEnd);
		logger1.info("Entered Traing End Date is : "+trainingEnd);
		
		verticalscrollup();
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		
		String agentStatus3="TRAININGDONE";
		verifyAgentStatusAfterMovedToBuckets(agentStatus3);
		//Thread.sleep(25000);
		/*waitForElements(By.id(Agent_Status_Id));
		//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		String getagentStatus3=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus3.equalsIgnoreCase(agentStatus3))
		{
			Assert.assertEquals(getagentStatus3,agentStatus3);
			logger1.info("Agent Status is Verified and it is : "+getagentStatus3);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus3);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus3);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus3);
		}*/
		ErrorMessages.fieldverification("Ok");
		//Thread.sleep(2000);
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		//ErrorMessages.fieldverification("Ok");
		String agentStatus4="PENDINGFOREXAMSCHEDULING";
		verifyAgentStatusAfterMovedToBuckets(agentStatus4);
		//Thread.sleep(20000);
		//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		/*waitForElements(By.id(Agent_Status_Id));
		String getAgentStatus4=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getAgentStatus4.equalsIgnoreCase(agentStatus4))
		{
			Assert.assertEquals(getAgentStatus4,agentStatus4);
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus4);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus4);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus4);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus4);
		}*/
		//ErrorMessages.fieldverification("Ok");
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		
		logger1.info(intermediaryCode);
		
		
		//Final Exam date
		String finalExamDate = read.getCellData(fis, "FinalExam_Date", rowNum);
		waitForElements(By.xpath("//input[@id='finalExamDate-0-0']"));
		enterText(By.xpath("//input[@id='finalExamDate-0-0']"), finalExamDate);
		/*Calendar.calender(fis, By.xpath("//input[@id='finalExamDate-0-0']"), "FinalExam_Date", rowNum);	*/	
		
		//Time Slot
		String timeSlot = read.getCellData(fis, "Time_Slot", rowNum);
		waitForElements(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"));
		enterText(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"), timeSlot);
		
		verticalscrollup();
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		//ErrorMessages.fieldverification("Ok");
		String agentStatus5="RESULTAWAITED";
		verifyAgentStatusAfterMovedToBuckets(agentStatus5);
		//Thread.sleep(25000);
		//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		/*waitForElements(By.id(Agent_Status_Id));
		String getagentStatus5=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus5.equalsIgnoreCase(agentStatus5))
		{
			Assert.assertEquals(getagentStatus5,agentStatus5);
			logger1.info("Agent Status is Verified and it is : "+getagentStatus5);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus5);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus5);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus5);
		}*/
		//ErrorMessages.fieldverification("Ok");
		//Fill Result Value in Exam and License Training
		ErrorMessages.fieldverification("Ok");
		String result = read.getCellData(fis, "Result", rowNum);
		if(result.equals(""))
		{
			logger1.info("There is no Data in Excel sheer wrt to Result.");
		}
		selecttext(By.xpath("//*[@id='examResultCd-0-0']"), result);
		logger1.info("Selected Result is : "+result);
		
		//Eaxm Roll
		String examRoll = read.getCellData(fis, "Exam_Roll", rowNum);
		if(examRoll.equals(""))
		{
			logger1.info("There is no Data in Excel sheer wrt to Exam Roll Number.");
		}else{
			waitForElements(By.xpath(ExamRoll_Xpath));
		enterText(By.xpath(ExamRoll_Xpath), examRoll);
		logger1.info("Exam Roll is : "+examRoll);
		}
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		
		//Fill Marks
		String marks = read.getCellData(fis, "Marks", rowNum);
		if(marks.equals("")){
			logger1.info("There is no Data in Excel sheer wrt to Marks.");
		}else{
			
		waitForElements(By.xpath(Marks_Xpath));
		enterText(By.xpath(Marks_Xpath), marks);
		logger1.info("Entered Mraks is : "+marks);
		}
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("ok");
		
		verticalscrollup();
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		//ErrorMessages.fieldverification("Ok");
		String agentStatus6="PENDINGFORACTIVATION";
		verifyAgentStatusAfterMovedToBuckets(agentStatus6);
		//Thread.sleep(20000);
		//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		/*waitForElements(By.id(Agent_Status_Id));
		String getagentStatus6=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus6.equalsIgnoreCase(agentStatus6))
		{
			Assert.assertEquals(getagentStatus6,agentStatus6);
			logger1.info("Agent Status is Verified and it is : "+getagentStatus6);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus6);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus6);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus6);
		}*/
		//ErrorMessages.fieldverification("Ok");
		//Thread.sleep(5000);
		ErrorMessages.fieldverification("Ok");
		waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		Thread.sleep(5000);
		
		if(driver.findElement(By.xpath(OK_Btn_Xpath)).isEnabled()==true){
			System.out.println("No need to Click");
		}
		else{
			clickElement(By.id(HPName_Id));
			clickElement(By.id(HPName_Id));
		}
		
		fluentwait(By.xpath(OK_Btn_Xpath), 60, "Unable to click on Ok button.");
		clickElement(By.xpath(OK_Btn_Xpath));
		
		PageNotFoundError.pagenotfound();
		
	/*	String AgentStatus7="PENDINGFORACTIVATION";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(10000);
		String GetAgentStatus7=driver.findElement(By.id(Agent_Status_Id)).getText();
		if(GetAgentStatus7.equalsIgnoreCase(AgentStatus7))
		{
			Assert.assertEquals(GetAgentStatus7,AgentStatus7);
			logger1.info("Agent Status is Verified and it is : "+GetAgentStatus7);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+GetAgentStatus7);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+GetAgentStatus7);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification it should be : "+AgentStatus7+" but Found : "+GetAgentStatus7);
		}
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		clickElement(By.xpath(Submit_Button_Xpath));
		Thread.sleep(5000);
		fluentwait(By.xpath(OK_Btn_Xpath), 60, "Unable to click on Ok button.");*/
		
		/*try{
			FooterError.Footererror();
			}
			catch(Exception e)
			{
				logger1.info("No Error in Footer.");
			}*/
		
		/*clickElement(By.xpath(OK_Btn_Xpath));
		
		PageNotFoundError.Pagenotfound();*/
		
		try
		{
			String agentStatus8="ACTIVE";
			verifyAgentStatusAfterMovedToBuckets(agentStatus8);
			ErrorMessages.fieldverification("ok");
			
			/*Thread.sleep(20000);
			ErrorMessages.fieldverification("ok");
			//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			//waitForElements(By.id(Agent_Status_Id));
			String getAgentStatus8=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getAgentStatus8.equalsIgnoreCase(agentStatus8))
			{
				Assert.assertEquals(getAgentStatus8,agentStatus8);
				logger1.info("Agent Status is Verified and it is : "+getAgentStatus8);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus8);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus8);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification it should be : "+agentStatus8+" but found : "+getAgentStatus8);
			}
			//ErrorMessages.fieldverification("ok");
*/		}
		catch(Exception e){
			
			FooterError.footererror();
			
		}
		}
		
		catch(Exception e){
			throw e;
			
		}
		
		}
		else 
		{
			logger1.info("Sub Category is not Agent.");
			String agentStatus="URNPENDING";
			verifyAgentStatusAfterMovedToBuckets(agentStatus);
			/*Thread.sleep(10000);
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			String getAgentStatus=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getAgentStatus.equalsIgnoreCase(agentStatus))
			{
				Assert.assertEquals(getAgentStatus,agentStatus);
				logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus);
			}*/
			ErrorMessages.fieldverification("Ok");
			//fluentWait(Submit_Button_Xpath);
			//waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			
			//waitForElements(By.xpath(Submit_Button_Xpath));
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			//clickElement(By.xpath(Submit_Button_Xpath));
			WebElement e1=driver.findElement(By.xpath(Submit_Button_Xpath));
			clickByJS(e1);
			
			//ErrorMessages.fieldverification("Ok");
			String agentStatus1="URNOK";
			//Thread.sleep(25000);
			//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			//waitForElements(By.id(Agent_Status_Id));
			verifyAgentStatusAfterMovedToBuckets(agentStatus1);
			//String getagentStatus1=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			/*if(getagentStatus1.equalsIgnoreCase(agentStatus1))
			{
				Assert.assertEquals(getagentStatus1,agentStatus1);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus1);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus1);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus1);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus1);
			}*/
			
			//ErrorMessages.fieldverification("Ok");
			//Moving Towards intermediary Tab
			//Thread.sleep(5000);
			ErrorMessages.fieldverification("Ok");
			waitForElementsToBeClickable(By.xpath(IntermediaryTab_Xapth));
			clickElement(By.xpath(IntermediaryTab_Xapth));
			
			//Enter URN Number in Intermediary Details
			String uRNNumber = read.getCellData(fis, "URN_Number", rowNum);
			enterText(By.xpath(URNNO_Xpath), uRNNumber);
			logger1.info("Entered URN Number is : "+uRNNumber);
			
			//Enter URN Date
			try{
			Calendar.calender(fis, By.id(URNDate_Id), "URN_Date", rowNum);
			}
			catch(Exception e)
			{
			logger1.info(e.getMessage());	
			}
			//Thread.sleep(2000);
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			//ErrorMessages.fieldverification("Ok");
			String agentStatus2="URNGENERATED";
			verifyAgentStatusAfterMovedToBuckets(agentStatus2);
			//Thread.sleep(25000);
			//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			/*waitForElements(By.id(Agent_Status_Id));
			String getagentStatus2=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus2.equalsIgnoreCase(agentStatus2))
			{
				Assert.assertEquals(getagentStatus2,agentStatus2);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus2);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus2);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus2);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus2);
			}*/
			
			
			
			//Field Verification for Pin Code
			//ErrorMessages.fieldverification("Ok");
			//Thread.sleep(5000);
			//Click on Exam and License tab
			ErrorMessages.fieldverification("Ok");
			waitForElementsToBeClickable(By.xpath(ExamandLicense_Xpath));
			clickElement(By.xpath(ExamandLicense_Xpath));
			
			Thread.sleep(2000);
			//Training Start Date
			String trainingStart = read.getCellData(fis, "Training_Start", rowNum);
			enterText(By.xpath(TrainingStart_Xpath), trainingStart);
			logger1.info("Entered Traing Start Date is : "+trainingStart);
			/*Calendar.calender(fis, By.xpath(TrainingStart_Xpath), "Training_Start", rowNum);*/
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("ok");
			Thread.sleep(2000);
			//Training End date
			String trainingEnd = read.getCellData(fis, "Training_End", rowNum);
			enterText(By.xpath(TrainingEnd_Xpath), trainingEnd);
			logger1.info("Entered Traing End Date is : "+trainingEnd);
			
			verticalscrollup();
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			ErrorMessages.fieldverification("Ok");
			//Field Verification for First Name
			String agentStatus3="TRAININGDONE";
			verifyAgentStatusAfterMovedToBuckets(agentStatus3);
			//Thread.sleep(10000);
			//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			/*waitForElements(By.id(Agent_Status_Id));
			String getagentStatus3=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus3.equalsIgnoreCase(agentStatus3))
			{
				Assert.assertEquals(getagentStatus3,agentStatus3);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus3);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus3);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus3);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus3);
			}*/
			//ErrorMessages.fieldverification("Ok");
			//Thread.sleep(2000);
			ErrorMessages.fieldverification("Ok");
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			//ErrorMessages.fieldverification("Ok");
			String agentStatus4="PENDINGFOREXAMSCHEDULING";
			verifyAgentStatusAfterMovedToBuckets(agentStatus4);
			//Thread.sleep(25000);		
			//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			/*waitForElements(By.xpath(Agent_Status_Id));
			String getAgentStatus4=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getAgentStatus4.equalsIgnoreCase(agentStatus4))
			{
				Assert.assertEquals(getAgentStatus4,agentStatus4);
				logger1.info("Agent Status is Verified and it is : "+getAgentStatus4);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus4);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus4);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus4);
			}*/

			//ErrorMessages.fieldverification("Ok");
			ErrorMessages.fieldverification("Ok");
			Thread.sleep(2000);
			
			//Final Exam date
			String finalExamDate = read.getCellData(fis, "FinalExam_Date", rowNum);
			enterText(By.xpath("//input[@id='finalExamDate-0-0']"), finalExamDate);
			/*Calendar.calender(fis, By.xpath("//input[@id='finalExamDate-0-0']"), "FinalExam_Date", rowNum);	*/	
			
			//Time Slot
			String timeSlot = read.getCellData(fis, "Time_Slot", rowNum);
			enterText(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"), timeSlot);
			
			verticalscrollup();
			//Thread.sleep(2000);
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			//ErrorMessages.fieldverification("Ok");
			String agentStatus5="RESULTAWAITED";
			verifyAgentStatusAfterMovedToBuckets(agentStatus5);
			//Thread.sleep(25000);
			//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			/*waitForElements(By.xpath(Agent_Status_Id));
			String getagentStatus5=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus5.equalsIgnoreCase(agentStatus5))
			{
				Assert.assertEquals(getagentStatus5,agentStatus5);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus5);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus5);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus5);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus5);
			}*/
			ErrorMessages.fieldverification("Ok");
			Thread.sleep(2000);
			//Fill Result Value in Exam and License Training
			String result = read.getCellData(fis, "Result", rowNum);
			selecttext(By.xpath("//*[@id='examResultCd-0-0']"), result);
			logger1.info("Selected Result is : "+result);
			
			//Eaxm Roll
			String examRoll = read.getCellData(fis, "Exam_Roll", rowNum);
			enterText(By.xpath(ExamRoll_Xpath), examRoll);
			logger1.info("Exam Roll is : "+examRoll);
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("Exam Roll number");
			
			//Fill Marks
			String marks = read.getCellData(fis, "Marks", rowNum);
			enterText(By.xpath(Marks_Xpath), marks);
			logger1.info("Entered Mraks is : "+marks);
			
			verticalscrollup();
			//Thread.sleep(2000);
			waitForElementsToBeClickable(By.xpath(Submit_Button_Xpath));
			clickElement(By.xpath(Submit_Button_Xpath));
			
			String agentStatus6="PENDINGFORACTIVATION";
			verifyAgentStatusAfterMovedToBuckets(agentStatus6);
			//Thread.sleep(10000);
			ErrorMessages.fieldverification("ok");
			//fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			/*String getagentStatus6=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus6.equalsIgnoreCase(agentStatus6))
			{
				Assert.assertEquals(getagentStatus6,agentStatus6);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus6);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus6);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus6);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus6);
			}*/
			
			}
				
     	String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		fluentwait(By.id(Intermediary_Code_Id), 60, "Unable to Read Intermediary Code because response is coming late on UI.");
		if(intermediaryCode.equals(""))
		{
			logger1.info("Intermediary Code is not generated.");
			logger.log(LogStatus.FAIL, "Intermediary Code is not Generated.");
		}else{
			logger1.info("Intermediary Code is : "+intermediaryCode);
			logger.log(LogStatus.INFO, "Intermediary Code is : "+intermediaryCode);
		
		}
	
		}

		
	public static void agentBopsBucketAssertion(int rowNum) throws Throwable
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "Existing_Agents";
		
			String agentType = read.getCellData(fis,"Agent Type",rowNum);
			try{
			if(agentType.equalsIgnoreCase("Exising Agent"))
			{
				setDetailsOfExistingAgent();
			}
			else if(agentType.equalsIgnoreCase("Fresh Agent"))
			{
				setDetailsOfFreshAgent(rowNum);
		
			}
			}
			catch(Exception e){
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Fail")));
				Assert.fail("Error While Agent Bucket Moved");
			}
			
	}
	
	public static void copsagentstatusverification(String intermediaryCode) throws InterruptedException
	{
		
	try {
		    Thread.sleep(5000);
			FooterError.footererror();
		}
		catch(Exception e)
		{
		String agentStatus="ACTIVE";
		fluentwait(By.id("status-"+intermediaryCode+""), 60, "Unable to Read Agent Status.");
		Thread.sleep(25000);
		String getAgentStatus=driver.findElement(By.id("status-"+intermediaryCode+"")).getText();
		try{
			if(getAgentStatus.equals(agentStatus)){
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
			logger.log(LogStatus.INFO, "Agent Status is Verified At Cops Submit and it is : "+getAgentStatus);
			}
			else {
				logger1.info("Agent Status is not Matched as Per Verification on Cops Submit, Agent Status Should be - "+agentStatus+" but Found - "+getAgentStatus);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification on Cops Submit, Agent Status Should be - "+agentStatus+" but Found - "+getAgentStatus);
			}
			ErrorMessages.fieldverification("ok");
		}catch(Exception e1)
		{
			logger1.info(e1.getMessage());
		}
		
		
		
	}
	}
	
	public static String modificationAgentStatusActive(String agentId) throws Exception
	{
		//String agentStatus="ACTIVE";
		//fluentwait(By.xpath("//*[@id='status-"+agentId.trim()+"']"), 60, "Unable to Read Agent Status.");
		//Thread.sleep(25000);
		//String getAgentStatus=driver.findElement(By.xpath("//*[@id='status-"+agentId.trim()+"']")).getText();
		
		//verifyAgentStatusAfterMovedToBucketsForAgentModification(agentStatus, agentId);
					
		/*if(getAgentStatus.equalsIgnoreCase(agentStatus))
		{
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus);
			testResult="Pass";
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus);
			testResult="Fail";
		}*/
		
		//Verify Agent Active Status
		try{
			String agentStatus8="ACTIVE";
			verifyAgentStatusAfterMovedToBucketsForAgentModification(agentStatus8, agentId);
			ErrorMessages.fieldverification("ok");
			}
			catch(Exception e){
				String agentStatus9="PENDINGFORACTIVATION";
				verifyAgentStatusAfterMovedToBucketsForAgentModification(agentStatus9, agentId);
				ErrorMessages.fieldverification("ok");
				//FooterError.footererror();
			}
		
		return testResult;
	}
	
	//******************** Negative Functions *********************************************
	
	public static void agentBopsBucketAssertionNegativeTestcase(int rowNum) throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "Existing_Agents";
		
			String agentType = read.getCellData(fis,"Agent Type",rowNum);
			
			if(agentType.equalsIgnoreCase("Exising Agent"))
			{
				setDetailsOfExistingAgentNegative();
			}
			else if(agentType.equalsIgnoreCase("Fresh Agent"))
			{
				setDetailsOfFreshAgentNegative(rowNum);
		
			}
	
	}
	
public static void setDetailsOfExistingAgentNegative() throws Exception{
		
		//ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		//String fis = "Existing_Agents";
		
		String agentStatus="SCANCOMPLETED";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(10000);
		String getAgentStatus=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getAgentStatus.equalsIgnoreCase(agentStatus))
		{
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus);
		}
		ErrorMessages.fieldverification("Ok");
		
		
		verticalscrollup();
		Thread.sleep(2000);
		clickElement(By.xpath(Submit_Button_Xpath));
		
		String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		fluentwait(By.id(Intermediary_Code_Id), 60, "Unable to Read Intermediary Code because response is coming late on UI.");
		if(intermediaryCode.equals(""))
		{
			logger1.info("Intermediary Code is not generated.");
			logger.log(LogStatus.FAIL, "Intermediary Code is not Generated.");
		}else{
			logger1.info("Intermediary Code is : "+intermediaryCode);
			logger.log(LogStatus.INFO, "Intermediary Code is : "+intermediaryCode);
		
		}
		
	}
	
	public static void setDetailsOfFreshAgentNegative(int rowNum) throws Exception{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String fis = "Existing_Agents";
		
		String subCategory = read.getCellData(fis,"Sub_Category",rowNum);
		
		if(subCategory.equalsIgnoreCase("Agent"))
		{
		try
		{	
		logger1.info("Sub Category is Agent.");
		String agentStatus="URNPENDING";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(25000);
		String getAgentStatus=driver.findElement(By.id(Agent_Status_Id)).getText();
		if(getAgentStatus.equalsIgnoreCase(agentStatus))
		{
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus);
		}
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		clickElement(By.xpath(Submit_Button_Xpath));
		
		
		String agentStatus1="URNOK";
		Thread.sleep(25000);
		String getagentStatus1=driver.findElement(By.id(Agent_Status_Id)).getText();
		if(getagentStatus1.equalsIgnoreCase(agentStatus1))
		{
			logger1.info("Agent Status is Verified and it is : "+getagentStatus1);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus1);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification its should be : "+agentStatus1+" but found : "+getagentStatus1);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification its should be : "+agentStatus1+" but found : "+getagentStatus1);
		}
		
		
		//Moving Towards intermediary Tab
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		
		clickElement(By.xpath(IntermediaryTab_Xapth));
		
		//Enter URN Number in Intermediary Details
		String uRNNumber = read.getCellData(fis, "URN_Number", rowNum);
		enterText(By.xpath(URNNO_Xpath), uRNNumber);
		logger1.info("Entered URN Number is : "+uRNNumber);
		
		//Enter URN Date
		try{
		Calendar.calender(fis, By.id(URNDate_Id), "URN_Date", rowNum);
		}catch(Exception e)
		{
			logger1.info(e.getMessage());
		}
		Thread.sleep(2000);
		clickElement(By.xpath(Submit_Button_Xpath));
		
		String agentStatus2="URNGENERATED";
		Thread.sleep(10000);
		String getagentStatus2=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus2.equalsIgnoreCase(agentStatus2))
		{
			logger1.info("Agent Status is Verified and it is : "+getagentStatus2);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus2);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus2);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus2);
		}
		
		//Field Verification for Pin Code
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		//Click on Exam and License tab
		clickElement(By.xpath(ExamandLicense_Xpath));
		
		//Training Start Date
		String trainingStart = read.getCellData(fis, "Training_Start", rowNum);
		enterText(By.xpath(TrainingStart_Xpath), trainingStart);
		logger1.info("Entered Traing Start Date is : "+trainingStart);
		/*Calendar.calender(fis, By.xpath(TrainingStart_Xpath), "Training_Start", rowNum);*/
		ErrorMessages.fieldverification("Training Start Date");
		
		//Training End date
		String trainingEnd = read.getCellData(fis, "Training_End", rowNum);
		enterText(By.xpath(TrainingEnd_Xpath), trainingEnd);
		logger1.info("Entered Traing End Date is : "+trainingEnd);
		/*Calendar.calender(fis, By.xpath(TrainingEnd_Xpath), "Training_End", rowNum);*/
		ErrorMessages.fieldverification("Training End Date");
		verticalscrollup();
		
		clickElement(By.xpath(Submit_Button_Xpath));
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("Error Field");
		
		String agentStatus3="TRAININGDONE";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(10000);
		String getagentStatus3=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus3.equalsIgnoreCase(agentStatus3))
		{
			Assert.assertEquals(getagentStatus3,agentStatus3);
			logger1.info("Agent Status is Verified and it is : "+getagentStatus3);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus3);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus3);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus3);
		}
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		clickElement(By.xpath(Submit_Button_Xpath));
		
		String agentStatus4="PENDINGFOREXAMSCHEDULING";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(10000);
		String getAgentStatus4=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getAgentStatus4.equalsIgnoreCase(agentStatus4))
		{
			Assert.assertEquals(getAgentStatus4,agentStatus4);
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus4);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus4);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus4);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus4);
		}
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		
		logger1.info(intermediaryCode);
		
		
		//Final Exam date
		String finalExamDate = read.getCellData(fis, "FinalExam_Date", rowNum);
		waitForElements(By.xpath("//input[@id='finalExamDate-0-0']"));
		enterText(By.xpath("//input[@id='finalExamDate-0-0']"), finalExamDate);
		/*Calendar.calender(fis, By.xpath("//input[@id='finalExamDate-0-0']"), "FinalExam_Date", rowNum);	*/	
		
		//Time Slot
		String timeSlot = read.getCellData(fis, "Time_Slot", rowNum);
		waitForElements(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"));
		enterText(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"), timeSlot);
		
		verticalscrollup();
		
		clickElement(By.xpath(Submit_Button_Xpath));
		
		String agentStatus5="RESULTAWAITED";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(10000);
		String getagentStatus5=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus5.equalsIgnoreCase(agentStatus5))
		{
			Assert.assertEquals(getagentStatus5,agentStatus5);
			logger1.info("Agent Status is Verified and it is : "+getagentStatus5);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus5);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus5);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus5);
		}
		ErrorMessages.fieldverification("Ok");
		//Fill Result Value in Exam and License Training
		String result = read.getCellData(fis, "Result", rowNum);
		if(result.equals(""))
		{
			logger1.info("There is no Data in Excel sheer wrt to Result.");
		}
		selecttext(By.xpath("//*[@id='examResultCd-0-0']"), result);
		logger1.info("Selected Result is : "+result);
		
		//Eaxm Roll
		String examRoll = read.getCellData(fis, "Exam_Roll", rowNum);
		if(examRoll.equals(""))
		{
			logger1.info("There is no Data in Excel sheer wrt to Exam Roll Number.");
		}else{
			waitForElements(By.xpath(ExamRoll_Xpath));
		enterText(By.xpath(ExamRoll_Xpath), examRoll);
		logger1.info("Exam Roll is : "+examRoll);
		}
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("Exam Roll number");
		
		//Fill Marks
		String marks = read.getCellData(fis, "Marks", rowNum);
		if(marks.equals("")){
			logger1.info("There is no Data in Excel sheer wrt to Marks.");
		}else{
			
			waitForElements(By.xpath(Marks_Xpath));
		enterText(By.xpath(Marks_Xpath), marks);
		logger1.info("Entered Mraks is : "+marks);
		}
		
		//Field Verification for First Name
		ErrorMessages.fieldverification("Exam Marks");
		
		verticalscrollup();
		waitForElements(By.xpath(Submit_Button_Xpath));
		clickElement(By.xpath(Submit_Button_Xpath));
		
		
		String agentStatus6="PENDINGFORACTIVATION";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(10000);
		String getagentStatus6=driver.findElement(By.id(Agent_Status_Id)).getText();
					
		if(getagentStatus6.equalsIgnoreCase(agentStatus6))
		{
			Assert.assertEquals(getagentStatus6,agentStatus6);
			logger1.info("Agent Status is Verified and it is : "+getagentStatus6);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus6);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus6);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus6);
		}
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(5000);
		clickElement(By.xpath(Submit_Button_Xpath));
		
		fluentwait(By.xpath(OK_Btn_Xpath), 60, "Unable to click on Ok button.");
		Thread.sleep(5000);
		clickElement(By.xpath(OK_Btn_Xpath));
		
		PageNotFoundError.pagenotfound();
		
		String agentStatus7="PENDINGFORACTIVATION";
		fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
		Thread.sleep(10000);
		String getAgentStatus7=driver.findElement(By.id(Agent_Status_Id)).getText();
		if(getAgentStatus7.equalsIgnoreCase(agentStatus7))
		{
			Assert.assertEquals(getAgentStatus7,agentStatus7);
			logger1.info("Agent Status is Verified and it is : "+getAgentStatus7);
			logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus7);
		}else
		{
			logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus7);
			logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification it should be : "+agentStatus7+" but Found : "+getAgentStatus7);
		}
		ErrorMessages.fieldverification("Ok");
		Thread.sleep(2000);
		clickElement(By.xpath(Submit_Button_Xpath));
		Thread.sleep(5000);
		fluentwait(By.xpath(OK_Btn_Xpath), 60, "Unable to click on Ok button.");
		
		try{
			FooterError.footererror();
			}
			catch(Exception e)
			{
				logger1.info("No Error in Footer.");
			}
		
		clickElement(By.xpath(OK_Btn_Xpath));
		
		PageNotFoundError.pagenotfound();
		
		try{
			FooterError.footererror();
			
		}catch(Exception e)
		{
			String agentStatus8="ACTIVE";
			Thread.sleep(25000);
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			String getAgentStatus8=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getAgentStatus8.equalsIgnoreCase(agentStatus8))
			{
				Assert.assertEquals(getAgentStatus8,agentStatus8);
				logger1.info("Agent Status is Verified and it is : "+getAgentStatus8);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus8);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus8);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification it should be : "+agentStatus8+" but found : "+getAgentStatus8);
			
			}
		}
		
		
		}catch(Exception e)
		{
			throw e;
			
		}
		
		
		
		}
		else 
		{
			logger1.info("Sub Category is not Agent.");
			String agentStatus="URNPENDING";
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			Thread.sleep(25000);
			String getAgentStatus=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getAgentStatus.equalsIgnoreCase(agentStatus))
			{
				Assert.assertEquals(getAgentStatus,agentStatus);
				logger1.info("Agent Status is Verified and it is : "+getAgentStatus);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus);
			}
			
			clickElement(By.xpath(Submit_Button_Xpath));
			
			String agentStatus1="URNOK";
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			Thread.sleep(25000);
			String getagentStatus1=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus1.equalsIgnoreCase(agentStatus1))
			{
				Assert.assertEquals(getagentStatus1,agentStatus1);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus1);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus1);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus1);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus1);
			}
			
			
			//Moving Towards intermediary Tab
			clickElement(By.xpath(IntermediaryTab_Xapth));
			
			//Enter URN Number in Intermediary Details
			String uRNNumber = read.getCellData(fis, "URN_Number", rowNum);
			enterText(By.xpath(URNNO_Xpath), uRNNumber);
			logger1.info("Entered URN Number is : "+uRNNumber);
			
			//Enter URN Date
			try{
			Calendar.calender(fis, By.id(URNDate_Id), "URN_Date", rowNum);
			}
			catch(Exception e)
			{
			logger1.info(e.getMessage());	
			}
			
			clickElement(By.xpath(Submit_Button_Xpath));
			
			String agentStatus2="URNGENERATED";
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			Thread.sleep(25000);
			String getagentStatus2=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus2.equalsIgnoreCase(agentStatus2))
			{
				Assert.assertEquals(getagentStatus2,agentStatus2);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus2);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus2);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus2);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus2);
			}
			
			//Field Verification for Pin Code
			ErrorMessages.fieldverification("Ok");
			
			//Click on Exam and License tab
			clickElement(By.xpath(ExamandLicense_Xpath));
			
			//Training Start Date
			String trainingStart = read.getCellData(fis, "Training_Start", rowNum);
			enterText(By.xpath(TrainingStart_Xpath), trainingStart);
			logger1.info("Entered Traing Start Date is : "+trainingStart);
			/*Calendar.calender(fis, By.xpath(TrainingStart_Xpath), "Training_Start", rowNum);*/
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("Nominee First Name");
			
			//Training End date
			String trainingEnd = read.getCellData(fis, "Training_End", rowNum);
			enterText(By.xpath(TrainingEnd_Xpath), trainingEnd);
			logger1.info("Entered Traing End Date is : "+trainingEnd);
			/*Calendar.calender(fis, By.xpath(TrainingEnd_Xpath), "Training_End", rowNum);*/
			
			verticalscrollup();
			
			clickElement(By.xpath(Submit_Button_Xpath));
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("Nominee First Name");
			
			String agentStatus3="TRAININGDONE";
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			Thread.sleep(10000);
			String getagentStatus3=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus3.equalsIgnoreCase(agentStatus3))
			{
				Assert.assertEquals(getagentStatus3,agentStatus3);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus3);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus3);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus3);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus3);
			}
			
			clickElement(By.xpath(Submit_Button_Xpath));
			
			String agentStatus4="PENDINGFOREXAMSCHEDULING";
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			Thread.sleep(10000);
			String getAgentStatus4=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getAgentStatus4.equalsIgnoreCase(agentStatus4))
			{
				Assert.assertEquals(getAgentStatus4,agentStatus4);
				logger1.info("Agent Status is Verified and it is : "+getAgentStatus4);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getAgentStatus4);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getAgentStatus4);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getAgentStatus4);
			}
			
			String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
			
			logger1.info(intermediaryCode);
			
			
			//Final Exam date
			String finalExamDate = read.getCellData(fis, "FinalExam_Date", rowNum);
			enterText(By.xpath("//input[@id='finalExamDate-0-0']"), finalExamDate);
			/*Calendar.calender(fis, By.xpath("//input[@id='finalExamDate-0-0']"), "FinalExam_Date", rowNum);	*/	
			
			//Time Slot
			String timeSlot = read.getCellData(fis, "Time_Slot", rowNum);
			enterText(By.xpath("//*[@id='exam-details-0']/div/div/div/div/table/tbody/tr/td[5]/input"), timeSlot);
			
			verticalscrollup();
			
			clickElement(By.xpath(Submit_Button_Xpath));
			
			String agentStatus5="RESULTAWAITED";
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			Thread.sleep(25000);
			String getagentStatus5=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus5.equalsIgnoreCase(agentStatus5))
			{
				Assert.assertEquals(getagentStatus5,agentStatus5);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus5);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus5);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus5);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus5);
			}
			
			//Fill Result Value in Exam and License Training
			String result = read.getCellData(fis, "Result", rowNum);
			selecttext(By.xpath("//*[@id='examResultCd-0-0']"), result);
			logger1.info("Selected Result is : "+result);
			
			//Eaxm Roll
			String examRoll = read.getCellData(fis, "Exam_Roll", rowNum);
			enterText(By.xpath(ExamRoll_Xpath), examRoll);
			logger1.info("Exam Roll is : "+examRoll);
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("Exam Roll number");
			
			//Fill Marks
			String marks = read.getCellData(fis, "Marks", rowNum);
			enterText(By.xpath(Marks_Xpath), marks);
			logger1.info("Entered Mraks is : "+marks);
			
			//Field Verification for First Name
			ErrorMessages.fieldverification("Exam Marks");
			
			verticalscrollup();
			Thread.sleep(2000);
			clickElement(By.xpath(Submit_Button_Xpath));
			
			
			String agentStatus6="PENDINGFORACTIVATION";
			fluentwait(By.id(Agent_Status_Id), 60, "Unable to Read Agent Status.");
			Thread.sleep(10000);
			String getagentStatus6=driver.findElement(By.id(Agent_Status_Id)).getText();
						
			if(getagentStatus6.equalsIgnoreCase(agentStatus6))
			{
				Assert.assertEquals(getagentStatus6,agentStatus6);
				logger1.info("Agent Status is Verified and it is : "+getagentStatus6);
				logger.log(LogStatus.INFO, "Agent Status is Verified and it is : "+getagentStatus6);
			}else
			{
				logger1.info("Agent Status is not Matched as Per Verification : "+getagentStatus6);
				logger.log(LogStatus.FAIL, "Agent Status is not Matched as Per Verification : "+getagentStatus6);
			}
			
			}
		
		verticalscrollup();
		Thread.sleep(2000);
		ErrorMessages.fieldverification("Exam Marks");
		clickElement(By.xpath(Submit_Button_Xpath));
		ErrorMessages.fieldverification("Product Map");
		
		String intermediaryCode = driver.findElement(By.id(Intermediary_Code_Id)).getText();
		fluentwait(By.id(Intermediary_Code_Id), 60, "Unable to Read Intermediary Code because response is coming late on UI.");
		if(intermediaryCode.equals(""))
		{
			logger1.info("Intermediary Code is not generated.");
			logger.log(LogStatus.FAIL, "Intermediary Code is not Generated.");
		}else{
			logger1.info("Intermediary Code is : "+intermediaryCode);
			logger.log(LogStatus.INFO, "Intermediary Code is : "+intermediaryCode);
		
		}
	
		}
}
