package com.org.propero.agencymodule.login;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.loginFn.Login;
import com.org.propero.agencymodule.loginFn.LogoutfromApp;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class LoginClass extends BaseClass implements ProjectInterface
{
	//private static Logger log = Logger.getLogger("devpinoyLogger");
	private static Logger logger1 = LoggerFactory.getLogger(LoginClass.class);

	@Test
	public void LoginCasesVerification() throws Exception
	{
		
		ReadExcel fis = new ReadExcel("D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx");
		int rowCount = fis.getRowCount("LoginTestCases");
				
		for(int n=1;n<rowCount;n++)
		{
			try
			{
		
			String executionStatus = fis.getCellData("LoginTestCases", "Execution Status", n);
			if(executionStatus.contains("Yes") || executionStatus.contains("yes") || executionStatus.contains("YES"))
			{
			String testCaseName = (fis.getCellData("LoginTestCases", "TestCaseNum", n)+ " - "+ fis.getCellData("LoginTestCases", "TestCase", n));
			logger = extent.startTest("Login - "+testCaseName);
			logger1.info("Login - " + testCaseName);
			
			Login.loginPage("D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx", "LoginTestCases" , n, "UserName", "Password");
		
			LogoutfromApp.logoutfromApplication();
		
			/*WriteExcel.setCellData1("LoginTestCases", "Pass", null, n, 6, 4);*/
			
			driver.quit();
			
			}else
			{
				String testCaseName = (fis.getCellData("LoginTestCases", "TestCaseNum", n)+ " - "+ fis.getCellData("LoginTestCases", "TestCase", n));
				logger = extent.startTest("Login - "+testCaseName);
				logger1.info("Login - " + testCaseName);
				logger1.info("Test Case Number "+n+" is Skipped.");
				logger.log(LogStatus.SKIP, "Test Case "+testCaseName+" is skipped.");
				/*WriteExcel.setCellData("LoginTestCases", "Skip", n, 4);*/
			}
			}
			catch(Exception e)
			{
/*				WriteExcel.setCellData("LoginTestCases", testResult, n, 4);
*/				logger1.info(e.getMessage());
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Login Issue")));
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" +e.getMessage());
				driver.quit();
			}
			continue;
		}
			
		
	}
	
}
