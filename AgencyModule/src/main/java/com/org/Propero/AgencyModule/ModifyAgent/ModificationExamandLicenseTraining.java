package com.org.propero.agencymodule.modifyAgent;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.modificationFn.ExamLicenseandTrainingModification;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ModificationExamandLicenseTraining extends BaseClass 
{

	private static String testResult=null;
	private static Logger logger1 = LoggerFactory.getLogger(ModificationExamandLicenseTraining.class);
	
	public static String examLicenseTrainingTab(String sheetName,String workbookname,int rowNum,String agentid) throws Exception
	{
	
		ExamLicenseandTrainingModification obj = new ExamLicenseandTrainingModification();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
			
			String agentType=read.getCellData(workbookname, "Agent_Type", rowNum);
			String newIrdaLicenseNumber=read.getCellData(workbookname, "IRDA_LicenseNum", rowNum);
			String newLicenseTypeCode=read.getCellData(workbookname, "License_TypeCode", rowNum);
			String newCompanyassociatewith=read.getCellData(workbookname, "Company_AssociatedWith", rowNum);
			String newLicenseState=read.getCellData(workbookname, "State_Name", rowNum);
			String newLicenseCity=read.getCellData(workbookname, "City_Name", rowNum);
			String newLicenseBranch=read.getCellData(workbookname, "Branch_Name", rowNum);
			String newLicenseIssueDate=read.getCellData(workbookname, "Issue_Date", rowNum);
			String newLicenseEndDate=read.getCellData(workbookname, "End_Date", rowNum);
			
			String newIntermediaryStartDate=read.getCellData(workbookname, "Intermediary_Start", rowNum);
			String newIntermediaryEndDate=read.getCellData(workbookname, "Intermediary_End date", rowNum);
			
			String newNoofPolicies=read.getCellData(workbookname, "Noof_Policies", rowNum);
			String newPremiumAmount=read.getCellData(workbookname, "Premium_Amount", rowNum);
			String newRecruitmentSource=read.getCellData(workbookname, "Recruitment_Source", rowNum);
			String newCertificateCourse=read.getCellData(workbookname, "Certificate_Course", rowNum);
			String newKnowReligareEmployee=read.getCellData(workbookname, "KnowReligare_Employee", rowNum);
			
			String newTrainingStartDate=read.getCellData(workbookname, "Training_Start", rowNum);
			String newTrainingEndDate=read.getCellData(workbookname, "Training_End", rowNum);
			
			String newAgentEducation=read.getCellData(workbookname, "Education", rowNum);
			String newBoardName=read.getCellData(workbookname, "Board_Name", rowNum);
			String newRollNumber=read.getCellData(workbookname, "Roll_No", rowNum);
			String newPassingYear=read.getCellData(workbookname, "Passing_Year", rowNum);
			String newExamCenter=read.getCellData(workbookname, "Exam_Center", rowNum);
			String newLanguage=read.getCellData(workbookname, "Language", rowNum);
			String newTentativeExamdate=read.getCellData(workbookname, "TentativeExam_Date", rowNum);
			String newFinalExamDate=read.getCellData(workbookname, "FinalExam_Date", rowNum);
			String newTimeSlot=read.getCellData(workbookname, "Time_Slot", rowNum);
			String newResult=read.getCellData(workbookname, "Result", rowNum);
			String newExamRoll=read.getCellData(workbookname, "Exam_Roll", rowNum);
			String newMarks=read.getCellData(workbookname, "Marks", rowNum);
			
			obj.setIrdaLicenseNumber(newIrdaLicenseNumber);
			obj.setLicenseTypeCode(newLicenseTypeCode);
			obj.setCompanyassociatewith(newCompanyassociatewith);
			obj.setLicenseState(newLicenseState);
			obj.setLicenseCity(newLicenseCity);
			obj.setLicenseBranch(newLicenseBranch);
			obj.setLicenseIssueDate(newLicenseIssueDate);
			obj.setLicenseEndDate(newLicenseEndDate);
			
			obj.setAgentEducation(newAgentEducation);
			obj.setBoardName(newBoardName);
			obj.setRollNumber(newRollNumber);
			obj.setPassingYear(newPassingYear);
			
			obj.setExamCenter(newExamCenter);
			obj.setLanguage(newLanguage);
			obj.setTentativeExamdate(newTentativeExamdate);
			obj.setFinalExamDate(newFinalExamDate);
			obj.setTimeSlot(newTimeSlot);
			obj.setResult(newResult);
			obj.setExamRoll(newExamRoll);
			obj.setMarks(newMarks);
			
			obj.setIntermediaryStartDate(newIntermediaryStartDate);
			obj.setIntermediaryEndDate(newIntermediaryEndDate);
			obj.setNoofPolicies(newNoofPolicies);
			obj.setPremiumAmount(newPremiumAmount);
			obj.setRecruitmentSource(newRecruitmentSource);
			obj.setCertificateCourse(newCertificateCourse);
			obj.setKnowReligareEmployee(newKnowReligareEmployee);
			
			obj.setTrainingStartDate(newTrainingStartDate);
			obj.setTrainingEndDate(newTrainingEndDate);
			
			
			obj.examandLicenseTraining1(agentid, workbookname, rowNum, agentType,
					obj.getIrdaLicenseNumber(), obj.getLicenseTypeCode(), obj.getCompanyassociatewith(), obj.getLicenseState(), 
					obj.getLicenseCity(), obj.getLicenseBranch(), obj.getLicenseIssueDate(),obj.getLicenseEndDate(),obj.getAgentEducation(), 
					obj.getBoardName(), obj.getRollNumber(), obj.getPassingYear(), obj.getExamCenter(), 
					obj.getLanguage(), obj.getTentativeExamdate(), obj.getFinalExamDate(), obj.getTimeSlot(),
					obj.getExamRoll(),obj.getMarks(), obj.getIntermediaryStartDate(),obj.getIntermediaryStartDate(),obj.getNoofPolicies(), 
					obj.getPremiumAmount(), obj.getRecruitmentSource(), obj.getCertificateCourse(),obj.getKnowReligareEmployee(),
					obj.getTrainingStartDate(),obj.getTrainingEndDate());
			
			testResult="Pass";
			
			obj=null;
			logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
		}catch(Exception  e)
		{
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			testResult="Fail";
			//Assert.fail("Error in Exam/Licence Training Section");
			throw e;
		}
		
		return testResult;
	}
}
