package com.org.propero.agencymodule.functions;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class IntermediaryTab extends BaseClass implements ProjectInterface,MessageInterface
{
	public static String agentCategory=null;
	public static String subAgentCategory=null;
	public static String parentAgentCode=null;
	public static String parentrmcode=null;
	public static String vertical=null;
	public static String sVertical=null;
	public static String location=null;
	public static String receivedDate=null;
	public static String urnno=null;
	public static String urngen=null;
	public static String modRemarks=null;
	public static String modReason=null;
	public static String classfi=null;
	public static String rejection=null;
	public static String secVertical=null;
	public static String symblogin=null;
	public static String cheqDD=null;
	
	private static Logger logger1 = LoggerFactory.getLogger(IntermediaryTab.class);
	
	public void setIntermediaryCategory(String intermediaryCategory) 
	{
		agentCategory=intermediaryCategory;
	}
	
	public String getIntermediaryCategory() 
	{
	return agentCategory;
	}
	
	public void setInterSubCategory(String intermediarySubCategory) 
	{
		subAgentCategory=intermediarySubCategory;
	}
	
	public String getIntermediarySubCategory() 
	{
	return subAgentCategory;
	}
	
	public void setParentIntermediary(String parentIntermediary) 
	{
		parentAgentCode=parentIntermediary;
	}
	
	public String getParentIntermediary() 
	{
	return parentAgentCode;
	}
	
	public void setRmCode(String rmCode) 
	{
		parentrmcode=rmCode;
	}
	
	public String getRmCode() 
	{
	return parentrmcode;
	}
	
	public void setSourcingVertical(String sourcingVertical) 
	{
		vertical=sourcingVertical;
	}
	
	public String getSourcingVertical() 
	{
	return vertical;
	}
	
	public void setSourcingSubVertical(String sourcingSubVertical) 
	{
		sVertical=sourcingSubVertical;
	}
	
	public String getSourcingSubVertical() 
	{
	return sVertical;
	}
	
	public void setSourcingLocation(String sourcingLocation) 
	{
		location=sourcingLocation;
	}
	
	public String getSourcingLocation() 
	{
	return location;
	}
	
	public void setArfReceivedDate(String arfReceivedDate)
	{
		receivedDate=arfReceivedDate;
	}
	
	public String getArfReceivedDate() 
	{
	return receivedDate;
	}
	
	public void setUrnNumber(String urnNumber) 
	{
		urnno=urnNumber;
	}
	
	public String getUrnNumber() 
	{
	return urnno;
	}
	
	public void setUrnGenerated(String urnGenerated) 
	{
		urngen=urnGenerated;
	}
	
	public String getUrnGenerated() 
	{
	return urngen;
	}
	
	public void setModificationRemarks(String modificationRemarks) 
	{
		modRemarks=modificationRemarks;
	}
	
	public String getModificationRemarks() 
	{
	return modRemarks;
	}
	
	public void setModificationReason(String modificationReason) 
	{
		modReason=modificationReason;
	}
	
	public String getModificationReason() 
	{
	return modReason;
	}
	
	public void setClassification(String classificationType) 
	{
		classfi=classificationType;
	}
	
	public String getClassification() 
	{
	return classfi;
	}
	
	public void setRejectionReason(String rejectionReason) 
	{
		rejection=rejectionReason;
	}
	
	public String getRejectionReason() 
	{
	return rejection;
	}
	
	
	public void setSymbLogin(String symbioLogin) 
	{
		symblogin=symbioLogin;
	}
	
	public String getSymbLogin() 
	{
	return symblogin;
	}
	

	public void setChequeDD(String chequeDD) 
	{
		cheqDD=chequeDD;
	}
	
	public String getChequeDD() 
	{
	return cheqDD;
	}
	
	public void setSecondaryVertical(String secondaryVertical) 
	{
		secVertical=secondaryVertical;
	}
	
	public String getSecondaryVertical() 
	{
	return secVertical;
	}
	
	public void internediaryDetailstab(String workbookname,int rowNum,String agentCategory,String subAgentCategory,String parentAgentCode,String parentrmcode,String vertical,String sVertical,String lcation,String secVertical,
	String receivedDate,String symblogin,String cheqDD,String classfi) throws Exception
	{
		
		//Selecting Agent Category
		if(agentCategory.equals(""))
		{
			logger1.info(INTERMEDIARYCATEGORYBLANK);
		}else
		{	
			fluentwait(By.xpath(Intermediary_Category_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			clickElement(By.xpath(Intermediary_Category_Xpath));
			DropDownSelect.selectValuesfromDropdown(By.xpath(IntermediaryCategoryOption_Xpath), agentCategory);
			logger1.info("Selected Intermediary Category : "+agentCategory);
		}
		
		//Selecting Agent Sub Category
		if(subAgentCategory.equals(""))
		{
			logger1.info(INTERMEDIARYSUBCATEGORYBLANK);
		}else
		{
			if(subAgentCategory instanceof String)
			{
			fluentwait(By.xpath(Intermediary_SubCategory_Xpath), 60, "Page is Loading Slow so Couldn't find Intermediary Sub Category.");
			clickElement(By.xpath(Intermediary_SubCategory_Xpath));
			DropDownSelect.selectValuesfromDropdown(By.xpath(Intermediary_SubCatOption_Xpath), subAgentCategory);
			logger1.info("Selected Sub Intermediary Category : "+subAgentCategory);
			}else
			{
				logger1.info(VALIDSTRINGINPUT);
			}
		}
		
		//Entering ParentIntermediary Code on UI 
		if(parentAgentCode.equals(""))
		{
			logger1.info(PARENTAGENTCODEBLANK);
		}
		else{
			boolean isInteger = isInteger(parentAgentCode);
			if(parentAgentCode.length()==8 && isInteger)
			{
			enterText(By.xpath(Parent_Intermediary_Xpath), parentAgentCode);
			logger1.info("Entered Parent Intermediary Code : "+parentAgentCode);
			driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
			}
			else
			{
			enterText(By.xpath(Parent_Intermediary_Xpath), parentAgentCode);
			logger1.info("Entered Parent Intermediary Code : "+parentAgentCode);
			//Verifying Error Message
			ErrorMessages.fieldverification("Parent Intermediary Code");
			driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
			}
		
		}
		
		//Entering RmCode on UI Using below 
		if(parentrmcode.equals(""))
		{
			logger1.info(RMCODEBLANK);
		}
		else
		{
			//Thread.sleep(2000);
			/*WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RmCodeEnter_Xpath)));
			WebElement RmCode_element = driver.findElement(By.xpath(RmCodeEnter_Xpath));*/
			Thread.sleep(5000);
			boolean isInteger = isInteger(parentrmcode);
			if(isInteger){
				//RmCode_element.sendKeys(parentrmcode);
				enterText(By.xpath(RmCodeEnter_Xpath), parentrmcode);
				logger1.info("Entered Parent Intermediary Code : "+parentrmcode);
				driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
				}
			else{
				//RmCode_element.sendKeys(parentrmcode);
				enterText(By.xpath(RmCodeEnter_Xpath), parentrmcode);
				logger1.info("Entered Rm Code is : "+parentrmcode);
				Thread.sleep(8000);
				ErrorMessages.fieldverification("RmCode");
				driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
			}
			
	}
		
		
		//Selecting Sourcing Vertical from Here
		if(vertical.equals(""))
		{
			logger1.info(VERTICALBLANK);
		}else
		{
			Thread.sleep(5000);
			clickElement(By.xpath(SourcingVertical_Xpath));
			fluentwait(By.xpath(SourcingVerticalOption_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(SourcingVerticalOption_Xpath), vertical);
			logger1.info("Selected Sourcing Vertical is : "+vertical);
		}
		
		
		//Selecting Sub Vertical from Here
		if(sVertical.equals(""))
		{
			logger1.info(SUBVERTICALBLANK);
		}else{
			ExplicitWait(By.xpath(Sub_Vertical_Xpath), 20);
			clickElement(By.xpath(Sub_Vertical_Xpath));
			fluentwait(By.xpath(SubVerticalOption_Xpath), 60, "Unable to Select Sourcing Sub Vertical.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(SubVerticalOption_Xpath), sVertical);
			logger1.info("Selected Sourcing Sub Vertical : "+sVertical);
		}
		
		//Selecting Sourcing Location Using Below
		if(location.equals(""))
		{
			logger1.info(SOURCINGLOCATIONBLANK);
		}else{
			Thread.sleep(5000);
			ExplicitWait(By.xpath(Sourcing_Location_Xpath), 20);
			clickElement(By.xpath(Sourcing_Location_Xpath));
			fluentwait(By.xpath(SourcingLocationOption_Xpath), 60, "Unable to Select Sourcing Sub Vertical.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(SourcingLocationOption_Xpath), location);
			logger1.info("Selected Sourcing Location is : "+location);
		}
		
		//Selecting Secondary Vertical Using Below 
		if(secVertical.equals(""))
		{
			logger1.info(SECONDARYVERTICALBLANK);
		}else{
			clickElement(By.xpath(Secondary_Vertical_Xpath));
			fluentwait(By.xpath(SecondaryVerticalOption_Xpath), 60, "Unable to Select Sourcing Sub Vertical.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(SecondaryVerticalOption_Xpath), secVertical);
			logger1.info("Entered Secondary Vertical is : "+secVertical);
		}
		
		//Selecting Arf Received Date
		if(receivedDate.equals(""))
		{
			logger1.info(RECEIVEDDATEBLANK);
		}else{
			Calendar.calender(workbookname, By.xpath(Arf_RcvDate_Xpath),"ARFRcvd_Date", rowNum);
		}
		
		
		//Checking Symbiosys Login
		if(symblogin.equals(""))
		{
		logger1.info(SYMBIOSYSLOGINBLANK);	
		}else{
		if(symblogin.equalsIgnoreCase("Yes"))
			{
				logger1.info("Symbiosys Login Activated.");
			}
		else if(symblogin.equalsIgnoreCase("No"))
			{
				clickElement(By.xpath(Symbiosys_Login_Xpath));
				logger1.info("Sysbiosys Login is Deactivated.");
			}
		}
		

		
		//Checking Cheque and DD Option from below String CheqDD
		if(cheqDD.equals(""))
		{
			logger1.info(CHEQUEDDBLANK);
		}
		else{
			if(cheqDD.equalsIgnoreCase("Yes"))
			{
				clickElement(By.xpath(ChequeandDD_Xpath));
				logger1.info("Cheque and DD option is Selected as : "+cheqDD);
			}else if(cheqDD.equalsIgnoreCase("No"))
			{
				logger1.info("Sysbiosys Login is Selected as : "+cheqDD);
			}
		}
		
		//Selecting Classification using below 
		if(classfi.equals(""))
		{
			logger1.info(CLASSIFIACTIONBLANK);
		}else
		{
			clickElement(By.xpath(Classifiaction_Xpath));
			fluentwait(By.xpath(ClassificationOption_Xpath), 60, "Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ClassificationOption_Xpath), classfi);
			logger1.info("Selected Classification is : "+classfi);
		}
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "Intermediary Details")));
		
	}
	
	public static boolean isInteger(String s) 
	{
	      boolean isValidInteger = false;
	      try
	      {
	         Integer.parseInt(s);
	         // s is a valid integer	 
	         isValidInteger = true;
	      }
	      catch (NumberFormatException ex)
	      {
	        logger1.info(ex.getMessage());
	      }
	 
	      return isValidInteger;
	   }
	
	
}
