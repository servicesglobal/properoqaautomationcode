package com.org.propero.agencymodule.innterface;

public interface MessageInterface 

{

	//Intermediary Tab 
	public String INTERMEDIARYCATEGORYBLANK="Test Data not found in Excel in Intermediary Category Column.";
	public String INTERMEDIARYSUBCATEGORYBLANK="Test Data not found in Excel in Intermediary Sub Category Column.";
	public String PARENTAGENTCODEBLANK="Test Data not found in Excel in Parent Agent Column.";
	public String VALIDSTRINGINPUT="Please Enter valid Input String Input.";
	public String RMCODEBLANK="Test Data not found in Excel in Rm Code Column.";
	public String VERTICALBLANK="Test Data not found in Excel in Sourcing Vertical Column.";
	public String SUBVERTICALBLANK="Test Data not found in Excel in Sub Vertical Column.";
	public String SOURCINGLOCATIONBLANK="Test Data not found in Excel in Sourcing Location Column.";
	public String SECONDARYVERTICALBLANK="Test Data not found in Excel in Secondary Vertical Column.";
	public String SYMBIOSYSLOGINBLANK="Test Data not found in Excel in Symbiosys Login Column.";
	public String RECEIVEDDATEBLANK="Test Data not found in Excel in Arf Received Date Column.";
	public String CHEQUEDDBLANK="Test Data not found in Excel in Cheque and DD Column.";
	public String CLASSIFIACTIONBLANK="Test Data not found in Excel in Classification Column.";
	
	
	//Personal Details
	public String CUSTOMERTYPEBLANK="Test Data not found in Excel in Customer Type Column.";
	public String TITLEBLANK="Test Data not found in Excel in Title Column.";
	public String FIRSTNAMEBLANK="Test Data not found in Excel in First Name Column.";
	public String LASTNAMEBLANK="Test Data not found in Excel in Last Name Column.";
	public String DATEOFBIRTHBLANK="Test Data not found in Excel in Date of Birth Column.";
	public String PINCODEBLANK="Test Data not found in Excel in Pincode Column.";
	public String PREFERREDADRESSBLANK="Test Data not found in Excel in Preferredadress Column.";
	public String FATHERSNAMEBLANK="Test Data not found in Excel in Fathers Name Column.";
	public String OFFICEADDRESSLINE1BLANK="Test Data not found in Excel in Office Addressline1 Column.";
	public String OFFICEADDRESSLINE2BLANK="Test Data not found in Excel in Office Addressline2 Column.";
	public String OFFICEPINCODEBLANK="Test Data not found in Excel in Office Pincode Column.";
	public String OFFICECITYBLANK="Test Data not found in Excel in Office City Column.";
	public String OFFICESTATEBLANK="Test Data not found in Excel in Office State Column.";
	public String OFFICELOCALITYBLANK="Test Data not found in Excel in Office Locality Column.";
	public String RESIDENTIALADDRESSLINE1BLANK="Test Data not found in Excel in Residential Addressline1 Column.";
	public String RESIDENTIALADDRESSLINE2BLANK="Test Data not found in Excel in Residential Addressline2 Column.";
	public String RESIDENTIALPINCODEBLANK="Test Data not found in Excel in Residential Pincode Column.";
	public String RESIDENTIALCITYBLANK="Test Data not found in Excel in Residential City Column.";
	public String RESIDENTIALSTATEBLANK="Test Data not found in Excel in Residential State Column.";
	public String RESIDENTIALLOCALITYBLANK="Test Data not found in Excel in Residential Locality Column.";
	public String EMAILIDBLANK="Test Data not found in Excel in Emailid Column.";
	public String MOBILENUMBERBLANK="Test Data not found in Excel in Mobile Number Column.";
	public String STDBLANK="Test Data not found in Excel in Std Column.";
	public String CONTACTNUMBERBLANK="Test Data not found in Excel in Contact Number Column.";
	public String SECONDARYEMAILIDBLANK="Test Data not found in Excel in Secondary Emailid Column.";
	public String SECONDARYMOBILENUMBERBLANK="Test Data not found in Excel in Secondary MobileNumber Column.";
	public String GSTREGISTRATIONSTATUSBLANK="Test Data not found in Excel in GstRegistrationStatus Column.";
	public String GSTREGISTRATIONDATEBLANK="Test Data not found in Excel in GstRegistrationdate Column.";
	public String GSTNUMBERBLANK="Test Data not found in Excel in GstNumber Column.";
	public String CONSTITUTIONBUSINESSBLANK="Test Data not found in Excel in ConstitutionBusiness Column.";
	public String CUSTOMERGSTTYPEBLANK="Test Data not found in Excel in CustomerGstType Column.";
	public String CUSTOMERCLASSIFIACTIONBLANK="Test Data not found in Excel in Customer Classifiaction Column.";	
	public String CORPORATENAMEBLANK="Test Data not found in Excel in Corporate Name Column.";
	public String CONTACTPERSONBLANK="Test Data not found in Excel in Contact Person Column.";
	public String DOIBLANK="Test Data not found in Excel in DOI Column.";
	public String RESIDENTIALADDRESS="Test Data not found in Excel in Residential Address Column.";
	
	
	//Other Personal Details
	public String NOMINEETITLEMESSAGE="Test Data not found in Excel in Nominee Title Column.";
	public String NOMINEEFIRSTNAMEMESSAGE="Test Data not found in Excel in Nominee Firstname Column.";
	public String NOMINEELASTNAMEMESSAGE="Test Data not found in Excel in Nominee Lastname Column.";
	public String NOMINEERELATIONMESSAGE="Test Data not found in Excel in Nominee Relation Column.";
	public String NOMINEEPANMESSAGE="Test Data not found in Excel in Pan Column.";
	public String NOMINEEAGEMESSAGE="Test Data not found in Excel in Nominee Age Column.";
	public String PAYMENTDISBURSMENTMODEMESSAGE="Test Data not found in Excel in Payment DisbursmentMode Column.";
	public String TDSMESSAGE="Test Data not found in Excel in TDS Column.";
	public String IFSCCODEMESSAGE="Test Data not found in Excel in IFSCCode Column.";
	public String BANKACCOUNTNUMBERMESSAGE="Test Data not found in Excel in Bank Account Number Column.";
	public String MICRNUMBERMESSAGE="Test Data not found in Excel in Micr Number Column.";
	public String BANKNAMEMESSAGE="Test Data not found in Excel in Bankname Column.";
	public String BRANCHNAMEMESSAGE="Test Data not found in Excel in Branch Name Column.";
	public String BRANCHSTATEMESSAGE="Test Data not found in Excel in Branch State Column.";
	public String BRANCHCITYMESSAGE="Test Data not found in Excel in Branch City Column.";
	public String RCDEDITABLEMESSAGE="Test Data not found in Excel in Rcd Editable Column.";
	public String EMPLOYEECODEMESSAGE="Test Data not found in Excel in Employee Code Column.";
	public String TOTALEXPERIENCEMESSAGE="Test Data not found in Excel in TotalExperience Column.";
	public String PASSPORTNUMBERMESSAGE="Test Data not found in Excel in Passport Number Column.";
	public String MARRIAGEDATEMESSAGE="Test Data not found in Excel in MarriageDate Column.";
	public String NUMBEROFCHILDRENMESSAGE="Test Data not found in Excel in Number of Children Column.";
	public String YEAROFSTAYMESSAGE="Test Data not found in Excel in Year of Stay Column.";
	public String EDUCATIONMESSAGE="Test Data not found in Excel in Education Column.";
	public String OCCUPATIONMESSAGE="Test Data not found in Excel in Occupation Column.";
	public String WORKLOCATIONMESSAGE="Test Data not found in Excel in WorkLocation Column.";
	public String LANGUAGEKNOWNMESSAGE="Test Data not found in Excel in Languageknown Column.";
	public String SPOUSETITLEMESSAGE="Test Data not found in Excel in Spouse Title Column.";
	public String SPOUSEFNAMEMESSAGE="Test Data not found in Excel in Spouse Fname Column.";
	public String SPOUSELNAMEMESSAGE="Test Data not found in Excel in Spouse LName Column.";
	public String SPOUSEQUALIFICATIONMESSAGE="Test Data not found in Excel in Spouse Qualification Column.";
	public String SPOUSEOCCUPATIONMESSAGE="Test Data not found in Excel in Spouse Occupation Column.";
	public String ASSOCIATEENTITYNAMEMESSAGE="Test Data not found in Excel in Associate EntityName Column.";
	public String ASSOCIATEENTITYDETAILSMESSAGE="Test Data not found in Excel in Associate EntityDetails Column.";
	public String REFERENCENAMEMESSAGE="Test Data not found in Excel in Reference Name Column.";
	public String REFERENCEADDRESSMESSAGE="Test Data not found in Excel in Reference Address Column.";
	public String REFERENCECONTACTNUMBERMESSAGE="Test Data not found in Excel in Reference ContactNumber Column.";
	public String REFERENCERELATIONSHIPMESSAGE="Test Data not found in Excel in Reference RelationShip Column.";
	public String VALIDPANNUMBER="Please Enter a valid Pan Number.";
	
	
	//Payment and Financial Page
	public String PAYTOMESSAGE="Test Data not found in Excel in Payto Column.";
	public String CLAWBACKMESSAGE="Test Data not found in Excel in Clawback Column.";
	public String PAYMENTFREQUENCYMESSAGE="Test Data not found in Excel in Payment Frequency Column.";
	public String PAYMENTDISMODEMESSAGE="Test Data not found in Excel in Payment Disbursement Mode Column.";
	public String SERVICETAXMESSAGE="Test Data not found in Excel in Service Tax Column.";
	public String FINTDSMESSAGE="Test Data not found in Excel in Tds Column.";
	public String LOWTDSMESSAGE="Test Data not found in Excel in Low Tds Column.";
	public String AADHAARCARDMESSAGE="Test Data not found in Excel in Aadhaar Card Number Column.";
	public String EFFECTIVEFROMMESSAGE="Test Data not found in Excel in Effective from Column.";
	public String EFFECTIVETOMESSAGE="Test Data not found in Excel in Effective To Column.";
	
	
	//Exam and License Tab
	public String IRDALICENSENUMBERMESSAGE="Test Data not found in Excel in Irda License Number To Column.";
	public String LICENSETYPEMESSAGE="Test Data not found in Excel in License Type To Column.";
	public String COMPANYASSOCIATEDWITHMESSAGE="Test Data not found in Excel in Company Associate Column.";
	public String LICENSESTATEMESSAGE="Test Data not found in Excel in Irda License State To Column.";
	public String LICENSECITYMESSAGE="Test Data not found in Excel in Irda License City Column.";
	public String LICENSEBRANCHMESSAGE="Test Data not found in Excel in Irda License Branch Column.";
	public String LICENSEISSUEDATEMESSAGE="Test Data not found in Excel in Irda License Issue date Column.";
	public String LICENSEENDDATEMESSAGE="Test Data not found in Excel in Irda License End Date Column.";
	public String AGENTEDUCATIONMESSAGE="Test Data not found in Excel in Education Column.";
	public String BOARDNAMEMESSAGE="Test Data not found in Excel in Board Name Column.";
	public String PASSINGYEARMESSAGE="Test Data not found in Excel in Passing Year Column.";
	public String EXAMCENTERMESSAGE="Test Data not found in Excel in Exam Center Column.";
	public String LANGUAGEMESSAGE="Test Data not found in Excel in Language Column.";
	public String TENTATIVEEXAMMESSAGE="Test Data not found in Excel in Tentative Exam Date Column.";
	public String FINALEXAMMESSAGE="Test Data not found in Excel in Final Exam Date Column.";
	public String TIMESLOTMESSAGE="Test Data not found in Excel in Time Slot Column.";
	public String RESULTMESSAGE="Test Data not found in Excel in Result Column.";
	public String EXAMROLLMESSAGE="Test Data not found in Excel in Exam Roll Column.";
	public String MARKSMESSAGE="Test Data not found in Excel in Marks Column.";
	public String INTERMEDIARYSTARTMESSAGE="Test Data not found in Excel in Intermediary Start Column.";
	public String INTERMEDIARYENDMESSAGE="Test Data not found in Excel in Intermediary End date Column.";
	public String NOOFPOLICIESMESSAGE="Test Data not found in Excel in No of Policies Column.";
	public String PREMIUMAMOUNTMESSAGE="Test Data not found in Excel in No of Premium Amount Column.";
	public String RecruitmentSourceMessage="Test Data not found in Excel in Recruitment Source Column.";
	public String CertificateCourseMessage="Test Data not found in Excel in Certificate Course Column.";
	public String KnowreligareEmployeeMessage="Test Data not found in Excel in Know Religare Employee Column.";
	public String TrainingStartDateMessage="Test Data not found in Excel in Training Start Date Column.";
	public String TrainingEndDateMessage="Test Data not found in Excel in Training EndDate Column.";

	//Modification after agent active
	public String ModificationReasonMessage="Test Data not found in Excel in Modification Reason Column.";
	public String ModificationRemarksMessage="Test Data not found in Excel in Modification Remarks Column.";
	public String UploadDocumentMessage="Test Data not found in Excel in Upload Documents Column.";
	
	
	
	
	
	
}
