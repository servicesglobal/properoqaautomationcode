package com.org.propero.agencymodule.basefunctions;
import java.util.regex.Pattern;

public class ValidExpression {

	 public static boolean isValid(String email) 
	    { 
	        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
	                            "[a-zA-Z0-9_+&*-]+)*@" + 
	                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
	                            "A-Z]{2,7}$"; 
	                              
	        Pattern pat = Pattern.compile(emailRegex); 
	        if (email == null) 
	            return false; 
	        return pat.matcher(email).matches(); 
	    } 
	 
	 
	 public static boolean isValidPanNumber(String panNumber)
	 {
			String panRegex = "[A-Z]{5}[0-9]{4}[A-Z]{1}"; 
                      
			Pattern pat = Pattern.compile(panRegex); 
			
			if (panNumber == null) 
				return false; 
			return pat.matcher(panNumber).matches(); 
			
			
	 }
	 
	 public static boolean isValidIfscCode(String ifscCode)
	 {
		 String ifscRegex = "[A-Z]{4}[0-9]{7}"; 
         
			Pattern pat = Pattern.compile(ifscRegex); 
			
			if (ifscCode == null) 
				return false; 
			return pat.matcher(ifscCode).matches(); 
	 }
	 
	 public static boolean isvalidMicrNumber(String micrNumber)
	 {
		 String micrRegex = "[0-9]{9}"; 
         
			Pattern pat = Pattern.compile(micrRegex); 
			
			if (micrNumber == null) 
				return false; 
			return pat.matcher(micrNumber).matches(); 
	 }
}
