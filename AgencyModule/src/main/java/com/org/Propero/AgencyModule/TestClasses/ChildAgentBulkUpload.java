package com.org.propero.agencymodule.testClasses;

import org.testng.annotations.Test;

import com.org.propero.agencymodule.bulkupload.BulkUploadFunction;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.login.LoginSingle;
import com.org.propero.agencymodule.utility.BaseClass;

public class ChildAgentBulkUpload extends BaseClass implements ProjectInterface{

	@Test 
	public void uploadIntermediary() throws Exception{
		logger = extent.startTest("Agent Creation - TC_"+1+" - " +"Test case 1");
		LoginSingle.login();
		BulkUploadFunction.selectUploadIntermediary();
		BulkUploadFunction.setUploadIntermediaryDetails("ChildbulkUpload",1);
		
	}
	
}
