package com.org.propero.agencymodule.testClasses;
import org.testng.annotations.Test;
import com.org.propero.agencymodule.utility.BaseClass;


public class MainTestClass extends BaseClass {
	
	@Test(priority=001, alwaysRun=true,enabled=true)
	public void agenTCreationTestCases() throws Throwable{
		AgentCreation.agentCreate();
		
	}
	
	@Test(priority=002, alwaysRun=true,enabled=true)
	public void agenTCreationTestCases_NegativeTestCase1() throws Throwable{
		
		AgentCreation.negativeTestCase1();
		
	}
	
	@Test(priority=003, alwaysRun=true,enabled=true)
	public void agenTCreationTestCases_NegativeTestCase2() throws Throwable{
		
		
		AgentCreation.negativeTestCase2();
	}
	
	@Test(priority=004,alwaysRun=true,enabled=true)
	public void agnetModificationTestCases()throws Exception{
		AgentModificationTestClass.modificationofAgent();
	}
	
	/*@Test(priority=003, alwaysRun=false, enabled=false)
	   public void communicationDeatilsTestcases() throws Exception{
			CommunicationDetailsTestClass.tc_01_VerifyAgentDetails_Using_communiCationType_Agent();
			
	}*/
	
}
