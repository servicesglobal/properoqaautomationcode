package com.org.propero.agencymodule.addagent;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.org.propero.agencymodule.assertion.FooterError;
import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.errormessages.PageNotFoundError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ExamandLicense extends BaseClass implements ProjectInterface
{

	private static String testresult;
	private static Logger logger1 = LoggerFactory.getLogger(ExamandLicense.class);
	
	public static String licenceDetails(String sheetName,int rowNum) throws Exception
	{
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		//Click on Exam and License tab
		clickElement(By.xpath(ExamandLicense_Xpath));
				
		try
		{

		//Entering IRDA License Number on UI
		String iRDALicenseNum = read.getCellData(sheetName, "IRDA_LicenseNum", rowNum);
		if(iRDALicenseNum.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Aadhar Card.");	
		}else{
		enterText(By.id(IRDALicenseNum_Id), iRDALicenseNum);
		logger1.info("Entered Irda License Number is : "+iRDALicenseNum);
		}
		
		//Selecting License type code is 
		String licenseType = read.getCellData(sheetName, "License_TypeCode", rowNum);
		if(licenseType.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to License Type.");	
		}else{
		selecttext(By.id(LicenseTypeCd_Id), licenseType);
		logger1.info("Selected License Type Code is : "+licenseType);
		}
		
		//Selecting Company Associated With
		String companyassociatedwith = read.getCellData(sheetName, "Company_AssociatedWith", rowNum);
		if(companyassociatedwith.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Company Associated With.");		
		}else{
		//selecttext(By.id(CompanyAssociated_Id), companyassociatedwith);
		selecttext2(By.id(CompanyAssociated_Id), companyassociatedwith);
		logger1.info("Selected Company Associated With is : "+companyassociatedwith);
		}
		
		
		//Select state 
		String state = read.getCellData(sheetName, "State_Name", rowNum);
		if(state.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to state.");	
		}else{
		//selecttext(By.id(State_Id), state);
		selecttext2(By.id(State_Id), state);
		logger1.info("Selected state is : "+state);
		}
		
		//Select City on UI
		Thread.sleep(5000);
		String city1 = read.getCellData(sheetName, "City_Name", rowNum);
		String city=city1.toUpperCase();
		if(city.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to City.");	
		}else{
		waitForElementsToBeClickable(By.xpath("//select[@id='city-0-0']"));
		clickElement(By.xpath("//select[@id='city-0-0']"));
		waitForElementsToBeClickable(By.xpath("//select[@id='city-0-0']//option[contains(text(),"+"'"+city+"'"+")]"));
		clickElement(By.xpath("//select[@id='city-0-0']//option[contains(text(),"+"'"+city+"'"+")]"));
		/*selecttext(By.id(City_Xpath), City);*/
		logger1.info("Selected City is : "+city);
		}
		
		//Select Branch on UI
		String branch  = read.getCellData(sheetName, "Branch_Name", rowNum);
		if(branch.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Branch.");	
		}else{
		enterText(By.id(Branch_Id), branch);
		logger1.info("Entered Branch Name : "+branch);
		}
		
		
		//Selecting Issue date from date-picker on UI by fecthing data from Excel
		Calendar.calender(sheetName, By.id(Issuedate_Id),"Issue_Date", rowNum);
		
		ExamandLicense.eucationDetails(sheetName,rowNum);
		ExamandLicense.examDetails(sheetName,rowNum);
		
		
	/*	//End date
		Calendar.calender(sheetName, By.xpath(ExpiryDate_Id),"End_Date", rowNum);*/
		Calendar.calender(sheetName, By.id(IntermediaryJoin_Id),"Intermediary_Start", rowNum);
		
		
		//No Of policies
		String noofpolicies = read.getCellData(sheetName, "Noof_Policies", rowNum);
		if(noofpolicies.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to No of Policies.");	
		}else{
		enterText(By.xpath(Noof_Policies_Xpath), noofpolicies);
		logger1.info("Entered No of policies are : "+noofpolicies);
		}
		
		
		//Premium Amount
		String premiumAmount=read.getCellData(sheetName, "Premium_Amount", rowNum);
		if(premiumAmount.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Premium Amount.");	
		}else{
		enterText(By.xpath(Premium_Amount_Xpath), premiumAmount);
		logger1.info("Entered Premium Amount is : "+premiumAmount);
		}
		
		//Enter Recruitment Source
		String recruitmentSource = read.getCellData(sheetName, "Recruitment_Source", rowNum);
		if(recruitmentSource.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Recruitment Source.");	
		}else{
		selecttext(By.xpath(RecruitmentSource_Xpath), recruitmentSource);
		logger1.info("Selected Recruitment Source is : "+recruitmentSource);
		}
		
		//Select Certificate Course
		String certificateCourse = read.getCellData(sheetName, "Certificate_Course", rowNum);
		if(certificateCourse.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Certificate Course.");	
		}else{
		selecttext(By.xpath(CertificateCourse_Xpath), certificateCourse);
		logger1.info("Selected Certificate Course is : "+certificateCourse);
		}	
		
		//Know religare Employee
		String  knowReligareEmployee = read.getCellData(sheetName, "KnowReligare_Employee", rowNum);
		if(knowReligareEmployee.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Know Religare Employee.");	
		}else{
		selecttext(By.xpath(KnowreligareEmployee_Xpath), knowReligareEmployee);
		logger1.info("Selected know Religare Emnployee Field is : "+knowReligareEmployee);
		}
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "License and Details Tab")));
		
		verticalscrollup();
		
		clickElement(By.xpath(Submit_Button_Xpath));
		
		ErrorMessages.fieldverification("Final Submit");
		
		ParentAgentIdCheck.parentagentcheck();
		testresult="Pass";
		
	}catch(Exception  e)
	{
		testresult="Fail";
		//Assert.fail("Error in Licence Details Page");
		throw e;
	}
		return testresult;
		
		
	}
	
	
	public static void eucationDetails(String sheetName,int rowNum) throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		
		String education=read.getCellData(sheetName, "Education", rowNum);
		if(education.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Education.");	
		}else{
		Boolean educationTable= driver.findElements(By.xpath("//table[@class='agent-education-table']//td//select")).size()!=0;
		if(educationTable==true)
		{
		if(education.equals(""))
		{
			logger1.info("No Need to Enter Education Details.");
		}else{
			clickElement(By.xpath("//table[@class='agent-education-table']//td//select"));
			clickElement(By.xpath("//table[@class='agent-education-table']//td//select//option[contains(text(),"+"'"+education+"'"+")]"));
			logger1.info("Selected Education is : "+education);
		}
		
		String boardName=read.getCellData(sheetName, "Board_Name", rowNum);
		if(boardName.equals(""))
		{
			logger1.info("No Need to Enter Board Name.");
		}else{
			enterText(By.xpath(BoardName_Xpath), boardName);
			logger1.info("Entered Board Name is : "+boardName);
		}
		
		String rollNum = read.getCellData(sheetName, "Roll_No", rowNum);
		if(rollNum.equals(""))
		{
			logger1.info("No Need to Enter Roll Number.");
		}else{
			enterText(By.xpath(RollNum_Xpath), rollNum);
			logger1.info("Entered Roll Number is : "+rollNum);
		}
		
		String passingYear=read.getCellData(sheetName, "Passing_Year", rowNum);
		if(passingYear.equals(""))
		{
			logger1.info("No Need to Enter Passing Year.");
		}else {
			enterText(By.xpath(PassingYear_Xpath), passingYear);
			logger1.info("Entered Passing Year is : "+passingYear);
		}
		}
		else if(educationTable==false)
		{
			logger1.info("No Need to Fill Education Table.");
		}
		
		}
		
	}
	
	public static void examDetails(String sheetName,int rowNum) throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		
		String examCenter=read.getCellData(sheetName, "Exam_Center", rowNum);
		Boolean examDet = driver.findElements(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select")).size()!=0;
		if(examDet==true)
		{
		if(examCenter.equals(""))
		{
			logger1.info("No Need to Select Exam Center.");
		}else {
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select"));
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select//option[contains(text(),"+"'"+examCenter+"'"+")]"));
			logger1.info("Selected Exam Center is : "+examCenter);
		}
		
		
		String language = read.getCellData(sheetName, "Language", rowNum);
		if(language.equals(""))
		{
			logger1.info("No Need to Select Language.");
		}else {
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select"));
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select//option[contains(text(),"+"'"+language+"'"+")]"));
			logger1.info("Selected Language is : "+language);
		}
		
		String tentativeExamDate=read.getCellData(sheetName, "TentativeExam_Date", rowNum);
		if(tentativeExamDate.equals(""))
		{
			logger1.info("No Need to Enter Tentative Exam Date.");
		}else {
			/*Calendar.calender(sheetName, By.xpath("//input[@id='tentativeExamDate-0-0']"), "TentativeExam_Date", rowNum);*/
			enterText(By.xpath("//input[@id='tentativeExamDate-0-0']"), tentativeExamDate);
		}
		
	}
		else if(examDet==false)
		{
			logger1.info("No Need to Fill Exam Details.");
		}
	}
	
	//*********************************** NegativeTesting ***********************************
	
	public static String licenceDetailsForNegativeTestcases(String sheetName,int rowNum) throws Exception
	{
		
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		try{
		//Click on Exam and License tab
		clickElement(By.xpath(ExamandLicense_Xpath));

		//Entering IRDA License Number on UI
		String iRDALicenseNum = read.getCellData(sheetName, "IRDA_LicenseNum", rowNum);
		if(iRDALicenseNum.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Aadhar Card.");	
		}else{
		enterText(By.id(IRDALicenseNum_Id), iRDALicenseNum);
		logger1.info("Entered Irda License Number is : "+iRDALicenseNum);
		}
		
		//Selecting License type code is 
		String licenseType = read.getCellData(sheetName, "License_TypeCode", rowNum);
		if(licenseType.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to License Type.");	
		}else{
		selecttext(By.id(LicenseTypeCd_Id), licenseType);
		logger1.info("Selected License Type Code is : "+licenseType);
		}
		
		//Selecting Company Associated With
		String companyassociatedwith = read.getCellData(sheetName, "Company_AssociatedWith", rowNum);
		if(companyassociatedwith.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Company Associated With.");		
		}else{
		selecttext(By.id(CompanyAssociated_Id), companyassociatedwith);
		logger1.info("Selected Company Associated With is : "+companyassociatedwith);
		}
		
		
		//Select state 
		String state = read.getCellData(sheetName, "State_Name", rowNum);
		if(state.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to state.");	
		}else{
		selecttext(By.id(State_Id), state);
		logger1.info("Selected state is : "+state);
		}
		
		//Select City on UI
		Thread.sleep(2000);
		String city1 = read.getCellData(sheetName, "City_Name", rowNum);
		
		String city=city1.toUpperCase();
		if(city.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to City.");	
		}else{
		clickElement(By.xpath("//select[@id='city-0-0']"));
		clickElement(By.xpath("//select[@id='city-0-0']//option[contains(text(),"+"'"+city+"'"+")]"));
		/*selecttext(By.id(City_Xpath), City);*/
		logger1.info("Selected City is : "+city);
		}
		
		//Select Branch on UI
		String branch  = read.getCellData(sheetName, "Branch_Name", rowNum);
		if(branch.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Branch.");	
		}else{
		enterText(By.id(Branch_Id), branch);
		logger1.info("Entered Branch Name : "+branch);
		}
		
		
		//Selecting Issue date from date-picker on UI by fecthing data from Excel
		Calendar.calender(sheetName, By.id(Issuedate_Id),"Issue_Date", rowNum);
		
		ExamandLicense.eucationDetailsNegative(sheetName,rowNum);
		ExamandLicense.examDetailsNegative(sheetName,rowNum);
		
		
	/*	//End date
		Calendar.calender(sheetName, By.xpath(ExpiryDate_Id),"End_Date", rowNum);*/
		Calendar.calender(sheetName, By.id(IntermediaryJoin_Id),"Intermediary_Start", rowNum);
		
		
		//No Of policies
		String noofpolicies = read.getCellData(sheetName, "Noof_Policies", rowNum);
		if(noofpolicies.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to No of Policies.");	
		}else{
		enterText(By.xpath(Noof_Policies_Xpath), noofpolicies);
		logger1.info("Entered No of policies are : "+noofpolicies);
		ErrorMessages.fieldverification("No. of Policies");
		}
		
		
		//Premium Amount
		String premiumAmount=read.getCellData(sheetName, "Premium_Amount", rowNum);
		if(premiumAmount.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Premium Amount.");	
		}else{
		enterText(By.xpath(Premium_Amount_Xpath), premiumAmount);
		logger1.info("Entered Premium Amount is : "+premiumAmount);
		ErrorMessages.fieldverification("Premium Amount");
		}
		
		//Enter Recruitment Source
		String recruitmentSource = read.getCellData(sheetName, "Recruitment_Source", rowNum);
		if(recruitmentSource.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Recruitment Source.");	
		}else{
		selecttext(By.xpath(RecruitmentSource_Xpath), recruitmentSource);
		logger1.info("Selected Recruitment Source is : "+recruitmentSource);
		}
		
		//Select Certificate Course
		String certificateCourse = read.getCellData(sheetName, "Certificate_Course", rowNum);
		if(certificateCourse.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Certificate Course.");	
		}else{
		selecttext(By.xpath(CertificateCourse_Xpath), certificateCourse);
		logger1.info("Selected Certificate Course is : "+certificateCourse);
		}	
		
		//Know religare Employee
		String  knowReligareEmployee = read.getCellData(sheetName, "KnowReligare_Employee", rowNum);
		if(knowReligareEmployee.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Know Religare Employee.");	
		}else{
		selecttext(By.xpath(KnowreligareEmployee_Xpath), knowReligareEmployee);
		logger1.info("Selected know Religare Emnployee Field is : "+knowReligareEmployee);
		}
		
		verticalscrollup();
		
		clickElement(By.xpath(Submit_Button_Xpath));
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhotpass(driver, "License and Details Tab")));
		
		ErrorMessages.fieldverification("Final Submit");
		
		//ParentAgentIdCheck.parentagentcheck();
		
		testresult="Pass";
		}
		catch(Exception e){
			testresult="Fails";
			throw e;
		}
		return testresult;
		
	}
	
	
	public static void eucationDetailsNegative(String sheetName,int rowNum) throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		
		String education=read.getCellData(sheetName, "Education", rowNum);
		if(education.equals(""))
		{
			logger1.info("There is no data in Excel Sheet wrt to Education.");	
		}else{
		Boolean educationTable= driver.findElements(By.xpath("//table[@class='agent-education-table']//td//select")).size()!=0;
		if(educationTable==true)
		{
		if(education.equals(""))
		{
			logger1.info("No Need to Enter Education Details.");
		}else{
			clickElement(By.xpath("//table[@class='agent-education-table']//td//select"));
			clickElement(By.xpath("//table[@class='agent-education-table']//td//select//option[contains(text(),"+"'"+education+"'"+")]"));
			logger1.info("Selected Education is : "+education);
		}
		
		String boardName=read.getCellData(sheetName, "Board_Name", rowNum);
		if(boardName.equals(""))
		{
			logger1.info("No Need to Enter Board Name.");
		}else{
			enterText(By.xpath(BoardName_Xpath), boardName);
			logger1.info("Entered Board Name is : "+boardName);
		}
		
		String rollNum = read.getCellData(sheetName, "Roll_No", rowNum);
		if(rollNum.equals(""))
		{
			logger1.info("No Need to Enter Roll Number.");
		}else{
			enterText(By.xpath(RollNum_Xpath), rollNum);
			logger1.info("Entered Roll Number is : "+rollNum);
			ErrorMessages.fieldverification("Roll No.");
		}
		
		String passingYear=read.getCellData(sheetName, "Passing_Year", rowNum);
		if(passingYear.equals(""))
		{
			logger1.info("No Need to Enter Passing Year.");
		}else {
			enterText(By.xpath(PassingYear_Xpath), passingYear);
			logger1.info("Entered Passing Year is : "+passingYear);
			ErrorMessages.fieldverification("Passing Year.");
		}
		}
		else if(educationTable==false)
		{
			logger1.info("No Need to Fill Education Table.");
		}
		
		}
		
	}
	
	public static void examDetailsNegative(String sheetName,int rowNum) throws Exception
	{
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		
		String examCenter=read.getCellData(sheetName, "Exam_Center", rowNum);
		Boolean examDet = driver.findElements(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select")).size()!=0;
		if(examDet==true)
		{
		if(examCenter.equals(""))
		{
			logger1.info("No Need to Select Exam Center.");
		}else {
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select"));
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMCENTER']//select//option[contains(text(),"+"'"+examCenter+"'"+")]"));
			logger1.info("Selected Exam Center is : "+examCenter);
		}
		
		
		String language = read.getCellData(sheetName, "Language", rowNum);
		if(language.equals(""))
		{
			logger1.info("No Need to Select Language.");
		}else {
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select"));
			clickElement(By.xpath("//dynamic-dropdown[@lookupkey='AGENTEXAMLANGUAGE']//select//option[contains(text(),"+"'"+language+"'"+")]"));
			logger1.info("Selected Language is : "+language);
		}
		
		String tentativeExamDate=read.getCellData(sheetName, "TentativeExam_Date", rowNum);
		if(tentativeExamDate.equals(""))
		{
			logger1.info("No Need to Enter Tentative Exam Date.");
		}else {
			/*Calendar.calender(sheetName, By.xpath("//input[@id='tentativeExamDate-0-0']"), "TentativeExam_Date", rowNum);*/
			enterText(By.xpath("//input[@id='tentativeExamDate-0-0']"), tentativeExamDate);
		}
		
	}
		else if(examDet==false)
		{
			logger1.info("No Need to Fill Exam Details.");
		}
	}
}