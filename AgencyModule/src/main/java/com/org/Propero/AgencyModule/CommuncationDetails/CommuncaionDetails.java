package com.org.propero.agencymodule.communcationDetails;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;

public class CommuncaionDetails extends BaseClass implements ProjectInterface {
	
	private static String testResult;
	
	CommuncationDetailsGetterAndSetterMethod obj;

	public String getDetailsOfAgentOrCustomer(int rowNum) throws Exception {
		obj = new CommuncationDetailsGetterAndSetterMethod();

		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String sheetName = "CommuncationDetails";

		try {

			String setCommunicationTypeValue = read.getCellData(sheetName, "Communcation Type", rowNum);
			String setAgentValue = read.getCellData(sheetName, "Agen ID", rowNum);
			String setPageCountValue = read.getCellData(sheetName, "PageCount", rowNum);
			String setPaginationCountValue = read.getCellData(sheetName, "PaginationCount", rowNum);

			obj.setCommunicationType(setCommunicationTypeValue);
			obj.setAgentId(setAgentValue);
			obj.setPageCount(setPageCountValue);
			obj.setPagenaitionCount(setPaginationCountValue);

			obj.setDetailscommuncationType("CommuncationDetails", rowNum, obj.getCommunicationType(), obj.getAgentId(),obj.getPageCount(), obj.getPagenaitionCount());

			testResult = "Pass";
		} catch (Exception e) {
			
			testResult = "Fail";
			throw e;

		}

		return testResult;
	}
}
