package com.org.propero.agencymodule.modificationFn;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.Calendar;
import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;

import junit.framework.Assert;

public class ExamLicenseandTrainingModification extends BaseClass implements ModificationInterface,MessageInterface
{

	private static Logger logger1 = LoggerFactory.getLogger(ExamLicenseandTrainingModification.class);
	
	private static Pattern dateFrmtPtrn = Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");

	public void setIrdaLicenseNumber(String newIrdaLicenseNumber) {
		Variable.IrdaLicenseNumber = newIrdaLicenseNumber;
	}

	public String getIrdaLicenseNumber() {
		return Variable.IrdaLicenseNumber;
	}
	
	public void setLicenseTypeCode(String newLicenseTypeCode) {
		Variable.LicenseTypeCode = newLicenseTypeCode;
	}

	public String getLicenseTypeCode() {
		return Variable.LicenseTypeCode;
	}
	
	public void setCompanyassociatewith(String newCompanyassociatewith) {
		Variable.Companyassociatewith = newCompanyassociatewith;
	}

	public String getCompanyassociatewith() {
		return Variable.Companyassociatewith;
	}
	
	public void setLicenseState(String newLicenseState) {
		Variable.LicenseState = newLicenseState;
	}

	public String getLicenseState() {
		return Variable.LicenseState;
	}
	
	public void setLicenseCity(String newLicenseCity)  {
		Variable.LicenseCity = newLicenseCity;
	}

	public String getLicenseCity() {
		return Variable.LicenseCity;
	}
	
	public void setLicenseBranch(String newLicenseBranch) {
		Variable.LicenseBranch = newLicenseBranch;
	}

	public String getLicenseBranch() {
		return Variable.LicenseBranch;
	}
	
	public void setLicenseIssueDate(String newLicenseIssueDate)  {
		Variable.LicenseIssueDate = newLicenseIssueDate;
	}

	public String getLicenseIssueDate() {
		return Variable.LicenseIssueDate;
	}
	
	public void setLicenseEndDate(String newLicenseEndDate)  {
		Variable.LicenseEndDate = newLicenseEndDate;
	}

	public String getLicenseEndDate() {
		return Variable.LicenseEndDate;
	}
	
	public void setIntermediaryStartDate(String newIntermediaryStartDate)  {
		Variable.IntermediaryStartDate = newIntermediaryStartDate;
	}

	public String getIntermediaryStartDate() {
		return Variable.IntermediaryStartDate;
	}
	
	public void setIntermediaryEndDate(String newIntermediaryEndDate)  {
		Variable.IntermediaryEndDate = newIntermediaryEndDate;
	}

	public String getIntermediaryEndDate() {
		return Variable.IntermediaryEndDate;
	}
	
	public void setNoofPolicies(String newNoofPolicies)  {
		Variable.NoofPolicies = newNoofPolicies;
	}

	public String getNoofPolicies(){
		return Variable.NoofPolicies;
	}
	
	public void setPremiumAmount(String newPremiumAmount)  {
		Variable.PremiumAmount = newPremiumAmount;
	}

	public String getPremiumAmount() {
		return Variable.PremiumAmount;
	}
	
	public void setRecruitmentSource(String newRecruitmentSource)  {
		Variable.RecruitmentSource = newRecruitmentSource;
	}

	public String getRecruitmentSource() {
		return Variable.RecruitmentSource;
	}
	
	public void setCertificateCourse(String newCertificateCourse) {
		Variable.CertificateCourse = newCertificateCourse;
	}

	public String getCertificateCourse() {
		return Variable.CertificateCourse;
	}
	
	public void setKnowReligareEmployee(String newKnowReligareEmployee) {
		Variable.KnowReligareEmployee = newKnowReligareEmployee;
	}

	public String getKnowReligareEmployee() {
		return Variable.KnowReligareEmployee;
	}
	
	public void setTrainingStartDate(String newTrainingStartDate)  {
		Variable.TrainingStartDate = newTrainingStartDate;
	}

	public String getTrainingStartDate() {
		return Variable.TrainingStartDate;
	}
	
	public void setTrainingEndDate(String newTrainingEndDate) throws Exception {
		Variable.TrainingEndDate = newTrainingEndDate;
	}

	public String getTrainingEndDate() {
		return Variable.TrainingEndDate;
	}
	
	
	public void setAgentEducation(String newAgentEducation) {
		Variable.AgentEducation = newAgentEducation;
	}

	public String getAgentEducation() {
		return Variable.AgentEducation;
	}
	
	public void setBoardName(String newBoardName) {
		Variable.BoardName = newBoardName;
	}

	public String getBoardName() {
		return Variable.BoardName;
	}
	
	public void setRollNumber(String newRollNumber)  {
		Variable.RollNumber = newRollNumber;
	}

	public String getRollNumber() {
		return Variable.RollNumber;
	}
	
	
	public void setPassingYear(String newPassingYear) {
		Variable.PassingYear = newPassingYear;
	}

	public String getPassingYear() {
		return Variable.PassingYear;
	}
	
	
	public void setExamCenter(String newExamCenter) {
		Variable.ExamCenter = newExamCenter;
	}

	public String getExamCenter() {
		return Variable.ExamCenter;
	}
	
	public void setLanguage(String newLanguage) {
		Variable.Language = newLanguage;
	}

	public String getLanguage() {
		return Variable.Language;
	}
	
	public void setTentativeExamdate(String newTentativeExamdate) {
		Variable.TentativeExamdate = newTentativeExamdate;
	}

	public String getTentativeExamdate() {
		return Variable.TentativeExamdate;
	}
	
	public void setFinalExamDate(String newFinalExamDate) {
		Variable.FinalExamDate = newFinalExamDate;
	}

	public String getFinalExamDate() {
		return Variable.FinalExamDate;
	}
	
	public void setTimeSlot(String newTimeSlot) {
		Variable.TimeSlot = newTimeSlot;
	}

	public String getTimeSlot() {
		return Variable.TimeSlot;
	}
	
	public void setResult(String newResult) {
		Variable.Result = newResult;
	}

	public String getResult() {
		return Variable.Result;
	}
	
	public void setExamRoll(String newExamRoll) {
		Variable.ExamRoll = newExamRoll;
	}

	public String getExamRoll() {
		return Variable.ExamRoll;
	}
	
	
	public void setMarks(String newMarks) {
		Variable.Marks = newMarks;
	}

	public String getMarks() {
		return Variable.Marks;
	}
	
	public void examandLicenseTraining1(String agentid,String workbookName,int rowNum,String agentType,
			String irdaLicenseNumber,String licenseTypeCode,String companyassociatewith,String licenseState,
			String licenseCity,String licenseBranch,String licenseIssueDate,String licenceEndDate,String agentEducation,
			String boardName,String rollNumber,String passingYear,String examCenter,
			String language,String tentativeExamdate,String finalExamDate,String timeSlot,
			String examRoll,String marks,String intermediaryStartDate,String intermediaryEndDate,String noofPolicies,
			String premiumAmount, String recruitmentSource,String certificateCourse,String knowReligareEmployee,
			String trainingStartDate,String trainingEndDate) throws Exception
	{
		verticalscrollup();
		Thread.sleep(3000);
		clickElement(By.xpath(ExamandLicense_Xpath));
		
		if(agentType.equalsIgnoreCase("Exising Agent"))
		{
			//clickElement(By.xpath(ExamandLicense_Xpath));
			ExamLicenseandTrainingModification.licenseDetails(agentid, workbookName, rowNum, irdaLicenseNumber, licenseTypeCode, companyassociatewith, licenseState, licenseCity, licenseBranch, licenseIssueDate,licenceEndDate);
			ExamLicenseandTrainingModification.othersDetails(agentid, workbookName, rowNum, intermediaryStartDate,intermediaryEndDate, noofPolicies,premiumAmount, recruitmentSource, certificateCourse, knowReligareEmployee);
			ExamLicenseandTrainingModification.training(agentid, trainingStartDate, trainingEndDate);
		}
		else if (agentType.equalsIgnoreCase("Fresh Agent"))
		{
			//clickElement(By.xpath(ExamandLicense_Xpath));
			ExamLicenseandTrainingModification.licenseDetails(agentid, workbookName, rowNum, irdaLicenseNumber, licenseTypeCode, companyassociatewith, licenseState, licenseCity, licenseBranch, licenseIssueDate,licenceEndDate);	
			ExamLicenseandTrainingModification.educationDetails(agentid, agentEducation, boardName, rollNumber, passingYear);
			ExamLicenseandTrainingModification.examDetails(agentid, examCenter, language, tentativeExamdate, finalExamDate, timeSlot, examRoll, marks);
			ExamLicenseandTrainingModification.othersDetails(agentid, workbookName, rowNum, intermediaryStartDate,intermediaryEndDate, noofPolicies, premiumAmount, recruitmentSource,certificateCourse,knowReligareEmployee);
			ExamLicenseandTrainingModification.training(agentid, trainingStartDate, trainingEndDate);
		}
			
	}
	
	
	public void examandLicenseTraining(String agentid,String workbookName,int rowNum,String agentType,
			String irdaLicenseNumber,String licenseTypeCode,String companyassociatewith,
			String licenseState,String licenseCity,String licenseBranch,String licenseIssueDate,String licenseEndDate,String agentEducation,String boardName,
			String rollNumber,String passingYear,String examCenter,String language,String tentativeExamdate,String finalExamDate,
			String timeSlot,String examRoll,String marks,String intermediaryStartDate,String intermediaryEndDate,String noofPolicies,String premiumAmount, String recruitmentSource
			,String certificateCourse,String knowReligareEmployee,String trainingStartDate,String trainingEndDate) throws Exception
	{
		
		    Thread.sleep(5000);
		    clickElement(By.xpath(ExamandLicense_Xpath));
			ExamLicenseandTrainingModification.licenseDetails(agentid, workbookName, rowNum, irdaLicenseNumber, licenseTypeCode, companyassociatewith, licenseState, licenseCity, licenseBranch, licenseIssueDate,licenseEndDate);	
			ExamLicenseandTrainingModification.educationDetails(agentid, agentEducation, boardName, rollNumber, passingYear);
			ExamLicenseandTrainingModification.examDetails(agentid, examCenter, language, tentativeExamdate, finalExamDate, timeSlot, examRoll, marks);
			ExamLicenseandTrainingModification.othersDetails(agentid, workbookName, rowNum, intermediaryStartDate,intermediaryEndDate,noofPolicies, premiumAmount, recruitmentSource, certificateCourse, knowReligareEmployee);
			ExamLicenseandTrainingModification.training(agentid, trainingStartDate, trainingEndDate);
		
	}
	
	public static void licenseDetails(String agentid1,String workbookName,int rowNum,String irdaLicenseNumber,String licenseTypeCode,String companyassociatewith,
			String licenseState,String licenseCity,String licenseBranch,String licenseIssueDate,String licenseEndDate) throws Exception
	{
		String agentid=agentid1.trim();
		//Filling IRDA Number on UI in License Details in Exam and License Details
		
		Thread.sleep(3000);
		WebElement e1=driver.findElement(By.xpath("//input[@id='licenseNo-"+agentid+"-0']"));
		boolean checkFieldIsEnabled=e1.isEnabled();
		
		if(checkFieldIsEnabled){
		
		if(irdaLicenseNumber.equals(""))
		{
			logger1.info(IRDALICENSENUMBERMESSAGE);
		}else{
		
			
			Thread.sleep(5000);
			clearTextfield(By.xpath("//input[@id='licenseNo-"+agentid+"-0']"));
			enterText(By.xpath("//input[@id='licenseNo-"+agentid+"-0']"), irdaLicenseNumber);
			logger1.info("Entered Irda License Number is : "+irdaLicenseNumber);
			}
			
			
			
		
		//Filling License Type Code in License Details in Exam and License Details
		if(licenseTypeCode.equals(""))
		{
			logger1.info(LICENSETYPEMESSAGE);
		}else
		{
			
			clickElement(By.xpath("//select[@id='licenseTypeCd-"+agentid+"-0']"));
			fluentwait(By.xpath("//select[@id='licenseTypeCd-"+agentid+"-0']//option"), 60,
					"License Type Code Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='licenseTypeCd-"+agentid+"-0']//option"), licenseTypeCode);
			logger1.info("Selected LicenseType Code is : " + licenseTypeCode);
			}
			
		
		
		//Filling Company Associated with in License Details in Exam and License Details
		if(companyassociatewith.equals(""))
		{
			logger1.info(COMPANYASSOCIATEDWITHMESSAGE);
		}else{
			
			clickElement(By.xpath("//select[@id='companyAssociateWith-"+agentid+"-0']"));
			fluentwait(By.xpath("//select[@id='companyAssociateWith-"+agentid+"-0']//option"), 60,
					"Company Associate Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='companyAssociateWith-"+agentid+"-0']//option"), companyassociatewith);
			logger1.info("Selected Company associate with is : " + companyassociatewith);
			}
			
		
		
		//Filling State with in License Details in Exam and License Details
		if(licenseState.equals(""))
		{
			logger1.info(LICENSESTATEMESSAGE);
		}else
		{
			
			clickElement(By.xpath("//select[@id='state-"+agentid+"-0']"));
			fluentwait(By.xpath("//select[@id='state-"+agentid+"-0']//option"), 60,
					"License State Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='state-"+agentid+"-0']//option"), licenseState);
			logger1.info("Selected License State is : " + licenseState);
			}
			
		
		
		//Filling City with in License Details in Exam and License Details
		if(licenseCity.equals(""))
		{
			logger1.info(LICENSECITYMESSAGE);
		}else{
			
			clickElement(By.xpath("//select[@id='city-"+agentid+"-0']"));
			fluentwait(By.xpath("//select[@id='city-"+agentid+"-0']//option"), 60,
					"License City Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//select[@id='city-"+agentid+"-0']//option"), licenseCity);
			logger1.info("Selected License City is : " + licenseCity);
			}
			
		
		
		//Filling Branch With in License Details in Exam and License Details
		if(licenseBranch.equals(""))
		{
			logger1.info(LICENSEBRANCHMESSAGE);
		}else{
			
			clearTextfield(By.xpath("//input[@id='branch-"+agentid+"-0']"));
			enterText(By.xpath("//input[@id='branch-"+agentid+"-0']"), licenseBranch);
			logger1.info("Entered License Branch Name : "+licenseBranch);
			}
			
		
		
		//Issue Date with in License Details in Exam and License Details
		if(licenseIssueDate.equals(""))
		{
			logger1.info(LICENSEISSUEDATEMESSAGE);
		}else{
			
			Matcher mtch = dateFrmtPtrn.matcher(licenseIssueDate);
			if (mtch.matches()) 
			{
				clearTextfield(By.xpath("//input[@id='issueDt-"+agentid+"-0']"));
				Thread.sleep(2000);
				Calendar.calender(workbookName, By.xpath("//input[@id='issueDt-"+agentid+"-0']"), "Issue_Date", rowNum);
			} else {
				clearTextfield(By.xpath("//input[@id='issueDt-"+agentid+"-0']"));
				Thread.sleep(2000);
				Calendar.calender(workbookName, By.xpath("//input[@id='issueDt-"+agentid+"-0']"), "Issue_Date", rowNum);
				ErrorMessages.fieldverification("License Issue Date");
			}
		}
		
		//End Date with in License Details in Exam and License Details
				if(licenseEndDate.equals(""))
				{
					logger1.info(LICENSEENDDATEMESSAGE);
				}else{
					
					Matcher mtch = dateFrmtPtrn.matcher(licenseIssueDate);
					if (mtch.matches()) 
					{
						clearTextfield(By.xpath("//input[@id='expiryDt-"+agentid+"-0']"));
						Thread.sleep(2000);
						Calendar.calender(workbookName, By.xpath("//input[@id='expiryDt-"+agentid+"-0']"), "End_Date", rowNum);
					} else {
						clearTextfield(By.xpath("//input[@id='expiryDt-"+agentid+"-0']"));
						Thread.sleep(2000);
						Calendar.calender(workbookName, By.xpath("//input[@id='expiryDt-"+agentid+"-0']"), "End_Date", rowNum);
						ErrorMessages.fieldverification("License End Date");
					}
				}
		}
		
		else{
			System.out.println("IRDA Section field will not get update because all fields are disabled");
		}
	}
	
	public static void educationDetails(String agentid1,String agentEducation,String boardName,String rollNumber,String passingYear) throws Exception
	{
		String agentid=agentid1.trim();
		//Filling Exam Center on UI
		if(agentEducation.equals(""))
		{
			logger1.info(AGENTEDUCATIONMESSAGE);
		}else{
			clickElement(By.xpath("//*[@id='education-details-"+agentid+"']/div/div/div/div/table/tbody/tr/td[1]/select"));
			fluentwait(By.xpath(EducationOption_Xpath), 60,
					"Education Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(EducationOption_Xpath), agentEducation);
			logger1.info("Selected Education is : " + agentEducation);
		}
		
		//Filling Board Name on UI
		if(boardName.equals(""))
		{
			logger1.info(BOARDNAMEMESSAGE);
		}else{
			clearTextfield(By.xpath("//*[@id='education-details-"+agentid+"']/div/div/div/div/table/tbody/tr/td[2]/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='education-details-"+agentid+"']/div/div/div/div/table/tbody/tr/td[2]/input"), boardName);
			logger1.info("Entered Board Name is : "+boardName);
		}
		
		
		//Filling Roll Number 
		if(rollNumber.equals(""))
		{
			logger1.info(EXAMROLLMESSAGE);
		}else{
			clearTextfield(By.xpath("//*[@id='education-details-"+agentid+"']/div/div/div/div/table/tbody/tr/td[3]/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='education-details-"+agentid+"']/div/div/div/div/table/tbody/tr/td[3]/input"), rollNumber);
			logger1.info("Entered Roll Number is : "+rollNumber);
			ErrorMessages.fieldverification("RollNumber");
		}
	
	
		
		//Filling Passing Year
		if(passingYear.equals(""))
		{
			logger1.info(PASSINGYEARMESSAGE);
		}else{
			clearTextfield(By.xpath("//*[@id='education-details-"+agentid+"']/div/div/div/div/table/tbody/tr/td[4]/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='education-details-"+agentid+"']/div/div/div/div/table/tbody/tr/td[4]/input"), passingYear);
			logger1.info("Entered Passing Year is : "+passingYear);
			ErrorMessages.fieldverification("PassingYear");
		}
		
		
	}
	
	public static void examDetails(String agentid1,String examCenter,String language,String tentativeExamdate,String finalExamDate,
			String timeSlot,String examRoll,String marks) throws InterruptedException, Exception
	{
	
		String agentid=agentid1.trim();
		//Filling Exam Center Details
		if(examCenter.equals(""))
		{
			logger1.info(EXAMCENTERMESSAGE);
		}else{
			
			clickElement(By.xpath(ModExamCenter_Xpath));
			fluentwait(By.xpath(ModExamCenterOption_Xpath), 60,
					"Exam Center Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModExamCenterOption_Xpath), examCenter);
			logger1.info("Selected Exam Center is : " + examCenter);
		}
		
		
		//Filling Language in Center Exam Details
		if(language.equals(""))
		{
			logger1.info(LANGUAGEMESSAGE);
		}else{
			clickElement(By.xpath(ModLanguage_Xpath));
			fluentwait(By.xpath(ModLangaugeOption_Xpath), 60,
					"Language Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModLangaugeOption_Xpath), language);
			logger1.info("Selected Language is : "+language);
		}
		
		//Filling Tentative Exam Date in Center Exam Details
		if(tentativeExamdate.equals(""))
		{
			logger1.info(TENTATIVEEXAMMESSAGE);
		}else{
			clearTextfield(By.xpath("//input[@id='tentativeExamDate-"+agentid+"-0']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='tentativeExamDate-"+agentid+"-0']"), tentativeExamdate);
			logger1.info("Tentative Exam Date is : "+tentativeExamdate);
		}
		
		
	}
	
	public static void othersDetails(String agentid1,String sheetName,int rowNum,String intermediaryStartDate,String intermediaryEndDate,String noofPolicies,String premiumAmount, String recruitmentSource
			,String certificateCourse,String knowReligareEmployee) throws Exception
	{
		String agentid=agentid1.trim();
		//Internediary Start Date in OtherDetails
		if(intermediaryStartDate.equals(""))
		{
			logger1.info(INTERMEDIARYSTARTMESSAGE);
		}else{
			
			Matcher mtch = dateFrmtPtrn.matcher(intermediaryStartDate);
			if (mtch.matches()) 
			{
				clearTextfield(By.xpath("//input[@id='joiningDt-"+agentid+"']"));
				Thread.sleep(2000);
				Calendar.calender(sheetName, By.xpath("//input[@id='joiningDt-"+agentid+"']"),"Intermediary_Start", rowNum);
			} else {
				clearTextfield(By.xpath("//input[@id='joiningDt-"+agentid+"']"));
				Thread.sleep(2000);
				Calendar.calender(sheetName, By.xpath("//input[@id='joiningDt-"+agentid+"']"),"Intermediary_Start", rowNum);
				ErrorMessages.fieldverification("IntermediaryStartDate");
			}
			
		}
		
		if(intermediaryEndDate.equals(""))
		{
			logger1.info(INTERMEDIARYENDMESSAGE);
		}else{
			
			Matcher mtch = dateFrmtPtrn.matcher(intermediaryEndDate);
			if (mtch.matches()) 
			{
				clearTextfield(By.xpath("//input[@id='terminationDt-"+agentid+"']"));
				Thread.sleep(2000);
				Calendar.calender(sheetName, By.xpath("//input[@id='terminationDt-"+agentid+"']"),"Intermediary_End date", rowNum);
			} else {
				clearTextfield(By.xpath("//input[@id='terminationDt-"+agentid+"']"));
				Thread.sleep(2000);
				Calendar.calender(sheetName, By.xpath("//input[@id='terminationDt-"+agentid+"']"),"Intermediary_End date", rowNum);
				ErrorMessages.fieldverification("IntermediaryEndDate");
			}
			
		}
		
	
		//Filling No of Policies
		if(noofPolicies.equals(""))
		{
			logger1.info(NOOFPOLICIESMESSAGE);
		}else{
			clearTextfield(By.xpath("//div[@id='license-exam-tab-"+agentid+"']//div[contains(@class,'row')]//div[3]//div[1]//input[1]"));
			Thread.sleep(2000);
			enterText(By.xpath("//div[@id='license-exam-tab-"+agentid+"']//div[contains(@class,'row')]//div[3]//div[1]//input[1]"), noofPolicies);
			logger1.info("Entered No of policies are : "+noofPolicies);
			ErrorMessages.fieldverification("NoofPolicies");
		}
	
		
		//Filling Premium Amount in Last Five Year
		if(premiumAmount.equals(""))
		{
			logger1.info(PREMIUMAMOUNTMESSAGE);
		}else{
			clearTextfield(By.xpath("//div[@id='license-exam-tab-"+agentid+"']//div[contains(@class,'row')]//div[4]//div[1]//input[1]"));
			Thread.sleep(2000);
			enterText(By.xpath("//div[@id='license-exam-tab-"+agentid+"']//div[contains(@class,'row')]//div[4]//div[1]//input[1]"), premiumAmount);
			logger1.info("Entered Premium Amount is : "+premiumAmount);
			ErrorMessages.fieldverification("PremiumAmount");
		}
		
		
		//Select Recruitment Source
		if(recruitmentSource.equals(""))
		{
			logger1.info(RecruitmentSourceMessage);
		}
		else{
			clickElement(By.xpath(ModRecruitmentSource_Xpath));
			fluentwait(By.xpath(ModRecruitmentSourceOption_Xpath), 60,
					"Recruitment Source Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModRecruitmentSourceOption_Xpath), recruitmentSource);
			logger1.info("Selected Language is : "+recruitmentSource);
		}
		
		//Select Certificae Course
		if(certificateCourse.equals(""))
		{
			logger1.info(CertificateCourseMessage);
		}else{
			clickElement(By.xpath(ModCertificateCourse_Xpath));
			fluentwait(By.xpath(ModCertificateCourseOption_Xpath), 60,
					"Certificate Course Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath(ModCertificateCourseOption_Xpath), certificateCourse);
			logger1.info("Selected Certificate Course is : "+certificateCourse);
		}
		
		//Select Know Your Religare Employee
		if(knowReligareEmployee.equals(""))
		{
		logger1.info(KnowreligareEmployeeMessage);	
		}else
		{
			clickElement(By.xpath("//*[@id='license-exam1-"+agentid+"']/div[2]/div[3]/div/select"));
			fluentwait(By.xpath("//*[@id='license-exam1-"+agentid+"']/div[2]/div[3]/div/select//option"), 60,
					"Knowreligare Employee Option is Loading Slow so Couldn't find Option.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//*[@id='license-exam1-"+agentid+"']/div[2]/div[3]/div/select//option"), knowReligareEmployee);
			logger1.info("Selected Know Religare Employee is : "+knowReligareEmployee);
		}
	}
	
	public static void training(String agentid1,String trainingStartDate,String trainingEndDate) throws Exception
	{
		String agentid=agentid1.trim();
		//Select Training Start Date
		if(trainingStartDate.equals(""))
		{
			logger1.info(TrainingStartDateMessage);
		}else{
			clearTextfield(By.xpath("//input[@id='trainingStartDt-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='trainingStartDt-"+agentid+"']"), trainingStartDate);
			ErrorMessages.fieldverification("TrainingStartDate");
		}
		
		//Select Training End Date
		if(trainingEndDate.equals(""))
		{
			logger1.info(TrainingEndDateMessage);
		}else{
			clearTextfield(By.xpath("//input[@id='trainingEndDt-"+agentid+"']"));
			Thread.sleep(2000);
			enterText(By.xpath("//input[@id='trainingEndDt-"+agentid+"']"), trainingEndDate);
			ErrorMessages.fieldverification("TrainingEndDate");
		}
		
	}
	

	
	
	
}
