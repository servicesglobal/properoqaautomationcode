package com.org.propero.agencymodule.modification;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.addagent.ExamandLicense;
import com.org.propero.agencymodule.addagent.IntermediaryDetails;
import com.org.propero.agencymodule.addagent.OtherPersonalDetails;
import com.org.propero.agencymodule.addagent.PaymentandFinancial;
import com.org.propero.agencymodule.addagent.PersonalDetails;
import com.org.propero.agencymodule.errormessages.PageNotFoundError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class CopsAgentModification extends BaseClass implements ProjectInterface
{
    static String testResult;
    private static Logger logger1 = LoggerFactory.getLogger(CopsAgentModification.class);
    
	public static void agentModificationCops(int rowNum) throws Exception
	{
		ReadExcel read = new ReadExcel("D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx");
		String excel="D:\\TestData_AgencyModule\\TestData_AgencyModule.xlsx";
		String fis = "AgentModification";
		
		try
		{
		
		//Move to intermediary Details tab	
		clickElement(By.xpath("//a[@name='intermediary']"));
			
		//Enter Data in Modification Remarks
		String modificationRemarks = read.getCellData(fis, "Modification_Remarks", rowNum);
		enterText(By.id(ModificationRemarks_Id), modificationRemarks);
		logger1.info("Entered modification Remark is : "+modificationRemarks);
		
		//Select Modification Reason from UI
		String modificationReason = read.getCellData(fis, "Modification_Reason", rowNum);
		selecttext(By.id(ModificationReason_Id), modificationReason);
		logger1.info("Selected Modification is : "+modificationReason);
		
		
		//Click on Submit Button
		try{
		clickElement(By.xpath(Submit_Button_Xpath));
		}
		catch(Exception e)
		{
			logger1.info("unable to click on Submit Button.");
		}
		
		//Verifying Parent Agent Check
		clickElement(By.xpath(OK_Btn_Xpath));
		
		//Verifying 404 Error Using below method
		PageNotFoundError.pagenotfound();
		
		//Here We are calling IntermediaryDetails Tab for Modification 
		logger1.info("Modification Started.");
		
		//Here we are calling Intermediary Module Tab Method to Fill data
				testResult = IntermediaryDetails.agentsDetails(excel, fis, rowNum);
				
				
				//Here We are calling Personal Tab Method to Fill data
				testResult = PersonalDetails.personalDetailsTab(excel, fis, rowNum);
				
				
				//Here We are Calling Other Details tab method to fill data
				testResult = OtherPersonalDetails.otherpersonaldetailstab(excel, fis, rowNum);
				
				//Here we are calling Payment and Financial Tab method to fill data
				testResult = PaymentandFinancial.paymentAndFinancial(excel, fis, rowNum);
				
				//Here we are Calling Exam and License Tab Method to fill data
				ExamandLicense.licenceDetails(fis,rowNum);
	
		}
		catch(Exception e)
		{
			logger1.info(e.getMessage());
			logger.log(LogStatus.FAIL, e);
		}
		
	}
	
}
