package com.org.propero.agencymodule.basefunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;

public class UploadFile extends BaseClass implements ProjectInterface
{
	public static Logger logger1 = LoggerFactory.getLogger(UploadFile.class);
	
	public static void uploadfile(By by) throws Exception
	{
		// Specify the file location with extension
		 StringSelection sel = new StringSelection("C:\\Users\\ashish.sgh\\Desktop\\HomeTiclet.pdf");
		 
		  // Copy to clipboard
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
		 logger1.info("selection" +sel);
		 
		 clickElement(by);
		 logger1.info("Browse button clicked");
		 
		// Create object of Robot class
		 Robot robot = new Robot();
		 Thread.sleep(1000);
		 
		// Press Enter
		 robot.keyPress(KeyEvent.VK_ENTER);
		 
		// Release Enter
		 robot.keyRelease(KeyEvent.VK_ENTER);
		 
		  // Press CTRL+V
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		// Release CTRL+V
		 robot.keyRelease(KeyEvent.VK_CONTROL);
		 robot.keyRelease(KeyEvent.VK_V);
		 Thread.sleep(1000);
		        
		 //Press Enter 
		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyRelease(KeyEvent.VK_ENTER);
		 
	}
	
}
