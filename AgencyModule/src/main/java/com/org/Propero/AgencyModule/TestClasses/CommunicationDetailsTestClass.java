package com.org.propero.agencymodule.testClasses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.org.propero.agencymodule.communcationDetails.CommuncaionDetails;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.login.LoginSingle;
import com.org.propero.agencymodule.utility.BaseClass;
import com.org.propero.agencymodule.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class CommunicationDetailsTestClass extends BaseClass implements ProjectInterface{

   static String testResult;
   static CommuncaionDetails communcaionDetails;
   private static Logger logger1 = LoggerFactory.getLogger(CommunicationDetailsTestClass.class);
	  
	@Test()
	public static void tc_01_VerifyAgentDetails_Using_communiCationType_Agent() throws Exception{
		logger1.info("*************  Communication Details Test Case Started **************");
		
		communcaionDetails=new CommuncaionDetails();
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		WriteExcel write = new WriteExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "CommuncationDetails";
		int rowCount = read.getRowCount("CommuncationDetails");
		logger1.info("Row Count is : "+rowCount);
		
		/*
		int j=0;
		String ENV = (read.getCellData("Credentials", "Environment", 1));
		if(ENV.equalsIgnoreCase("QC")){
			j=2;
		}
		else if(ENV.equalsIgnoreCase("UAT")){
			j=8;
		}
		*/
		
		for(int n =2; n <=9; n++) {
			
		try{
			String executionstatus=read.getCellData(workbookName, "Execution_Status", n);
			if(executionstatus.equalsIgnoreCase("Execute"))	{
				String testCaseID = (read.getCellData("CommuncationDetails", "TestCase_ID", n));
				String testCaseName = (read.getCellData("CommuncationDetails", "Test_Case", n));
				/*logger = extent.startTest("CommunicationDetails- TC_"+n+" - " +testCaseName);
				logger1.info("CommunicationDetails- TC_"+n+" - " +testCaseName);*/
				
				logger = extent.startTest("CommunicationDetails-"+testCaseID+" "+testCaseName);
				logger1.info("CommunicationDetails-"+testCaseID+" "+testCaseName);
				
				LoginSingle.login();
				
				testResult=communcaionDetails.getDetailsOfAgentOrCustomer(n);
				testResult="Pass";
				Thread.sleep(5000);
				logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "PASS")));
				driver.quit();
			}
			else if(executionstatus.equalsIgnoreCase("Skip"))
			{
				
				logger1.info("Test Case Number : "+n+" is Skip.");
			}
			
		}
		catch(Exception e){
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			driver.quit();
			logger1.info(e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			write.setCellData(workbookName, "Test_Result", n, "Fail");
			
		}
		continue;
	}
		logger1.info("*************  Communication Details Test Case Completed **************");
	}
		
}
