package com.org.propero.agencymodule.modificationFn;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.org.propero.agencymodule.basefunctions.DropDownSelect;
import com.org.propero.agencymodule.basefunctions.ValidExpression;
import com.org.propero.agencymodule.errormessages.ErrorMessages;
import com.org.propero.agencymodule.innterface.MessageInterface;
import com.org.propero.agencymodule.innterface.Variable;
import com.org.propero.agencymodule.utility.BaseClass;


public class OtherPersonalDetailsModification extends BaseClass implements ModificationInterface,MessageInterface
{
	private static Logger logger1 = LoggerFactory.getLogger(OtherPersonalDetailsModification.class);
	
	public void setCustomerType(String newCustomerType) throws Exception {
		Variable.CustomerType = newCustomerType;
	}

	public String getCustomerType() {
		return Variable.CustomerType;
	}

	public void setNomineeTitle(String newNomineeTitle) throws Exception {
		Variable.NomineeTitle = newNomineeTitle;
	}

	public String getNomineeTitle() {
		return Variable.NomineeTitle;
	}
	
	public void setNomineeFirstname(String newNomineeFirstname) throws Exception {
		Variable.NomineeFirstname = newNomineeFirstname;
	}

	public String getNomineeFirstname() {
		return Variable.NomineeFirstname;
	}

	public void setNomineeLastname(String newNomineeLastname) throws Exception {
		Variable.NomineeLastname = newNomineeLastname;
	}

	public String getNomineeLastname() {
		return Variable.NomineeLastname;
	}
	
	public void setNomineeRelation(String newNomineeRelation) throws Exception {
		Variable.NomineeRelation = newNomineeRelation;
	}

	public String getNomineeRelation() {
		return Variable.NomineeRelation;
	}
	
	public void setNomineePan(String newNomineePan) throws Exception {
		Variable.NomineePan = newNomineePan;
	}

	public String getNomineePan() {
		return Variable.NomineePan;
	}
	
	public void setNomineeAge(String newNomineeAge) throws Exception {
		Variable.NomineeAge = newNomineeAge;
	}

	public String getNomineeAge() {
		return Variable.NomineeAge;
	}
	
	public void setPaymentDisbursmentMode(String newPaymentDisbursmentMode) throws Exception {
		Variable.PaymentDisbursmentMode = newPaymentDisbursmentMode;
	}

	public String getPaymentDisbursmentMode() {
		return Variable.PaymentDisbursmentMode;
	}

	public void setTDS(String newTDS) throws Exception {
		Variable.TDS = newTDS;
	}

	public String getTDS() {
		return Variable.TDS;
	}

	public void setIFSCCode(String newIFSCCode) throws Exception {
		Variable.IFSCCode = newIFSCCode;
	}

	public String getIFSCCode() {
		return Variable.IFSCCode;
	}

	public void setBankAccountNumber(String newBankAccountNumber) throws Exception {
		Variable.BankAccountNumber = newBankAccountNumber;
	}

	public String getBankAccountNumber() {
		return Variable.BankAccountNumber;
	}

	public void setMicrNumber(String newMicrNumber) throws Exception {
		Variable.MicrNumber = newMicrNumber;
	}

	public String getMicrNumber() {
		return Variable.MicrNumber;
	}

	public void setBankname(String newBankname) throws Exception {
		Variable.Bankname = newBankname;
	}

	public String getBankname() {
		return Variable.Bankname;
	}

	public void setBranchName(String newBranchName) throws Exception {
		Variable.BranchName = newBranchName;
	}

	public String getBranchName() {
		return Variable.BranchName;
	}

	public void setBranchState(String newBranchState) throws Exception {
		Variable.BranchState = newBranchState;
	}

	public String getBranchState() {
		return Variable.BranchState;
	}

	public void setBranchCity(String newBranchCity) throws Exception {
		Variable.BranchCity = newBranchCity;
	}

	public String getBranchCity() {
		return Variable.BranchCity;
	}

	public void setRcdEditable(String newRcdEditable) throws Exception {
		Variable.RcdEditable = newRcdEditable;
	}

	public String getRcdEditable() {
		return Variable.RcdEditable;
	}

	public void setEmployeeCode(String newEmployeeCode) throws Exception {
		Variable.EmployeeCode = newEmployeeCode;
	}

	public String getEmployeeCode() {
		return Variable.EmployeeCode;
	}

	public void setTotalExperience(String newTotalExperience) throws Exception {
		Variable.TotalExperience = newTotalExperience;
	}

	public String getTotalExperience() {
		return Variable.TotalExperience;
	}

	public void setPassportNumber(String newPassportNumber) throws Exception {
		Variable.PassportNumber = newPassportNumber;
	}

	public String getPassportNumber() {
		return Variable.PassportNumber;
	}

	public void setMarriageDate(String newMarriageDate) throws Exception {
		Variable.MarriageDate = newMarriageDate;
	}

	public String getMarriageDate() {
		return Variable.MarriageDate;
	}

	public void setNumberofChildren(String newNumberofChildren) throws Exception {
		Variable.NumberofChildren = newNumberofChildren;
	}

	public String getNumberofChildren() {
		return Variable.NumberofChildren;
	}

	public void setYearofStay(String newYearofStay) throws Exception {
		Variable.YearofStay = newYearofStay;
	}

	public String getYearofStay() {
		return Variable.YearofStay;
	}

	public void setEducation(String newEducation) throws Exception {
		Variable.Education = newEducation;
	}

	public String getEducation() {
		return Variable.Education;
	}

	public void setOccupation(String newOccupation) throws Exception {
		Variable.Occupation = newOccupation;
	}

	public String getOccupation() {
		return Variable.Occupation;
	}

	public void setWorkLocation(String newWorkLocation) throws Exception {
		Variable.WorkLocation = newWorkLocation;
	}

	public String getWorkLocation() {
		return Variable.WorkLocation;
	}

	public void setLanguageknown(String newLanguageknown) throws Exception {
		Variable.Languageknown = newLanguageknown;
	}

	public String getLanguageknown() {
		return Variable.Languageknown;
	}

	public void setSpouseTitle(String newSpouseTitle) throws Exception {
		Variable.SpouseTitle = newSpouseTitle;
	}

	public String getSpouseTitle() {
		return Variable.SpouseTitle;
	}

	public void setSpouseFname(String newSpouseFname) throws Exception {
		Variable.SpouseFname = newSpouseFname;
	}

	public String getSpouseFname() {
		return Variable.SpouseFname;
	}

	public void setSpouseLName(String newSpouseLName) throws Exception {
		Variable.SpouseLName = newSpouseLName;
	}

	public String getSpouseLName() {
		return Variable.SpouseLName;
	}

	public void setSpouseQualification(String newSpouseQualification) throws Exception {
		Variable.SpouseQualification = newSpouseQualification;
	}

	public String getSpouseQualification() {
		return Variable.SpouseQualification;
	}

	public void setSpouseOccupation(String newSpouseOccupation) throws Exception {
		Variable.SpouseOccupation = newSpouseOccupation;
	}

	public String getSpouseOccupation() {
		return Variable.SpouseOccupation;
	}

	public void setAssociateEntityName(String newAssociateEntityName) throws Exception {
		Variable.AssociateEntityName = newAssociateEntityName;
	}

	public String getAssociateEntityName() {
		return Variable.AssociateEntityName;
	}

	public void setAssociateEntityDetails(String newAssociateEntityDetails) throws Exception {
		Variable.AssociateEntityDetails = newAssociateEntityDetails;
	}

	public String getAssociateEntityDetails() {
		return Variable.AssociateEntityDetails;
	}

	public void setReferenceName(String newReferenceName) throws Exception {
		Variable.ReferenceName = newReferenceName;
	}

	public String getReferenceName() {
		return Variable.ReferenceName;
	}

	public void setReferenceAddress(String newReferenceAddress) throws Exception {
		Variable.ReferenceAddress = newReferenceAddress;
	}

	public String getReferenceAddress() {
		return Variable.ReferenceAddress;
	}

	public void setReferenceContactNumber(String newReferenceContactNumber) throws Exception {
		Variable.ReferenceContactNumber = newReferenceContactNumber;
	}

	public String getReferenceContactNumber() {
		return Variable.ReferenceContactNumber;
	}

	public void setReferenceRelationShip(String newReferenceRelationShip) throws Exception {
		Variable.ReferenceRelationShip = newReferenceRelationShip;
	}

	public String getReferenceRelationShip() {
		return Variable.ReferenceRelationShip;
	}

	public void otherpersonalDetails(String agentid,String newCustomerType, String nomineeTitle, String nomineeFirstname, String nomineeLastname,
			String nomineeRelation, String nomineePan, String nomineeAge,String paymentDisbursmentMode,String tDS,String iFSCCode,String bankAccountNumber,
			String micrNumber, String bankname,String branchName,String branchState, String branchCity) throws Exception 
	{

		verticalscrollup();
		
		//Clicking on Other Personal tab
	   clickElement(By.xpath(ModOtherPersonalDetails_Tab_Xpath));
		
		if(newCustomerType.equalsIgnoreCase("")){
			logger1.info(CUSTOMERTYPEBLANK);
		}
		else if(newCustomerType.equalsIgnoreCase("Individual")){
			//Calling Nominee Details Method to Fill all details in Other Personal Details Tab
			OtherPersonalDetailsModification.nomineeDetails(agentid, nomineeTitle, nomineeFirstname, nomineeLastname, nomineeRelation, nomineePan,nomineeAge);
			
			//Calling Nominee Account Details Method to Fill all details in Other Personal Details Tab
			OtherPersonalDetailsModification.nomineeAccountDetails(agentid, paymentDisbursmentMode, tDS, iFSCCode, bankAccountNumber, micrNumber, bankname, branchName, branchState, branchCity);
		}
		else if(newCustomerType.equalsIgnoreCase("Corporate")){
			logger1.info("No Need to Fill Nominee Details for Corporate Customer Type");
		}
		
	}

	public static void nomineeDetails(String agentid1,String nomineeTitle, String nomineeFirstname, String nomineeLastname,
			String nomineeRelation, String nomineePan, String nomineeAge) throws Exception {
        String agentid=agentid1.trim();
		// Selecting Nominee Title on UI in Other Personal Details Tabb
		if (nomineeTitle.equals("")) {
			logger1.info(NOMINEETITLEMESSAGE);
		} else {
			
			clickElement(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[1]/div/select"));
			fluentwait(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[1]/div/select//option"), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[1]/div/select//option"), nomineeTitle);
			logger1.info("Selected Intermediary Title is : " + nomineeTitle);
		}

		// Entering Nominee First Name on UI in Other Personal Details Tab
		if (nomineeFirstname.equals("")) {
			logger1.info(NOMINEEFIRSTNAMEMESSAGE);
		} else {
			clearTextfield(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[2]/div/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[2]/div/input"), nomineeFirstname);
			if (nomineeFirstname instanceof String && nomineeFirstname.length() <= 60) {
				logger1.info("Nominee First Name is : " + nomineeFirstname);
			} else {
				logger1.info("Nominee First Name is : " + nomineeFirstname);
				ErrorMessages.fieldverification("Nominee First Name");
			}
		}

		// Entering Nominee Last Name on UI in Other Personal Details Tab
		if (nomineeLastname.equals("")) {
			logger1.info(NOMINEELASTNAMEMESSAGE);
		} else {
			clearTextfield(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[3]/div/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[3]/div/input"), nomineeLastname);
			if (nomineeLastname instanceof String && nomineeLastname.length() <= 60) {
				logger1.info("Nominee Last Name is : " + nomineeLastname);
			} else {
				logger1.info("Nominee Last Name is : " + nomineeLastname);
				ErrorMessages.fieldverification("Nominee Last Name");
			}
		}

		// Selecting Nominee Relation on UI in Other Personal Details Tab
		if (nomineeRelation.equals("")) {
			logger1.info(NOMINEERELATIONMESSAGE);
		} else {
			clickElement(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[4]/div/select"));
			fluentwait(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[4]/div/select//option"), 60,
					"Page is Loading Slow so Couldn't find IntermediaryCategory.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[4]/div/select//option"), nomineeRelation);
			logger1.info("Selected Nominee Relation is : " + nomineeRelation);
		}

		// Entering Pan Number on UI in other Personal Details Tab
		if (nomineePan.equals("")) {
			logger1.info(NOMINEEPANMESSAGE);
		} else {
			if (ValidExpression.isValidPanNumber(nomineePan)) 
			{
				clearTextfield(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[5]/div/input"));
				Thread.sleep(2000);
				enterText(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[5]/div/input"), nomineePan);
				logger1.info("Entered Nominee Pan Number is : " + nomineePan);
			} else {
				clearTextfield(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[5]/div/input"));
				Thread.sleep(2000);
				enterText(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[5]/div/input"), nomineePan);
				logger1.info(VALIDPANNUMBER);
				ErrorMessages.fieldverification("Nominee Pan Number");
			}
		}

		// Entering Nominee Age on UI in other Personal Details Tab
		if (nomineeAge.equals("")) {
			logger1.info(NOMINEEAGEMESSAGE);
		} else {
			clearTextfield(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[6]/div/input"));
			Thread.sleep(2000);
			enterText(By.xpath("//*[@id='nominee-details-"+agentid+"']/div/div[6]/div/input"), nomineeAge);
			logger1.info("Entered Nominee Age is : " + nomineeAge);
		}		

	}

	public static void nomineeAccountDetails(String agentid1,String paymentDisbursmentMode,String tDS,String iFSCCode,String bankAccountNumber,
			String micrNumber, String bankname,String branchName,String branchState, String branchCity) throws Exception
	{
		String agentid=agentid1.trim();
		//Selecting Payment Disbursement Mode
		if(paymentDisbursmentMode.equals(""))
		{
			logger1.info(PAYMENTDISBURSMENTMODEMESSAGE);
		}else if(paymentDisbursmentMode.equalsIgnoreCase("Cheque")){
		
			clickElement(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[1]/div/select"));
			fluentwait(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[1]/div/select//option"), 60,
					"Payment Disbursement Mode Option is Loading Slow so Couldn't find Payment Disbursment Mode.");
			DropDownSelect.selectValuesfromDropdown(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[1]/div/select//option"), paymentDisbursmentMode);
			logger1.info("Selected Payment Disbursment Mode is : " + paymentDisbursmentMode);	
			
			//Selecting TDS Details
			if(tDS.equals("")){
				logger1.info(TDSMESSAGE);
			}else{
				clickElement(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[2]/div/select"));
				fluentwait(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[1]/div/select//option"), 60,
						"Tds Option is Loading Slow so Couldn't find Tds Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[1]/div/select//option"), tDS);
				logger1.info("Selected Tds Details is : " + tDS);	
			}

				
		}else if(paymentDisbursmentMode.equalsIgnoreCase("Direct Credit"))
		{
			//Selecting TDS Details
			if(tDS.equals("")){
				logger1.info(TDSMESSAGE);
			}else{
				clickElement(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[2]/div/select"));
				fluentwait(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[1]/div/select//option"), 60,
						"Tds Option is Loading Slow so Couldn't find Tds Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[1]/div/select//option"), tDS);
				logger1.info("Selected Tds Details is : " + tDS);	
			}
			
			//Entering IFSC Code on UI in Nominee Account Details Section in Other Personal Details Tab
			if(iFSCCode.equals(""))
			{
				logger1.info(IFSCCODEMESSAGE);
			}
			else{
				
				if(ValidExpression.isValidIfscCode(iFSCCode))
				{
				clearTextfield(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[3]/div/input"));
				Thread.sleep(2000);
				enterText(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[3]/div/input"), iFSCCode);
				logger1.info("Entered IFSC Code is : "+iFSCCode);
				}else{
					clearTextfield(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[3]/div/input"));
					Thread.sleep(2000);
					enterText(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[3]/div/input"), iFSCCode);
					logger1.info("Entered IFSC Code is : "+iFSCCode);
					ErrorMessages.fieldverification("IFSC Code");
				}
				
		}
			
			//Entering Bank Account Number on UI in Nominee Account Details Section in Other Personal Details Tab
			if(bankAccountNumber.equals(""))
			{
				logger1.info(BANKACCOUNTNUMBERMESSAGE);
			}else{
				clearTextfield(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[4]/div/input"));
				Thread.sleep(2000);
				enterText(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[4]/div/input"), bankAccountNumber);
				logger1.info("Entered Bank Account Number is : "+bankAccountNumber);
				ErrorMessages.fieldverification("Bank Account Number");
			}
			
			//Entering MICR Number on UI in Nominee Account Details Section in Other Personal Details Tab
			if(micrNumber.equals(""))
			{
				logger1.info(MICRNUMBERMESSAGE);
			}else{
				if(ValidExpression.isvalidMicrNumber(micrNumber))
				{
					clearTextfield(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[5]/div/input"));
					Thread.sleep(2000);
					enterText(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[5]/div/input"), micrNumber);
					logger1.info("Entered MICR Number is : "+micrNumber);
				}else{
					clearTextfield(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[5]/div/input"));
					Thread.sleep(2000);
					enterText(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[5]/div/input"), micrNumber);
					logger1.info("Entered MICR Number is : "+micrNumber);
					ErrorMessages.fieldverification("Micr Number");
				}
			}
			
			//Entering Bank Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(bankname.equals(""))
			{
				logger1.info(BANKNAMEMESSAGE);
			}else{
				clearTextfield(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[6]/div/input"));
				Thread.sleep(2000);
				enterText(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[6]/div/input"), bankname);
				logger1.info("Entered Bank Name is : "+bankname);
			}
			
			//Entering Branch Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchName.equals(""))
			{
				logger1.info(BRANCHNAMEMESSAGE);
			}else{
				clearTextfield(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[7]/div/input"));
				Thread.sleep(2000);
				enterText(By.xpath("//*[@id='nomineeaccount-details-"+agentid+"']/div/div[7]/div/input"), branchName);
				logger1.info("Entered Branch Name is : "+branchName);
			}
			
			
			
			
			//Selecting State Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchState.equals(""))
			{
				logger1.info(BRANCHSTATEMESSAGE);
			}else{
				clickElement(By.xpath(ModState_Xpath));
				fluentwait(By.xpath(ModStateOption_Xpath), 60,
						"State Option is Loading Slow so Couldn't find State Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(ModStateOption_Xpath), branchState);
				logger1.info("Selected State is : " + branchState);	
			}
			
			//Selecting City Name on UI in Nominee Account Details Section in Other Personal Details Tab
			if(branchCity.equals(""))
			{
				logger1.info(BRANCHCITYMESSAGE);
			}else{
				clickElement(By.xpath(ModCity_Xpath));
				fluentwait(By.xpath(ModCityOption_Xpath), 60,
						"City Option is Loading Slow so Couldn't find City Details.");
				DropDownSelect.selectValuesfromDropdown(By.xpath(ModCityOption_Xpath), branchCity);
				logger1.info("Selected City is : " + branchCity);	
			}
			
		}	
		
	}
	
	
}
