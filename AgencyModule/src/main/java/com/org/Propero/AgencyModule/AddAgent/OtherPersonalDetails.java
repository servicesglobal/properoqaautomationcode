package com.org.propero.agencymodule.addagent;

import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.functions.OtherPersonalDetailsTab;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class OtherPersonalDetails extends BaseClass implements ProjectInterface
{

	private static String testResult;
	
	public static String otherpersonaldetailstab(String sheetName,String workbookname,int rowNum) throws Exception
	{
		
		OtherPersonalDetailsTab obj = new OtherPersonalDetailsTab();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
			
		String newNomineeTitle = read.getCellData(workbookname,"Nominee_Title",rowNum);
		String newNomineeFirstname = read.getCellData(workbookname,"Nominee_Name",rowNum);
		String newNomineeLastname = read.getCellData(workbookname,"Nominee_Last_Name",rowNum);
		String newNomineeRelation = read.getCellData(workbookname,"Relation",rowNum);
		String newNomineePan = read.getCellData(workbookname,"Pan_Number",rowNum);
		String newNomineeAge = read.getCellData(workbookname,"Nominee_Age",rowNum);
		String newPaymentDisbursmentMode = read.getCellData(workbookname,"Payment_Disbursement_Mode",rowNum);
		String newTDS = read.getCellData(workbookname,"TDS_Certificate",rowNum);
		String newIFSCCode = read.getCellData(workbookname,"IFSC_Code",rowNum);
		String newBankAccountNumber = read.getCellData(workbookname,"Bank_Account_Number",rowNum);
		String newMicrNumber = read.getCellData(workbookname,"MICR_Number",rowNum);
		String newBankname = read.getCellData(workbookname,"Bank",rowNum);
		String newBranchName = read.getCellData(workbookname,"Branch_Name",rowNum);
		String newBranchState = read.getCellData(workbookname,"State_Name",rowNum);
		String newBranchCity = read.getCellData(workbookname,"City_Name",rowNum);
		
		
		obj.setNomineeTitle(newNomineeTitle);
		obj.setNomineeFirstname(newNomineeFirstname);
		obj.setNomineeLastname(newNomineeLastname);
		obj.setNomineeRelation(newNomineeRelation);
		obj.setNomineePan(newNomineePan);
		obj.setNomineeAge(newNomineeAge);
		obj.setPaymentDisbursmentMode(newPaymentDisbursmentMode);
		obj.setTDS(newTDS);
		obj.setIFSCCode(newIFSCCode);
		obj.setBankAccountNumber(newBankAccountNumber);
		obj.setMicrNumber(newMicrNumber);
		obj.setBankname(newBankname);
		obj.setBranchName(newBranchName);
		obj.setBranchState(newBranchState);
		obj.setBranchCity(newBranchCity);
		
		
		obj.otherpersonalDetails(obj.getNomineeTitle(), obj.getNomineeFirstname(), obj.getNomineeLastname(), 
				obj.getNomineeRelation(), obj.getNomineePan(), obj.getNomineeAge(), obj.getPaymentDisbursmentMode(), 
				obj.getTDS(), obj.getIFSCCode(), obj.getBankAccountNumber(), obj.getMicrNumber(), obj.getBankname(), 
				obj.getBranchName(), obj.getBranchState(), obj.getBranchCity(), rowNum);
		
		testResult="Pass";
		
		obj=null;
		
		}catch(Exception e)

		{
			testResult="Fail";
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Fail")));
			throw e;
			//Assert.fail("Error in Other Personal Details Page");
		}
		
		return testResult;
	}
		
	//*********** Function for Negative Test cases *********************************
	public static String otherpersonaldetailstabNegative(String sheetName,String workbookname,int rowNum) throws Exception
	{
		
		OtherPersonalDetailsTab obj = new OtherPersonalDetailsTab();
		ReadExcel read = new ReadExcel(sheetName);
		
		try{
			
		String newNomineeTitle = read.getCellData(workbookname,"Nominee_Title",rowNum);
		String newNomineeFirstname = read.getCellData(workbookname,"Nominee_Name",rowNum);
		String newNomineeLastname = read.getCellData(workbookname,"Nominee_Last_Name",rowNum);
		String newNomineeRelation = read.getCellData(workbookname,"Relation",rowNum);
		String newNomineePan = read.getCellData(workbookname,"Pan_Number",rowNum);
		String newNomineeAge = read.getCellData(workbookname,"Nominee_Age",rowNum);
		String newPaymentDisbursmentMode = read.getCellData(workbookname,"Payment_Disbursement_Mode",rowNum);
		String newTDS = read.getCellData(workbookname,"TDS_Certificate",rowNum);
		String newIFSCCode = read.getCellData(workbookname,"IFSC_Code",rowNum);
		String newBankAccountNumber = read.getCellData(workbookname,"Bank_Account_Number",rowNum);
		String newMicrNumber = read.getCellData(workbookname,"MICR_Number",rowNum);
		String newBankname = read.getCellData(workbookname,"Bank",rowNum);
		String newBranchName = read.getCellData(workbookname,"Branch_Name",rowNum);
		String newBranchState = read.getCellData(workbookname,"State_Name",rowNum);
		String newBranchCity = read.getCellData(workbookname,"City_Name",rowNum);
		
		
		obj.setNomineeTitle(newNomineeTitle);
		obj.setNomineeFirstname(newNomineeFirstname);
		obj.setNomineeLastname(newNomineeLastname);
		obj.setNomineeRelation(newNomineeRelation);
		obj.setNomineePan(newNomineePan);
		obj.setNomineeAge(newNomineeAge);
		obj.setPaymentDisbursmentMode(newPaymentDisbursmentMode);
		obj.setTDS(newTDS);
		obj.setIFSCCode(newIFSCCode);
		obj.setBankAccountNumber(newBankAccountNumber);
		obj.setMicrNumber(newMicrNumber);
		obj.setBankname(newBankname);
		obj.setBranchName(newBranchName);
		obj.setBranchState(newBranchState);
		obj.setBranchCity(newBranchCity);
		
		
		obj.otherpersonalDetailsNegative(obj.getNomineeTitle(), obj.getNomineeFirstname(), obj.getNomineeLastname(), 
				obj.getNomineeRelation(), obj.getNomineePan(), obj.getNomineeAge(), obj.getPaymentDisbursmentMode(), 
				obj.getTDS(), obj.getIFSCCode(), obj.getBankAccountNumber(), obj.getMicrNumber(), obj.getBankname(), 
				obj.getBranchName(), obj.getBranchState(), obj.getBranchCity());
		
		testResult="Pass";
		
		obj=null;
		
		}catch(Exception e)

		{
			testResult="Fail";
			throw e;
		}
		
		return testResult;
	}
		
		
}
