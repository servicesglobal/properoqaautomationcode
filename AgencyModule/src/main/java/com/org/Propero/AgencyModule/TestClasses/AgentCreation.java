package com.org.propero.agencymodule.testClasses;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.org.propero.agencymodule.addagent.AgentType;
import com.org.propero.agencymodule.addagent.ExistingAgents;
import com.org.propero.agencymodule.addagent.FreshAgent;
import com.org.propero.agencymodule.assertion.FooterError;
import com.org.propero.agencymodule.errormessages.PageNotFoundError;
import com.org.propero.agencymodule.excelreadandwrite.ReadExcel;
import com.org.propero.agencymodule.innterface.ProjectInterface;
import com.org.propero.agencymodule.login.LoginSingle;
import com.org.propero.agencymodule.utility.BaseClass;
import com.org.propero.agencymodule.utility.WriteExcel;
import com.relevantcodes.extentreports.LogStatus;

public class AgentCreation extends BaseClass implements ProjectInterface
{
	private static String testResult;
	private static String intermediaryCode;
	private static Logger logger1 = LoggerFactory.getLogger(AgentCreation.class);
	
	@Test(priority=1, enabled=true)
	public static void agentCreate() throws Throwable
	{
		logger1.info("*************  Agent Creation Test Case Started **************");
	
		//Reading Excel Sheet Data For Number of Test Cases
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		WriteExcel write = new WriteExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		int rowCount = read.getRowCount("Existing_Agents");
		logger1.info("Row Count is : "+rowCount);
		
		//Applying For loop it will execute till number of row exist in Excel for mentioned Workbook
		
		int firstRow=1;
		int lastRow=44;
		
		for (int n=firstRow; n <=lastRow; n++) 
		{
		try
		{
		
		//Reading Execution Status from Excel sheet
		String executionstatus=read.getCellData(workbookName, "Execution_Status", n);
			
		if(executionstatus.equalsIgnoreCase("Execute"))	
		{	
		//Reading Test Case Name from Excel
		String testCaseName = (read.getCellData("Existing_Agents", "Test_Case", n));
		logger = extent.startTest("Agent Creation - TC_"+n+" - " +testCaseName);
		logger1.info("Agent Creation - TC_"+n+" - " +testCaseName);
		
		//Here we are calling Login Method to Login in the Propero Application
		LoginSingle.login();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		waitForElementsToBeClickable(By.xpath("//i[@title='FullScreen']"));
		driver.findElement(By.xpath("//i[@title='FullScreen']")).click();
		Thread.sleep(2000);
		
   		//We are Selecting Agent Type by using below method(Test Result is uses to capture return result)
		AgentType.agentTypeCreation("Existing_Agents", "Agent Type", n);
		
		String AgentType = read.getCellData(workbookName,"Agent Type",n);
		
		if(AgentType.equalsIgnoreCase("Exising Agent"))
		{
		ExistingAgents.fillIntermediary(n);
		
		Thread.sleep(5000);
		//Here we are calling Write Excel Method to write data in Excel
		intermediaryCode = driver.findElement(By.xpath(IntermediaryCode_Xpath)).getText(); 
		System.out.println("New Agent ID is: "+ intermediaryCode);
		write.setCellData(workbookName, "Test_Result", n, "Pass");  
		write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
		logger.log(LogStatus.PASS, "New Agent ID is: "+ intermediaryCode);
		
		}
		else if (AgentType.equalsIgnoreCase("Fresh Agent"))
		{
			
		FreshAgent.freshAgents(n);
		
		Thread.sleep(5000);
		//Here we are calling Write Excel Method to write data in Excel
		intermediaryCode = driver.findElement(By.xpath(IntermediaryCode_Xpath)).getText(); 
		System.out.println("New Agent ID is: "+ intermediaryCode);
		write.setCellData(workbookName, "Test_Result", n, "Pass");  
		write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
		logger.log(LogStatus.PASS, "New Agent ID is: "+ intermediaryCode);
		
		}   
	
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		driver.quit();
       
		//LogoutFn.LogoutfromApp();
		
		}
		else{
		logger1.info("No Need to Execute for Test case Number: " + n);
		write.setCellData(workbookName, "Test_Result", n, "Skip");
		}
		
		}
		
		catch(AssertionError e){
			FooterError.footererror();
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			write.setCellData(workbookName, "Test_Result", n, "Fail");
			write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
			driver.quit();
		}
		catch(Exception e){
			FooterError.footererror();
			PageNotFoundError.pagenotfound();
			logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
			write.setCellData(workbookName, "Test_Result", n, "Fail");
			write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
			logger1.info(e.getMessage());
			logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
			driver.quit();
				
		}
		//driver.quit();
		continue; 
		}
		logger1.info("*************  Agent Creation Test Case Completed **************");
	}
	
	//*****************************************************************************8
	
	@Test(priority=2, enabled=true)
	public static void negativeTestCase1() throws Exception
	{
		logger1.info("*************  Agent Creation Test Case Started **************");
	
		//Reading Excel Sheet Data For Number of Test Cases
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		WriteExcel write = new WriteExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		int rowCount = read.getRowCount("Existing_Agents");
		logger1.info("Row Count is : "+rowCount);
		
		int firstRowNum=46;
		int lastRowNum=55;
		
		//Applying For loop it will execute till number of row exist in Excel for mentioned Workbook
		for (int n = firstRowNum; n <=lastRowNum; n++) 
		{
		try
		{
		
			//Reading Execution Status from Excel sheet
		String executionstatus=read.getCellData(workbookName, "Execution_Status", n);
			
		if(executionstatus.equalsIgnoreCase("Execute"))	
		{	
		//Reading Test Case Name from Excel
		String testCaseNumber = (read.getCellData("Existing_Agents", "TestCase_ID", n));
		String testCaseName = (read.getCellData("Existing_Agents", "Test_Case", n));
		
		logger = extent.startTest("Agent Creation - "+testCaseNumber+" - " +testCaseName);
		logger1.info("Agent Creation - "+testCaseNumber+" - " +testCaseName);
		
		//Here we are calling Login Method to Login in the Propero Application
		LoginSingle.login();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		waitForElementsToBeClickable(By.xpath("//i[@title='FullScreen']"));
		driver.findElement(By.xpath("//i[@title='FullScreen']")).click();
		Thread.sleep(2000);
		//We are Selecting Agent Type by using below method(Test Result is uses to capture return result)
		AgentType.agentTypeCreation("Existing_Agents", "Agent Type", n);
		
		String agentType = read.getCellData(workbookName,"Agent Type",n);
		
		if(agentType.equalsIgnoreCase("Exising Agent"))
		{
		    ExistingAgents.fillIntermediaryForNegativeTesting(n);
		}
		else if (agentType.equalsIgnoreCase("Fresh Agent"))
		{
			
			FreshAgent.freshAgentsNegativeTestCase(n);
		}
		
		write.setCellData(workbookName, "Test_Result", n, "Pass");  
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
		
        driver.quit();
		
		}
		else{
			logger1.info("No Need to Execute for Test case Number: " + n);
			write.setCellData(workbookName, "Test_Result", n, "Skip");
			}
			
			}
			
			catch(AssertionError e){
				FooterError.footererror();
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				write.setCellData(workbookName, "Test_Result", n, "Fail");
				write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
			}
			catch(Exception e){
				FooterError.footererror();
				PageNotFoundError.pagenotfound();
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				write.setCellData(workbookName, "Test_Result", n, "Fail");
				write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
				logger1.info(e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				// driver.quit();
					
			}
			driver.quit();
			continue; 
			}
			logger1.info("*************  Agent Creation Test Case Completed **************");
	}
	
	@Test(priority=3, enabled=true)
	public static void negativeTestCase2() throws Exception
	{
		logger1.info("*************  Agent Creation Test Case Started **************");
	
		//Reading Excel Sheet Data For Number of Test Cases
		ReadExcel read = new ReadExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		WriteExcel write = new WriteExcel(".\\TestData\\TestData_AgencyModule.xlsx");
		String workbookName = "Existing_Agents";
		int rowCount = read.getRowCount("Existing_Agents");
		logger1.info("Row Count is : "+rowCount);
		
		int firstRowNum=57;
		int lastRowNum=61;
		
		//Applying For loop it will execute till number of row exist in Excel for mentioned Workbook
		for (int n = firstRowNum; n <=lastRowNum; n++) 
		{
		try
		{
		//Reading Execution Status from Excel sheet
		String executionstatus=read.getCellData(workbookName, "Execution_Status", n);
			
		if(executionstatus.equalsIgnoreCase("Execute"))	
		{	
		//Reading Test Case Name from Excel
		String testCaseNumber = (read.getCellData("Existing_Agents", "TestCase_ID", n));
		String testCaseName = (read.getCellData("Existing_Agents", "Test_Case", n));
		
		logger = extent.startTest("Agent Creation - "+testCaseNumber+" - " +testCaseName);
		logger1.info("Agent Creation - "+testCaseNumber+" - " +testCaseName);
		
		//Here we are calling Login Method to Login in the Propero Application
		LoginSingle.login();
		
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		waitForElementsToBeClickable(By.xpath("//i[@title='FullScreen']"));
		driver.findElement(By.xpath("//i[@title='FullScreen']")).click();
		Thread.sleep(2000);
		
		//We are Selecting Agent Type by using below method(Test Result is uses to capture return result)
	    AgentType.agentTypeCreation("Existing_Agents", "Agent Type", n);
		
		String agentType = read.getCellData(workbookName,"Agent Type",n);
		
		if(agentType.equalsIgnoreCase("Exising Agent"))
		{
			ExistingAgents.fillIntermediaryForNegativeTesting2(n);
		}
		else if (agentType.equalsIgnoreCase("Fresh Agent"))
		{
			
			FreshAgent.freshAgentsNegativeTestCase2(n);
		}
		
	   write.setCellData(workbookName, "Test_Result", n, "Pass"); 
		
		logger.log(LogStatus.PASS, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Pass")));
		logger.log(LogStatus.PASS, "Test Case Passed");
       
		//LogoutFn.LogoutfromApp();
       driver.quit();    
		
		}
		else{
			logger1.info("No Need to Execute for Test case Number: " + n);
			write.setCellData(workbookName, "Test_Result", n, "Skip");
			}
			
			}
			
			catch(AssertionError e){
				FooterError.footererror();
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				write.setCellData(workbookName, "Test_Result", n, "Fail");
				write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
			}
			catch(Exception e){
				FooterError.footererror();
				PageNotFoundError.pagenotfound();
				logger.log(LogStatus.FAIL, logger.addScreenCapture(BaseClass.getScreenhot(driver, "Failure")));
				write.setCellData(workbookName, "Test_Result", n, "Fail");
				write.setCellData(workbookName, "Agent_Id", n, intermediaryCode);
				logger1.info(e.getMessage());
				logger.log(LogStatus.FAIL, "Test Case is Failed beacuse getting :" + e.getMessage());
				// driver.quit();
					
			}
			driver.quit();
			continue; 
			}
			logger1.info("*************  Agent Creation Test Case Completed **************");
	}
	
}
